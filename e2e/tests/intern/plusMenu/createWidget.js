import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    createAndSetupWidget
} from './../../../functions/intern/plusMenu';

describe('when viewing the dashboard', () => {
    beforeAll(() => {
        openPageAs('dashboardempty');
    });

    describe('and creating a "my open cases" widget', () => {
        beforeAll(() => {
            createAndSetupWidget('Zoekopdracht', 'Mijn openstaande zaken');
        });

        it('the dashboard should contain a functional "my open cases"', () => {
            expect($('[data-name="mine"] [href="/intern/zaak/47"]').isPresent()).toBe(true);
        });

        afterAll(() => {
            $('.widget-header-remove-button').click();
        });
    });

    describe('and creating a favorite casetype widget', () => {
        beforeAll(() => {
            createAndSetupWidget('Favoriete zaaktypen');
        });

        it('the dashboard should contain a functional favorite casetype widget', () => {
            expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');
        });

        afterAll(() => {
            $('.widget-header-remove-button').click();
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
