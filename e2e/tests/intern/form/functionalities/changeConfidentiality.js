import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    changeConfidentiality
} from './../../../../functions/common/form';
import {
    getSummaryValue
} from './../../../../functions/intern/caseView/caseMenu';

const confidentialityLevels = ['Openbaar', 'Intern', 'Vertrouwelijk'];

confidentialityLevels.forEach(confidentialityLevel => {
    describe(`when starting a registration form and setting confidentiality to ${confidentialityLevel}`, () => {
        beforeAll(() => {
            const data = {
                casetype: 'Wijzig vertrouwelijkheid bij registratie aan',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            openPageAs();
            startForm(data);
            goNext();
            changeConfidentiality(confidentialityLevel);
            goNext();
        });

        it(`the confidentiality should have been set to ${confidentialityLevel}`, () => {
            expect(getSummaryValue('Vertrouwelijkheid')).toEqual(confidentialityLevel);
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
