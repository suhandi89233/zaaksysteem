import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    goBack
} from './../../../../functions/common/form';
import {
    getValue
} from './../../../../functions/common/input/caseAttribute';

const choice1 = $('[data-name="toon_verberg_1_keuze"]');
const choice2 = $('[data-name="toon_verberg_2_keuze"]');
const text3 = $('[data-name="toon_verberg_3_tekstveld"]');
const choice4 = $('[data-name="toon_verberg_4_keuze"]');
const choice5 = $('[data-name="toon_verberg_5_keuze"]');
const text6 = $('[data-name="toon_verberg_6_tekstveld"]');
const choiceB1 = $('[data-name="toon_verberg_blok_1_keuze"]');
const choiceB2 = $('[data-name="toon_verberg_blok_2_keuze"]');
const block1 = $('[data-name="text_block_1812"]');
const choiceG1 = $('[data-name="toon_verberg_groep_1_keuze"]');
const choiceG2 = $('[data-name="toon_verberg_groep_2_keuze"]');
const textG3 = $('[data-name="toon_verberg_groep_3_tekstveld"]');
const group3 = $('[data-name="groep-3"]');
const group6 = $('[data-name="groep-6"]');
const systemattribute = $('[data-name="toon_verberg_systeemkenmerk');
const choiceG4 = $('[data-name="toon_verberg_groep_4_keuze"]');

describe('when opening a registration form with the show and hide testscenarios', () => {

    beforeAll(() => {

        openPageAs();

        const data = {
            casetype: 'Toon en verberg',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

    });

    describe('and hiding the later shown attribute 3', () => {
    
        beforeAll(() => {

            text3.$('input').sendKeys('Test');
            choice2.$('[value="Ja"]').click();
            choice1.$('[value="Nee"]').click();
    
        });
    
        it('attribute 3 should not be hidden', () => {
    
            expect(text3.isPresent()).toBe(true);
    
        });

        it('attribute 3 should still have its value', () => {
    
            expect(getValue(text3)).toEqual('Test');
    
        });

        describe('and stop showing attribute 3', () => {
        
            beforeAll(() => {
            
                choice2.$('[value="Nee"]').click();
            
            });
        
            it('attribute 3 should be hidden', () => {
    
                expect(text3.isPresent()).toBe(false);
        
            });
        
            describe('and when showing attribute 3 again', () => {
            
                beforeAll(() => {
                
                    choice2.$('[value="Ja"]').click();
                
                });
            
                it('attribute 3 should not be hidden', () => {
    
                    expect(text3.isPresent()).toBe(true);
            
                });

                it('attribute 3 should not have lost its value', () => {
            
                    expect(getValue(text3)).toEqual('Test');
            
                });
            
            });
        
        });
    
    });

    describe('and showing systemattribute 5, filling it with yes, triggering the showing of attribute 6', () => {
    
        beforeAll(() => {
        
            choice4.$('[value="Ja"]').click();
        
        });
    
        it('attribute 5 should be hidden', () => {
    
            expect(choice5.isPresent()).toBe(false);
    
        });

        it('attribute 6 should not be hidden', () => {
    
            expect(text6.isPresent()).toBe(true);
    
        });

        describe('and when hiding systemattribute 5', () => {
        
            beforeAll(() => {
            
                choice4.$('[value="Nee"]').click();
            
            });
        
            it('attribute 6 should be hidden', () => {
    
                expect(text6.isPresent()).toBe(false);
        
            });
        
        });
    
    });

    describe('and when hiding text block 1', () => {
    
        beforeAll(() => {

            goNext();
        
            choiceB1.$('[value="Nee"]').click();
        
        });
    
        it('text block 1 should be hidden', () => {
    
            expect(block1.isPresent()).toBe(false);
    
        });
    
        describe('and when showing text block 1', () => {
        
            beforeAll(() => {
            
                choiceB2.$('[value="Ja"]').click();

                browser.sleep(1000);
            
            });
        
            it('text block 1 should be shown', () => {
    
                expect(block1.isPresent()).toBe(true);
        
            });
        
        });
    
    });

    describe('and hiding the later shown group 3', () => {
    
        beforeAll(() => {

            goNext(2);

            textG3.$('input').sendKeys('Test');

            goBack();

            choiceG2.$('[value="Ja"]').click();
            choiceG1.$('[value="Nee"]').click();
    
        });
    
        it('group 3 should not be hidden', () => {
    
            expect(group3.isPresent()).toBe(true);
    
        });

        it('attribute 3 should still have its value', () => {

            goNext();
    
            expect(getValue(textG3)).toEqual('Test');

            goBack();
    
        });

        describe('and stop showing group 3', () => {
        
            beforeAll(() => {
            
                choiceG2.$('[value="Nee"]').click();
            
            });
        
            it('group 3 should be hidden', () => {
    
                expect(group3.isPresent()).toBe(false);
        
            });
        
            describe('and when showing group 3 again', () => {
            
                beforeAll(() => {
                
                    choiceG2.$('[value="Ja"]').click();
                
                });
            
                it('attribute 3 should not be hidden', () => {
    
                    expect(group3.isPresent()).toBe(true);
            
                });

                it('attribute 3 should not have lost its value', () => {

                    goNext();
            
                    expect(getValue(textG3)).toEqual('Test');

                    goBack();
            
                });
            
            });
        
        });
    
    });

    describe('and showing group 5, filling its choice attribute with yes, triggering the showing of group 5 and 6', () => {
    
        beforeAll(() => {
        
            choiceG4.$('[value="Ja"]').click();
        
        });
    
        it('systemattribute 5 should still be hidden', () => {

            goNext(2);
    
            expect(systemattribute.isPresent()).toBe(false);

            goBack(2);
    
        });

        it('group 6 should not be hidden', () => {
    
            expect(group6.isPresent()).toBe(true);
    
        });

        describe('and when hiding group 5', () => {
        
            beforeAll(() => {
            
                choiceG4.$('[value="Nee"]').click();
            
            });
        
            it('group 6 should be hidden', () => {
    
                expect(group6.isPresent()).toBe(false);
        
            });
        
        });
    
    });

    afterAll(() => {

        openPage();

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
