import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';

const restrictedPage = $('.no-access');

describe('when logging in as behandelaar', () => {
    beforeAll(() => {
        openPageAs('behandelaar');
    });

    describe('and opening the catalog', () => {
        beforeAll(() => {
            browser.get('/beheer/bibliotheek');
        });

        it('should show the no access page', () => {
            expect(restrictedPage.isPresent()).toBe(true);
        });
    });

    describe('and opening a case', () => {
        beforeAll(() => {
            openPage(24);
        });

        it('should not have the rule mode button present', () => {
            expect($('[data-name="debug_rules"]').isPresent()).toBe(false);
        });
    });
});

describe('when logging in as zaaktypebeheerder', () => {
    beforeAll(() => {
        openPageAs('zaaktypebeheerder');
    });

    describe('and opening the catalog', () => {
        beforeAll(() => {
            browser.get('/beheer/bibliotheek');
        });

        it('should show the no access page', () => {
            expect(restrictedPage.isPresent()).toBe(false);
        });
    });

    describe('and opening a case', () => {
        beforeAll(() => {
            openPage(24);
        });

        it('should have the rule mode button present', () => {
            expect($('[data-name="debug_rules"]').isPresent()).toBe(true);
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
