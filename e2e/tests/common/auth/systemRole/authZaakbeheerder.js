import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    skipOption
} from './../../../../functions/common/form';

describe('when logging in as behandelaar and open a registration form', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Basic casetype',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs('behandelaar');
        startForm(data);
    });

    it('should not have the skip required option present', () => {
        expect(skipOption.isPresent()).toBe(false);
    });
});

describe('when logging in as zaakbeheerder and open a registration form', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Basic casetype',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs('zaakbeheerder');
        startForm(data);
    });

    it('should have the skip required option present', () => {
        expect(skipOption.isPresent()).toBe(true);
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
