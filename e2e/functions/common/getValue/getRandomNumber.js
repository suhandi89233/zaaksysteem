import getRandomNumber from 'lodash/random';

export default numberLength =>
	Math.floor( getRandomNumber(0.1, 1) * Math.pow(10, numberLength) );

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
