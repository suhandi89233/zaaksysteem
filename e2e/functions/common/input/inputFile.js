export default ( attribute, file ) =>
    attribute
    	.$('.file-upload-zone button')
    	.getAttribute('name')
    	.then(name => {
	        const path = require('path');
	        const inputFile = file ? `./../../../utilities/documents/${file}` : './../../../utilities/documents/text.txt';
	        const absolutePath = path.resolve(__dirname, inputFile);

	        $(`input[name="${name}"]`).sendKeys(absolutePath);
    	});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
