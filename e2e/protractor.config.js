require('babel-core/register')({
    presets: ['es2015'],
    plugins: ['add-module-exports']
});

// add a reporter for machine readable output (xml) and a live reporter for the terminal
const reporters = require('jasmine-reporters');
const xmlReporter = new reporters.JUnitXmlReporter({
    savePath: './output/',
    consolidateAll: true
});
const terminalReporter = require('./custom_reporter.js');

const { npm_config_remote, npm_config_domain, npm_config_sessions, npm_config_browser, npm_config_set, npm_config_folder, npm_config_file } = process.env;

// set baseUrl
const domain = npm_config_domain || 'testbase-instance.dev.zaaksysteem.nl';
const baseUrl = `https://${domain}`;

// set maximum number of sessions to run simultaniously
const maxSessions = npm_config_sessions || 1;

// set browsers to test with
const allBrowsers = ['chrome', 'firefox', 'internet explorer', 'MicrosoftEdge'];
let browsers = [];

if ( npm_config_browser === 'all' ) {
    browsers = allBrowsers;
} else if ( npm_config_browser ) {
    browsers = [npm_config_browser];
} else {
    browsers = ['chrome'];
}

// predefined sets of tests
const sets = {
    apps: ['meeting', 'mor'],
    attributes: ['attributesPhaseView', 'attributesForm'],
    rules: ['rulesPhaseView', 'rulesForm']
};
const paths = {
    all: 'tests/**/*.js',
        common: 'tests/common/**/*.js',
        meeting: 'tests/meeting/**/*.js',
        mor: 'tests/mor/**/*.js',
        intern: 'tests/intern/**/*.js',
            caseview: 'tests/intern/caseView/**/*.js',
                documentTab: 'tests/intern/caseView/documentTab/**/*.js',
                phaseView: 'tests/intern/caseView/phaseView/**/*.js',
                    attributesPhaseView: 'tests/intern/caseView/phaseView/attributes/**/*.js',
                    checklist: 'tests/intern/caseView/phaseView/checklist/**/*.js',
                    phaseActions: 'tests/intern/caseView/phaseView/phaseActions/**/*.js',
                    rulesPhaseView: 'tests/intern/caseView/phaseView/rules/**/*.js',
                    registrationRestricted: 'tests/intern/caseView/phaseView/registrationRestricted.js',
                summary: 'tests/intern/caseView/summary/**/*.js',
                caseActions: 'tests/intern/caseView/caseActions.js',
                objectMutations: 'tests/intern/caseView/objectMutations.js',

            catalogus: 'tests/intern/catalogus/**/*.js',
            dashboard: 'tests/intern/dashboard/**/*.js',
            documentintake: 'tests/intern/documentintake/**/*.js',
            form: 'tests/intern/form/**/*.js',
                addRelation: 'tests/intern/form/functionalities/addRelation.js',
                assign: 'tests/intern/form/functionalities/assign.js',
                assignSettings: 'tests/intern/form/functionalities/assignSettings.js',
                changeConfidentiality: 'tests/intern/form/functionalities/changeConfidentiality.js',
                changeContactInfo: 'tests/intern/form/functionalities/changeContactInfo.js',
                reuseValues: 'tests/intern/form/functionalities/reuseValues.js',
                rulesForm: 'tests/intern/form/rules/**/*.js',
                attributesForm: 'tests/intern/form/allAttributes.js',
                registerCase: 'tests/intern/form/registerCase.js',
                requiredness: 'tests/intern/form/requiredness.js',
            topbar: 'tests/intern/topbar/**/*.js',
            plusMenu: 'tests/intern/plusMenu/**/*.js'
};

// run the tests of a given set or path, or else for a specific folder/file combination, or else everything
const getPathNames = set => sets.hasOwnProperty(set) ? sets[set] : [set];
const getPaths = pathNames => pathNames.map(pathName => paths[pathName]);
const getSpecificPath = (folder, file) => [`tests/**/${folder || '**'}/**/${file || '*'}.js`];
let tests = [];

if ( npm_config_set ) {
    tests = getPaths(getPathNames(npm_config_set));
} else if ( npm_config_folder || npm_config_file ) {
    tests = getSpecificPath(npm_config_folder, npm_config_file);
} else {
    tests = getPaths(['all']);
}

// set multicapabilities for each browser and path combination
const multiCapabilities = browsers
    .map(browser =>
        tests.map(test =>
            ({
                browserName: browser,
                chromeOptions: {
                    args: [
                        '--disable-extensions',
                        '--disable-web-security',
                        '--start-maximized'
                    ]
                },
                specs: test
            })
        )
    )
    .reduce((accumulator, array) => accumulator.concat(array), []);

// set config settings
// onPrepare clears the default reporter and sets our own
// followed by starting zaaksysteem on the logout page and storages cleared, to ensure a fresh start
const TIMEOUT = 6000000;

exports.config = {
    baseUrl,
    maxSessions,
    multiCapabilities,
    framework: 'jasmine2',
    directConnect: !npm_config_remote,
    seleniumAddress: npm_config_remote,
    allScriptsTimeout: TIMEOUT,
    jasmineNodeOpts: {
        defaultTimeoutInterval: TIMEOUT
    },
    onPrepare() {
        jasmine.getEnv().clearReporters();
        jasmine.getEnv().addReporter(terminalReporter);
        jasmine.getEnv().addReporter(xmlReporter);
        browser.driver.get(`${baseUrl}/auth/logout`);
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    }
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
