package TestFor::General::Object::Types::Controlpanel;

# ./zs_prove -v t/lib/TestFor/General/Object/Types/Controlpanel.pm
use base qw(ZSTest);

use TestSetup;
use Zaaksysteem::Object::Types::Controlpanel;

sub zs_object_types_controlpanel : Tests {

    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                owner         => 'betrokkene-bedrijf-1',
                shortname     => 'gemmintlab',
                template      => 'mintlab',
                customer_type => 'commercial',
                domain        => 'zaaksysteem.net',
            );

            my %defaults = (
                allowed_diskspace => 100,
                allowed_instances => 25,
            );

            my ($obj, $saved) = $zs->create_object_ok('Controlpanel', { %defaults, %opts });

            throws_ok(
                sub {
                    $model->save_object(object => $obj);
                },
                qr#object/unique/constraint: Object unique constraint violation: \w+#,
                "Unique constraint violation",
            );

            lives_ok(
                sub { $model->save_object(object => $saved) },
                "Saved object can be saved if it exists"
            );

            my $object = $model->save(
                json => $zs->open_testsuite_json('controlpanel.json'));

            my $cd = $zs->create_instance_ok;
            my $relation = $saved->add_customer_d($cd);
            ok($relation, "Relationship has been created");

            lives_ok(
                sub { $saved->add_customer_d($cd) },
                "Creating an existing relationship is allowed"
            );

            throws_ok(
                sub {
                    my $customer_config = $zs->create_customer_config_ok();
                    my $cd = $zs->create_instance_ok();
                    $customer_config->add_customer_d($cd);

                    $model->save_object(object => $customer_config);

                    $cd = $model->retrieve(uuid => $cd->id);
                    $saved->add_customer_d($cd);
                },
                qr/Existing relationship, unable to set new relationship/,
                "Existing relationship, polygamy is not allowed here",
            );

        },
        'Controlpanel Object'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
