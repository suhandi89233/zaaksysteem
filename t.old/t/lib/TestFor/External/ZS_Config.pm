package TestFor::External::ZS_Config;
use base qw(Test::Class);

# Don't use TestSetup, you will break the tests because it loads the
# etc/customer.d and you will have multiple hosts present, causing failures.
use Test::More;
use Zaaksysteem::Config;
use File::Spec::Functions qw(catdir);

sub zs_config : Tests {

    my $configs = Zaaksysteem::Config->new(
        zs_customer_d => 't/data/etc/customer.d',
        zs_conf       => 't/data/etc/zaaksysteem.conf',
    );

    my $zs_base_conf = $configs->config;
    isa_ok($zs_base_conf, "HASH");

    my @customers = $configs->get_all_customers();
    @customers = sort { $a cmp $b } @customers;
    is_deeply(\@customers, [ qw(10.44.0.11 testsuite vagrant)], "Found all the customers");

    foreach (@customers) {
        my $config = $configs->get_customer_config($_);
        ok($config, "Got the configs");

        my $test_schema = $configs->get_customer_schema($_);
        isa_ok($test_schema, "Zaaksysteem::Schema");

        {
            my $res = { map { $_ => $test_schema->$_() } qw(output_path storage_path tmp_path) };
            my $basedir = "/var/tmp/zs";
            is_deeply(
                $res,
                {
                    output_path  => $basedir,
                    storage_path => catdir($basedir, 'storage'),
                    tmp_path     => catdir($basedir, 'tmp'),
                },
                "Directories are setup correctly"
            );
        }

        $test_schema = $configs->get_customer_schema($_, 1);

        {
            my $res = { map { $_ => $test_schema->$_() } qw(output_path storage_path tmp_path) };
            my $basedir = "/mnt/vagrant/meuk";
            is_deeply(
                $res,
                {
                    output_path  => $basedir,
                    storage_path => catdir($basedir, 'storage'),
                    tmp_path     => catdir($basedir, 'tmp'),
                },
                "Directories are setup correctly"
            );
        }

        my $rs
            = $test_schema->resultset("Config")->search_rs({}, { rows => 42 });
        isa_ok($rs, "Zaaksysteem::Backend::Config::ResultSet");

        $test_schema = $configs->get_customer_schema($_, {with_defaults => 1});
        $rs = $test_schema->resultset("Config")->search_rs({}, { rows => 42});
        isa_ok($rs, "Zaaksysteem::Backend::Config::ResultSet");
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
