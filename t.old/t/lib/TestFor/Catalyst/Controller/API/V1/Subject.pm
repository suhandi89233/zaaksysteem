package TestFor::Catalyst::Controller::API::V1::Subject;
use base qw(ZSTest::Catalyst);

use TestSetup;

use constant SUBJECT_PARAMS => {
    'company' => {
        'subject_type'                      => 'company',
        'subject'                   => {
            'company'       => 'Mintlab B.V.',
            'coc_number'            => '12345678',
            'coc_location_number'   => '545698785412',
            'company_type'          => {
                code    => 1,
                label   => 'Eenmanszaak',
            },
            'address_residence'          => {
                street                  => 'Donker Curtiusstraat',
                street_number           => 7,
                street_number_letter    => 'a',
                street_number_suffix    => '521',
                city                    => 'Amsterdam',
                zipcode                 => '1051JL',
                country         => {
                    label           => 'Nederland',
                    dutch_code      => 6030,
                }
            }
        }
        # 'handelsnaam'                       => 'Gemeente Zaakstad',
        # 'dossiernummer'                     => '25654589',
        # 'vestigingsnummer'                  => '123456789012',
        # 'vestiging_landcode'                => '6030',
        # 'vestiging_straatnaam'              => 'Zaakstraat',
        # 'vestiging_huisnummer'              => 44,
        # 'vestiging_huisletter'              => 'a',
        # 'vestiging_huisnummertoevoeging'    => '1rechts',
        # 'vestiging_postcode'                => '1234AB',
        # 'vestiging_woonplaats'              => 'Amsterdam',
    },
    'person'  => {
        subject_type    => 'person',
        subject         => {
            'personal_number'           => '123456789',
            'personal_number_a'         => '1987654321',
            'initials'                  => 'D.',
            'first_names'               => 'Don',
            'family_name'               => 'Fuego',
            'surname'                   => 'The Fuego',
            'prefix'                    => 'The',
            'gender'                    => 'M',
            'place_of_birth'            => 'Amsterdam',
            'date_of_birth'             => '1982-06-05',
            'use_of_name'               => 'E',
            'address_residence'         => {
                street                  => 'Muiderstraat',
                street_number           => 42,
                street_number_letter    => 'a',
                street_number_suffix    => '521',
                city            => 'Amsterdam',
                zipcode         => '1011PZ',
                country         => {
                    label           => 'Nederland',
                    dutch_code      => 6030,
                }
            },
            'address_correspondence'         => {
                street                  => 'Donkerstraat',
                street_number           => 1,
                street_number_letter    => 'b',
                street_number_suffix    => '522',
                city            => 'Donkerdam',
                zipcode         => '1011PA',
                country         => {
                    label           => 'Nederland',
                    dutch_code      => 6030,
                }
            }
        },
    }
};

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Subject - Proves the boundaries of our API: Subject

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Subject.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/subject> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create subject

=cut

sub cat_api_v1_subject_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;

        $mech->zs_login;
        my $data = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/subject/create',
            SUBJECT_PARAMS->{company}
        );

        ### Now retrieve by reference
        is($data->{result}->{type}, 'subject', 'Got subject as object type');
        is($data->{result}->{instance}->{subject}->{type}, 'company', 'Got company as inner subject object type');

        ok($data->{result}->{reference}, 'Got a reference for this subject');
        is($data->{result}->{reference}, $data->{result}->{instance}->{subject}->{reference}, 'Reference of subject is identical to inner subject reference');
    }, 'api/v1/subject/create: create a simple subject object: company');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;

        $mech->zs_login;
        my $data = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/subject/create',
            SUBJECT_PARAMS->{person}
        );

        ### Now retrieve by reference
        is($data->{result}->{type}, 'subject', 'Got subject as object type');
        is($data->{result}->{instance}->{subject}->{type}, 'person', 'Got company as inner subject object type');

        ok($data->{result}->{reference}, 'Got a reference for this subject');
        is($data->{result}->{reference}, $data->{result}->{instance}->{subject}->{reference}, 'Reference of subject is identical to inner subject reference');
    }, 'api/v1/subject/create: create a simple subject person');

}

## TODO:
# sub cat_api_v1_subject_update : Tests {

#     $zs->zs_transaction_ok(sub {
#         my $mech     = $zs->mech;

#         $mech->zs_login;
#         my $data = $mech->post_json_ok(
#             $mech->zs_url_base . '/api/v1/subject/create',
#             SUBJECT_PARAMS->{person}
#         );


#         $data = $mech->postget_json_ok(
#             $mech->zs_url_base . '/api/v1/subject',
#             {
#                 query   => {
#                     match => {
#                         subject_type => 'person',
#                         'subject.personal_number' => '123456789',
#                     }
#                 }
#             }
#         );

#         is($data->{result}->{instance}->{pager}->{rows}, 1, 'Got one row in database');
#         my $update = $data->{result}->{instance}->{rows}->[0];

#         $update->{subject}->{peronal_number} = '987654321';
        
#         $data = $mech->postget_json_ok(
#             $mech->zs_url_base . '/api/v1/subject/' . $update->{reference} . '/update',
#             $update,
#         );

#         $data = $mech->postget_json_ok(
#             $mech->zs_url_base . '/api/v1/subject',
#             {
#                 query   => {
#                     match => {
#                         subject_type => 'person',
#                         'subject.personal_number' => '123456789',
#                     }
#                 }
#             }
#         );

#         is($data->{result}->{instance}->{pager}->{rows}, 1, 'Still one row in database');
#         my $updated = $data->{result}->{instance}->{rows}->[0]->{instance};

#         is ($updated->{subject}->{personal_number}, '987654321', 'Personal number updated');
#     }, 'api/v1/subject/create: create, retrieve and save subject');
# }

=head2 search

=cut

sub cat_api_v1_subject_search : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech      = $zs->mech;
        $mech->zs_login;

        my $bridge    = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        my $object    = $bridge->object_from_params(SUBJECT_PARAMS->{company});
        $bridge->save($object);

        my $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject',
            {
                query   => {
                    match => {
                        subject_type => 'company',
                        'subject.company' => 'Mintlab B.V.',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 1, 'Got a single result');

        $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject',
            {
                query   => {
                    match => {
                        subject_type => 'company',
                        'subject.company' => 'Mintlab B.VVV.',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 0, 'Got no results');

    }, 'api/v1/subject: search for a simple subject object: company');

    $zs->zs_transaction_ok(sub {
        my $mech      = $zs->mech;
        $mech->zs_login;

        my $bridge    = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        my $object    = $bridge->object_from_params(SUBJECT_PARAMS->{person});
        $bridge->save($object);

        my $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject',
            {
                query   => {
                    match => {
                        subject_type => 'person',
                        'subject.personal_number' => '123456789',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 1, 'Got a single result');

        $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject',
            {
                query   => {
                    match => {
                        subject_type => 'person',
                        'subject.personal_number' => '1234567894',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 0, 'Got no results');

    }, 'api/v1/subject: search for a simple subject object: person');

    $zs->zs_transaction_ok(sub {
        my $mech      = $zs->mech;
        $mech->zs_login;

        $zs->create_subject_ok(
            username    => 'fritsie',
            properties  => {
                cn => 'fritsie',
                sn => 'de Boer',
                displayname => 'F. de Boer',
                givenname   => 'Fritsie',
                initials    => 'F',
                mail        => 'fritsie.devnull@zaaksysteem.nl',
                telephonenumber => '1234567890',
            }
        );

        my $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject',
            {
                query   => {
                    match => {
                        subject_type        => 'employee',
                        'subject.surname'   => 'de Boer',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 1, 'Got a single result');

        $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject',
            {
                query   => {
                    match => {
                        subject_type => 'employee',
                        'subject.surname'      => 'de Boerrr',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 0, 'Got no results');

    }, 'api/v1/subject: search for a simple subject object: employee');
}

sub cat_api_v1_subject_remote : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        $self->_create_nps_interface;

        my $mech      = $zs->mech;
        $mech->zs_login;

        my $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject/remote_search',
            {
                query   => {
                    match => {
                        subject_type => 'person',
                        'subject.personal_number' => '54568789',
                    }
                }
            }
        );

        is($data->{result}->{type}, 'set', 'Got a set of data');
        is($data->{result}->{instance}->{pager}->{rows}, 3, 'Got 3 results');

        $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject/remote_import',
            $data->{result}->{instance}->{rows}->[0]
        );


    }, 'api/v1/subject/remote_search: search for a simple subject object: person');
}

sub cat_api_v1_subject_remote_import : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        $self->_create_nps_interface;

        my $mech      = $zs->mech;
        $mech->zs_login;

        my $data = $mech->postget_json_ok(
            $mech->zs_url_base . '/api/v1/subject/remote_search',
            {
                query   => {
                    match => {
                        subject_type => 'person',
                        'subject.personal_number' => '987654321',
                    }
                }
            }
        );

        $data = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/subject/remote_import',
            $data->{result}->{instance}->{rows}->[0]->{instance}
        );

        is($data->{result}->{instance}->{subject}->{instance}->{personal_number}, 987654321, 'Import succesfull');

    }, 'api/v1/subject/remote_import: search and import: person');
}

sub _create_nps_interface {
    my $self            = shift;
    my $params          = shift || {};

    $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                stuf_supplier           => 'centric',
                synchronization_type    => 'question',
                gemeentecode            => '363',
                mk_sender               => 'ZSNL',
                mk_ontvanger            => 'CGS',
                gbav_applicatie         => 'CML',
                mk_ontvanger_afnemer    => 'CMODIS',
                mk_async_url            => 'https://localhost/stuf/async',
                mk_sync_url             => 'https://localhost/stuf/sync',
                gbav_pink_url           => 'https://localhost/stuf/pink',
                gbav_search             => 1,
                mk_spoof                => 1,
            }
        },
    );

    return $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF NPS Parsing',
            active          => 1,
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
