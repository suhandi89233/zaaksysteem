package TestFor::Catalyst::Controller::API::V1::Session;
use base qw(ZSTest::Catalyst);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Session - Proves the boundaries of our API: Session

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Session.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/session> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 api/v1/session/current

=cut

sub cat_api_v1_session_current : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;

        my $json = $mech->get_json_ok($mech->zs_url_base . '/api/v1/session/current');

        ### Verify attributes
        is($json->{result}->{type}, 'session', 'Got correct type');
        $self->_check_session_structure($json->{result}->{instance});

    }, 'api/v1/session/current: retrieve current session');
}

sub _check_session_structure {
    my $self      = shift;
    my $instance  = shift;

    is($instance->{account}->{type}, 'session_account', 'Got correct type for account');

    ok($instance->{$_}, "Value set for $_") for qw/account hostname logged_in_user/;

    ok($instance->{account}->{instance}->{$_}, "Value set for account->$_") for qw/
      address
      company
      email
      homepage
      phonenumber
      place
      zipcode
    /;

    ok($instance->{logged_in_user}->{$_}, "Value set for logged_in_user->$_") for qw/
        id
        display_name
        given_name
        initials
        email
        surname
        telephonenumber
        system_roles
        organizational_unit
        system_permissions
    /;

    is($instance->{logged_in_user}->{organizational_unit}, 'Backoffice', "Got backoffice as department");
    is(@{ $instance->{logged_in_user}->{system_roles} }, 2, "Got two values for system_roles");
    ok(@{ $instance->{logged_in_user}->{system_permissions} } > 5, "Got at least 5 global rights");

    ok((grep { $_ eq 'Administrator'} @{ $instance->{logged_in_user}->{system_roles} }), "Got 'Administrator' as system role");

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
