package TestFor::Catalyst::Controller::API::V1::General::LegalEntityType;
use base qw(ZSTest::Catalyst);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::General::LegalEntityType - Test file for General::LegalEntityTypes in our v1 API

=head1 SYNOPSIS

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/General/LegalEntityType.pm

=head1 DESCRIPTION

This module tests the C</api/v1/general/legal_entities> namespace.

=cut


sub api_v1_general_legal_entity_types : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(
        sub {
            my $mech = $zs->mech;
            $mech->zs_login;
            my $base_url
                = $mech->zs_url_base . '/api/v1/general/legal_entity_types';

            {
                my $data        = $mech->get_json_ok($base_url);
                my $type = {
                    code        => '41',
                    label       => 'Besloten vennootschap met gewone structuur',
                    description => undef,
                    active      => JSON::true,
                };

                cmp_deeply($data->{result}{instance}{rows}[0]{instance}, $type, "Besloten vennootschap met gewone structuur is correct");
            }

            {
                my $data        = $mech->get_json_ok("$base_url/7");
                my $type = {
                    code        => '7',
                    label       => 'Maatschap',
                    description => undef,
                    active      => JSON::true,
                };

                cmp_deeply($data->{result}{instance}, $type, "Entity type 7 is correct");
            }

            {
                $mech->get("$base_url/10001");
                $mech->validate_api_v1_error(
                    regexp => qr/Unable to find legal entity type with id '10001'/,
                );
            }
        },
        'Get all the legal entity types',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
