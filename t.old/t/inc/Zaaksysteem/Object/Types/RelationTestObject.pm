package Zaaksysteem::Object::Types::RelationTestObject;

use Moose;

extends 'Zaaksysteem::Object';

has name => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]]
);

has rel => (
    is => 'rw',
    type => 'relation_test_object',
    traits => [qw[OR]]
);

has embedded_rel => (
    is => 'rw',
    type => 'relation_test_object',
    embed => 1,
    traits => [qw[OR]]
);

has rels => (
    is => 'rw',
    type => 'relation_test_object',
    traits => [qw[OR]],
    isa_set => 1,
);

has embedded_rels => (
    is => 'rw',
    type => 'relation_test_object',
    embed => 1,
    traits => [qw[OR]],
    isa_set => 1,
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
