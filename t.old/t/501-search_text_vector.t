#! perl

use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;
use Test::DummyResultSet;
use Zaaksysteem::Search::TSVectorResultSet;

use Moose::Util qw[apply_all_roles];

{
    my $brs = Test::DummyResultSet->new;

    apply_all_roles $brs, 'Zaaksysteem::Search::TSVectorResultSet';

    my $rs = $brs->search_text_vector('derp')->[0][0];

    ok exists $rs->{ text_vector }, 'text_vector column used for query';

    my $query = $rs->{ text_vector };

    ok exists $query->{ '@@' }, 'test_vector is queried with @@ operator';

    my $rterm = $query->{ '@@' };

    is $rterm, 'derp:*', 'value is properly injected';
}

{
    no warnings 'qw'; # Because qw[#] generates a warn

    for my $char (qw[! @ # $ % ^ & * ( ) { } | \ ; ' : " , . < > / ?]) {
        my $brs = Test::DummyResultSet->new;

        apply_all_roles $brs, 'Zaaksysteem::Search::TSVectorResultSet';

        my $keyword = sprintf('%skey%s', $char, $char);

        my $value = $brs->search_text_vector($keyword)->[0][0]{ text_vector }{ '@@' };

        is $value, 'key:*', "'$char' punctuation is correctly removed from text vector keyword";
    }
}

{
    my $brs = Test::DummyResultSet->new;

    apply_all_roles $brs, 'Zaaksysteem::Search::TSVectorResultSet';

    my $value = $brs->search_text_vector('a b c d')->[0][0]{ text_vector }{ '@@' };

    is $value, 'a:* & b:* & c:* & d:*', 'multiple keywords are correctly AND-ed implicitly';
}

{
    my $brs = Test::DummyResultSet->new;

    apply_all_roles $brs, 'Zaaksysteem::Search::TSVectorResultSet';

    lives_ok sub { $brs->search_text_vector('') },
        'search_text_vector throws exception on empty input string';

    lives_ok sub { $brs->search_text_vector; },
        'search_text_vector throws exception on no arguments';
}

zs_done_testing;
