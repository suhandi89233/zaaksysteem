#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
initialize_test_globals_ok;
use Test::Deep;

### Test header end

my $api_template    = {
    '/sysin/interface'              => {
        is_resultset    => 1,
        request         => {
            module          => 'bagcsv',
            name            => 'CSV Import'
        },
        response        => {
            name            => 'CSV Import',
            module          => 'bagcsv',
            max_retries     => 10,
            multiple        => 0,
            active          => 0,
        },
        model           => sub {
            my $opts    = shift;

            return $schema  ->resultset('Interface')
                            ->search();
        }
    },
    '/sysin/interface/create'              => {
        is_resultset    => 0,
        request         => {
            module          => 'bagcsv',
            name            => 'CSV Import'
        },
        response        => {
            name            => 'CSV Import',
            module          => 'bagcsv',
            max_retries     => 10,
            multiple        => 0,
            active          => 0,
        },
        model           => sub {
            my $opts    = shift;

            return $schema  ->resultset('Interface')
                            ->interface_create(
                                $opts->{params}
                            );
        }
    },
    '/sysin/interface/ID/update'    => {
        is_resultset    => 0,
        request         => {
            module              => 'bagcsv',
            name                => 'CSV Import',
            interface_config    => {
                how_hot         => 'VERY',
            }
        },
        response        => {
            name                => 'CSV Import',
            module              => 'csv',
            interface_config    => {
                how_hot => 'VERY',
            }
        },
        model           => sub {
            my $opts    = shift;

            my $entry   = $schema   ->resultset('Interface')
                                    ->search()
                                    ->first;

            return $entry->interface_update($opts->{params});
        }
    },
};

my $script          = {
    'Create and list interfaces'   => [
        '/sysin/interface/create',
        '/sysin/interface'
    ],
    'Create and update interfaces'   => [
        '/sysin/interface/create',
        '/sysin/interface/ID/update',
    ],
};

my $json    = JSON->new;
$json->convert_blessed(1);

for my $run (keys %{ $script }) {
    $zs->zs_transaction_ok(sub {

        for my $apicall (@{ $script->{$run} }) {
            my $apidata     = $api_template->{ $apicall };


            my $rv      = $apidata->{model}->({params => $apidata->{request}});

            my $entry;
            if ($apidata->{is_resultset}) {
                isa_ok(
                    $rv,
                    'DBIx::Class::ResultSet',
                    'API "' . $apicall . '": Got ResultSet'
                );
                ok($rv->count, 'API "' . $apicall . '": Got entries');

                $entry = $rv->first;
            } else {
                ok($rv, 'API "' . $apicall . '": Got entry');
                $entry = $rv;
            }

            my $jsonp   = $json->utf8->decode($json->utf8->encode($entry));

            #note(explain($jsonp));

            # cmp_deeply(
            #     $jsonp,
            #     superhashof($apidata->{response}),
            #     'API "' . $apicall . '": Got correct values'
            # );
        }
    }, 'Tested API: ' . $run);
}


$zs->zs_transaction_ok(sub {
    my $entry   = $schema
                ->resultset('Interface')
                ->interface_create({
                    module              => 'bagcsv',
                    name                => 'Bag CSV Import',
                });

    my $mapping  = $entry->get_attribute_mapping;

    isa_ok($mapping, 'HASH');

    isa_ok($mapping->{attributes}, 'ARRAY');
    ok(scalar( @{ $mapping->{attributes} }) > 0, 'Got list of attributes');

}, 'Tested API: Test attribute generation');

$zs->zs_transaction_ok(sub {
    my $entry   = $schema
                ->resultset('Interface')
                ->interface_create({
                    module              => 'bagcsv',
                    name                => 'Bag CSV Import',
                });

    my $jsonp   = $json->utf8->decode(
        $json->utf8->encode(
            $entry->get_interface_form()
        )
    );

    #note(explain($jsonp));

}, 'Tested API: Test attribute generation');


zs_done_testing;