#!/usr/bin/perl -w

use strict;
use DateTime;
use Zaaksysteem::Object::Types::ScheduledJob;

Zaaksysteem::Object::Types::ScheduledJob->new(
    job => 'CaseUrgencyUpdater',
    interval_period => 'hours',
	interval_value => 1,
	next_run => DateTime->now(),
	data => {
		label => 'Zaken voortgang herberekening'
	}
);
