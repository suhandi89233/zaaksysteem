const glob = require('glob');

const apps = glob
  .sync('src/*/index.js', {
    ignore: 'src/shared'
  })
  .map(app => app.split('/')[1]);

module.exports = apps;
