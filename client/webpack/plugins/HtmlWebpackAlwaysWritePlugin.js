const { join } = require('path');
const async = require('async');
const fsExtra = require('fs-extra');
const find = require('lodash/find');
const identity = require('lodash/identity');

class HtmlWebpackAlwaysWritePlugin {
  constructor( settings ) {
    this.settings = settings || {};
  }

  apply( compiler ) {
    const stripPrefix = this.settings.stripPrefix;
    const root = this.settings.root || '.';
    const entries = this.settings.entries || [];

    compiler.plugin('after-emit', ( compilation, callback ) => {
      const files = entries
        .map(entryName => {
          const file = find(
            compilation.assets,
            ( asset, filename ) =>
              filename.endsWith(join(entryName, 'index.html'))
          );
          const filename = join(
            root,
            compilation.outputOptions.publicPath.replace(stripPrefix, ''),
            entryName,
            'index.html'
          );

          return file ?
            {
              filename,
              source: file.source()
            }
            : null;
        })
        .filter(identity);

      async
        .parallel(
          files.map(file => cb => fsExtra.outputFile(file.filename, file.source, cb)),
          callback
        );
    });
  }

}

module.exports = HtmlWebpackAlwaysWritePlugin;
