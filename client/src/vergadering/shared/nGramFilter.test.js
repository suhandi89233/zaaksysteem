import { buildNGrams } from 'word-ngrams';
import {
  NGRAM_BUILD_OPTIONS,
  SENTENCE_SPLITTERS,
  isSafeSubject
} from './nGramFilter';

const EMPTY = [
  ' ',
  ',',
  ':'
];
const NGRM_UNIT = 1;
// Test this on likely placeholder user input.
const PRINTABLE_ASCII_POINTS = [32, 126];
const THROWN_ERROR_MESSAGE = 'Cannot read property \'length\' of null';

const buildFactory = subject =>
  buildNGrams(subject, NGRM_UNIT, NGRAM_BUILD_OPTIONS);

const getResultPropertyName = char => {
  if (EMPTY.includes(char)) {
    return '';
  }

  return char.toLowerCase();
};

const getResultFromChar = char => ({
  [getResultPropertyName(char)]: NGRM_UNIT
});

// The nGramFilter itself closes over  module variables and
// isn't exportable/testable without a lot of mocking/stubbing.
// Test the filter and the installed package that has the internal problem instead.
test('The nGramFilter module has a function that guards against exclusive sentence splitter subjects',
  () => {
    let [index] = PRINTABLE_ASCII_POINTS;
    const [, length] = PRINTABLE_ASCII_POINTS;
    const [sampleSplitter] = SENTENCE_SPLITTERS;
    const { fromCodePoint } = String;

    // For any printable ASCII code point concatenated with a sentence splitter, test if
    // - buildNGrams throws an Error or succeeds
    // - if the filter function would guard against that Error or not
    while (index <= length) {
      const char = fromCodePoint(index);
      const subject = `${char}${sampleSplitter}`;

      if (SENTENCE_SPLITTERS.includes(char)) {
        expect(() => buildFactory(subject)).toThrow(THROWN_ERROR_MESSAGE);
        expect(isSafeSubject(subject)).toBe(false);
      } else {
        const actual = buildFactory(subject);
        const expected = getResultFromChar(char);

        expect(actual).toEqual(expected);
        expect(isSafeSubject(actual)).toBe(true);
      }

      index += 1;
    }
  });
