import find from 'lodash/find';
import getAttributes from './getAttributes';

/**
 * Helper function.
 *
 * @param {Object} topic
 * @return {number|string}
 */
export const getSearchableObjectId = topic =>
	topic
		.internal_name
		.searchable_object_id;

/**
 * Helper function.
 *
 * @param {Object} columns
 * @return {Object}
 */
export const findTopic = columns =>
	find(
		columns,
		col => (col.external_name === 'voorstelonderwerp')
	);

/**
 * Helper function.
 *
 * @param {Object} config
 * @return {Object}
 */
export const getTopic = config =>
	findTopic(getAttributes(config).voorstel);

/**
 * Helper function.
 *
 * @param {Object} caseType
 * @return {string}
 */
export const getName = caseType =>
	caseType.instance.name;

/**
 * Get the case title.
 *
 * @param attributes
 * @param casetype
 * @param topic
 * @return {string}
 */
export function getTitle( { attributes, casetype, topic } ) {
	if (topic) {
		const searchableObjectId = getSearchableObjectId(topic);

		return attributes[searchableObjectId];
	}

	return getName(casetype);
}

/**
 * Get the formatted case title for the UI.
 *
 * @param {Object} proposal
 * @param {Object} config
 * @return {string}
 */
export default function getFormattedTitle( proposal, config ) {
	const {
		instance: {
			attributes,
			casetype,
			number
		}
	} = proposal;
	const value = getTitle({
		attributes,
		casetype,
		topic: getTopic(config)
	});

	return `${number}: ${value}`;
}
