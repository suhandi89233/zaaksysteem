import angular from 'angular';
import proposalItemListModule from './proposalItemList';
import resourceModule from '../../../shared/api/resource';
import rwdServiceModule from '../../../shared/util/rwdService';

import controller from './MeetingListItemController';
import template from './template.html';
import './styles.scss';

export default angular
	.module('Zaaksysteem.meeting.meetingListItem', [
		resourceModule,
		proposalItemListModule,
		rwdServiceModule
	])
	.component('meetingListItem', {
		bindings: {
			onToggleExpand: '&',
			isExpanded: '&',
			isGrouped: '&',
			meeting: '&',
			appConfig: '&'
		},
		controller,
		template
	})
	.name;
