import includes from 'lodash/includes';
import first from 'lodash/head';
import isArray from 'lodash/isArray';

export default class MeetingListItemController {
	static get $inject() {
		return ['$scope', '$state', 'resource', 'rwdService', 'dateFilter'];
	}

	constructor($scope, $state, resource, rwdService, dateFilter) {
		let ctrl = this;

		ctrl.loadingProposals = false;

		$scope.$on('Proposals: Started loading.', (event, uuid) => {
			if ( uuid === ctrl.meeting().id ) {
				ctrl.loadingProposals = true;
			}
		});

		$scope.$on('Proposals: Done loading.', (event, uuid) => {
			if ( uuid === ctrl.meeting().id ) {
				ctrl.loadingProposals = false;
			}
		});

		ctrl.getDate = ( ) => {
			return dateFilter( ctrl.meeting().date, 'dd MMM yyyy');
		};

		ctrl.getTime = ( ) => {
			return ctrl.meeting().time;
		};

		ctrl.getTitle = ( ) => {
			return isArray(ctrl.meeting().label) ? first(ctrl.meeting().label) : ctrl.meeting().label || ctrl.meeting().instance.casetype.instance.name;
		};

		ctrl.getLocation = ( ) => {
			if (isArray(ctrl.meeting().location )) {
				return first(ctrl.meeting().location);
			}
			return ctrl.meeting().location;
		};

		ctrl.getChairman = ( ) => {
			return ctrl.meeting().chairman;
		};

		ctrl.getProposals = ( ) => {
			return !ctrl.isExpanded() ? [] : ctrl.meeting().children;
		};

		ctrl.toggleExpand = ( ) => {
			ctrl.onToggleExpand();
		};

		ctrl.isMeetingList = ( ) => {
			return !!($state.current.name.indexOf('meetingList') !== -1);
		};

		ctrl.getViewSize = ( ) => {
			if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
				return 'small';
			}
			return 'wide';
		};
	}
}
