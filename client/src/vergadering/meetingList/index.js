import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import meetingListViewModule from './meetingListView';
import meetingListItemModule from '../shared/meetingListItem';
import snackbarServiceModule from '../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import flattenAttrs from '../shared/flattenAttrs';
import first from 'lodash/head';
import find from 'lodash/find';

export default {

	moduleName:
		angular.module('Zaaksysteem.meeting.meetingList', [
			resourceModule,
			meetingListItemModule,
			meetingListViewModule,
			snackbarServiceModule,
			composedReducerModule
		])
		.controller('meetingListController', [
			'$state', '$stateParams', '$scope', 'meetingResource', 'currentAppConfig',
			( $state, $stateParams, $scope, meetingResource, currentAppConfig ) => {
				$scope.meetingResource = meetingResource;
				$scope.meetingType = $stateParams.meetingType;
				$scope.appConfig = currentAppConfig.data;

				$scope.$on('$destroy', ( ) => {
					[ meetingResource ].forEach( resource => {
						resource.destroy();
					});
				});
			}
		])
		.name,
	config: [
		{
			route: {
				url: '/:group/',
				scope: {
					appConfig: '&'
				},
				params: {
					meetingType: { squash: true, value: 'open' },
					paging: { squash: true, value: 50 }
				},
				template,
				resolve: {
					currentAppConfig: [
						'$rootScope', 'composedReducer', '$stateParams', 'appConfig',
						( $rootScope, composedReducer, $stateParams, appConfig ) => {

							return composedReducer( { scope: $rootScope }, appConfig, $stateParams )
								.reduce( (config, state ) => find(config, ( configItem ) => configItem.instance.interface_config.short_name === state.group));

					}],
					meetingResource: [
						'$rootScope', '$q', '$stateParams', 'resource', 'currentAppConfig', 'snackbarService',
						( $rootScope, $q, $stateParams, resource, currentAppConfig, snackbarService ) => {
							let meetingResource = resource( ( ) => {

								let statuses =
									$stateParams.meetingType === 'open' ?
										[ 'open', 'new' ]
										: [ 'resolved' ],
									publicationAttribute = currentAppConfig.data().instance.interface_config.meeting_publication_filter_attribute,
									publicationAttributeValue = currentAppConfig.data().instance.interface_config.meeting_publication_filter_attribute_value,
									zqlOrder = $stateParams.meetingType === 'open'
										? ''
										: 'ORDER BY case.date_of_completion DESC',
									publicationQueryPartial = publicationAttribute && publicationAttributeValue ? `AND (${publicationAttribute.object.column_name} = "${publicationAttributeValue}")` : '',
									zql = `SELECT {} FROM case WHERE (case.casetype.id = ${first(currentAppConfig.data().instance.interface_config.casetype_meeting)}) ${publicationQueryPartial} AND (case.status IN ("${statuses.join('","')}") ) ${zqlOrder}`;
						
								return {
									url: '/api/v1/case',
									params: {
										zql,
										rows_per_page: $stateParams.paging
									}
								};

							}, { scope: $rootScope })
								.reduce( ( requestOptions, data ) => {
									return (data || seamlessImmutable([])).map(flattenAttrs);
								});

							return meetingResource.asPromise()
								.then( ( ) => meetingResource)
								.catch( err => {
									snackbarService.error('Er ging iets fout bij het ophalen van de vergaderingen. Neem contact op met uw beheerder voor meer informatie.');
									return $q.reject(err);
								});

					}]
				},
				controller: 'meetingListController'
			},
			state: 'meetingList'
		}
	]
};
