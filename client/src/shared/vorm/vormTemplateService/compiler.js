import angular from 'angular';
import propCheck from './../../util/propCheck';
import assign from 'lodash/assign';
import each from 'lodash/each';
import get from 'lodash/get';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import map from 'lodash/map';
import last from 'lodash/last';

let compileFn = ( $compile, $interpolate, defaults = {} ) => {

	let templateConfigs = assign({}, defaults.registrations),
		compilers = assign({}, defaults.compilers),
		clones = [],
		defaultDisplay = defaults.defaultDisplay
			|| angular.element('<span class="delegate-value-display">{{delegate.value}}</span>'),
		defaultWrapper = defaults.defaultWrapper
			|| angular.element(
			`<label id="{{::vm.getInputId()}}-label" for="{{::vm.getInputId()}}">{{vm.label}}</label>

			<ul 
				class="value-list multiple" 
				ng-class="{ empty: vm.isEmpty(), disabled: vm.disabled() }"  
			>
				<li
					class="value-item"
					ng-switch="delegate.disabled"
					ng-repeat="delegate in vm.getDelegates() track by $index"
					ng-class="delegate.classes"
				>
					<vorm-display
						class="vorm-display"
						ng-if="!delegate.empty"
					></vorm-display>

					<vorm-control></vorm-control>

					<ul 
						class="value-validity-list" 
						ng-repeat="(key, value) in delegate.valid track by key" 
						ng-show="delegate.valid"   
					>
						<li class="value-validity value-validity-{{::key}}">
							{{value}}
						</li>
					</ul>

					<button
						type="button"
						class="vorm-clear"
						aria-describedby="{{::vm.getInputId()}}-label"
						aria-label="Veld legen"
						ng-click="vm.handleDelegateRemove(delegate, $event)"
						ng-show="delegate.clearable"
					>
						<zs-icon icon-type="close"></zs-icon>
					</button>
				</li>
			</ul>

			<button 
				type="button" 
				class="vorm-field-add-button btn btn-secondary" 
				ng-click="vm.addDelegate()" 
				ng-show="vm.isAddButtonVisible()"  
			>
				{{vm.addLabel||'Veld toevoegen'}}
			</button>
		`);

	let createElement = ( template, fallback ) => {

		let el;

		if (typeof template === 'function') {
			el = template(angular.element(fallback).clone());
		} else {
			el = angular.element(template);
		}

		return el;

	};

	let createWrapper = ( base, ...rest ) => {

		[ 'control', 'display' ]
			.forEach(( type, index ) => {

				let elToReplace = angular.element(base).find(`vorm-${type}`),
					replacement = rest[index],
					attributes;

				if (replacement) {


					if (!elToReplace.length) {
						throw new Error(`Cannot create wrapper: ${type} element to replace not found`);
					}

					// copy attributes from element to replace to replacement

					attributes = elToReplace[0].attributes;

					for (let i = 0, l = attributes.length; i < l; ++i) {
						let attribute = attributes.item(i);
						
						// append classes, not replace classes
						if (attribute.name !== 'class') {
							replacement.attr(attribute.name, attribute.value);
						} else {
							replacement.attr(attribute.name, replacement.attr('class').split(' ').concat(attribute.value.split(' ')).join(' '));
						}
						
					}

					elToReplace.replaceWith(replacement);
				}

			});


		return base;

	};

	let registerType = ( type, options ) => {

		propCheck.throw(propCheck.shape({
			type: propCheck.string,
			inherits: propCheck.string.optional,
			control: options.inherits ?
				propCheck.any.optional
				: propCheck.any,
			display: propCheck.any.optional,
			wrapper: propCheck.func.optional,
			defaults: propCheck.object.optional
		}), assign({ type }, options));

		clones.forEach(clone => {
			clone.registerType(type, options);
		});

		templateConfigs[type] =
			assign(
				{},
				{
					defaults: get(templateConfigs[options.inherits], 'defaults', {})
				},
				options
			);

	};

	let createCompiler = ( name, ...rest ) => {

		let ngModelEl,
			template,
			options =
				rest.reduce(( previous, current ) => {

					let control = current.control ? createElement(current.control, previous.control) : previous.control,
						display = current.display ? createElement(current.display, previous.display) : previous.display;

					return {
						control,
						display
					};

				}, {
					control: null,
					display: defaultDisplay.clone()
				}),
			wrapperEl;


		options = assign(options,
			mapValues(
				keyBy([ 'control', 'display']),
				( type ) => {

					let el = options[type].clone(),
						attrs = {
							class: `vorm-${type}`,
							delegate: 'delegate',
							'template-data': 'vm.templateData()'
						};

					if (type === 'control') {
						attrs['ng-switch-default'] = '';
						attrs['input-id'] = 'vm.getInputId()';
					} else {
						attrs['ng-switch-when'] = 'true';
						attrs['tabindex'] = 0;
					}

					each(attrs, ( value, key ) => {

						switch (key) {
							default:
							el.attr(key, value);
							break;

							case 'class':
							el.attr(key, `${el.attr(key) || ''} ${value}`);
							break;
						}
						

					});

					return el;

			})
		);

		if (last(rest).wrapper) {
			wrapperEl = last(rest).wrapper(defaultWrapper.clone());
		} else {
			wrapperEl = defaultWrapper.clone();
		}
		
		template = createWrapper(wrapperEl, options.control, options.display);

		for (let i = 0, l = template.length; i < l; ++i) {

			let node = template[i],
				el = 'querySelector' in node ? node.querySelector('[ng-model]') : null;

			if (el) {
				ngModelEl = el;
				break;
			}
		}

		if (!ngModelEl) {
			throw new Error('No element was found with an ng-model specified.');
		}

		ngModelEl.setAttribute('ng-model', 'delegate.value');
		ngModelEl.setAttribute('ng-change', 'delegate.onChange()');
		ngModelEl.setAttribute('ng-model-options', 'vm.getModelOptions()');

		template.attr(
			'data-inherits',
			map(
				rest.concat({ inherits: name })
					.filter(opt => opt.inherits)
					.map(opt => opt.inherits)
			).join(' ')
		);

		if ($interpolate.startSymbol() !== '{{') {
			
			template =
				angular.element(
					map(
						template,
						el => {
							return (el.outerHTML || '').replace(/{{/g, $interpolate.startSymbol())
									.replace(/}}/g, $interpolate.endSymbol());
						}
					).join('')
				);
		}

		return $compile(template);

	};

	let getInheritanceChain = ( options ) => {

		let inheritanceChain = [];

		if (options.inherits) {
			inheritanceChain.push(templateConfigs[options.inherits]);
		}

		inheritanceChain.push(options);

		return inheritanceChain;
	};

	let getTypeCompiler = ( type ) => {

		let compiler;

		if (compilers[type]) {
			compiler = compilers[type];
		} else if (templateConfigs[type]) {
			compiler = compilers[type] = createCompiler(type, ...getInheritanceChain(templateConfigs[type]));
		} else {
			throw new Error(`Cannot create compiler for type ${type}`);
		}

		return compiler;

	};

	let getCompiler = ( options ) => {

		let compiler;

		propCheck.throw(propCheck.oneOfType([ propCheck.string, propCheck.object ]), options);

		if (typeof options === 'string') {
			compiler = getTypeCompiler(options);
		} else if (typeof options === 'function') {
			compiler = options;
		} else {
			compiler = createCompiler(null, ...getInheritanceChain(options));
		}

		return compiler;
	};

	let getDefaults = ( type ) => get(templateConfigs, `${type}.defaults`, {});

	let modifyWrapper = ( fn ) => {
		defaultWrapper = fn(defaultWrapper);
		compilers.wrapper = $compile(defaultWrapper.clone());
	};

	return {
		registerType,
		getDefaults,
		modifyWrapper,
		compile: ( template, scope, element ) => {
			getCompiler(template)(scope, el => {
				element.append(el);
			});
		},
		clone: ( ) => {
			let clone =
				compileFn($compile, $interpolate, {
					registrations: templateConfigs,
					compilers,
					defaultWrapper,
					defaultDisplay
				});

			clones = clones.concat(clone);

			return clone;
		}
	};

};

export default compileFn;
