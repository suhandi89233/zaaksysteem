import angular from 'angular';
import escapeString from 'lodash/escape';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsTruncateHtmlModule from './../../../ui/zsTruncate/zsTruncateHtml';
 
export default
	angular.module('vorm.types.textarea', [
			vormTemplateServiceModule,
			zsTruncateHtmlModule
		])
			.filter('newlines', [ ( ) => {

				return ( val ) => {
					const escapedVal = escapeString(val);

					return escapedVal ?
						escapedVal.replace(/\n/g, '<br/>')
						: escapedVal;
				};

			}])
			.run([ 'vormTemplateService', function ( vormTemplateService ) {
				
				const el = angular.element(`
					<textarea
						ng-model
						placeholder="{{vm.invokeData('placeholder')}}"
					>
					</textarea>
				`);
				
				vormTemplateService.registerType('textarea', {
					control: el,
					display:
						angular.element(
							'<zs-truncate-html data-value="delegate.value | newlines" data-length=500></zs-truncate-html>'
						)
				});
				
			}])
			.name;
