import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsTableHeader', [
	])
		.directive('zsTableHeader', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					columns: '&',
					onColumnClick: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.handleColumnClick = ( column ) => {
						ctrl.onColumnClick({ $columnId: column.id });
					};

					ctrl.getSortingLabel = (column) => {
						return column.sort
							? `Sorteren op ${column.label} ${column.iconType === 'chevron-down' ? 'oplopend': 'aflopend'}`
							: column.label;
					};

				}],
				controllerAs: 'zsTableHeader'
			};

		}])
		.name;
