import angular from 'angular';
import controller from './../controller';
import template from './../template.html';
import './../pagination.scss';

export default
	angular.module('zsResourcePagination', [
	])
	.directive('zsResourcePagination', [ ( ) => {

		let tpl = template.replace(/zsPagination/g, 'zsResourcePagination');

		return {
			restrict: 'E',
			scope: {
				resource: '&'
			},
			template: tpl,
			bindToController: true,
			controller: [ '$scope', function ( $scope ) {

				let ctrl = this;

				ctrl.currentPage = ( ) => ctrl.resource().cursor();

				ctrl.hasNextPage = ( ) => !!ctrl.resource().next();

				ctrl.hasPrevPage = ( ) => !!ctrl.resource().prev();

				ctrl.onPageChange = ( values ) => ctrl.resource().request(ctrl.resource().cursor(values.$page));

				ctrl.limit = ( ) => ctrl.resource().limit();

				ctrl.isLoading = ( ) => ctrl.resource().state() === 'pending';

				ctrl.onLimitChange = ( values ) => {
					let request = ctrl.resource().limit(values.$limit, ctrl.resource().cursor(1));
					
					ctrl.resource().request(request);
				};

				controller.call(ctrl, $scope);

			}],
			controllerAs: 'zsResourcePagination'
		};

	}])
	.name;
