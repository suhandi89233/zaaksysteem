import angular from 'angular';
import controller from './TruncateHtmlController';
import template from './template.html';

export default angular
	.module('zsTruncateHtml', [])
	.component('zsTruncateHtml', {
		bindings: {
			length: '&',
			value: '&'
		},
		controller,
		template
	})
	.name;
