import angular from 'angular';
import assign from 'lodash/assign';
import UserCancellationError from './UserCancellationError.js';

let decorate = ( promise, cancel ) => {

	'then catch finally'
		.split(' ')
		.forEach( key => {
			
			let func = promise[key];

			promise[key] = ( ...rest ) => decorate(func.apply(promise, rest), cancel);
		});

	promise.abortRequest = ( ) => {
		cancel({ ignore: false });
	};

	promise.abortRequestAndIgnore = ( ) => {
		cancel({ ignore: true });
	};

	return promise;
};

export default
	angular.module('shared.api', [
	])
		.factory('api', [ '$http', '$q', '$timeout', ( $http, $q, $timeout ) => {

			let api = function ( options ) {
				let cancel,
					promise,
					state;

				promise =
					$http(
						assign(options, {
							// TODO: this is for backwards compatibility, please remove when updating angular to 1.3.x
							timeout: $q( ( resolve/*, reject*/ ) => {

								if (options.timeout) {

									if (typeof options.timeout === 'number') {
										$timeout(resolve, options.timeout);
									} else {
										options.timeout.then(resolve);
									}

								}

								cancel = resolve;

							})
						})
					);
						

				decorate(promise, ( opts ) => {
					state = opts;
					cancel();
				});

				return promise
					.catch(( response ) => {

						if (state && state.ignore) {
							return $q.defer().promise;
						} else if (state && !state.ignore) {
							return $q.reject(new UserCancellationError());
						}
						
						return $q.reject(response);
					});
			};

			'get post put delete'
				.split(' ')
				.forEach(key => {
				
				api[key] = ( url, options ) => {

					return api(
						assign({
							method: key.toUpperCase(),
							url
						}, options)
					);
				};

			});

			return api;
		}])
		.name;
