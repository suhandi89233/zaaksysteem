import angular from 'angular';
import pull from 'lodash/pull';
import invoke from 'lodash/invokeMap';
import assign from 'lodash/assign';
import propCheck from './../../util/propCheck';
import immutable from 'seamless-immutable';
import isEqual from 'lodash/isEqual';
import ApiResource from './ApiResource.js';
import apiModule from '../';
import resourceReducerModule from './resourceReducer';
import mutationServiceModule from './mutationService';
import resourceCacheModule from './resourceCache';
import listenerFn from './../../util/listenerFn';
import omit from 'lodash/omit';

export default
	angular.module('shared.api.resource', [
			apiModule,
			resourceReducerModule,
			mutationServiceModule,
			resourceCacheModule
		])
		.provider('resource', [ 'resourceReducerProvider', ( resourceReducerProvider ) => {

			let getRequestOpts = ( request ) => {
				let opts = request;
				
				if (typeof request === 'string') {
					opts = {
						url: request
					};
				}

				return opts;
			};

			return {
				$get: [ 'api', 'resourceReducer', 'resourceCache', 'mutationService', ( api, resourceReducer, resourceCache, mutationService ) => {

					let factory = ( request, resourceOptions ) => {

						propCheck.throw(
							propCheck.shape({
								request: propCheck.oneOfType([
									propCheck.object.nullOk,
									propCheck.string,
									propCheck.func
								]).optional,
								resource: propCheck.shape({
									scope: propCheck.object,
									cache: propCheck.shape({
										since: propCheck.number.optional
									}).optional,
									reduce: propCheck.shape({
										stateful: propCheck.bool.optional
									}).optional
								})
							}),
							{
								request,
								resource: {
									// passing scope to apiCheck might cause memory leaks
									scope: resourceOptions.scope ? { } : null, //eslint-disable-line
									cache: resourceOptions.cache,
									reduce: resourceOptions.reduce
								}
							}
						);

						let listeners = [],
							{ scope } = resourceOptions,
							destroyListener,
							resource = new ApiResource(),
							reducer,
							cache,
							requestOptions,
							onDestroy = [],
							onError = listenerFn();

						let invokeUpdateListeners = ( ) => {
							invoke(listeners, 'call', null, resource.data());
						};

						let reset = ( ) => {

							if (!isEqual(reducer.data(), reducer.run())) {
								reducer.resetCache();
								invokeUpdateListeners();
							}

						};

						let setSource = ( source ) => {

							let data = reducer.data();

							reducer.$source = immutable(source);

							if (!isEqual(data, reducer.run())) {
								reducer.resetCache();
								invokeUpdateListeners();
							}
						};

						let onMutationChange = ( ) => {

							reset();
							
						};

						let destroy = ( ) => {
							
							pull(mutationService.onAdd, onMutationChange);
							pull(mutationService.onRemove, onMutationChange);

							invoke(onDestroy, 'call', null);

							cache.destroy();
							destroyListener();
						};

						assign(resource,
							{
								state: ( ) => cache.state(),
								loading: ( ) => cache.state() === 'pending',
								destroy,
								onUpdate: ( fn ) => {

									listeners.push(fn);
									
									fn(resource.data());

									return ( ) => {
										pull(listeners, fn);
									};
									
								},
								mutate: ( type, data ) => {
									return mutationService.add({ type, data, request: requestOptions });
								},
								request: ( ...rest ) => {

									if (!rest.length) {
										return requestOptions;
									}

									let nwRequestOptions = reducer.merge(getRequestOpts(rest[0]));

									if (!angular.equals(requestOptions, nwRequestOptions)) {

										requestOptions = nwRequestOptions;

										reducer.setArgs(requestOptions);
										cache.setRequestOptions(requestOptions);
										
									}

								},
								mutations: ( ) => mutationService.filter(requestOptions),
								invalidate: ( ) => {
									reset();
								},
								onDestroy: ( fn ) => {

									onDestroy.push(fn);

									return ( ) => {
										pull(onDestroy, fn);
									};
								},
								asPromise: ( ) => {
									return cache.asPromise()
										.then( ( ) => {
											return resource.data();
										});
								},
								reload: ( ) => cache.reload(),
								onError: ( ...rest ) => {
									
									onError.register(...rest);

									return reducer;
								},
								fetching: ( ) => cache.fetching(),
								onStateChange: ( fn ) => cache.onStateChange(fn),
								source: ( ) => reducer.$source
							}
						);

						reducer = resourceReducer({ reduce: resourceOptions.reduce, args: [ null ], thisObj: resource });

						assign(resource, omit(reducer, 'resetCache', 'onError'));

						cache = resourceCache(null, resourceOptions.cache);
							
						mutationService.onAdd.push(onMutationChange);
						mutationService.onRemove.push(onMutationChange);
						
						destroyListener = scope.$on('$destroy', destroy);

						cache.onUpdate(( ) => {
							setSource(cache.data());
						});

						cache.onError(err => {
							onError.invoke(err);
						});

						reducer.onError(err => {
							onError.invoke(err);
						});

						reducer.reduce(( opts, data ) => {
							return opts ? mutationService.exec(opts, data) : data;
						});

						if (typeof request === 'function') {
							onDestroy.push(
								scope.$watch(request, ( result ) => {
									resource.request(result);
								}, true)
							);
							resource.request(request());
						} else {
							resource.request(request);
						}

						return resource;

					};
					
					return factory;
				}],
				configure: resourceReducerProvider.configure
			};
		}])
		.name;
