import BaseReducer from './BaseReducer';

class ComposedReducer extends BaseReducer {

	constructor ( options, children ) {

		super(options);

		this.children = children;

		children.forEach(child => {

			child.onInvalidate(( ) => {
				this.$onChildInvalidate();
			});

			child.onStateChange(( ) => {
				this.$onChildStateChange();
			});

		});

		this.$state = this.$getStateFromChildren();

		this.invalidate();
	}

	$setChildSources ( ) {
		this.setSrc(...this.children.map(child => child.value()));
	}

	$onChildInvalidate ( ) {
		this.invalidate();
	}

	$onChildStateChange ( ) {
		this.$setState(this.$getStateFromChildren());
	}

	recalculate ( ) {

		if (this.$invalidated) {

			if (this.$state === 'resolved') {

				this.$setChildSources();

				super.recalculate();

			}
		}

	}

	$getStateFromChildren ( ) {

		let eager = this.$options.eager,
			states = this.children.map(child => child.state());

		if (states.indexOf('rejected') !== -1) {
			return 'rejected';
		} else if (eager && states.indexOf('resolved') !== -1) {
			return 'resolved';
		} else if (states.indexOf('pending') !== -1) {
			return 'pending';
		}

		return 'resolved';

	}

}

export default ComposedReducer;
