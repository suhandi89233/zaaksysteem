import BaseReducer from './BaseReducer';

class AsyncReducer extends BaseReducer {

	constructor ( options ) {
		super(options);
	}

	$resolve ( value ) {

		if (this.$destroyed) {
			return;
		}
		
		this.$setState('resolved');
		
		this.setSrc(value);
	}
	
	$reject ( err ) {

		if (this.$destroyed) {
			return;
		}

		this.$setState('rejected');

		this.$onError.invoke(err);

	}
}

export default AsyncReducer;
