import AsyncReducer from './AsyncReducer';

class PromiseReducer extends AsyncReducer {

	constructor ( ...rest ) {

		let options,
			promise;

		if (rest.length === 1) {
			options = {};
			promise = rest[0];
		} else {
			options = rest[0];
			promise = rest[1];
		}

		super(options);

		promise.then(( value ) => {
			this.$resolve(value);
		})
			.catch(( err ) => {
				this.$reject(err);
			});

	}

}

export default PromiseReducer;
