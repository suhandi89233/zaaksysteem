import angular from 'angular';
import zsTableModule from './../../../../ui/zsTable';
import composedReducerModule from './../../../../api/resource/composedReducer';
import snackbarServiceModule from './../../../../ui/zsSnackbar/snackbarService';
import zsConfirmModule from './../../../../ui/zsConfirm';
import assign from 'lodash/assign';
import find from 'lodash/find';
import range from 'lodash/range';
import identity from 'lodash/identity';
import get from 'lodash/get';
import person from './../person';
import company from './../company';
import template from './template.html';
import './styles.scss';

function pickMessageFromSubjectInstance(subject) {

  let messages = [];
  if (subject.date_of_death) {
    messages.push('Contact is overleden');
  }
  if (subject.is_secret) {
    messages.push('Contact heeft geheimhouding');
  }
  if (!subject.address_residence && subject.address_correspondence) {
    messages.push('Contact heeft correspondentie-adres');
  }
  return messages.join('\n');
}

export default
	angular.module('zsSuggestionSubjectList', [
		zsTableModule,
		composedReducerModule,
		snackbarServiceModule,
		zsConfirmModule
	])
		.directive('zsSuggestionSubjectList', [
				'$q', '$interpolate', '$http', 'composedReducer', 'snackbarService', 'zsConfirm',
				( $q, $interpolate, $http, composedReducer, snackbarService, zsConfirm ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					results: '&',
					type: '&',
					onSelect: '&',
					remoteInterfaceId: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						columnReducer,
						rowReducer;

					columnReducer = composedReducer( { scope }, ctrl.type)
						.reduce( type => {

							let columns = type === 'natuurlijk_persoon' ?
									person().columns
									: company().columns;

							columns = columns.map(
								col => {
									const valuePath = `instance.subject.instance.${col.resolve || col.id }`;
									return assign(
										{
											valuePath,
											template: `<span>{{::item.${valuePath}}}</span>`
										},
										col
									);

								}
							);

							if ($interpolate.startSymbol() !== '{{') {
								columns = columns.map(
									col => {
										return assign(
											{ },
											col,
											{
												template:
													col.template.replace(/{{/g, $interpolate.startSymbol())
														.replace(/}}/g, $interpolate.endSymbol())
											}
										);
									}
								);
							}
								

							return columns;

						});
						
					rowReducer = composedReducer( { scope }, ctrl.results)
						.reduce( results => {
							return (results || []).map(
								result => {

									const addressObj = result.instance.subject.instance.address_residence
											|| result.instance.subject.instance.address_correspondence;
									let address = '-';

									if(addressObj) {
										address = addressObj.instance.foreign_address_line1 ?
											range(3).map(
												pt => addressObj.instance[`foreign_address_line${pt}`]
											)
												.filter(identity)
												.concat(addressObj.instance.country.instance.label)
												.join(', ')
											: [
												[ 'street' ],
												[ 'street_number', 'street_number_letter', 'street_number_suffix' ],
												[ 'zipcode' ],
												[ 'city' ]
											].map(keys => {
												return keys.map(
													key => addressObj.instance[key]
												)
													.filter(identity)
													.join('-');
											})
												.filter(identity)
												.join(', ');
									}

									const messages = pickMessageFromSubjectInstance(result.instance.subject.instance);

									return result.merge({
										address,
                    messages,
									});
								}
							);

						});

					ctrl.getRows = rowReducer.data;

					ctrl.getColumns = columnReducer.data;

					ctrl.handleRowClick = ( item ) => {

						let promise,
							object = find(ctrl.results(), { reference: item.reference }),
							displayName = get(object, 'instance.display_name');

						if (ctrl.remoteInterfaceId()) {

							promise = zsConfirm(`Weet u zeker dat u ${displayName} wilt importeren?`, 'Bevestigen')
								.then(( ) => {

									return snackbarService.wait(
										`${displayName} wordt geimporteerd`,
										{
											promise: $http({
												method: 'POST',
												url: `/api/v1/subject/remote_import/${ctrl.remoteInterfaceId()}`,
												data: object
											}),
											catch: ( ) => `${displayName} kon niet worden geimporteerd. Neem contact op met uw beheerder voor meer informatie.`
										}
									);
								})
								.then(resp => {
									return resp.data.result;
								});

						} else {
							promise = $q.when(object);
						}

						promise.then( obj => {

							let identifier = obj.instance.old_subject_identifier.split('-'),
								type = identifier[1],
								id = identifier[2],
								uuid = obj.reference;

							ctrl.onSelect({
								$object: {
									type,
									data: {
										type,
										id,
										uuid
									},
									label: obj.instance.display_name
								}
							});

						});

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
