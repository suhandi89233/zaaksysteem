import some from 'lodash/some';
import isEqual from 'lodash/isEqual';
import isArray from 'lodash/isArray';
import includes from 'lodash/includes';
import get from 'lodash/get';

export default
	( condition, values = {}, hidden = {}) => {

		let attrName = condition.attribute_name,
			attrValue = hidden[attrName] ? null : get(values, attrName, null),
			matches = false,
			isValArray = isArray(attrValue);

		if (condition.validates_true !== undefined) {
			matches = !!condition.validates_true;
		} else {
			matches = some(condition.values, val => {
				return isValArray ?
					attrValue.length === 0 && val === null ? true : includes(attrValue, val)
					: isEqual(val, attrValue);
			});
		}

		return matches;

	};
