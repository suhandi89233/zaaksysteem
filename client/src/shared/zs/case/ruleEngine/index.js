import executeRule from './executeRule';
import each from 'lodash/each';
import propCheck from './../../../util/propCheck';

export default
	( rules, values, options = { debug: false }) => {

		propCheck.throw(
			propCheck.shape({
				debug: propCheck.bool.optional
			}),
			options
		);

		let engineResult,
			initials;

		initials = { values, hidden: {}, disabled: {}, hiddenByGroup: {}, hiddenByAttribute: {} };

		if (options.debug) {
			initials.debug = {};
		}

		engineResult = rules.reduce(
			( result, rule ) => executeRule(rule, result, { debug: options.debug }),
			initials
		);

		each(engineResult.hidden, ( value, key ) => {
			if (value) {
				engineResult.values[key] = null;
			}
		});

		return engineResult;

	};
