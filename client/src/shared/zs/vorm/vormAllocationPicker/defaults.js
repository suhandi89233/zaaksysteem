export default {
  me: {
    data: {
      changeDept: true,
    }
  },
  coworker: {
    data: {
      changeDept: true,
      informAssignee: true
    }
  }
};
