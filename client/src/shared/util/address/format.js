import escapeString from 'lodash/escape';
import isEmpty from '../../../shared/vorm/util/isEmpty';

const zipcodeExpression = /^([0-9]{4}) ?([A-Z]{2})$/;

export const NBSP = '\u00A0';

export function formatZipcode(input) {
	const match = zipcodeExpression.exec(input);

	if (match) {
		return `${match[1]}${NBSP}${match[2]}`;
	}

	return input;
}

/**
 * @param {Array} foreign
 * @param {Object} local
 * @property {string|null} local.street
 * @property {string|null} local.street_number
 * @property {string|null} local.street_number_letter
 * @property {string|null} local.street_number_suffix
 * @property {string|null} local.zipcode
 * @property {string|null} local.city
 * @return {Function}
 */
export const addressRowFactory = ( foreign, local ) =>
	function getAddressRows(object) {
		// heads up: local address data is currently not cleaned up when residence is changed to foreign
		if (isEmpty(object[foreign[0]])) {
			const get = key => escapeString(object[local[key]]);
			const zipcode = formatZipcode(get('zipcode'));
			const result = [
				[
					get('street'),
					get('street_number'),
					get('street_number_letter'),
					get('street_number_suffix')
				]
					.filter(partial => !isEmpty(partial))
					.join(' '),
				`${zipcode}<span class="preformatted">  </span>${get('city')}`
			];

			if (local.hasOwnProperty('country')) {
				result.push(get('country').instance.label);
			}

			return result;
		}

		return foreign
			.map(key => {
				if (key === 'country') {
					return object[key].instance.label;
				}

				return object[key];
			})
			.filter(value => !isEmpty(value));
	};
