import angular from 'angular';
import seamlessImmutable from 'seamless-immutable';
import invoke from 'lodash/invokeMap';
import find from 'lodash/find';
import identity from 'lodash/identity';
import defaultMessage from './defaultMessage';

export default angular
	.module('windowUnload', [])
	.factory('windowUnload', [
		'$rootScope', '$window',
		( $rootScope, $window ) => {
			let handlers = seamlessImmutable([]);
			let msg;

			$window.addEventListener('beforeunload', event => {
				msg = find(
					invoke(handlers, 'call', null),
					identity
				);

				if (msg === true) {
					msg = defaultMessage;
				}

				if (msg) {
					event.returnValue = msg;
					$rootScope.$apply();
				}

				return msg;
			});

			return {
				register: ( scope, handler ) => {
					handlers = handlers.concat(handler);

					scope.$on('$destroy', () => {
						handlers = handlers.filter(h => h !== handler);
					});
				}
			};

		}
	])
	.name;
