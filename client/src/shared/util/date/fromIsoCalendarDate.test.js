import toIsoCalendarDate from './toIsoCalendarDate';
import fromIsoCalendarDate from './fromIsoCalendarDate';

/**
 * @test {fromIsoCalendarDate}
 */
describe('The `fromIsoCalendarDate` function', () => {
	test('returns a `Date` object', () => {
		const date = fromIsoCalendarDate('2000-01-01');

		expect(date).toBeInstanceOf(Date);
	});

	test('is complementary to the `toIsoCalendarDate` function', () => {
		const dateString = '2000-01-01';
		const dateObject = fromIsoCalendarDate(dateString);

		expect(toIsoCalendarDate(dateObject)).toBe(dateString);
	});
});
