/**
 * @param {Date} referenceDate
 * @param {number} offset
 * @return {Date}
 */
export default function shiftMonth(referenceDate, offset) {
	const date = new Date(Number(referenceDate));
	const day = date.getDate();

	date.setMonth(date.getMonth() + offset);

	if (date.getDate() !== day) {
		date.setDate(0);
	}

	return date;
}
