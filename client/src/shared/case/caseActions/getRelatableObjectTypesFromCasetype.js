import uniq from 'lodash/uniq';
import flatten from 'lodash/flatten';
import get from 'lodash/get';
import map from 'lodash/map';

export default ( casetype ) => {

	return uniq(
		flatten(
			map(get(casetype, 'instance.phases'), 'fields')
		).filter(field => field.type === 'object' && field.object_metadata.relate_object)
	).map(
		field => {

			return {
				value: field.object_type_prefix,
				label: field.label
			};
		}
	);

};
