import get from 'lodash/get';

/* getAllocationRoute(vals, user);
 *
 * Returns a Hash or null value. The group_id and role_id as keys
 *
 */
const getAllocationRoute = (allocation, user) => {
  if (allocation === undefined) {
    return null;
  }

  const allocationType = get(allocation, 'type');

  if (allocationType === 'me') {
    if (get(allocation, 'data.changeDept') === true) {
      const role = find(user.legacy.positions, el => {
        return (
          get(el, 'instance.role.instance.system_role') === true &&
          get(el, 'instance.role.instance.name').toLowerCase() === 'behandelaar'
        );
      });

      if (!role || !user.legacy.ou_id) {
        return null;
      }

      return {
        group_id: Number(user.legacy.ou_id),
        role_id: Number(get(role, 'instance.role.instance.role_id'))
      };
    }
  } else if (allocationType === 'org-unit') {
    return {
      group_id: Number(allocation.data.unit),
      role_id: Number(allocation.data.role)
    };
  } else if (allocationType === 'coworker') {
    if (get(allocation, 'data.changeDept') === true) {
      return {
        group_id: Number(allocation.data.route),
        role_id: Number(allocation.data.role)
      };
    }
  }
  return null;
};

export default getAllocationRoute;
