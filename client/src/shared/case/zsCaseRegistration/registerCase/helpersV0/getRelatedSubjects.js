import map from 'lodash/map';
import identity from 'lodash/identity';
import getRoleValue from '../../../../../intern/views/case/zsCaseAddSubject/getRoleValue';

const typeDictionary = {
  natuurlijk_persoon: 'person',
  bedrijf: 'company'
};

const getRelatedSubjects = (relatedSubjects) =>
  map(
    relatedSubjects,
    subject => {
      const {
        related_subject,
        pip_authorized,
        notify_subject,
        magic_string_prefix
      } = subject;

      return {
        subject: {
          type: typeDictionary[related_subject.type],
          reference: related_subject.data.uuid
        },
        role: getRoleValue(subject),
        magic_string_prefix,
        pip_authorized,
        send_auth_notification: notify_subject,
      };
    }
  ).filter(identity);

export default getRelatedSubjects;
