const formatAsApiSubject = recipient => {
  if(!recipient) {
    return;
  }

  return {
    type: 'subject',
    reference: recipient.reference,
  };
};

export default formatAsApiSubject;
