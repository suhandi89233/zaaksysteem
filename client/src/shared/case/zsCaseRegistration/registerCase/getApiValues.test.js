import getApiValues from './getApiValues';

const valutaTypes = [
  'valuta',
  'valutaex',
  'valutaex21',
  'valutaex6',
  'valutain',
  'valutain21',
  'valutain6'
];

describe('The getApiValues function', () => {
  test('casts the numeric type to number', () => {
    const { answer } = getApiValues({
      answer: '42'
    }, {
      answer: {
        $attribute: {
          type: 'numeric'
        },
        type: 'numeric'
      }
    });

    expect(answer[0]).toBe(42);
  });

  test('casts valuta types to numbers', () => {
    valutaTypes
      .forEach(type => {
        const { answer } = getApiValues({
          answer: '0,42'
        }, {
          answer: {
            $attribute: {
              type
            },
            type
          }
        });

        expect(answer[0]).toBe(0.42);
      });
  });
});
