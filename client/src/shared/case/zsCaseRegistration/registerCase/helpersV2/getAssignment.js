const getDepartmentRole = (data, allocationData) => {
  const departmentData = allocationData.find(department => department.org_unit_id === data.unit);
  const roleData = departmentData.roles.find(role => role.role_id === data.role);

  return {
    department: {
      type: 'department',
      id: departmentData.org_unit_uuid,
    },
    role: {
      type: 'role',
      id: roleData.role_uuid,
    }
  };
};

const getEmployee = (data, user, type) => {
  const id = type === 'coworker' ? data.uuid : user.uuid;

  return {
    employee: {
      type: 'employee',
      id,
      use_employee_department: data.changeDept,
      send_email_notification: data.informAssignee
    }
  };
};

// grab a coffee, put on some soothing music, and read the following

// possible scenarios on form, followed by what 'allocation' is, followed by what the desired output should be:
// - no assignment options present              --> undefined                               --> undefined
// - only 'assignment to self'-option present:
//   - assign to 'me' checked                   --> { type: 'me', data: { me: true }        --> { employee: { id: user.id }}
//   - assign to 'me', manually unchecked       --> { type: 'me', data: { me: false }       --> undefined
// - all assignment options present:
//   - assign to 'me' (as default)              --> { type: 'me', data: { me: true }        --> { employee: { id: user.id }}
//   - assign to 'me' (manually chosen)         --> { type: 'me', data: { me: undefined }   --> { employee: { id: user.id }}
//   - assign to 'coworker'                     --> { type: 'coworker, data: ... }          --> { employee: { id: data.id }}
//   - assign to 'org-unit'                     --> { type: 'org-unit, data: ... }          --> { department: { id: org_unit.id }, role: { id: role.id } }
const getAssignment = (allocation, user, allocationData) => {
  if(!allocation) {
    return;
  }

  const { type, data } = allocation;
  // when type='me' and data.me is 'true' or 'undefined' it should assign to the user
  if(type === 'me' && data.me === false) {
    return;
  }

  return type === 'org-unit'
    ? getDepartmentRole(data, allocationData)
    : getEmployee(data, user, type);
};

export default getAssignment;
