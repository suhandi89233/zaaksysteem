import uniq from 'lodash/uniq';
import toArray from 'lodash/toArray';

const unsupportedAttributeTypes = [
  'appointment',
  'calendar',
  'calendar_supersaas',
  'bag_adres',
  'bag_adressen',
  'bag_straat_adres',
  'bag_straat_adressen',
  'bag_openbareruimte',
  'bag_openbareruimtes',
  'file',
  'geolatlon',
  'googlemaps',
  'subject'
];

// we only submit attributes with a value, therefore we only check those
// 'fieldDefinitions' contains the types, and vals only contains attributes with values
// so the intersect lets us know whether the form has unsupported attributes with a value
const getAttributeTypeEligibility = (fieldDefinitions, vals) => {
  const namesWithValue = Object.keys(vals).filter(val => val[0] !== '$');
  const definitions = toArray(fieldDefinitions);

  const definitionsWithValue =
    namesWithValue.map(name => definitions.find(definition => name === definition.name));

  const definitionsWithValueTypes =
    uniq(definitionsWithValue.map(attr => attr.type));

  const hasUnsupportedAttributeTypesWithValues =
    definitionsWithValueTypes.every(type => !unsupportedAttributeTypes.includes(type));

  return hasUnsupportedAttributeTypesWithValues;
};

export default getAttributeTypeEligibility;
