import angular from 'angular';
import angularCookies from 'angular-cookies';
import zsTooltipModule from './../../../../shared/ui/zsTooltip';
import zsIconModule from './../../../../shared/ui/zsIcon';
import zsDropdownMenuModule from './../../../../shared/ui/zsDropdownMenu';
import resourceModule from './../../../../shared/api/resource';
import tpl from '../../../../../../frontend/zaaksysteem/src/html/docs/docs.swig';
import caseRelatedCasesModule from './../../../../shared/case/caseRelatedCases';
import messageBoxModule from './../../../../shared/case/sendMessage/messageBoxService';
import _ from 'lodash';
import first from 'lodash/head';
import get from 'lodash/get';
import './documents.scss';

window._ = _;

let testsContext;

require('../../../../../../frontend/zaaksysteem/src/js/_namespace.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/object/inherit.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/object/clone.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromLocalToGlobal.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromGlobalToLocal.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportPosition.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportSize.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/setMouseEnabled.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/intersects.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/contains.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/indexOf.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/trim.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getDocumentPosition.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getMousePosition.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/addEventListener.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/removeEventListener.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/cancelEvent.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/generateUid.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/generateUuid.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/safeApply.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/_filters.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/size.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/form/_form.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/form/formValidatorService.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/_directives.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngEditable.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngDroppable.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngDraggable.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngUpload.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsConfirm.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsModal.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPopup.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsStyle.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsTitle.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsContextmenu.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsHoverMenu.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDropdownMenu.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPlaceholder.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSpotEnlighter.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsTemplate.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDateRangePicker.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDatePickerModel.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsModelTransformers.js');


require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/events/_events.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/events', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/locale/_locale.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/locale', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/net/_net.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/net', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/dom/_dom.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/dom', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/message/_message.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/message', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/user/_user.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/user', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/data/_data.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/data', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/docs/_docs.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/docs', true, /^[^_]+\.js$/);

testsContext.keys()
	.forEach(testsContext);

export default
	angular.module('zsCaseDocumentView', [
		angularCookies,
		zsTooltipModule,
		zsDropdownMenuModule,
		resourceModule,
		zsIconModule,
		caseRelatedCasesModule,
		messageBoxModule
	])
		.directive('zsCaseDocumentView', [
			'$q', '$window', '$http', '$compile', '$templateCache', '$interpolate', '$ocLazyLoad', '$cookies', 'resource', '$stateParams',
			( $q, $window, $http, $compile, $templateCache, $interpolate, $ocLazyLoad, $cookies, resource, $stateParams ) => {

			let legacyTpl =
				tpl.replace(/<\[/g, $interpolate.startSymbol())
					.replace(/\]>/g, $interpolate.endSymbol())
					.replace(/%%(.*?)%%/g, '$1');

			$templateCache.put('/html/docs/docs.html', legacyTpl);

			return {
				restrict: 'E',
				scope: {
					case: '&',
					caseId: '&',
					caseUuid: '&',
					userId: '&',
					readOnly: '&',
					canUpload: '&',
					canDelete: '&',
					onFileAccept: '&',
					onFileReject: '&',
					onFileUpdate: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						docScope = scope.$new(true);

					docScope.readOnly = false;
					docScope.pip = false;
					docScope.userId = ctrl.userId();
					docScope.case = ctrl.case();
					docScope.caseId = ctrl.caseId();
					docScope.caseUuid = ctrl.caseUuid();
					docScope.highlightedId = $stateParams.documentId;
					docScope.externalDocumentGenerator = null;
					docScope.externalStufGenerator = null;

					scope.$watch(( ) => ctrl.readOnly(), ( readOnly ) => {
						docScope.readOnly = readOnly;
						docScope.canDelete = ctrl.canDelete();
					});

					scope.$watch(( ) => ctrl.canUpload(), canUpload => {
						docScope.canUpload = canUpload;
					});

					$window.getXSRFToken = ( ) => $cookies.get('XSRF-TOKEN');

					docScope.$on('file.accept', ( ) => {
						ctrl.onFileAccept();
					});

					docScope.$on('file.reject', ( ) => {
						ctrl.onFileReject();
					});

					docScope.$on('file.update', ( ) => {
						ctrl.onFileUpdate();
					});

					docScope.setNotificationCount = ( ) => {

					};

					$q.all([
						$ocLazyLoad.load(
							'events net dom docs'
								.split(' ')
								.map(name => {
									return {
										name: `Zaaksysteem.${name}`
									};
								})
						),
						resource('/api/v1/sysin/interface/get_by_module_name/xential', { scope }).asPromise(),
						resource('/api/v1/sysin/interface/get_by_module_name/stuf_dcr', { scope }).asPromise()
					]).then( ( args ) => {

						let xentialModule = first(args[1]);
						let stufDcrModule = first(args[2]);

						docScope.externalDocumentGenerator = get(xentialModule, 'instance.module');
						docScope.externalStufGenerator = get(stufDcrModule, 'instance.module');

						$compile(
							`<div>
								<div
									ng-include="'/html/docs/docs.html'"
									ng-controller="nl.mintlab.docs.DocumentController"
									ng-init="init()" class="document-module">
									<h2>Documenten</h2>
								</div>
							</div>`
						)(docScope, ( el ) => {
							element.append(el);
						});

					})
					.catch( ( err ) => {

						console.log('error loading file', err);

					});
					

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
