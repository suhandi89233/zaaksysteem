import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import assign from 'lodash/assign';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import extendedTemplate from './extendedTemplate.html';
import first from 'lodash/head';
import get from 'lodash/get';
import identity from 'lodash/identity';
import last from 'lodash/last';
import popupTemplate from './../../../../shared/ui/zsMap/popup-template.html';
import shortid from 'shortid';
import template from './template.html';
import zsMapModule from  './../../../../shared/ui/zsMap';
import zsMapSearchModule from  './../../../..//shared/ui/zsMap/zsMapSearch';
import zsModalModule from './../../../../shared/ui/zsModal';
import zsSessionServiceModule from './../../../../shared/user/sessionService';
import './styles.scss';

const TYPES = [
  {
    key: 'nummeraanduiding',
    label: 'BAG Nummeraanduiding'
  },
  {
    key: 'verblijfsobject',
    label: 'BAG Verblijfsobject'
  },
  {
    key: 'pand',
    label: 'BAG Pand'
  },
  {
    key: 'standplaats',
    label: 'BAG Standplaats'
  },
  {
    key: 'ligplaats',
    label: 'BAG Ligplaats'
  },
  {
    key: 'openbareruimte',
    label: 'BAG Openbare ruimte'
  },
  {
    key: 'woonplaats',
    label: 'BAG Woonplaats'
  }
];

const FIELDS = [
  {
    key: 'human_identifier',
    label: 'Locatie'
  },
  {
    key: 'bag_id',
    label: 'Identificatie',
    format: ( value ) => last(value.split('-'))
  },
  {
    key: 'address_data.surface_area',
    label: 'Oppervlakte',
    format: ( value ) => `${value} m²`
  },
  {
    key: 'address_data.gebruiksdoel',
    label: 'Gebruiksdoel',
    format: ( value ) => value.join(', ')
  },
  {
    key: 'address_data.straat',
    label: 'Straat'
  },
  {
    key: 'address_data.huisletter',
    label: 'Huisletter'
  },
  {
    key: 'address_data.huisnummer',
    label: 'Huisnummer'
  },
  {
    key: 'address_data.huisnummertoevoeging',
    label: 'Huisnummertoevoeging'
  },
  {
    key: 'address_data.postcode',
    label: 'Postcode'
  },
  {
    key: 'address_data.woonplaats',
    label: 'Woonplaats'
  },
  {
    key: 'address_data.usage',
    label: 'Gebruik'
  },
  {
    key: 'address_data.year_of_construction',
    label: 'Bouwjaar'
  },
  {
    key: 'address_data.status',
    label: 'Status'
  },
  {
    key: 'address_data.under_investigation',
    label: 'In onderzoek'
  },
  {
    key: 'address_data.start_date',
    label: 'Begindatum'
  },
  {
    key: 'address_data.end_date',
    label: 'Einddatum'
  },
  {
    key: 'address_data.document_number',
    label: 'Documentnummer'
  },
  {
    key: 'address_data.document_date',
    label: 'Documentdatum'
  },
  {
    key: 'address_data.brk.identification_number',
    label: 'Identificatienummer'
  },
  {
    key: 'address_data.brk.parcel_number',
    label: 'Perceelnummer'
  },

];

function _randomize(num) {
  return Math.floor(Math.random() * num);
}

/* caseObjectToLocation
 *
 * @caseObject - Case object instance information
 * @brkEnabled - Boolean value to check if brk_demo is enabled
 */

function caseObjectToLocation(caseObject, brkEnabled) {
  if (caseObject.location === undefined) {
    return null;
  }

  let hasLocation;
  let locations = TYPES.map(
    type => {
      let location = caseObject.location.instance[type.key];

      if (location) {
        hasLocation = true;
        return assign({ location }, type);
      }
      return null;
    }
  ).filter(identity);

  if (brkEnabled && hasLocation === true) {
    locations.push({
      'label': 'BRK informatie',
      'key': 'brk',
      'location' : {
        'address_data' : {
          'brk' : {
            'identification_number' : caseObject.number * 2,
            'parcel_number' : _randomize(caseObject.number),
          },
          'surface_area' : _randomize(5000),
        },
      },
    });
  }
  return locations;
}

/* getLocationInformation
 *
 * @locations - array with location data
 */
function getLocationInformation(locations) {
  return locations.map(
    location => {
      return {
        id: shortid(),
        label: location.label,
        fields: FIELDS.map(
          field => {

            let value = get(location.location, field.key);
            if (!value && field.key.substring(0,3) === 'brk' ) {
              value = get(location, field.key);
              if (!value) {
                return null;
              }
            }
            else if (!value) {
              return null;
            }

            return {
              label: field.label,
              value: field.format ? field.format(value) : value
            };
          }
        ).filter(identity)
      };
    }
  ).filter(location => location.fields.length);
}

export default
	angular.module('zsCaseMapView', [
		angularUiRouter,
		zsMapModule,
		zsMapSearchModule,
		composedReducerModule,
		zsSessionServiceModule,
		zsModalModule
	])
		.directive('zsCaseMapView', [
				'$compile', '$state', '$rootScope', 'composedReducer', 'sessionService', 'zsModal',
				( $compile, $state, $rootScope, composedReducer, sessionService, zsModal ) => {


			let userResource = sessionService.createResource($rootScope, { cache: { every: 15 * 60 * 1000 } });
			const brkEnabled = userResource.data().instance.configurable.brk_demo || false;

			return {
				restrict: 'E',
				template,
				scope: {
					caseResource: '&',
					showMore: '&'
				},
				bindToController: true,
				controller: [ '$rootScope', '$scope', function ( $rootScope, scope ) {

					let ctrl = this;
					const locationReducer = composedReducer( { scope },
						ctrl.caseResource()) .reduce(( caseObj ) => {
							return caseObjectToLocation(caseObj.instance, brkEnabled);
						});

					const extendedReducer = composedReducer( { scope }, locationReducer)
						.reduce(( locations ) => {
							return getLocationInformation(locations);
						});

					let markerTemplate = angular.element(popupTemplate);
					let center = null;

					angular.element(markerTemplate[0].querySelector('.map-popup-body')).append(
						`<a href="${$state.href('case.location', { more: 'meer' }, { inherit: true })}" class="btn btn-flat">Meer informatie</a>`
					);

					const markerReducer = composedReducer(
						{ scope }, locationReducer).reduce( locations => {

						let location = get(first(locations), 'location'),
						markers,
						keys = [
							{
								name: 'woonplaats',
								label: 'Woonplaats'
							},
							{
								name: 'postcode',
								label: 'Postcode'
							},
							{
								name: 'straat',
								label: 'Straat'
							}
						];

					const lat_lon = get(location, 'address_data.gps_lat_lon', '');
					if (!lat_lon) {
						return;
					}

					markers = location ?
						[
							{
								coordinates: {
									lat: lat_lon.replace('(', '').split(',')[0],
									lng: lat_lon.replace(')', '').split(',')[1]
								},
								title: location.human_identifier,
								description:
									keys.map(
										key => {
											let value = get(location.address_data, key.name);

											return value ?
												`${key.label}: ${value}`
												: '';
										}
									)
										.filter(identity)
										.join('<br/>'),
								template: markerTemplate

							}
						]
						: [];

					return markers;

					});

					ctrl.getMarkers = markerReducer.data;

					ctrl.getLocations = extendedReducer.data;

					ctrl.getCenter = ( ) => center;

					ctrl.openModal = ( ) => {
	
						let modalScope = scope.$new(),
							modal = zsModal({
								title: 'Uitgebreide adresinformatie',
								el: $compile(extendedTemplate)(modalScope)
							});

						let close = ( ) => {
							modal.close();
							modalScope.$destroy();
						};

						modal.open();

						modal.onClose(( ) => {
							$state.go('case.location', { more: null }, { inherit: true });
							return false;
						});

						$rootScope.$on('$stateChangeSuccess', ( ) => {
							close();
						});
						
					};

					scope.$watch(( ) => ctrl.showMore(), more => {

						if (more) {
							ctrl.openModal();
						}

					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
