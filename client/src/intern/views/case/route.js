import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import assign from 'lodash/assign';
import get from 'lodash/get';
import find from 'lodash/find';
import first from 'lodash/head';
import keyBy from 'lodash/keyBy';
import keys from 'lodash/keys';
import mapValues from 'lodash/mapValues';
import identity from 'lodash/identity';
import flatten from 'lodash/flatten';
import _values from 'lodash/values';
import uniq from 'lodash/uniq';
import map from 'lodash/map';
import pickBy from 'lodash/pickBy';
import seamlessImmutable from 'seamless-immutable';

import auxiliaryRouteModule from './../../../shared/util/route/auxiliaryRoute';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import observableStateParamsModule from './../../../shared/util/route/observableStateParams';
import onRouteActivateModule from './../../../shared/util/route/onRouteActivate';
import resourceModule from './../../../shared/api/resource';
import routeRelationProgressModule from './routeRelationProgress';
import shouldReloadModule from './../../../shared/util/route/shouldReload';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import zsModalModule from './../../../shared/ui/zsModal';
import zsDisabledModule from './../../../shared/ui/zsDisabled';
import zsTrapKeyboardFocus from './../../../shared/ui/zsTrapKeyboardFocus';
import zsMoveFocusToModule from './../../../shared/ui/zsMoveFocusTo';
import assignToScope from './../../../shared/util/route/assignToScope';
import convertv0Case from './../../../shared/api/mock/convertv0Case';
import getRelatableObjectTypesFromCasetype from './../../../shared/case/caseActions/getRelatableObjectTypesFromCasetype';
import normalizePhaseName from './zsCaseView/zsCasePhaseView/normalizePhaseName';

import template from './template.html';

export default angular
  .module('Zaaksysteem.intern.case.route', [
    angularUiRouterModule,
    auxiliaryRouteModule,
    composedReducerModule,
    observableStateParamsModule,
    ocLazyLoadModule,
    onRouteActivateModule,
    resourceModule,
    routeRelationProgressModule,
    shouldReloadModule,
    snackbarServiceModule,
    zsModalModule,
    zsDisabledModule,
    zsMoveFocusToModule,
    zsTrapKeyboardFocus,
  ])
  .config([
    '$stateProvider', '$urlMatcherFactoryProvider', 'RELATION_TAB_URL',
    ($stateProvider, $urlMatcherFactoryProvider, RELATION_TAB_URL) => {
      $urlMatcherFactoryProvider.strictMode(false);

      const getLegacyErrorMessage = distinct =>
        [
          distinct,
          'Neem contact op met uw beheerder voor meer informatie'
        ].join(' ');

      $stateProvider
        .state('case', {
          url: '/zaak/{caseId}',
          resolve: {
            timeout: [
              '$timeout',
              $timeout =>
                $timeout(angular.noop, 50)],
            caseId: [
              '$rootScope', '$state', '$stateParams', 'resource',
              ($rootScope, $state, $stateParams, resource) => {
                // the param caseId can either be the caseNumber or the caseUuid
                // so we either return the caseNumber or call v2 to get the caseNumber
                if(isNaN(parseInt($stateParams.caseId, 10))) {
                  const caseIdResource = resource({
                    url: '/api/v2/cm/case/get_case',
                    params: {
                      case_uuid: $stateParams.caseId
                    }
                  }, {
                      scope: $rootScope
                    })
                    .reduce((requestOptions, data) =>
                      get(data, 'data.attributes.number')
                    );

                  return caseIdResource
                    .asPromise()
                    .then(() => {
                      const caseId = caseIdResource.data();

                      $state.go('case', { caseId }, {notify: false});

                      return caseId;
                    });
                } else {
                  return $stateParams.caseId;
                }
              }
            ],
            case: [
              '$rootScope', '$q', '$state', '$http', 'caseId', 'snackbarService', 'resource',
              ($rootScope, $q, $state, $http, caseId, snackbarService, resource) => {
                const caseResource = resource({
                  url: `/api/v0/case/${caseId}`
                }, {
                    scope: $rootScope
                  })
                  .reduce((requestOptions, data) => {
                    const caseObj = first(data);

                    // reducers are called with error data,
                    // gotta check if it's an error first
                    if (!caseObj || !caseObj.id) {
                      return null;
                    }

                    return convertv0Case(caseObj);
                  });

                return caseResource
                  .asPromise()
                  .then((response) => {
                    // a successful response can be gotten before the case registration is completed
                    // there are two stages in this (unfinished, finished)
                    // the unfinished stage is a static one, where 'instance' misses many properties
                    // 'number' is one of the missing properties
                    // so we use that to determine whether an error should be thrown
                    // in which case the caseNumber is extracted, so it can be used to provide a link to the case
                    const caseNumber = get(caseResource.data(), 'instance.number');

                    if (!caseNumber) {
                      const extractFirstNumber = str => str.match(/^[^\d]*(\d+)/)[1];
                      const actionUrl = response.instance.actions[1][0].url;
                      const extractedCaseNumber = extractFirstNumber(actionUrl);

                      throw new Error(extractedCaseNumber);
                    }

                    $http({
                      url: `/api/v0/case/${caseNumber}/peruse`,
                      method: 'POST'
                    });

                    return caseResource;
                  })
                  .catch(response => {
                    const type = response.data ? get(first(response.data.result), 'type') : 'case_creation_in_progress';
                    const messageTypes = {
                      'api/case/find_case': 'De zaak kon niet worden gevonden. Mogelijk is de zaak verwijderd.',
                      'api/case/authorization': 'U heeft niet voldoende rechten om deze zaak te bekijken.',
                      case_creation_in_progress: 'Het aanmaken van de zaak is nog niet voltooid.',
                      default: getLegacyErrorMessage('Er ging iets mis bij het laden van de zaak.')
                    };
                    const labelTypes = {
                      case_creation_in_progress: 'Laad opnieuw',
                      default: 'Terug naar dashboard'
                    };
                    const linkTypes = {
                      case_creation_in_progress: `/intern/zaak/${response.message}/`,
                      default: $state.href('home')
                    };

                    snackbarService
                      .error(messageTypes[type] || messageTypes['default'], {
                        actions: [
                          {
                            type: 'link',
                            label: labelTypes[type] || labelTypes['default'],
                            link: linkTypes[type] || linkTypes['default']
                          }
                        ]
                      });

                    return $q.reject(response);
                  });
              }],
            casetype: [
              '$rootScope', '$q', '$state', 'resource', 'case', 'snackbarService',
              ($rootScope, $q, $state, resource, caseResource, snackbarService) => {
                const { reference } = caseResource.data().instance.casetype;
                let { version } = caseResource.data().instance.casetype.instance;
                let casetypeResource = resource({
                  url: `/api/v1/casetype/${reference}`,
                  params: {
                    version
                  }
                }, {
                    scope: $rootScope
                  })
                  .reduce((requestOptions, data) => first(data));

                return casetypeResource
                  .asPromise()
                  .then(() => casetypeResource)
                  .catch(response => {
                    snackbarService
                      .error('Het zaaktype kan niet worden geladen.', {
                        actions: [
                          {
                            type: 'link',
                            label: 'Terug naar dashboard',
                            link: $state.href('home')
                          }
                        ]
                      }
                      );

                    return $q.reject(response);
                  });
              }],
            jobs: [
              '$rootScope', 'resource', 'case',
              ($rootScope, resource, caseResource) => {
                const jobsResource = resource(() => {
                  const reference = get(caseResource.data(), 'reference');

                  return reference ?
                    {
                      url: `/api/v1/case/${reference}/queue`,
                      params: {
                        paging: 100
                      }
                    }
                    : null;
                }, {
                    scope: $rootScope
                  })
                  .reduce((requestOptions, data) =>
                    (data || [])
                      .filter(
                        job => (
                          (job.instance.status !== 'finished')
                          && (job.instance.status !== 'failed')
                        )
                      ));

                return jobsResource
                  .asPromise()
                  .then(() => jobsResource)
                  .catch(() => {
                    // not important enough to bug the user w/ a snack
                  });
              }],
            rules: [
              '$rootScope', 'resource', 'caseId',
              ($rootScope, resource, caseId) => {
                const ruleResource = resource({
                  url: '/api/rules',
                  params: {
                    case_id: caseId,
                    zapi_no_pager: 1
                  }
                }, {
                    scope: $rootScope
                  });

                return ruleResource
                  .asPromise()
                  .then(() => ruleResource);
              }],
            __SIDE_EFFECT__: [
              '$rootScope', '$ocLazyLoad', '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q(resolve => {
                  require([
                    './',
                    './controller'
                  ], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad
                        .load(names.map(name => ({ name })));
                      resolve();
                    });
                  });
                })]
          },
          template,
          controller: 'CaseController',
          controllerAs: 'vm',
          onActivate: [
            '$state', '$location', 'auxiliaryRouteService', 'case',
            ($state, $location, auxiliaryRouteService, caseResource) => {
              let names = auxiliaryRouteService.parseNames($state.current.name, $location.url());
              let state = auxiliaryRouteService.state('case.phase', $state.current.name, $location.url());
              let params = { phaseName: normalizePhaseName(get(caseResource.data(), 'instance.phase') || '') };

              if (!params.phaseName) {
                params.phaseName = 'afhandelen';
              }

              if (names.base === 'case') {
                $state.go(state, params, { location: false });
              }
            }],
          title: [
            'case',
            caseResource => {
              let caseObj = caseResource.data();
              let caseId = caseObj.instance.number;
              let casetypeName = caseObj.instance.casetype.instance.name;
              let subTitle = caseObj.instance.subject;
              let title = {
                mainTitle: `Zaak ${caseId}: ${casetypeName}`,
                subTitle
              };

              return title;
            }],
          settings:
            `<zs-case-settings 
               case-resource="viewController.getCaseResource()" 
               casetype-resource="viewController.getCasetypeResource()" 
               user="viewController.getUser()"
             ></zs-case-settings>`,
          actions: [
            {
              name: 'upload',
              label: 'Document uploaden',
              iconClass: 'upload',
              template:
                `<zs-case-file-upload
                  on-batch-start="close($promise)"
                  case-id="viewController.getCaseNumber()"
                ></zs-case-file-upload>`,
              when: [
                'viewController',
                viewController =>
                  viewController.canUpload()
              ]
            },
            {
              name: 'sjabloon',
              label: 'Sjabloon gebruiken',
              iconClass: 'file',
              template:
                `<zs-case-template-generate
                  on-close="close(viewController.createTemplate($values))"
                  case-id="viewController.getCaseNumber()"
                  data-templates="viewController.getCaseTemplates()"
                  case-documents="viewController.getCaseDocuments()"
                  data-requestor="viewController.getRequestor()"
                ></zs-case-template-generate>`,
              when: [
                'viewController',
                viewController => (
                  !viewController.isResolved()
                  && viewController.canEdit()
                  && (get(viewController.getCaseTemplates(), 'length', 0) > 0)
                )]
            },
            {
              name: 'betrokkene',
              label: 'Betrokkene toevoegen',
              iconClass: 'account',
              template:
                `<zs-case-add-subject
                  on-subject-add="close($promise)"
                  case-id="viewController.getCaseNumber()"
                ></zs-case-add-subject>`,
              when: [
                'viewController',
                viewController => (
                  viewController.canEdit()
                  && !viewController.isResolved()
                  && !!viewController.getRequestor()
                )]
            },
            {
              name: 'email',
              label: 'E-mail versturen',
              iconClass: 'email',
              template:
                `<zs-case-send-email
                  case-id="viewController.getCaseNumber()"
                  data-templates="viewController.getEmailTemplates()"
                  data-requestor="viewController.getRequestor()"
                  on-email-send="close($promise)"
                >
                </zs-case-send-email>`,
              when: [
                'viewController',
                viewController => (
                  viewController.canEdit()
                  && !viewController.isResolved()
                  && !!viewController.getRequestor()
                )]
            },
            {
              name: 'geplande-zaak',
              label: 'Geplande zaak toevoegen',
              iconClass: 'calendar-plus',
              template:
                `<zs-case-plan
                  case-id="viewController.getCaseNumber()"
                  case-uuid="viewController.getCase().reference"
                  on-submit="close($promise)"
                ></zs-case-plan>`,
              when: [
                'viewController',
                viewController => (
                  viewController.canEdit()
                  && !viewController.isResolved())
              ]
            }
          ]
        })
        .state('case.phase', {
          url: '/fase/:phaseName/:tab/',
          template:
            `<zs-case-phase-view
              on-phase-advance="vm.handlePhaseAdvance($taskIds)"
              phase-name="phaseName"
              data-case="case"
              data-casetype="casetype"
              data-case-action-resource="vm.caseActionResource"
              data-case-actions="vm.getCaseActions()"
              data-case-actions-loading="vm.caseActionsLoading()"
              data-checklist-resource="vm.checklistResource"
              data-checklists="vm.getChecklists()"
              data-checklists-loading="vm.checklistsLoading()"
              data-mutation-resource="vm.mutationResource"
              data-mutations="vm.getMutations()"
              data-mutations-loading="vm.mutationsLoading()"
              data-rules="rules"
              data-tab="get('tab')"
              data-templates="vm.getCaseTemplates()"
              case-documents="vm.getCaseDocuments()"
              data-requestor="vm.getRequestor()"
              can-advance="vm.canAdvance()"
              can-edit="vm.canEdit()&&!vm.isResolved()"
              is-resolved="vm.isResolved()"
              can-self-assign="!vm.isResolved()"
              data-user="vm.getUser()"
            ></zs-case-phase-view>`,
          title: [
            '$stateParams', 'case', 'casetype',
            ($stateParams, caseObj, casetype) => {
              const { phaseName } = $stateParams;
              const isCurrent = (
                (get(caseObj, 'values[\'case.phase\']') || '')
                  .toLowerCase()
                === phaseName
              );
              const phaseObj = find(
                get(casetype, 'instance[\'phases\']', []),
                p => p.name.toLowerCase() === phaseName
              );
              let label;

              if (phaseObj) {
                label = phaseObj.name;

                if (isCurrent) {
                  label += ' - Actieve fase';
                }
              }

              return label;
            }],
          controller: assignToScope(['rules'], 'case', 'casetype', 'rules'),
          params: {
            tab: {
              squash: true,
              value: null
            }
          },
          shouldReload: (current, currentParams, to, toParams) => {
            const sameName = (
              get(current, 'name')
              === get(to, 'name')
            );
            const samePhase = (
              sameName
              && (
                get(currentParams, 'phaseName')
                === get(toParams, 'phaseName')
              ));

            return !samePhase;
          }
        })
        .state('case.docs', {
          url: '/documenten/:documentId/',
          title: 'Documenten',
          template:
            `<zs-case-document-view
              case="vm.getCase()"
              case-id="vm.getCaseNumber()"
              case-uuid="vm.getCaseUuid()"
              read-only="!vm.canEdit()||vm.isResolved()"
              can-upload="vm.canUpload()"
              can-delete="vm.canDelete()"
              on-file-accept="vm.reloadCaseResource()"
              on-file-reject="vm.reloadCaseResource()"
              on-file-update="vm.reloadCaseResource()"
            ></zs-case-document-view>`,
          params: {
            documentId: {
              squash: true,
              value: null
            }
          },
          resolve: {
            __SIDE_EFFECT__:
              ['$rootScope', '$ocLazyLoad', '$q',
                ($rootScope, $ocLazyLoad, $q) =>
                  $q(resolve => {
                    require([
                      './zsCaseDocumentView'
                    ], (...names) => {
                      $rootScope.$evalAsync(() => {
                        $ocLazyLoad
                          .load(names.map(name => ({ name })));
                        resolve();
                      });
                    });
                  })]
          }
        })
        .state('case.timeline', {
          url: '/timeline/:eventId/',
          title: 'Timeline',
          template:
            `<zs-case-timeline-view
              case-id="vm.getCaseNumber()"
              data-requestor="vm.getRequestor()"
              event-id="vm.getEventId()"
            ></zs-case-timeline-view>`,
          params: {
            eventId: {
              squash: true,
              value: null
            }
          },
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope', '$ocLazyLoad', '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q(resolve => {
                  require([
                    './zsCaseTimelineView'
                  ], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad
                        .load(names.map(name => ({ name })));
                      resolve();
                    });
                  });
                })]
          }
        })
        .state('case.communication', {
          url: '/communicatie/:action/:id',
          title: 'Communication',
          template:
            `<zs-case-communication-view
              case-id="vm.getCaseNumber()"
              case-uuid="vm.getCaseUuid()"
              thread-id="vm.getThreadId()"
            ></zs-case-communication-view>`,
          params: {
            action: {
              squash: true,
              value: null
            },
            id: {
              squash: true,
              value: null
            }
          },
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope', '$ocLazyLoad', '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q(resolve => {
                  require([
                    './zsCaseCommunicationView'
                  ], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad
                        .load(names.map(name => ({ name })));
                      resolve();
                    });
                  });
                })]
          }
        })
        .state('case.location', {
          url: '/locatie/:more/',
          title: 'Kaart',
          params: {
            more: {
              squash: true,
              value: null
            }
          },
          template:
            `<zs-case-map-view
               case-resource="vm.getCaseResource()"
               show-more="more()"
             ></zs-case-map-view>`,
          shouldReload: (current, currentParams, to) => {
            let sameName = get(current, 'name') === get(to, 'name');

            return !sameName;
          },
          controller: [
            '$scope', 'observableStateParams',
            ($scope, observableStateParams) => {
              $scope.more = () => observableStateParams.get('more');
            }]
        })
        .state('case.relations', {
          url: RELATION_TAB_URL,
          title: 'Relaties',
          template:
            `<zs-case-relation-view
              related-cases="relationVm.relations.data().related"
              child-cases="relationVm.relations.data().children"
              parent-case="relationVm.relations.data().parent"
              sibling-cases="relationVm.relations.data().siblings"
              planned-cases="relationVm.relations.data().plannedCases"
              related-subjects="relationVm.relations.data().relatedSubjects"
              related-objects="relationVm.relations.data().relatedObjects"
              planned-emails="relationVm.relations.data().plannedEmails"
              supports-planned-emails="relationVm.supportsPlannedEmails()"
              has-relatable-object-types="relationVm.hasRelatableObjectTypes()"
              can-relate="vm.canRelate()"
              on-case-relate="relationVm.handleCaseRelate($reference)"
              on-case-unrelate="relationVm.handleCaseUnrelate($reference)"
              on-case-reorder="relationVm.handleCaseReorder($reference, $after)"
              on-planned-case-update="relationVm.handlePlannedCaseUpdate($id, $values)"
              on-planned-case-remove="relationVm.handlePlannedCaseRemove($id)"
              on-related-subject-update="relationVm.handleRelatedSubjectUpdate($id, $values)"
              on-related-subject-remove="relationVm.handleRelatedSubjectRemove($id)"
              on-object-relate="relationVm.handleObjectRelate($object)"
              on-email-reschedule="relationVm.handleEmailReschedule()"
              current-case="vm.getCase()"
              data-disabled="!vm.canEdit()||vm.isResolved()"
            ></zs-case-relation-view>`,
          resolve: {
            emailInterfaceResource: [
              '$rootScope', '$q', 'resource',
              ($rootScope, $q, resource) => {
                const emailInterfaceResource = resource(
                  '/api/v1/sysin/interface/get_by_module_name/email',
                  { scope: $rootScope }
                );

                return emailInterfaceResource
                  .asPromise()
                  .then(() => emailInterfaceResource)
                  .catch(reason => {
                    console.error(reason);

                    return $q.resolve(emailInterfaceResource);
                  });
              }],
            relatedSubjectResource: [
              '$rootScope', '$stateParams', 'resource',
              'snackbarService', 'relationProgressService',
              (
                $rootScope, $stateParams, resource,
                snackbarService, relationProgressService
              ) => {
                const relatedSubjectResource = resource(
                  `/api/case/${$stateParams.caseId}/subjects`,
                  { scope: $rootScope }
                );

                return relatedSubjectResource
                  .asPromise()
                  .then(() => relatedSubjectResource)
                  .catch(() => {
                    snackbarService
                      .emit(getLegacyErrorMessage('De betrokkenen konden niet worden geladen.'));
                    relatedSubjectResource
                      .destroy();
                  })
                  .finally(() => {
                    relationProgressService.resolve(3);
                  });
              }],
            relatedObjectResource: [
              '$q', '$rootScope', 'resource', 'snackbarService', '$stateParams',
              ($q, $rootScope, resource, snackbarService, $stateParams) => {
                const relatedObjectResource = resource(
                  () => {
                    return {
                      url: `/api/case/${$stateParams.caseId}`,
                      params: {
                        deep_relations: true
                      }
                    };

                  }, { scope: $rootScope }
                )
                  .reduce((requestOptions, data) => {
                    let relatedObjects =
                      data ?
                        seamlessImmutable(get(convertv0Case(first(data)), 'instance.relations.instance.rows', []))
                        : [];

                    return relatedObjects;
                  });

                return relatedObjectResource
                  .asPromise()
                  .then(() => relatedObjectResource)
                  .catch(reason => {
                    snackbarService
                      .emit(getLegacyErrorMessage('De geplande zaken konden niet worden geladen.'));
                    relatedObjectResource
                      .destroy();

                    return $q.reject(reason);
                  });
              }],
            plannedEmailsResource: [
              '$q', '$rootScope', 'resource', 'snackbarService',
              '$stateParams', 'emailInterfaceResource', 'relationProgressService',
              (
                $q, $rootScope, resource, snackbarService,
                $stateParams, emailInterfaceResource, relationProgressService
              ) => {
                const plannedEmailsResource = resource(
                  () => {
                    return emailInterfaceResource.data() && emailInterfaceResource.data().length ?
                      `/zaak/${$stateParams.caseId}/scheduled_jobs`
                      : null;
                  },
                  { scope: $rootScope }
                );

                return plannedEmailsResource
                  .asPromise()
                  .then(() => plannedEmailsResource)
                  .catch(reason => {
                    plannedEmailsResource
                      .destroy();
                    snackbarService
                      .error(getLegacyErrorMessage('De geplande emails konden niet worden geladen.'));

                    return $q.reject(reason);
                  })
                  .finally(() => {
                    relationProgressService.resolve(5);
                  });

              }],
            objectTypeResource: [
              '$q', '$rootScope', 'resource',
              'snackbarService', 'relationProgressService',
              'relatedObjectResource',
              (
                $q, $rootScope, resource,
                snackbarService, relationProgressService,
                relatedObjectResource
              ) => {
                const objectTypeResource = resource(() => {
                  let objectTypes = uniq(
                    (relatedObjectResource.data() || [])
                      .map(relation => relation.type)
                  );

                  if (!objectTypes.length) {
                    return null;
                  }

                  return {
                    url: '/api/object/search',
                    params: {
                      zql: `SELECT {} FROM type WHERE prefix in ("${objectTypes.join('","')}")`
                    }
                  };
                }, {
                    scope: $rootScope
                  });

                return objectTypeResource
                  .asPromise()
                  .then(() => objectTypeResource)
                  .catch((reason) => {
                    snackbarService
                      .emit(getLegacyErrorMessage('De objecttypes konden niet worden geladen.'));
                    objectTypeResource
                      .destroy();

                    return $q.reject(reason);
                  })
                  .finally(() => {
                    relationProgressService.resolve(2);
                    relationProgressService.resolve(4);
                  });

              }],
            relationsResources: [
              '$rootScope', '$stateParams', '$q',
              'case', 'resource', 'composedReducer',
              'snackbarService', 'relationProgressService',
              (
                $rootScope, $stateParams, $q,
                caseResource, resource, composedReducer,
                snackbarService, relationProgressService
              ) => {
                const resourceScope = {
                  scope: $rootScope
                };
                const getReferences = data =>
                  assign(
                    {
                      child: [],
                      parent: [],
                      plain: [],
                      initiator: [],
                      continuation: []
                    },
                    mapValues(
                      get(data, 'instance.case_relationships', {}),
                      value => {
                        let set = flatten(
                          get(value, 'instance.rows', [value])
                        )
                          .filter(identity);

                        return set.map(row => row.reference);
                      }
                    )
                  );

                const currentResource = resource({
                  url: `/api/v1/case/${caseResource.data().reference}`
                }, resourceScope)
                  .reduce((requestOptions, data) => first(data));

                const parentResource = resource(() => {
                  const references = getReferences(currentResource.data());

                  if (references.parent.length) {
                    return `/api/v1/case/${references.parent[0]}`;
                  }

                  return null;
                }, resourceScope)
                  .reduce((requestOptions, data) => first(data));

                const childrenResource = resource(() => {
                  const currentRefs = getReferences(currentResource.data());
                  const parentRefs = parentResource.data() ? getReferences(parentResource.data()) : {};
                  const uuids = uniq(
                    flatten(_values(currentRefs))
                      .concat(parentRefs.child)
                      .concat(
                        [currentResource.data(), parentResource.data()]
                          .filter(identity)
                          .map(c => c.reference)
                      )
                      .filter(identity)
                  );

                  if (uuids.length) {
                    return {
                      url: '/api/object/search',
                      params: {
                        zql: `SELECT {} FROM case WHERE object.uuid IN ("${uuids.join('","')}")`,
                        zapi_num_rows: 100
                      }
                    };
                  }

                  return null;
                }, resourceScope)
                  .reduce((requestOptions, data) =>
                    (data || [])
                      .map(convertv0Case));

                const resources = [currentResource, parentResource, childrenResource];

                return $q
                  .all(resources.map(res => res.asPromise()))
                  .then(() => $q.resolve(resources))
                  .catch(reason => {
                    snackbarService
                      .emit(getLegacyErrorMessage('De relaties konden niet worden geladen.'));
                    resources
                      .forEach((failedResource) => {
                        failedResource.destroy();
                      });

                    return $q.reject(reason);
                  })
                  .finally(() => {
                    relationProgressService.resolve(0);
                    relationProgressService.resolve(1);
                  });
              }],
            relations: [
              '$rootScope', 'composedReducer', 'case', 'relationsResources', 'relatedSubjectResource',
              'relatedObjectResource', 'objectTypeResource', 'plannedEmailsResource',
              (
                $rootScope, composedReducer, caseResource, relationsResources, relatedSubjectResource,
                relatedObjectResource, objectTypeResource, plannedEmailsResource
              ) =>
                composedReducer(
                  { scope: $rootScope },
                  caseResource,
                  ...relationsResources,
                  relatedSubjectResource, relatedObjectResource, objectTypeResource, plannedEmailsResource
                )
                  .reduce((
                    caseObj,
                    currentCase, parentCase, childCases,
                    relatedSubjects, relatedObjects, objectTypes, plannedEmails
                  ) => {
                    const cases = keyBy(
                      [currentCase]
                        .concat(parentCase)
                        .concat(childCases)
                        .filter(identity),
                      'reference'
                    );

                    const caseRelations = mapValues(
                      assign(
                        {
                          sibling: get(parentCase, 'instance.case_relationships.child')
                        },
                        currentCase.instance.case_relationships
                      ),
                      value => {
                        let set = flatten(
                          get(value, 'instance.rows', [value])
                        )
                          .filter(identity);

                        return set.map(row => cases[row.reference]);
                      }
                    );

                    const relations = {
                      self: caseObj,
                      parent: first(caseRelations.parent),
                      siblings:
                        uniq(caseRelations.sibling, 'reference')
                          .filter(relation => relation.reference !== currentCase.reference),
                      children: caseRelations.child,
                      related:
                        currentCase
                          .instance
                          .relations
                          .instance
                          .rows
                          .map(row => {
                            const type = keys(
                              pickBy(
                                caseRelations,
                                (relatedCases) => !!find(relatedCases, { reference: row.reference })
                              )
                            )[0];

                            const relatedCase = cases[row.reference];

                            return relatedCase ?
                              relatedCase.merge({
                                relation_type: type
                              })
                              : null;
                          })
                          .filter(identity),
                      plannedCases:
                        relatedObjects
                          .filter(obj => obj.type === 'scheduled_job'),
                      relatedSubjects,
                      relatedObjects: map(
                        relatedObjects
                          .filter(obj => obj.type !== 'scheduled_job' && obj.type !== 'case'),
                        obj => {
                          const objectType =
                            find(objectTypes, (type) => {
                              return type.values.prefix === obj.type;
                            });

                          return obj.merge({
                            related_object_type_label: get(objectType, 'values.name', obj.related_object_type)
                          });

                        }
                      ),
                      plannedEmails
                    };

                    return relations;
                  })],
            __SIDE_EFFECT__:
              ['$rootScope', '$ocLazyLoad', '$q',
                ($rootScope, $ocLazyLoad, $q) =>
                  $q(resolve => {
                    require([
                      './zsCaseRelationView'
                    ], (...names) => {
                      $rootScope.$evalAsync(() => {
                        $ocLazyLoad
                          .load(names.map(name => ({ name })))
                          .catch(reason => {
                            console.error(reason);

                            return $q.reject(reason);
                          });
                        resolve();
                      });
                    });
                  })]
          },
          controller: [
            '$scope', 'composedReducer', 'case', 'casetype',
            'relationsResources', 'relations', 'relatedSubjectResource', 'relatedObjectResource',
            'objectTypeResource', 'plannedEmailsResource', 'emailInterfaceResource',
            function CaseRelationRouteController(
              $scope, composedReducer, caseResource, casetypeResource,
              relationsResources, relations, relatedSubjectResource, relatedObjectResource,
              objectTypeResource, plannedEmailsResource, emailInterfaceResource
            ) {
              const ctrl = this;
              const resourcesToDestroy = relationsResources
                .concat(
                  relatedSubjectResource,
                  relatedObjectResource,
                  objectTypeResource,
                  plannedEmailsResource,
                  emailInterfaceResource
                );
              const hasRelatableObjectTypesReducer = composedReducer({ scope: $scope }, casetypeResource)
                .reduce(casetype =>
                  getRelatableObjectTypesFromCasetype(casetype)
                    .length
                );

              ctrl.relations = relations;
              ctrl.currentResource = relationsResources[0];

              ctrl.handleCaseRelate = reference =>
                ctrl
                  .currentResource
                  .mutate('case/relation/add', {
                    caseReference: caseResource.data().reference,
                    relatedCaseReference: reference
                  })
                  .asPromise();

              ctrl.handleCaseUnrelate = reference =>
                ctrl
                  .currentResource
                  .mutate('case/relation/remove', {
                    caseReference: caseResource.data().reference,
                    relatedCaseReference: reference
                  })
                  .asPromise();

              ctrl.handleCaseReorder = (reference, after) =>
                ctrl
                  .currentResource
                  .mutate('case/relation/move', {
                    caseReference: caseResource.data().reference,
                    relatedCaseReference: reference,
                    afterCaseReference: after
                  })
                  .asPromise();

              ctrl.handlePlannedCaseUpdate = (reference, values) =>
                relatedObjectResource
                  .mutate('case/relation/scheduled_job/update', {
                    reference,
                    values
                  }
                  )
                  .asPromise();

              ctrl.handlePlannedCaseRemove = reference =>
                relatedObjectResource
                  .mutate('case/relation/scheduled_job/remove', {
                    reference
                  });

              ctrl.handleRelatedSubjectUpdate = (id, values) =>
                relatedSubjectResource
                  .mutate('case/relation/subject/update', assign({
                    caseId: caseResource.data().instance.number,
                    subjectId: id
                  }, values));

              ctrl.handleRelatedSubjectRemove = id =>
                relatedSubjectResource
                  .mutate('case/relation/subject/remove', {
                    caseId: caseResource.data().instance.number,
                    subjectId: id
                  });

              ctrl.handleObjectRelate = object =>
                relatedObjectResource
                  .mutate('case/relation/add/object', {
                    caseReference: caseResource.data().reference,
                    relatedObjectReference: object.id,
                    relatedObjectType: object.type,
                    relatedObjectLabel: object.label
                  })
                  .asPromise();

              ctrl.handleEmailReschedule = () =>
                plannedEmailsResource
                  .mutate('case/relation/reschedule', {
                    caseId: caseResource.data().instance.number
                  })
                  .asPromise();

              ctrl.supportsPlannedEmails = () =>
                (get(emailInterfaceResource.data(), 'length', 0)
                  > 0);

              ctrl.hasRelatableObjectTypes = hasRelatableObjectTypesReducer.data;

              $scope.$on('$destroy', () => {
                resourcesToDestroy.forEach(res => {
                  res.destroy();
                });
              });
            }],
          controllerAs: 'relationVm'
        })
        .state('case.admin', {
          url: '/beheer/:action',
          auxiliary: true,
          resolve: {
            acls: [
              '$rootScope', '$q', 'resource', '$stateParams', 'case',
              ($rootScope, $q, resource, $stateParams, caseResource) => {
                const aclsResource = resource(
                  $stateParams.action === 'rechten' ?
                    {
                      url: `/api/v1/case/${caseResource.data().reference}/acl`,
                      params: {
                        rows_per_page: 100
                      }
                    }
                    : null,
                  {
                    scope: $rootScope,
                    cache: {
                      disabled: true
                    }
                  }
                )
                  .reduce((requestOptions, data) => {
                    if (data) {
                      return seamlessImmutable(data.asMutable().reverse());
                    }

                    return data;
                  });

                // immediately resolve to keep UI responsive (modal opens immediately)
                return $q.resolve(aclsResource);
              }
            ],
            actions: [
              '$rootScope', '$q', '$state', 'composedReducer', 'case', 'casetype',
              ($rootScope, $q, $state, composedReducer, caseResource, casetypeResource) =>
                $q(resolve => {
                  require([
                    '../../../shared/case/caseActions'
                  ], actions => {
                    $rootScope.$evalAsync(() => {
                      resolve(
                        composedReducer({ scope: $rootScope }, caseResource, casetypeResource)
                          .reduce((caseObj, casetype, acls) =>
                            actions({
                              caseObj,
                              casetype,
                              $state,
                              acls
                            }))
                      );
                    });
                  });
                })]
          },
          onExit: [
            'actions',
            actions => {
              actions.destroy();
            }],
          onEnter: [
            '$rootScope', '$compile', '$state', 'user', 'case', 'casetype',
            'acls', 'actions', '$stateParams', 'zsModal',
            (
              $rootScope, $compile, $state, user, caseResource, casetypeResource,
              aclsResource, actions, $stateParams, zsModal
            ) => {
              const action = find(actions.data(), { name: $stateParams.action });
              let modal;
              let unregister;

              let closeModal = () => {
                modal.close();
                scope.$destroy();
                unregister();
              };

              const scope = $rootScope.$new(true);

              scope.action = action;
              scope.caseResource = caseResource;
              scope.casetypeResource = casetypeResource;
              scope.aclsResource = aclsResource;
              scope.user = user.data().instance.logged_in_user;

              scope.onSubmit = (promise, reload, willRedirect) => {
                modal.hide();

                promise
                  .then(() => {
                    modal.close();

                    if (!willRedirect) {
                      $state.go('^', null, { reload });
                    }
                  })
                  .catch(() => {
                    modal.show();
                  });
              };

              modal = zsModal({
                title: action.label,
                scope,
                el: $compile(`
                <zs-case-admin-view
                  data-action="action"
                  case-resource="caseResource"
                  data-user="user"
                  casetype-resource="casetypeResource"
                  acl-resource="aclsResource"
                  on-submit="onSubmit($promise, $reload, $willRedirect)"
                ></zs-case-admin-view>`)(scope)
              });

              modal.open();

              modal.onClose(() => {
                $state.go('^');

                return false;
              });

              unregister = $rootScope.$on('$stateChangeStart', closeModal);
            }]
        })
        .state('case.info', {
          url: '/informatie',
          auxiliary: true,
          onEnter: [
            '$rootScope', '$compile', '$state', 'case', 'casetype', 'zsModal',
            ($rootScope, $compile, $state, caseResource, casetypeResource, zsModal) => {
              const scope = assign(
                $rootScope.$new(true), {
                  caseResource,
                  casetypeResource
                });

              const modal = zsModal({
                title: 'Meer informatie over de zaak',
                el: $compile(
                  `<zs-case-about-view 
                     case-resource="caseResource"
                     casetype-resource="casetypeResource"
                   ></zs-case-about-view>`
                )(scope)
              });

              const unregister = $rootScope.$on('$stateChangeStart', closeModal);

              const closeModal = () => {
                modal.close();
                scope.$destroy();
                unregister();
              };

              modal.open();

              modal.onClose(() => {
                $state.go('^');

                return false;
              });
            }]
        });
    }])
  .name;
