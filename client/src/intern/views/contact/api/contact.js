import fields from '../fields';
import { EMPTY } from '../fields/format';

export function extract( data, sce, showEmpty = true ) {
	const { subject_type } = data.instance;

	const mapFields = field => {
		const { key, label, getValue } = field;
		const summary = Boolean(field.summary);
		const noEmpty = Boolean(field.noEmpty);

		return {
			summary,
			key,
			noEmpty,
			label,
			get value() {
				const value = getValue(key, data);

				return sce.trustAsHtml(value);
			}
		};
	};

	const filterFields = field =>
		(!field.noEmpty || (field.value.valueOf() !== EMPTY));

	if (showEmpty) {
		return fields[subject_type]
			.map(mapFields)
			.filter(filterFields);
	}

	return fields[subject_type]
		.filter(field => (field.getValue(field.key, data) !== EMPTY))
		.map(mapFields);
}
