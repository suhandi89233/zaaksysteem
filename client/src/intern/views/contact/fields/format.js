import assign from 'lodash/assign';
import get from 'lodash/get';
import isEmpty from '../../../../shared/vorm/util/isEmpty';
import { addressRowFactory } from '../../../../shared/util/address/format';

export const EMPTY = '-';

const resolveKey = ( key, data ) => get(data, `instance.subject.instance.${key}`);

const arrayToObject = ( result, item ) => assign(result, { [item]: item });

// ZS-TODO: find or create date utilities
const padDatePart = input => `0${input}`.slice(-2);

function getDisplayDate( input ) {
	const date = (typeof input === 'string') ?
		new Date(input)
		: input;

	return [
		padDatePart(date.getDate()),
		padDatePart(date.getMonth() + 1),
		date.getFullYear()
	].join('-');
}

const getAddressRows = addressRowFactory([
	'foreign_address_line1',
	'foreign_address_line2',
	'foreign_address_line3',
	'country'
], [
	'street',
	'street_number',
	'street_number_letter',
	'street_number_suffix',
	'zipcode',
	'city',
	'country'
].reduce(arrayToObject, {}));

export function getValue( key, data ) {
	const value = resolveKey(key, data);

	if (isEmpty(value)) {
		return EMPTY;
	}

	return value;
}

export function getAddressValue( key, data ) {
	const value = resolveKey(key, data);

	if (!isEmpty(value)) {
		const rows = getAddressRows(value.instance);

		return rows.join('<br>');
	}

	return EMPTY;
}

export function getBooleanValue( key, data ) {
	const value = resolveKey(key, data);

	if (typeof value === 'boolean') {
		return value ? 'Ja' : 'Nee';
	}

	return EMPTY;
}

export function getCompanyTypeValue( key, data ) {
	const value = resolveKey(key, data);

	if (isEmpty(value)) {
		return EMPTY;
	}

	return value.instance.label;
}

export function getDateValue( key, data ) {
	const value = resolveKey(key, data);

	if (isEmpty(value)) {
		return EMPTY;
	}

	return getDisplayDate(value);
}

export function getEmailValue( key, data ) {
	const value = resolveKey(key, data);

	if (isEmpty(value)) {
		return EMPTY;
	}

	return `<a href="mailto:${value}">${value}</a>`;
}

export function getGenderValue( key, data ) {
	const value = resolveKey(key, data);

	switch (value) {
		case 'M':
			return 'Man';
		case 'V':
			return 'Vrouw';
		default:
			return EMPTY;
	}
}

export function getSourceValue( key, data ) {
	const value = data.instance[key];

	if (isEmpty(value)) {
		return EMPTY;
	}

	// ZS-TODO: use label once the API provides it
	return get(value, 'instance.interface_uuid');
}
