import angular from 'angular';
import angularDragulaModule from 'angular-dragula';
import template from './template.html';
import controller from './controller';
import resourceModule from './../../../../../../../shared/api/resource';
import snackbarServiceModule from './../../../../../../../shared/ui/zsSnackbar/snackbarService';
import zsObjectSuggestModule from './../../../../../../../shared/object/zsObjectSuggest';
import actionsModule from './actions';
import './styles.scss';

export default
	angular.module('intern.home.zsDashboard.zsDashboardWidget.zsDashboardWidgetFavorite', [
		angularDragulaModule(angular),
		resourceModule,
		zsObjectSuggestModule,
		snackbarServiceModule,
		actionsModule
	])
		.directive('zsDashboardWidgetFavorite', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					type: '&',
					placeholder: '@'
				},
				bindToController: true,
				controller: [ 'resource', 'dragulaService', 'snackbarService', '$scope', function ( resource, dragulaService, snackbarService, $scope ) {
					
					controller.call(this, resource, dragulaService, snackbarService, $scope);
					
				}],
				controllerAs: 'zsDashboardWidgetFavorite'
			};

		}])
		.name;
