import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsDashboardWidgetSearchSelectListGroup', [
	])
		.directive('zsDashboardWidgetSearchSelectListGroup', [ ( ) => {

			return {
				restrict: 'E',
				scope: {
					onSelect: '&',
					items: '&'
				},
				template,
				bindToController: true,
				controller: [ function ( ) {
				}],
				controllerAs: 'ctrl'
			};

		}])
		.name;
