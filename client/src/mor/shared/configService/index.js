import angular from 'angular';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import first from 'lodash/first';
import get from 'lodash/get';
import url from 'url';
import appServiceModule from '../appService';
import flattenDeep from 'lodash/flattenDeep';

export default
	angular.module('Zaaksysteem.mor.configService', [
		resourceModule,
		composedReducerModule,
		appServiceModule
	])
		.factory('configService', [ '$document', 'resource', '$rootScope', 'composedReducer', ( $document, resource, $rootScope, composedReducer ) => {

			let configResource,
				attributesReducer,
				getCaseSpecificAttributes;

			configResource = resource('/api/v1/app/app_mor', {
				scope: $rootScope,
				cache: {
					every: 60 * 1000
				}
			})
				.reduce( ( requestOptions, data ) => {
					return first(data) || { instance: { interface_config: {} } };
				});

			configResource.onUpdate(( data ) => {

				let manifestEl = $document[0].querySelector('link[rel="manifest"]'),
					manifestUrl,
					config = get(data, 'instance.interface_config', {});

				if (!manifestEl) {
					manifestEl = $document[0].createElement('link');
					manifestEl.setAttribute('rel', 'manifest');

					$document.find('head').append(manifestEl);
				}

				manifestUrl = url.format({
					// pathname: '/clientutil/proxy/manifest',
					query: {
						name: config.title,
						start_url: config.app_uri,
						icons: [],
						background_color: config.header_bgcolor,
						theme_color: '#FFFFFF',
						display: 'standalone',
						orientation: 'portrait'
					}
				});

				manifestEl.setAttribute('href', manifestUrl);

			});

			attributesReducer = composedReducer({ scope: $rootScope }, configResource.data)
				.reduce( config => {
					return flattenDeep(
						get(config, 'instance.interface_config.casetypes')
						.map( ( caseType ) => {

							let attributes = [],
								subjectAttribute = caseType.casetype_subject_attribute,
								caseAttributes = caseType.casetype_case_attributes;

							if (subjectAttribute && caseAttributes) {
								attributes = caseAttributes.concat(subjectAttribute);
							} else if (caseAttributes) {
								attributes = caseAttributes;
							} else if (subjectAttribute) {
								attributes = subjectAttribute;
							}

							return attributes;
						})
					);
				});

			getCaseSpecificAttributes = composedReducer({ scope: $rootScope }, configResource)
				.reduce( config => {
					return get(config, 'instance.interface_config.casetypes')
					.map( ( caseType ) => {
						return {
							casetype_id: caseType.casetype.values.casetype_id,
							casetype_reference: caseType.casetype.id,
							casetype_name: caseType.casetype.values.name,
							subject_attribute: caseType.casetype_subject_attribute,
							completion_attributes: caseType.casetype_case_attributes
						};
					});
				});

			return {
				getResource: ( ) => configResource,
				getInterfaceId: ( ) => get(configResource.data(), 'instance.id'),
				getConfig: ( ) => get(configResource.data(), 'instance.interface_config'),
				getAttributes: ( ) => attributesReducer.data(),
				getCaseSpecificAttributes: ( ) => getCaseSpecificAttributes.data(),
				getSupportLink: ( ) => get(configResource.data(), 'instance.interface_config.app_help_uri')
			};

		}])
		.name;
