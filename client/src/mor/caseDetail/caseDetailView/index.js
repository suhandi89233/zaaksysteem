import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import configServiceModule from '../../shared/configService';
import actionsModule from './actions';
import shortid from 'shortid';
import capitalize from 'lodash/capitalize';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import isArray from 'lodash/isArray';
import sessionServiceModule from '../../../shared/user/sessionService';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import appServiceModule from '../../shared/appService';
import get from 'lodash/get';
import find from 'lodash/find';
import zsMapModule from '../../../shared/ui/zsMap';
import first from 'lodash/first';
import omitBy from 'lodash/omitBy';
import isNull from 'lodash/isNull';
import compact from 'lodash/compact';
import uniq from 'lodash/uniq';
import uniqBy from 'lodash/uniqBy';
import startsWith from 'lodash/startsWith';
import isObject from 'lodash/isObject';
import identity from 'lodash/identity';
import popupTemplate from '../../../shared/ui/zsMap/popup-template.html';
import './styles.scss';

export default
		angular.module('Zaaksysteem.mor.caseDetailView', [
			composedReducerModule,
			configServiceModule,
			angularUiRouterModule,
			auxiliaryRouteModule,
			sessionServiceModule,
			actionsModule,
			snackbarServiceModule,
			appServiceModule,
			zsMapModule
		])
		.directive('caseDetailView', [ '$q', '$sce', '$window', '$document', '$http', '$compile', '$timeout', '$state', 'dateFilter', 'composedReducer', 'configService', 'auxiliaryRouteService', 'sessionService', 'snackbarService', 'zsModal', 'caseAttrTemplateCompiler', 'vormValidator', 'appService', ( $q, $sce, $window, $document, $http, $compile, $timeout, $state, dateFilter, composedReducer, configService, auxiliaryRouteService, sessionService, snackbarService, zsModal, caseAttrTemplateCompiler, vormValidator, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					case: '&',
					documents: '&',
					caseType: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						fieldReducer,
						styleReducer,
						caseSpecificAttributes = find(configService.getCaseSpecificAttributes(), ( attribute ) => attribute.casetype_reference === ctrl.case().data().instance.casetype.reference),
						markerReducer,
						centerReducer,
						caseAttributes = uniqBy(ctrl.caseType().data().instance.phases.flatMap(phase => phase.fields), 'magic_string'),
						userResource = sessionService.createResource($scope),
						assignmentReducer,
						completedFieldReducer,
						locationReducer;


					/* HEADER */
					ctrl.getTitle = ( ) => `${ctrl.case().data().instance.number}: ${ctrl.case().data().instance.subject_external || ctrl.case().data().instance.casetype.instance.name || '-'}`;

					styleReducer = composedReducer( { scope: $scope }, configService.getConfig)
						.reduce( config => {
							return {
								'background-color': get(config, 'header_bgcolor', '#FFF')
							};
						});

					ctrl.getStyle = styleReducer.data;


					/* MAP */
					locationReducer = composedReducer( { scope: $scope }, ctrl.case() )
						.reduce( ( caseObj ) => {

							return get(caseObj, 'instance.case_location.nummeraanduiding')
								|| get(caseObj, 'instance.case_location.openbareruimte');

						});

					markerReducer =
						composedReducer( { scope: $scope }, locationReducer )
							.reduce( location => {

								let markers,
									markerTemplate = angular.element(popupTemplate),
									locationValue = omitBy(get(location, 'address_data'), isNull),
									locationTemplate = `${get(locationValue, 'straat')} ${get(locationValue, 'huisnummer', '')} ${get(locationValue, 'huisletter', '')} ${get(locationValue, 'huisnummertoevoeging', '')}`;

								angular.element(markerTemplate[0].querySelector('.map-popup-body')).append(
									'<div class="popup-arrow"></div>'
								);

								markers = isObject(location) ?
									[
										{
											coordinates: {
												lat: get(location, 'address_data.gps_lat_lon', '').replace('(', '').split(',')[0],
												lng: get(location, 'address_data.gps_lat_lon', '').replace(')', '').split(',')[1]
											},
											title: locationTemplate,
											description: '',
											template: markerTemplate
										}
									]
									: [];

							return markers;

						});

					ctrl.getMarker = markerReducer.data;

					ctrl.getMarkerStyle = ( ) => {
						return { fill: 'red', size: 35, 'stroke': '#B72626', width: 1 };
					};

					centerReducer =
						composedReducer( { scope: $scope }, markerReducer )
							.reduce( markers => {

								if (markers.length) {

									let markerLocation = first(markers).coordinates;

									return [ markerLocation.lng, markerLocation.lat ];
								}

								return null;

							});

					ctrl.getCenter = centerReducer.data;

					/* CASE */

					ctrl.isCaseOpen = ( ) => ctrl.case().data().instance.status !== 'resolved';

					assignmentReducer = composedReducer({ scope: $scope }, ctrl.case(), userResource)
						.reduce( ( caseItem, user ) => {

							if ( get(caseItem, 'instance.assignee.reference') === get(user, 'instance.logged_in_user.uuid') ) {
								return true;
							}
							return false;

						});

					ctrl.getAssigned = assignmentReducer.data;

					/* FIELDS */
					fieldReducer = composedReducer({ scope: $scope }, ctrl.case(), caseAttributes, ctrl.caseType() )
						.reduce( ( caseItem, fieldConfig, caseType ) => {

							let caseData = caseItem,
								result = {
									label: 'Resultaat',
									magic_string: 'result'
								},
								caseFields =
									compact(
										uniq(
											fieldConfig
										)
										.concat(result)
										.map( ( field ) => {

											let label = field.public_label || field.label,
												value = caseData.instance.attributes[field.magic_string] || caseData.instance[field.magic_string] || '',
												tpl = String(value);

											if ( isArray(value) && !startsWith(field.type, 'bag_') ) {

												if (value.length > 1) {
													tpl = '<ul>';

													value.map( ( el ) => {
														tpl += `<li>${el}</li>`;
													});

													tpl += '</ul>';
												} else {
													tpl = `${value.join()}`;
												}
											}

											if (label === 'zaaknummer' ) {
												label = capitalize(label);
												value = caseData.instance.number;
												tpl = String(value);
											}

											if (field.type === 'date') {
												tpl = dateFilter( value, 'dd MMMM yyyy');
											}

											if (startsWith(field.type, 'bag_')) {

												if (value.hasOwnProperty('address_data') && !isArray(value)) {
													value = omitBy(get(value, 'address_data'), isNull);
													tpl = `${get(value, 'straat')} ${get(value, 'huisnummer', '')} ${get(value, 'huisletter', '')} ${get(value, 'huisnummertoevoeging', '')}<br />${get(value, 'postcode', '')} ${get(value, 'woonplaats', '')}`;
												}
												
												if ( isArray(value) ) {
													tpl = '';

													value = value.map( ( address ) => {
														let addressValue = omitBy(get(address, 'address_data'), isNull);
														
														tpl += `${get(addressValue, 'straat')} ${get(addressValue, 'huisnummer', '')} ${get(addressValue, 'huisletter', '')} ${get(addressValue, 'huisnummertoevoeging', '')} ${get(addressValue, 'postcode', '')} ${get(addressValue, 'woonplaats', '')}<br />`;
													});
												}
											}

											if (field.magic_string === 'result' && value !== '') {
												value = find(caseType.instance.results, ( resultItem ) => resultItem.type === value);
												tpl = value.label || capitalize(value.type);
											}

											if (field.is_system === true || field.type === 'file' || (field.magic_string === 'result' && caseItem.instance.status !== 'resolved' && value !== '') ) {
												tpl = '';
											}

											let fieldObject = {
												id: shortid(),
												name: field.magic_string,
												label,
												value,
												template: $sce.trustAsHtml(tpl)
											};

											if (tpl.length) {
												return fieldObject;
											}

											return null;

										})
									);

							return caseFields;

						});

					ctrl.getFields = fieldReducer.data;

					completedFieldReducer = composedReducer( { scope: $scope }, fieldReducer, caseSpecificAttributes.completion_attributes)
						.reduce( (fields, completionAttributes ) => {

							let result = [{
									label: 'Resultaat',
									magic_string: 'result',
									object: {
										column_name: 'attribute.result'
									}
								}];

							let attributes =
									completionAttributes
									.concat(result)
									.asMutable()
									.map( ( field ) => {
										return find(fields, ( caseField ) => {

											return caseField.name === field.object.column_name.replace('attribute.', '');
										
										});
									})
									.filter(identity);

							return attributes;
						});

					ctrl.getCompletedFields = completedFieldReducer.data;

					ctrl.getAttachments = ctrl.documents;

					ctrl.getFileUrl = ( documentId ) => `/api/v1/case/${ctrl.case().data().reference}/document/${documentId}/download`;

					/*  HANDLING */
					ctrl.handleCase = ( ) => {

						if ( !ctrl.getAssigned() ) {

							let caseRef = ctrl.case().data().reference,
								userUuid = get(userResource.data(), 'instance.logged_in_user.uuid');

							ctrl.case().mutate('MOR_TAKE_CASE', {
								caseRef,
								userUuid
							})
							.asPromise()
							.then( ( ) => {

								appService.dispatch('reload_case_groups');

							});

						} else {

							$state.go('caseList.caseComplete', { caseID:  ctrl.case().data().instance.number } );

						}

					};

					ctrl.goBack = ( ) => {
						$state.go('^');
					};

				}],
				controllerAs: 'caseDetailView'

			};
		}
		])
		.name;
