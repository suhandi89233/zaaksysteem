/*global define, window*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.utils.events.cancelEvent', function ( ) {
		
		var win = window,
			event = win.document.createEvent ? win.document.createEvent('Event') : win.document.createEventObject();
			
		if(event.stopPropagation) {
			return function ( event, preventDefault ) {
				if(preventDefault === undefined) {
					preventDefault = true;
				}
				event.stopPropagation();
				event.stopImmediatePropagation();
				if(preventDefault) {
					event.preventDefault();
				}
				return false;
			};
		} else {
			return function ( event, preventDefault) {
				if(preventDefault === undefined) {
					preventDefault = true;
				}
				event.returnValue = !preventDefault;
				event.cancelBubble = true;
				return false;
			};
		}
		
	});
	
})();
