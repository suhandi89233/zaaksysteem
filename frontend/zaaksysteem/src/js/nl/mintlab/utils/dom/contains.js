/*global define*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.utils.dom.contains', function ( ) {
		
		return function ( parent, element ) {
			while(element) {
				element = element.parentNode;
				if(element === parent) {
					return true;
				}
			}
			return false;
		};
	});
	
})();
