/*global angular*/
angular
	.module('Zaaksysteem')
	.directive('script', [
		'$rootScope', '$parse',
		function scopeData( $rootScope, $parse ) {
			var trim = window.zsFetch('nl.mintlab.utils.shims.trim');

			function monkeyPatchSavedChildren( data ) {
				return JSON.parse(angular.toJson(data));
			}

			return {
				restrict: 'E',
				scope: true,
				terminal: 'true',
				compile: function compileScopeData( tElement, tAttrs ) {
					if (tAttrs.type !== 'text/zs-scope-data') {
						return angular.noop;
					}

					var parsedData = null;
					var error;
					var text = trim(tElement[0].text);

					try {
						parsedData = text ?
							JSON.parse(text)
							: null;
					} catch (e) {
						error = e;
					}

					// 2017-09-28: $scope data might previously have been saved without sanitizing
					// (including angular $$hashKey), so every subsequent save *might* have created
					// a duplicate. NB: the resulting error could be suppressed by using `track by $index`
					// in the repeater, but it is safer to work with a canonical data signature.
					// Cf. frontend/zaaksysteem/src/js/nl/mintlab/admin/casetype/ChildCaseTypeController.js
					// @see https://mintlab.atlassian.net/browse/ZS-15797
					parsedData = monkeyPatchSavedChildren(parsedData);

					return function linkScopeData( scope, element, attrs ) {
						var parentScope = angular.isDefined(attrs.zsScopeDataRoot) ?
							$rootScope
							// restrict: 'E' always creates new scope
							: scope.$parent;

						if (error) {
							scope.$emit('zs.scope.data.error', parentScope, error);
						} else {
							for (var key in parsedData) {
								parentScope[key] = parsedData[key];
							}

							if (attrs.zsScopeDataLoad !== undefined) {
								$parse(attrs.zsScopeDataLoad)(scope, {
									'$data': parsedData
								});
							}

							scope.$emit('zs.scope.data.apply', parentScope, parsedData);
						}
					};
				}
			};
		}
	]);
