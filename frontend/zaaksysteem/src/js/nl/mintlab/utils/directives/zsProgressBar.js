/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsProgressBar', [ function ( ) {
			
			return {
				restrict: 'E',
				replace: false,
				templateUrl: '/partials/directives/case/case-progress.html',
				scope: {
					progress: '@'
				}
			};
			
		}]);
	
})();
