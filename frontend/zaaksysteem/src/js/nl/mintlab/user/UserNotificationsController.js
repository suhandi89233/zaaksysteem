/*global angular,fetch*/
(function () {

    angular.module('Zaaksysteem.user')
        .controller('nl.mintlab.user.UserNotificationsController', [ '$scope', 'smartHttp', 'translationService', 'systemMessageService', function ( $scope, smartHttp, translationService, systemMessageService ) {
        $scope.$on('form.submit.success', function ( ) {
            $scope.$emit('systemMessage', {
                type: 'info',
                content: translationService.get('Notificatie instellingen opgeslagen.')
            });
        });
    }]);
}());
