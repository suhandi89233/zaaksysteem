/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.pip')
		.controller('nl.mintlab.pip.PipCommunicationController', [ '$scope', '$element', function ( $scope, $element ) {
            var fullscreen = false;
            
            document.body.className = (document.body.className || '') + ' communication';
            
            $scope.isFullscreen = function () {
                return fullscreen;
            }

            $scope.toggleFullscreen = function () {
                fullscreen = !fullscreen;
            }
		}]);
})();
