/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.pip')
		.controller('nl.mintlab.pip.PipFeedbackController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.loading = false;
			$scope.feedbackEmail = false;
			
			$scope.submitFeedback = function ( ) {
				var message = $scope.message,
					caseId = $scope.caseId,
					feedbackEmail = $scope.feedbackEmail;
					
				$scope.loading = true;
					
				smartHttp.connect({
					method: 'POST',
					url: '/api/message/create',
					data: {
						message: message,
						feedback_email: feedbackEmail,
						case_id: caseId
					}
				})
					.success(function ( ) {
						$scope.$emit('systemMessage', {
							content: translationService.get('Uw feedback is verstuurd naar de behandelaar van deze zaak'),
							type: 'info'
						});
						$scope.message = '';
						$scope.feedback.$setPristine();
						$scope.loading = false;
					})
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							content: translationService.get('Er ging helaas iets fout bij het versturen van uw feedback. Probeer het later opnieuw.'),
							type: 'error'
						});
						$scope.loading = false;
					});
						
			};
			
		}]);
	
})();