/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.widget.search.FilterController', [ '$scope', 'searchService', 'systemMessageService', function ( $scope, searchService, systemMessageService ) {
			
			var showShareSettings = false;
			
			$scope.expanded = false;
			$scope.setEditMode = false;
			
			$scope.setExpanded = function ( exp ) {
				$scope.expanded = exp;
			};
			
			$scope.setEditMode = function ( ) {
				$scope.newName = $scope.filter.values.label;
				$scope.editMode = true;
			};
			
			$scope.saveName = function ( ) {
				var newName = $scope.newName;
				if(newName) {
					$scope.filter.values.label = $scope.newName;
					$scope.editMode = false;
					$scope.search.saveFilter($scope.filter);
				} else {
					systemMessageService.emitError('Kan zoekopdracht niet opslaan zonder titel. Voer een titel in en probeer het opnieuw.');
				}
			};
			
			$scope.cancelNameChange = function ( ) {
				$scope.editMode = false;
			};
			
			$scope.onKeyUp = function ( $event ) {
				if($event.keyCode === 27) {
					$scope.cancelNameChange();
				} else if($event.keyCode === 13) {
					$scope.saveName();
				}
			};
			
			$scope.openShareSettings = function ( ) {
				showShareSettings = true;
			};
			
			$scope.closeShareSettings = function ( ) {
				showShareSettings = false;
			};
			
			$scope.isShareSettingsVisible = function ( ) {
				return showShareSettings;
			};
			
			$scope.isSharable = function ( ) {
				return searchService.isSharable($scope.filter);
			};
			
			$scope.isEditable = function ( ) {
				return searchService.isEditable($scope.filter);
			};
			
			$scope.isRemovable = function ( ) {
				return searchService.isRemovable($scope.filter);	
			};
			
			if($scope.filter.id === undefined) {
				$scope.setEditMode();
			}
			
		}]);
	
})();