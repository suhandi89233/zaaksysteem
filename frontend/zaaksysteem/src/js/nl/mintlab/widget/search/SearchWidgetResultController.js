/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetResultController', [ '$scope', '$parse', '$interpolate', 'userService', 'searchService', 'formService', function ( $scope, $parse, $interpolate, userService, searchService, formService ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
				crud = null,
				numRows = 20,
				crudItems = [],
				count = 0,
				maxCount = searchService.getMaxResults();
				
			$scope.displayMode = 'list';
			
			numRows = userService.getSetting('search.numPerPage') || numRows;
			
			$scope.$on('zs.pagination.numrows.change', function ( event, rows ) {
				userService.setSetting('search.numPerPage', rows);
			});

			function getPublicColumns ( cols ) {
				return cols
					.filter(function ( column ) {
						return [ 'actions', 'case.status', 'notifications' ].indexOf(column.id) === -1;
					})
					.map(function ( column ) {

						var template = column.template;

						switch(column.id) {
							case 'case.number':
							case 'case.subject':
							template = undefined;
							break;
						}

						switch(column.attributeType) {
							case 'file':
							template =
							'<ul class="search-column-type-list-list">' +
								'<li class="search-column-type-list-list-item" data-ng-repeat="file in item.values[column.id]">' +
									'<a data-ng-href="' + $scope.getApiBaseUrl() + '/file/<[file.uuid]>"><[file.filename||file.original_name]></a>' +
								'</li>' +
							'</ul>';
							break;
						}

						if(template) {
							column.templateUrl = null;
						}

						return _.assign(column, {
							locked: true,
							sort: false,
							template: template
						});

					});
			}
			
			function rebuildCrud ( ) {
				var form = formService.get('searchWidgetForm'),
					columns = $scope.getActiveColumns();
					
				if(!form || !$scope.activeSearch) {
					return null;
				}
				
				crud = {
					url: $scope.getCrudUrl(),
					options: {
						select: $scope.isPublic() ? 'none' : 'all',
						link: $scope.getItemLink(),
						resolve: $scope.getItemResolve(),
						sort: $scope.getActiveSort()
					},
					actions: $scope.getActions() || [],
					columns: $scope.isPublic() ? getPublicColumns(columns) : columns,
					style: $scope.getItemStyle(),
					numrows: numRows
				};
			}
			
			function hasOpenGroup ( ) {
				return !!_.find($scope.groups, function ( group ) {
					return group.value === $scope.openGroup;
				});
			}
			
			$scope.getCrud = function ( group ) {
				if(group.value !== $scope.openGroup) {
					return null;
				}
				return crud;
			};
			
			$scope.onGroupClick = function ( group ) {
				if($scope.openGroup === group.value) {
					$scope.setOpenGroup(null);
				} else {
					$scope.setOpenGroup(group.value);
				}
			};
			
			$scope.setDisplayMode = function ( mode ) {
				$scope.displayMode = mode;
			};
			
			$scope.getLocations = function ( ) {
				var locations = [],
					mapConfig = $scope.map || {},
					locationResolve = mapConfig.resolve,
					parser = $parse(locationResolve),
					location;
					
				if(!locationResolve) {
					return locations;
				}
				
				_.each(crudItems, function ( crudItem ) {
					var lat,
						lng,
						data = {},
						mapping = mapConfig.mapping || {};

					location = parser(crudItem);
					
					if(location) {
						lat = parseFloat(location.replace('(', '').split(',')[0]);
						lng = parseFloat(location.replace(')', '').split(',')[1]);

						for(var key in mapping) {
							data[key] = $parse(mapping[key])(crudItem);
						}
						
						locations.push({
							item: crudItem,
							marker: {
								latitude: lat,
								longitude: lng,
								popup_data: data,
								no_popup: 1
							}
						});
					}
				});
				
				return locations;
			};
			
			$scope.handleCrudDataChange = function ( $response ) {
				count = $response.num_rows;
			};
			
			$scope.getNumResults = function ( ) {
				var countLabel = count;
				
				if(count > maxCount) {
					countLabel = maxCount + '+';
				}
				
				return countLabel;
			};
			
			$scope.hasResults = function ( ) {
				return !$scope.isLoading() && !isNaN(count) && hasOpenGroup();
			};
			
			this.exceedsLimit = function ( ) {
				return searchService.getMaxResults() < count;
			};
			
			$scope.$on('activeSearch.change', function ( ) {
				$scope.displayMode = 'list';
			});
			
			$scope.$watch('groups', function onGroupChange ( ) {
				
				if(!hasOpenGroup()) {
					$scope.setOpenGroup($scope.groups.length === 1 ? $scope.groups[0].value : null);
				}
				
			}, true);
			
			$scope.$watch(function onChange ( ) {
				rebuildCrud();
			});
			
			$scope.$on('zs.pagination.numrows.change', function onNumRowsChange ( event, nr ) {
				safeApply($scope, function ( ) {
					numRows = nr;
				});
			});
			
			$scope.$on('zs.crud.item.change', function ( event, items ) {
				count = NaN;
				crudItems = items;
			});
			
		}]);
	
})();
