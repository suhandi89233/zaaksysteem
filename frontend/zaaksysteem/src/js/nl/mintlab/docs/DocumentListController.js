/* global angular,_,$,isSelectionReset */
// ZS-TODO: refactor module and declare isSelectionReset in the controller
angular
	.module('Zaaksysteem.docs')
	.controller('nl.mintlab.docs.DocumentListController', [
		'$rootScope', '$scope', '$interval', '$q', '$timeout', '$window',
		'smartHttp', 'translationService', 'systemMessageService', 'snackbarService', 'sessionService',
		function ( $rootScope, $scope, $interval, $q, $timeout, $window,
				   smartHttp, translationService, systemMessageService, snackbarService, sessionService ) {
			// ZS-TODO: (dario) refactor to use different controllers for
			//          list,intake,trash, using js inheritance
			var File = window.zsFetch('nl.mintlab.docs.File'),
				Folder = window.zsFetch('nl.mintlab.docs.Folder'),
				userResource = sessionService.createResource($scope),
				indexOf = _.indexOf,
				words,
				cutFiles = [],
				_url = 'file/',
				renewLockPromise;
			var resetSelection = false;

			$scope.root = null;
			$scope.viewType = 'listView';
			$scope.selectedFiles = [];
			$scope.userId = userResource.data().instance.logged_in_user.uuid;
			$scope.maxDepth = 1;
			$scope.dragging = false;
			$scope.accepting = false;

			function clearData() {
				var ids = _.map($scope.selectedFiles, 'id'),
					fileList,
					files;

				$scope.deselectAll();
				fileList = getFlatFileList();
				ids = _.intersection(ids, _.map(fileList, 'id'));
				files = _(fileList)
					.filter(function ( file ) {
						return ids.indexOf(file.id) !== -1;
					})
					.value();
				_.each(files, $scope.selectEntity);
			}

			$scope.onToggleClick = function ( event, entity ) {
				var isFolder = entity instanceof Folder;

				resetSelection = false;

				if ($scope.isSelected(entity) && isFolder) {
					$scope.deselectEntity(entity, true);
				} else if ($scope.isSelected(entity) && !isFolder) {
					$scope.deselectEntity(entity, false);
				} else if (!$scope.isSelected(entity) && isFolder) {
					$scope.selectEntity(entity, true);
				} else if (!$scope.isSelected(entity) && !isFolder) {
					$scope.selectEntity(entity, false);
				}

				if (event) {
					event.stopPropagation();
				}
			};

			$scope.onEntityClick = function ( event, entity ) {
				resetSelection = false;

				if ($scope.isSelected(entity) && $scope.selectedFiles.length === 1) {
					$scope.deselectEntity(entity);
				} else {
					if ((entity.lock_subject_id === null || entity.lock_subject_id === $scope.userId) || entity instanceof Folder) {
						$scope.deselectAll();
						$scope.selectEntity(entity);
					}
				}
			};

			$scope.onSelectAllClick = function ( /*event*/ ) {
				resetSelection = false;

				if ($scope.isSelected($scope.root)) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};

			$scope.selectEntity = function ( entity, recursive ) {
				if (!entity.getSelected()) {
					entity.setSelected(true);

					if (entity !== $scope.root && (entity.lock_subject_id === null || entity.lock_subject_id === $scope.userId || entity instanceof Folder)) {
						$scope.selectedFiles.push(entity);
					}
				}

				if (recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.selectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.selectEntity(value);
					});
				}

				setRootState();
			};

			$scope.deselectEntity = function ( entity, recursive ) {
				var selectedFiles = $scope.selectedFiles,
					index = indexOf(selectedFiles, entity);

				if (entity.getSelected()) {
					entity.setSelected(false);

					if (entity !== $scope.root) {
						selectedFiles.splice(index, 1);
					}
				}

				if (recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.deselectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.deselectEntity(value);
					});
				}

				$scope.root.setSelected(false);
			};

			$scope.selectAll = function () {
				$scope.selectEntity($scope.root, true);
			};

			$scope.deselectAll = function () {
				$scope.deselectEntity($scope.root, true);

				while ($scope.selectedFiles.length) {
					$scope.deselectEntity($scope.selectedFiles[0]);
				}

				isSelectionReset = true;
			};

			$scope.isSelected = function ( entity ) {
				if (!entity) {
					return null;
				}

				return entity.getSelected();
			};

			$scope.moveEntity = function ( entities, target ) {
				angular.forEach(entities, function ( entity ) {
					var currentParent = entity.getParent(),
						directoryId = target.getDepth() > 0 ? target.id : null;

					if (validateEntity(entity, target)) {
						target.add(entity);
					}

					entity.updating = true;

					if (entity.getEntityType() === 'file') {
						smartHttp
							.connect({
								url: 'file/update/',
								method: 'POST',
								data: {
									file_id: entity.id,
									directory_id: directoryId
								},
								blocking: false
							})
							.success(function () {
								entity.directory_id = target.id;
								entity.updating = false;
							})
							.error(function () {
								currentParent.add(entity);
								entity.updating = false;
							});
						/* Assume we're moving a directory instead */
					} else {
						if (entity.id === directoryId) {
							entity.updating = false;
							return;
						}

						smartHttp
							.connect({
								url: 'api/case/' + $scope.caseId + '/directory/move_directory',
								method: 'POST',
								blocking: false,
								data: {
									directory_id: entity.id,
									parent_directory_id: directoryId
								}
							})
							.success(function () {
								target.add(entity);
								entity.updating = false;
							})
							.error(function () {
								currentParent.add(entity);
								entity.updating = false;
							});
					}
				});
			};

			$scope.removeEntity = function ( entities ) {
				var toDelete = entities.concat(),
					folders = _.filter(entities, function ( entity ) {
						return entity instanceof Folder;
					}),
					files;

				_.each(folders, function ( folder ) {
					var children = folder.getFiles();
					toDelete = toDelete.concat(children);
				});

				files = _.filter(toDelete, function ( entity ) {
					return entity instanceof File;
				});

				_.each(toDelete, function ( entity ) {
					if ($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
				});

				_.each(files, function ( file ) {
					var parent = file.getParent();

					if ($scope.caseId) {
						$scope.trash.add(file);
					} else {
						parent.remove(file);
					}
				});

				return smartHttp
					.connect({
						url: '/api/bulk/file/update',
						method: 'POST',
						data: {
							files: _(files)
								.map(function ( file ) {
									return {
										id: file.id,
										action: 'update_properties',
										data: {
											deleted: true
										}
									};
								})
								.mapKeys('id')
								.omit('id')
								.value()
						},
						blocking: false
					})
					.success(function ( response ) {
						_(response.result)
							.filter({ result: 'success' })
							.each(function ( r ) {
								var file = _.find(files, { id: r.file_id });
								if (file) {
									file.updateWith(r.data);
								}
							});

						if (_.countBy(response.result, { result: 'error' }) > 0) {
							throw new Error();
						}
					})
					['finally'](function () {
					return $q.all(folders.map(function ( folder ) {
						return smartHttp.connect({
							url: 'directory/delete/',
							method: 'POST',
							data: {
								directory_id: folder.id
							},
							blocking: false
						});
					}));
				})
					['catch'](function () {
					throw systemMessageService.emitError('Niet alle bestanden konden worden verwijderd. Probeer het later opnieuw.');
				})
					['finally'](function () {
					$scope.reloadData();
				});
			};

			$scope.restoreEntity = function ( entities ) {
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					$scope.list.add(entity);

					if ($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}

					smartHttp
						.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: entity.id,
								deleted: false
							},
							blocking: false
						})
						.success(function () {
							entity.deleted = false;
						})
						.error(function () {
							$scope.trash.add(entity);
						});
				});
			};

			$scope.destroyEntity = function ( entities ) {
				entities = entities.concat();

				angular.forEach(entities, function ( entity ) {
					entity.getParent().remove(entity);

					if ($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}

					smartHttp
						.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: entity.id,
								destroyed: true
							},
							blocking: false
						})
						.success(function () {
							// ZS-TODO: check if something should happen here or remove this handler
						})
						.error(function ( response ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get(response.messages.join(""))
							});

							$scope.trash.add(entity);
						});
				});

			};

			$scope.acceptFile = function ( files ) {
				$scope.accepting = true;

				var fileList = getFlatFileList(),
					toAccept = _.filter(files, function ( file ) {
						return Number(file.is_revision) !== 1;
					}),
					toUpdate = _.difference(files, toAccept),
					actions;

				files = files.concat();

				if (_.uniq(toUpdate, 'is_duplicate_of').length < toUpdate.length) {
					systemMessageService.emitError('U kunt niet meerdere versies van hetzelfde bestand tegelijkertijd accepteren.');
					return;
				}

				_.each(files, function ( file ) {
					file.updating = true;

					if ($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}
				});

				_.each(toUpdate, function ( file ) {
					var fileToUpdate = _.find(fileList, function ( f ) {
						return f.id === file.is_duplicate_of || f.root_file_id === file.is_duplicate_of;
					});

					file.getParent().remove(file);

					// if it doesn't exist, it's deleted
					if (fileToUpdate) {
						fileToUpdate.updating = true;
					} else {
						$scope.list.add(file);
					}

				});

				_.each(toAccept, function ( file ) {
					$scope.list.add(file);
				});

				// rebuild file list after
				fileList = getFlatFileList();

				actions = _.map(toUpdate, function ( file ) {
					return {
						id: file.is_duplicate_of,
						action: 'update_file',
						data: {
							existing_file_id: file.id
						}
					};
				}).concat(_.map(toAccept, function ( file ) {
					return {
						id: file.id,
						action: 'update_properties',
						data: {
							accepted: true
						}
					};
				}));

				actions = _(actions)
					.mapKeys('id')
					.mapValues(function ( action ) {
						return _.omit(action, 'id');
					})
					.value();

				smartHttp
					.connect({
						url: '/api/bulk/file/update',
						method: 'POST',
						data: {
							files: actions
						},
						blocking: false
					})
					.success(function ( response ) {
						var statuses = response.result;

						_.each(statuses, function ( status ) {

							var fileId = Number(status.file_id),
								result = status.result,
								file,
								data = status.data;

							if (result === 'error') {
								systemMessageService.emitSaveError('uw wijzigingen');

								var fail = _.find(data.result, function ( resultItem ) {
									return resultItem.type === 'file/lock/file_locked';
								});

								if (fail) {
									snackbarService.error('Kan een of meerdere documenten niet accepteren. Een of meerdere documenten met dezelfde bestandsnaam wordt op dit moment bewerkt.');
								}
							} else {
								file = _.find(fileList, status.action === 'update_file' ? { root_file_id: data.root_file_id } : { id: fileId });

								// if it doesn't exist, it's deleted
								if (file) {
									file.updating = false;
									file.updateWith(data);

									if (file.destroyed) {
										file.getParent().remove(file);
									}
								}
							}
						});

						$scope.$emit('file.accept');
					})
					.error(function () {
						systemMessageService.emitSaveError('uw wijzigingen');
					})
					['finally'](function () {
					$scope.reloadData();
					$scope.accepting = false;
				});
			};

			$scope.rejectFile = function ( files, rejectionReason, rejectToQueue ) {
				rejectionReason = rejectionReason || 'niet opgegeven';
				files = files.concat();

				angular.forEach(files, function ( file ) {
					if ($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}

					$scope.intake.remove(file);
					smartHttp
						.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: file.id,
								accepted: false,
								rejection_reason: rejectionReason,
								reject_to_queue: rejectToQueue
							},
							blocking: false
						})
						.success(function () {
							$scope.$emit('file.reject', file);
						})
						.error(function () {
							$scope.intake.add(file);
						});
				});
			};

			$scope.canCopyAsPdf = function ( file ) {
				return file.getEntityType() === 'file'
					&& [
						'pdf',
						'csv',
						'xls',
						'xlsx'
					].indexOf(file.extension_dotless) === -1;
			};

			$scope.copyFileAsPdf = function ( file ) {
				file.updating = true;

				snackbarService.wait('Bestand wordt gekopiëerd', {
					promise: smartHttp
						.connect({
							method: 'POST',
							url: '/zaak/' + $scope.caseId + '/document/' + file.id + '/copy',
						})
						.then(function () {
							return $scope.reloadData();
						})
						.finally(function () {
							file.updating = false;
						}),
					catch: function ( data ) {
						var type = _.get(data, 'data.result[0].type', 'unknown'),
							msg = 'Er ging iets fout bij het kopiëren van het document. Neem contact op met uw beheerder voor meer informatie.';

						if (type === 'documents/copy/pdf') {
							msg = 'Het is niet mogelijk om bestanden van dit type te kopiëren als PDF';
						}

						return msg;
					},
					then: function () {
						return 'Bestand gekopiëerd';
					}
				});
			};

			$scope.assignCaseDoc = function ( files, caseDoc ) {
				angular.forEach(files, function ( file ) {
					var data = {
							file_id: file.id
						},
						ids = _.map(file.case_documents, function ( cd ) {
							return cd.id;
						});

					if (indexOf(ids, caseDoc.id) !== -1) {
						return;
					}

					ids.push(caseDoc.id);
					file.case_documents.push(caseDoc);
					data.case_document_ids = ids;

					smartHttp
						.connect({
							url: 'file/update',
							method: 'POST',
							data: data,
							blocking: false
						})
						.success(function () {
							$scope.reloadData();
							$scope.$emit('file.update', { files: [file] });
						})
						.error(function () {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het toewijzen van het kenmerk')
							});
							file.case_documents = _.without(file.case_documents, caseDoc);
						});
				});
			};

			$scope.moveCaseDoc = function ( from, to, caseDoc ) {
				var data = {
						to: to.id,
						from: from.id,
						case_document_id: caseDoc.id
					},
					ids;

				// Return if the ID already exists in the target file
				ids = _.map(to.case_documents, function ( cd ) {
					return cd.id;
				});

				if (indexOf(ids, caseDoc.id) !== -1) {
					return;
				}

				// Process the update in the UI
				from.case_documents = _.filter(from.case_documents, function ( cd ) {
					return cd.id !== caseDoc.id;
				});

				to.case_documents.push(caseDoc);

				// Process remote
				smartHttp
					.connect({
						url: 'file/move_case_document',
						method: 'POST',
						data: data,
						blocking: false
					})
					.success(function () {
						$scope.reloadData();

						$scope.$emit('file.update', { files: [from, to] });
					})
					.error(function () {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het toewijzen van het kenmerk')
						});

						from.case_documents.push(caseDoc);
						to.case_documents = _.without(to.case_documents, caseDoc);
					});
			};

			$scope.removeCaseDoc = function ( files, caseDoc ) {
				angular.forEach(files, function ( file ) {
					var data = {
							file_id: file.id
						},
						ids = _.map(file.case_documents, function ( cd ) {
							return cd.id;
						}),
						index = indexOf(ids, caseDoc.id);

					if (index === -1) {
						return;
					}

					ids.splice(index, 1);
					file.case_documents = _.filter(file.case_documents, function ( cd ) {
						return cd.id !== caseDoc.id;
					});
					data.case_document_ids = ids;

					smartHttp
						.connect({
							url: 'file/update',
							method: 'POST',
							data: data,
							blocking: false
						})
						.success(function () {
							$scope.reloadData();
							$scope.$emit('file.update', { files: [file] });
						})
						.error(function () {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het verwijderen van het kenmerk')
							});
							file.case_documents.push(caseDoc);
						});
				});
			};

			$scope.clearCaseDocs = function ( files ) {
				angular.forEach(files, function ( file ) {
					var caseDocs = file.case_documents;

					file.case_documents = [];

					smartHttp
						.connect({
							url: 'file/update',
							method: 'POST',
							data: {
								file_id: file.id,
								case_document_ids: []
							},
							blocking: false
						})
						.success(function () {
							$scope.reloadData();
							$scope.$emit('file.update', { files: [file] });
						})
						.error(function () {
							file.case_documents = caseDocs;
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het verwijderen van het kenmerk')
							});
						});
				});
			};

			$scope.handleCaseDocClick = function ( caseDoc/*, $event*/ ) {
				var files = $scope.selectedFiles.concat();

				if (!caseDoc) {
					$scope.clearCaseDocs(files);
				} else {
					if (!$scope.hasCaseDoc(files, caseDoc)) {
						$scope.assignCaseDoc(files, caseDoc);
					} else {
						$scope.removeCaseDoc(files, caseDoc);
					}
				}
			};

			$scope.hasCaseDoc = function ( files, caseDoc ) {
				var i,
					l;

				function has( file ) {
					if (!caseDoc) {
						return file.case_documents && file.case_documents.length === 0;
					}

					return _.filter(file.case_documents, function ( cd ) {
						return caseDoc.id === cd.id;
					}).length > 0;
				}

				for (i = 0, l = files.length; i < l; ++i) {
					if (!has(files[i])) {
						return false;
					}
				}

				return true;
			};

			$scope.getCaseDocAssignment = function ( caseDoc ) {
				var id = caseDoc ? caseDoc.id : null,
					flatFileList = getFlatFileList(),
					i,
					l,
					file;

				for (i = 0, l = flatFileList.length; i < l; ++i) {
					file = flatFileList[i];
					if (file.case_type_document_id && file.case_type_document_id.id === id) {
						return file;
					}
				}

				return null;
			};

			$scope.filterEntity = function ( entity ) {
				var name = entity.name,
					desc = entity.desc,
					type = entity.type ? entity.type.name : '',
					i,
					l,
					j,
					m,
					toTest = [],
					word,
					match;

				if (resetSelection) {
					$scope.deselectEntity(entity, true);
				}

				if ($scope.filter.query === '') {
					return true;
				}

				if (name) {
					toTest.push(name.toLowerCase());
				}

				if (desc) {
					toTest.push(desc.toLowerCase());
				}

				if (type) {
					toTest.push(type.toLowerCase());
				}

				for (i = 0, l = words.length, m = toTest.length; i < l; ++i) {
					word = words[i].toLowerCase();
					match = false;

					for (j = 0; j < m; ++j) {
						if (toTest[j].indexOf(word) !== -1) {
							match = true;
							break;
						}
					}

					if (!match) {
						return false;
					}
				}

				return true;
			};

			$scope.getFilteredChildren = function ( source ) {
				var filtered = [];

				function filterChildren( parent, filterRoot ) {
					var i,
						l,
						children,
						entity;

					if (filterRoot && $scope.filterEntity(parent)) {
						filtered.push(parent);
					}

					children = parent.getFolders();

					for (i = 0, l = children.length; i < l; ++i) {
						entity = children[i];

						if ($scope.filterEntity(entity)) {
							filtered.push(entity);
						}

						filterChildren(entity, true);
					}

					children = parent.getFiles();

					for (i = 0, l = children.length; i < l; ++i) {
						entity = children[i];

						if ($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
					}
				}

				if (!source) {
					source = $scope.root;
				}

				filterChildren(source, false);

				return filtered;
			};

			$scope.getAttachments = function () {
				var attachments = [],
					selection = $scope.selectedFiles.concat(),
					file;

				for (var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if (file.getEntityType() === 'file') {
						attachments.push(file);
					}
				}

				return attachments;
			};

			$scope.clearCutSelection = function () {
				cutFiles.length = 0;
			};

			$scope.cutFiles = function ( files ) {
				var i,
					l,
					file;

				$scope.clearCutSelection();

				for (i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if (file.getEntityType() === 'file') {
						cutFiles.push(file);
					}
				}
			};

			$scope.pasteFiles = function () {
				var target,
					files = $scope.selectedFiles;

				if (files.length && files[0].getEntityType() === 'folder') {
					target = files[0];
				} else if (files.length) {
					target = files[0].getParent();
				}

				$scope.moveEntity(cutFiles, target);
				$scope.clearCutSelection();
			};

			$scope.isCut = function ( file ) {
				return indexOf(cutFiles, file) !== -1;
			};

			$scope.hasCutFiles = function () {
				return cutFiles && cutFiles.length;
			};

			$scope.isCuttable = function () {
				var files = $scope.selectedFiles,
					i,
					l,
					file;

				for (i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if (file.getEntityType() === 'file') {
						return true;
					}
				}

				return false;
			};

			$scope.isPastable = function () {
				return cutFiles.length;
			};

			/* Kind of duplicate with hasFilesInSubdirectories, which doesn't
			 * really look like it does what the box promises */
			$scope.hasHiddenFilesInSelection = function () {
				var selection = $scope.selectedFiles,
					file,
					root = $scope.root,
					entity;

				for (var i = 0, l = selection.length; i < l; ++i) {
					entity = selection[i];

					if (entity.getEntityType() === 'folder') {
						if (entity.getFolders().length > 0 || entity.getFiles().length > 0) {
							return true;
						}
					}
				}

				return false;
			};

			$scope.hasFilesInSubdirectories = function () {
				var selection = $scope.selectedFiles || [],
					file,
					root = $scope.root;

				for (var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if (file.getEntityType() === 'file' && file.getParent() !== root) {
						return true;
					}
				}

				return false;
			};

			$scope.downloadFile = function ( fileId, as, $event ) {
				var isIntake = $scope.isIntake,
					url = isIntake ? ('/zaak/intake/' + fileId + '/download') : ('/zaak/' + $scope.caseId + '/document/' + fileId + '/download');

				if (as) {
					url += '/' + as;
				}

				if ($scope.pip) {
					url = '/pip' + url;
				}

				$window.open(url);

				if ($event) {
					$event.stopPropagation();
				}
			};

			$scope.downloadZip = function ( files, $event ) {
				var toDownload = _.filter(files, function ( entity ) {
						return entity.getEntityType() === 'file';
					}),
					url = '/api/case/' + $scope.caseId + '/file/download/?format=zip&';

				url += _.map(toDownload, function ( file ) {
					return 'file=' + file.id;
				}).join('&');

				$window.open(url);

				if ($event) {
					$event.stopPropagation();
				}
			};

			$scope.downloadAllFiles = function () {
				$window.open('/api/case/' + $scope.caseId + '/file/download/?format=zip');
			};

			$scope.isEditable = function ( file ) {
				var allowedExtensions = $scope.externalDocumentGenerator ? ['odt', 'docx', 'doc'] : ['odt'];

				return !$scope.readOnly && allowedExtensions.indexOf(file.extension_dotless) !== -1;
			};

			function renewLock( fileId ) {
				smartHttp
					.connect({
						method: 'GET',
						url: '/file/' + fileId + '/lock/extend'
					})
					.error(function () {
						snackbarService.error('Het is niet gelukt om het document geblokkeerd te houden. Neem contact op met uw beheerder voor meer informatie.');
					});
			};

			$scope.isLocallyEditable = function (file) {
				return Boolean(file.locally_editable);
			};

			$scope.openLocally = function (file) {
				smartHttp.connect({
					method: 'GET',
					url: '/file/' + file.id + '/generate_edit_invitation',
				}).success(function(response) {
					$window.location.href = response.result;
				}).error(function(response) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Het document kon niet lokaal worden geopend om te bewerken. Neem contact op met uw beheerder voor meer informatie.')
					});
				});
			};

			$scope.isSignable = function (file) {
				return Boolean(file.signable);
			};

			$scope.signDocument = function (file) {
                snackbarService.wait(translationService.get('Document wordt aangeboden, een moment geduld alstublieft.'), {
                    promise: smartHttp
                    .connect({
                        method: 'POST',
                        url: '/file/' + file.id + '/request_signature',
                    }),
                    catch: function ( data ) {
                        return translationService.get('Document kon niet aangeboden worden om te ondertekenen. Neem contact op met uw beheerder voor meer informatie.');
                    },
                    then: function () {
                        return translationService.get('Document is aangeboden aan de dienst voor ondertekenen.');
                    }
                });
			}

			$scope.handleEditClick = function ( cb ) {
				if ($scope.externalDocumentGenerator) {
					smartHttp
						.connect({
							method: 'GET',
							url: '/file/edit_file',
							params: {
								file_id: $scope.selectedFiles[0].id,
								interface_module: $scope.externalDocumentGenerator
							}
						})
						.success(function ( response ) {
							$window.open(response.redirect_url);
						})
						.error(function () {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Het document kon niet worden geopend om te bewerken. Neem contact op met uw beheerder voor meer informatie.')
							});
						});
				} else {
					smartHttp
						.connect({
							method: 'GET',
							url: '/file/' + $scope.selectedFiles[0].id + '/lock/acquire'
						})
						.success(function () {
							cb();
							// on 10 min interval, renew lock
							renewLockPromise = $interval(function () {
								renewLock($scope.selectedFiles[0].id)
							}, 600000);
						})
						.error(function () {
							snackbarService.error('Het document kan niet bewerkt worden omdat het blokkeren niet is gelukt. Neem contact op met uw beheerder voor meer informatie.');
						});
				}
			};

			$scope.$on('popupclose', function ( event, element, file ) {
				if (element[0].className.indexOf('modal-webodf') !== -1) {
					$interval.cancel(renewLockPromise);

					smartHttp
						.connect({
							method: 'GET',
							url: '/file/' + $scope.selectedFiles[0].id + '/lock/release'
						})
						.catch(function ( response ) {
							snackbarService.error('Het is niet gelukt om het document weer vrij te geven. Het document zal over maximaal 15 minuten weer bewerkbaar zijn voor andere gebruikers.');
						});
				}
			});

			function validateEntity( entity/*, target*/ ) {
				if (entity instanceof Folder) {
					return false;
				}

				return true;
			}

			function moveEntities( targetFolder, data ) {
				var dropData = data,
					selectedFiles = $scope.selectedFiles;

				if (indexOf(selectedFiles, dropData) === -1) {
					$scope.moveEntity([dropData], targetFolder);
				} else {
					$scope.moveEntity(selectedFiles, targetFolder);
				}
			}

			function setRootState() {
				var files = $scope.root.getFiles(),
					folders = $scope.root.getFolders(),
					isSelected;

				if (files.length || folders.length) {
					isSelected = checkSelected($scope.root);
				} else {
					isSelected = $scope.root.getSelected();
				}

				$scope.root.setSelected(isSelected);
			}

			function checkSelected( parent ) {
				var files = parent.getFiles(),
					folders = parent.getFolders(),
					i,
					l;

				for (i = 0, l = folders.length; i < l; ++i) {
					if (!folders[i].getSelected() || !checkSelected(folders[i])) {
						return false;
					}
				}

				for (i = 0, l = files.length; i < l; ++i) {
					if (!files[i].getSelected()) {
						return false;
					}
				}

				return true;
			}

			function getFlatFileList() {
				var files = [];

				function getChildrenOf( parent ) {
					var children = parent.getFolders(),
						i,
						l;

					files = files.concat(parent.getFiles());

					for (i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}

				getChildrenOf($scope.root);
				getChildrenOf($scope.list);

				return _.uniq(files);
			}

			$scope.$watch('filter.query', function (newValue, oldValue) {
				if (newValue !== oldValue) {
					words = $scope.filter.query.split(' ');
					resetSelection = true;
				}
			});

			$scope.$on('zsSelectableList:change', function ( scope, selection ) {
				if (selection) {
					selection = selection.concat();
					$scope.deselectAll();
					_.each(selection, function ( entity ) {
						$scope.selectEntity(entity);
					});
				}
			});

			$scope.$on('drop', function ( event, data, mimeType ) {
				var targetFolder = event.targetScope.entity;

				if (mimeType !== 'Files') {
					moveEntities(targetFolder, data);
				}

				$scope.dragging = false;
				$scope.$apply();
			});

			$scope.$on('reload', function () {
				clearData();
			});

			$scope.$on('fileselect', function ( event, files ) {
				$scope.uploadFile(files);
			});

			$scope.$on('startdrag', function ( event, element, mimeType ) {
				// can't change the dom immediately, causes a stopdrag
				$scope.$apply(function () {
					$timeout(function () {
						var entity = event.targetScope.entity;

						if (entity && !$scope.isSelected(entity)) {
							$scope.deselectAll();
							$scope.selectEntity(entity);
						}

						$scope.dragging = mimeType;
					}, 0);
				});
			});

			$scope.$on('stopdrag', function () {
				$scope.dragging = '';
				$scope.$apply();
			});

			$scope.$on('contextmenuopen', function ( event ) {
				var entity = event.targetScope.entity;

				event.stopPropagation();

				if (!$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			});

			$rootScope.$on('fileFromTemplate', function ( event, file ) {
				if (file.accepted) {
					$scope.list.add(file);
				} else {
					$scope.intake.add(file);
				}

				$('.tab-documents > a').click();
			});
		}]
	);
