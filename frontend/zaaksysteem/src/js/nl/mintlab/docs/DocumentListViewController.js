/*global angular*/
(function () {
		
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentListViewController', [ '$scope', function ( $scope ) {
			
			$scope.sort = 'name';
			$scope.reversed = false;
			
			$scope.sortBy = function ( by ) {
				if($scope.sort === by) {
					$scope.reversed = !$scope.reversed;
				} else {
					$scope.reversed = false;
				}
				$scope.sort = by;
			};
			
			$scope.getSort = function ( ) {
				var sort = '';
				switch($scope.sort) {
					case 'type':
					sort = '[ metadata_id.document_category, extension ]';
					break;

					case 'origin':
					sort = 'metadata_id.origin';
					break;

					case 'origin_date':
					sort = 'metadata_id.origin_date';
					break;
					
					case 'edit':
					sort = 'date_created';
					break;
					
					default:
					sort = $scope.sort;
					break;
				}
				return sort;
			};
			
			
			
		}]);
})();
