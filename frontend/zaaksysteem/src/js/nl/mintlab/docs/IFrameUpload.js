/*global define,document,fetch*/
(function ( ) {
	
	var doc = document;
	
	window.zsDefine('nl.mintlab.docs.IFrameUpload', function ( ) {
		
		var inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
			addEventListener = window.zsFetch('nl.mintlab.utils.events.addEventListener'),
			removeEventListener = window.zsFetch('nl.mintlab.utils.events.removeEventListener'),
			EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
			generateUid = window.zsFetch('nl.mintlab.utils.generateUid');
			
		function getIFrame ( ) {
			var iframe = doc.createElement('iframe');
			iframe.id = iframe.name = generateUid();
			iframe.style.display = 'none';
			doc.body.appendChild(iframe);
			return iframe;
		}
		
		function IFrameUpload ( file ) {
			
			this.completed = false;
			this.error = null;
			this.ended = false;
			this.progress = 0;
			this.loadedBytes = NaN;
			this.totalBytes = NaN;
			this.file = file;
			
			var that = this;
			
			function end ( ) {
				removeEventListener(that.iframe, 'error', onError);
				removeEventListener(that.iframe, 'error', onLoad);
				that.ended = true;
				that.getData();
				that.publish('end');
				doc.body.removeChild(that.iframe);
				that.iframe = null;
			}
			
			function triggerLoad ( ) {
				that.progress = 1;
				that.publish('progress', that, that.progress);
				that.completed = true;
				that.publish('complete', that, that.getData());
				end();
			}
			
			function triggerError ( ) {
				that.progress = 1;
				that.publish('progress', that, that.progress);
				that.error = true;
				that.publish('error', that, that.getData());
				that.completed = true;
				that.publish('complete', that, that.getData());
				end();
			}
			
			function onLoad ( /*event*/ ) {
				if(!that.getData().result) {
					triggerError();
				} else {
					triggerLoad();
				}
			}
			
			function onError ( /*event*/ ) {
				triggerError();
			}
			
			this.send = function ( url, params ) {
				var iframe = getIFrame(),
					form = this.file.form,
					input,
					hiddenFields = [];
				
				this.iframe = iframe;
					
				for(var id in params) {
					input = doc.createElement('input');
					input.type = 'hidden';
					input.name = id;
					input.value = params[id];
					hiddenFields.push(input);
					form.appendChild(input);
				}
				
				form.target = iframe.name;
				form.submit();
				
				for(var i = 0, l = hiddenFields.length; i < l; ++i) {
					form.removeChild(hiddenFields[i]);	
				}
				
				addEventListener(iframe, 'error', onError);
				addEventListener(iframe, 'load', onLoad);
				
			};
			
			this.abort = function ( ) {
				this.error = true;
				this.publish('error', this, null);
				end();
			};
			
			this.getData = function ( ) {
				if(this.data === undefined) {
					var body = (this.iframe.contentDocument || this.iframe.contentWindow.document).body,
						content = 'textContent' in body ? body.textContent : body.innerText;
					
					try {
						this.data = JSON.parse(content);
					} catch ( error ) {
						this.data = {};
					}
				}
				return this.data;
			};
		}
		
		inherit(IFrameUpload, EventDispatcher);
		
		return IFrameUpload;
		
	});
	
})();
