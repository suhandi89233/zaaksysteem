/*global angular, _, console*/
(function ( ) {
	angular.module('Zaaksysteem.docs')
		.directive('zsCaseDocumentOdfEditor', [ '$http', '$interpolate', '$interval', 'WebODFCanvas', 'systemMessageService', '$q', 'snackbarService',  function ( $http, $interpolate, $interval, WebODFCanvas, systemMessageService, $q, snackbarService ) {
			return {
				restrict: 'E',
				scope: {
					'caseId': '&',
					'file': '&',
					'replaceFile': '&',
					'uploadFile': '&',
					'fileList': '&'
				},
				priority: 1,
				template:
					('<div class="zs-spinner-small" data-zs-spinner="caseDocumentOdfEditor.isLoading()"></div>' +
					'<ul class="toolbar clearfix">' +
						'<li ' +
							'data-ng-repeat="item in toolbarItems" ' +
							'data-zs-template="{{caseDocumentOdfEditor.getTemplate(item)}}" ' +
						'>' +
						'</li>' +
					'</ul>').replace(/{{/g, $interpolate.startSymbol())
							.replace(/}}/g, $interpolate.endSymbol()),
				controller: [ '$scope', '$element', 'userService', function ( $scope, $element, userService ) {

					var ctrl = this,
						loading = true, // meteen loading tonen omdat hij meteen het document gaat laden.
						url = '/zaak/' + $scope.caseId() + '/document/' + $scope.file().id + '/download/odt',
						canvas = WebODFCanvas.createCanvas($element),
						templates = {
							fontSize: '<input type="number" data-ng-model="data[item.id]" data-ng-change="item.onClick()"/>',
							dialogButton: '<zs-odf-editor-expandable-button ' +
									'title="<[item.title]>" ' +
									'button-title="<[item.buttonTitle]>" ' +
									'id="<[item.id]>" ' +
									'on-submit="item.onClick($inputValue)"' +
								'></zs-odf-editor-expandable-button>',
							disclaimer: '<button title="<[item.title]>" class="toolbar-button <[item.id | lowercase]>" data-ng-click="item.onClick()" data-ng-class="{ \'toolbar-button-active\': caseDocumentOdfEditor.isButtonActive(item.id) }"></button>' +
								'<div class="toolbar-button-tooltip" data-ng-show="caseDocumentOdfEditor.isButtonActive(item.id)">' +
									'<p>De mogelijkheden met deze ODT editor zijn beperkter dan met een volledige editor. Zie voor meer informatie de <a href="http://wiki.zaaksysteem.nl/wiki/Redirect_zaakdossier_webodf" target="_blank" rel="noopener">Zaaksysteem Wiki</a>.</p>' +
									'<button data-ng-click="item.onClick()" class="btn btn-primary">Ok</button>' +
								'</div>',
							button: '<button title="<[item.title]>" class="toolbar-button <[item.id | lowercase]>" data-ng-click="item.onClick()" data-ng-class="{ \'toolbar-button-active\': caseDocumentOdfEditor.isButtonActive(item.id) }"></button>',
							insertImage: '<zs-case-document-image-uploader on-upload="item.onUpload($fileType,$imageData,$imageWidth,$imageHeight)"' +
									'title="<[item.title]>" ' +
									'id="<[item.id]>" ' +
								'></zs-case-document-image-uploader>'
						},
						toolbarStatusObject = { // for the toggle buttons we keep there states here:
							bold: false,
							italic: false,
							underline: false,
							alignLeft: false,
							alignCenter: false,
							alignRight: false,
							alignJustified: false,
							disclaimer: false
						},
						textChangeHashMap = { // onTextChange hashMap lookup object.
							isBold: 'bold',
							isItalic: 'italic',
							hasUnderline: 'underline',
							isAlignedLeft: 'alignLeft',
							isAlignedRight: 'alignRight',
							isAlignedJustified: 'justify',
							isAlignedCenter: 'alignCenter'
						};

					templates = _.mapValues(
						templates, function ( value, key ) {
							return value.replace(/<\[/g, $interpolate.startSymbol())
								.replace(/\]>/g, $interpolate.endSymbol());
						}
					);

					ctrl.isLoading = function ( ) {
						return loading;
					};
					ctrl.save = function ( ) {
						loading = true;
						var fileObj = $scope.file();
						canvas.getBlob().then(function (blob) {
							$scope.replaceFile({
									$file: fileObj,
									$replace: [ blob ],
									$params: {
										file_id: fileObj.id,
										filename: fileObj.filestore_id.original_name
									}
								})
								.finally(function ( ) {
									loading = false;
									snackbarService.info('Bestand opgeslagen.');
								});
						});
					};
					ctrl.onSaveAs = function (fileName) {

						loading = true;

						if(fileName && fileName.indexOf('.odt') === -1) {
							fileName = fileName + '.odt';
						}
						return canvas.getBlob().then(function (blob) {
							return $scope.uploadFile({
								$files: [blob],
								$params: [{
									filename: fileName
								}]
							});
						})
							.then(function ( ) {
								loading = false;
								snackbarService.info('Bestand opgeslagen.');
							})
							.catch(function (error) {
								loading = false;
								var message = 'Er is iets mis gegaan bij het opslaan. Uw bestand is niet opgeslagen.';
								if(error && error.id === 'upload/filename_exists') {
									message = 'Bestand kan niet worden opgeslagen onder een naam die al bestaat. Voer een unieke naam in.';
								}
								$scope.$emit('systemMessage', {
									type: 'error',
									content: message
								});
								throw error;
							});
					};

					ctrl.insertImage = function (fileType, imageData, imageWidth, imageHeight) {
						canvas.insertImage(fileType, imageData, imageWidth, imageHeight);
					};
					ctrl.onHyperlink = function (url) {
						var deferred = $q.defer();
						if(url !== '' || url.indexOf('.') === -1) {
							canvas.setHyperlink(url);
							deferred.resolve();
						} else {
							deferred.reject({
								id: 'webodf/no_hyperlink_url',
								message: 'No hyperlink url found'
							});
							$scope.$emit('systemMessage', {
								type: 'error',
								content: 'Geen geldige url gevonden. Voer een geldige url in.'
							});
						}
						return deferred.promise;
					};
					ctrl.setFontSize = function ( ) {
						canvas.setFontSize($scope.data.fontSize);
					};
					ctrl.disclaimer = function ( ) {
						toolbarStatusObject.disclaimer = !toolbarStatusObject.disclaimer;
						userService.setSetting('webOdfEditor.disclaimerDismissed', true);
					};
					ctrl.getTemplate = function (item) {
						return item.template;
					};
					ctrl.isButtonActive = function(id) {
						return toolbarStatusObject[id];
					};

					$scope.data = {
						fontSize: 12
					};

					$scope.toolbarItems = [
						{
							title: 'Opslaan',
							id: 'save',
							onClick: ctrl.save
						}, {
							title: 'Opslaan als',
							id: 'saveAs',
							buttonTitle: 'Opslaan',
							onClick: ctrl.onSaveAs
						}, {
							title: 'Undo',
							id: 'undo'
						}, {
							title: 'Redo',
							id: 'redo'
						}, {
							title: 'FontSize',
							id: 'fontSize',
							onClick: ctrl.setFontSize
						}, {
							title: 'Bold',
							id: 'bold'
						}, {
							title: 'Italic',
							id: 'italic'
						}, {
							title: 'Underline',
							id: 'underline'
						}, {
							title: 'Align left',
							id: 'alignLeft'
						}, {
							title: 'Align center',
							id: 'alignCenter'
						}, {
							title: 'Align right',
							id: 'alignRight'
						}, {
							title: 'Justify',
							id: 'justify'
						}, {
							title: 'Outdent',
							id: 'outdent'
						}, {
							title: 'Indent',
							id: 'indent'
						}, {
							title: 'Bulletlist',
							id: 'bulletlist'
						}, {
							title: 'Numberedlist',
							id: 'numberedlist'
						}, {
							title: 'Plaatje invoegen',
							id: 'insertImage',
							onUpload: ctrl.insertImage
						}, {
							title: 'Hyperlink',
							id: 'hyperlink',
							buttonTitle: 'Pas toe',
							onClick: ctrl.onHyperlink
						}, {
							title: 'Disclaimer',
							id: 'disclaimer',
							onClick: ctrl.disclaimer
						}
					];

					function handleTextChange (params) {
						_.forIn(params, function(value, key) {
							toolbarStatusObject[textChangeHashMap[key]] = value;
						});
						if(params.fontSize) {
							$scope.data.fontSize = params.fontSize;
						}

						if(!$scope.$$phase && !$scope.$root.$$phase) {
							$scope.$digest();
						}
					}

					_.each($scope.toolbarItems, function ( item ) {
						var id = item.id,
							template;

						if(!item.onClick) { // if no function is defined, set canvas function from item.id
							item.onClick = canvas[item.id];
						}

						if(id === 'disclaimer') {
							template = templates.disclaimer;
						} else if(id === 'fontSize') {
							template = templates.fontSize;
						} else if(id === 'insertImage') {
							template = templates.insertImage;
						} else if([ 'saveAs', 'hyperlink' ].indexOf(id) !== -1) {
							template = templates.dialogButton;
						} else {
							template = templates.button;
						}

						item.template = template;
					});

					// check whether to show the disclaimer:
					toolbarStatusObject.disclaimer = !userService.getSetting('webOdfEditor.disclaimerDismissed');

					canvas.load(url).then(function ( ) {
						loading = false;
						canvas.onTextChange.push(handleTextChange);
						canvas.onParagraphChange.push(handleTextChange);
					})
						.catch(function() {
							systemMessageService.emit('Er ging iets fout bij het laden van het document.');
						});

					$scope.$on('$destroy', function ( ) {
						canvas.destroy();
						_.pull(canvas.onTextChange, handleTextChange);
						_.pull(canvas.onParagraphChange, handleTextChange);
 					});

					return ctrl;
				}],
				controllerAs: 'caseDocumentOdfEditor'
			};

		}]);
})();
