/*global angular, setTimeout*/
(function ( ) {
	angular.module('Zaaksysteem.docs')
		.directive('zsOdfEditorExpandableButton', [ '$document', '$interpolate', function ($document, $interpolate) {
			return {
				restrict: 'E',
				scope: {
					'title': '@',
					'buttonTitle': '@',
					'id': '@',
					'validate': '&',
					'onSubmit': '&'
				},
				template: 
					('<button title="{{title}}" class="toolbar-button {{id | lowercase}}" data-ng-click="odfEditorExpandableButton.toggleDialog()"></button>' +
					'<div class="toolbar-button-tooltip" data-ng-show="dialogShow">' +
						'<input type="text" data-ng-model="data.input"/>' +
						'<button title="Done" data-ng-click="odfEditorExpandableButton.handleDialogClick()" data-ng-class="{ \'toolbar-button-active\': dialogShow }">' + 
							'{{buttonTitle}}' + 
						'</button>' +
					'</div>').replace(/{{/g, $interpolate.startSymbol())
							.replace(/}}/g, $interpolate.endSymbol()),
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					var ctrl = this;
					
					$scope.data = {
						input: ''
					};
					$scope.dialogShow = false;

					function onDocumentClick (e) {
						if(!$element[0].contains(e.target) || e.target === $element[0]) {
							$scope.$apply(function ( ) {
								$scope.dialogShow = false;
								destroyDialog();
							});
						}
					}
					
					function createDialog ( ) {
						$scope.$$postDigest(function() {
							$element.find('input')[0].focus();
							$document.bind('click', onDocumentClick);
						});
					}

					function destroyDialog ( ) {
						$document.unbind('click', onDocumentClick);
					}

					ctrl.toggleDialog = function ( ) {
						if($scope.dialogShow) {
							destroyDialog();
						} else {
							createDialog();
						}
						$scope.dialogShow = !$scope.dialogShow;
					};
					
					ctrl.handleDialogClick = function ( ) {

						$scope.onSubmit({$inputValue: $scope.data.input})
							.then(function ( ) {
								destroyDialog();
								$scope.dialogShow = false;
							})
					};
					return ctrl;
				}],
				controllerAs: 'odfEditorExpandableButton'
			};
			
		}]);
})();
