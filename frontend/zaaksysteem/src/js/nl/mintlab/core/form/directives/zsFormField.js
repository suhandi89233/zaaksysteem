/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormField', [ '$timeout', 'formValidatorService', function ( $timeout, formValidatorService ) {
			
			var TIMEOUT = 400;
			
			return {
				priority: 100,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0],
							canceler,
							validators;
						
						function commitChange ( ) {
							scope.$emit('form.change.committed', scope.field);
						}
						
						function cancelTimer ( ) {
							if(canceler) {
								$timeout.cancel(canceler);
							}
							canceler = null;
						}
						
						function startTimer ( ) {
							canceler = $timeout(onTimer, TIMEOUT, false);
						}
						
						function onTimer ( ) {
							canceler = null;
							commitChange();
						}
						
						function onKeyUp ( event ) {
							scope.$apply(function ( ) {
								cancelTimer();
								if(event.keyCode === 13 || event.keyCode === 27) {
									commitChange();
								} else {
									startTimer();
								}
							});
						}
						
						function onFocusOut ( ) {
							scope.$apply(commitChange);
						}
						
						if(_.indexOf(['text', 'number', 'email', 'url' ], element.attr('type')) !== -1)  {
							if(attrs.zsSpotEnlighter === undefined) {
								element.bind('focusout', onFocusOut);
							} else {
								ngModel.$viewChangeListeners.push(commitChange);
							}
							element.bind('keyup', onKeyUp);
						} else {
							ngModel.$viewChangeListeners.push(function ( ) {
								commitChange();
							});
						}
						
						scope.$on('$destroy', function ( ) {
							cancelTimer();
							element.unbind('focusout', onFocusOut);
							element.unbind('keyup', onKeyUp);
						});
						 
						validators = scope.$eval(attrs.validators);
						if(validators && validators.length) {
							_.each(validators, function ( type ) {
								var func = function ( value ) {
										var parsed = formValidatorService.validate(type, value);
										ngModel.$setValidity(type, ngModel.$isEmpty(value) || parsed !== undefined);
										return parsed;
									};
								ngModel.$parsers.push(func);
								ngModel.$formatters.push(func);
							});
						}
						
						// TODO: implement configurable autosave
						
					};
					
				}
			};
			
		}]);
	
})();
