/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformObjectTypeFormEdit', [ function ( ) {
			
			return {
				require: [ 'zsCaseWebformObjectTypeFormEdit', 'zsCaseWebformObjectTypeForm', '^zsCaseWebformObjectTypeMutation' ],
				controller: [ function ( ) {
					
					var ctrl = this,
						zsCaseWebformObjectTypeForm,
						zsCaseWebformObjectTypeMutation;
					
					ctrl.setControls = function ( ) {
						zsCaseWebformObjectTypeForm = arguments[0];
						zsCaseWebformObjectTypeMutation = arguments[1];
						zsCaseWebformObjectTypeMutation.setForm(zsCaseWebformObjectTypeForm);
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseWebformObjectTypeFormEdit',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
