/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.notification')
		.controller('nl.mintlab.notification.NotificationListController', [ '$scope', 'notificationService', function ( $scope, notificationService ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');
			
			$scope.notifications = notificationService.getNotifications().concat();
			
			function invalidateScroll ( ) {
				$scope.$broadcast('scrollinvalidate');
			}
			
			$scope.getNumUnread = function ( ) {
				return notificationService.getNumUnread();
			};
			
			$scope.$on('notification.add', function ( /*event, n*/ ) {
				safeApply($scope, function ( ) {
					$scope.notifications = notificationService.getNotifications().concat();
				});
			});
			
			$scope.$on('notification.read', function ( /*n*/ ) {
				safeApply($scope);
			});
			
			$scope.$on('zs-popup-menu.open', function ( ) {
				// notificationService.markAllAsRead();
			});
			
			$scope.$on('scrollend', function ( ) {
				safeApply($scope, function ( ) {
					var promise = notificationService.getMore();
					if(promise) {
						promise.then(invalidateScroll);
					}
				});
				
			});
			
		}]);
	
})();
