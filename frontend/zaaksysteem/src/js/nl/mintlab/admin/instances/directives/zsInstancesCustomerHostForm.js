/*global angular,_*/
(function ( ) {

    angular.module('Zaaksysteem.admin.instances')
        .directive('zsInstancesCustomerHostForm', [ 'systemMessageService', 'zsApi', function ( systemMessageService, api ) {

            return {
                restrict: 'E',
                template:
                    '<div data-zs-form-template-parser data-config="getFormConfig()" zs-form-submit="handleFormSubmit($values)">' +
                    '</div>',
                scope: {
                    'controlpanel' : '&',
                    'host': '&',
                    'instances': '&',
                    'onSubmit': '&',
                    'onComplete': '&',
                    'ips': '&'
                },
                controller: [ '$scope', '$element', function ( $scope, $element ) {

                    var config = {
                            name: 'customerHostForm',
                            fields: [
                                {
                                    name: 'label',
                                    label: 'Titel',
                                    type: 'text',
                                    required: true
                                },
                                {
                                    name: 'fqdn',
                                    label: 'Host',
                                    type: 'text',
                                    required: true
                                },
                                {
                                    name: 'ssl_cert',
                                    label: 'SSL Certificaat',
                                    type: 'textarea'
                                },
                                {
                                    name: 'ssl_key',
                                    label: 'SSL Private Key',
                                    type: 'textarea'
                                },
                                {
                                    name: 'template',
                                    type: 'select',
                                    label: 'Template',
                                    data: {
                                        options: []
                                    }
                                },
                                {
                                    name: 'instance',
                                    type: 'select',
                                    label: 'Omgeving',
                                    data: {
                                        options: []
                                    }
                                }
                            ],
                            actions: [
                                {
                                    name: 'submit',
                                    type: 'submit',
                                    label: 'Opslaan',
                                    disabled: 'isSubmitDisabled()'
                                }
                            ]
                        },
                        loading;

                    function getIpOptions ( ) {

                        return _($scope.ips() || [])
                                .push($scope.host() ? $scope.host().instance.ip : null)
                                .filter(_.identity)
                                .unique()
                                .map(function ( ip ) {
                                    return {
                                        value: ip,
                                        label: ip
                                    };
                                })
                                .value();
                    }

                    function getForm ( ) {
                        var ctrl = $element.children().eq(0).inheritedData('$zsFormTemplateParserController');

                        if(ctrl) {
                            return ctrl.getForm();
                        }
                        return null;
                    }

                    $scope.getFormConfig = function ( ) {
                        return config;
                    };

                    $scope.handleFormSubmit = function ( $values ) {


                        var host = $scope.host() || { type: 'host' },
                            promise;

                        host =
                            _.extend(host, {
                                instance: angular.copy($values)
                            });

                        promise = $scope.onSubmit({
                            $host: host
                        });

                        if(promise) {

                            loading = true;

                            promise
                                .then(function ( ) {
                                    $scope.onComplete();
                                })
                                ['catch'](function ( error ) {

                                    var form = getForm();

                                    if(error && error.type === 'validationexception') {
                                        form.setValidity(api.getLegacyFormValidations(config.fields, error));
                                        systemMessageService.emitValidationError();
                                    } else {
                                        systemMessageService.emitSaveError();
                                    }


                                })
                                ['finally'](function ( ) {
                                    loading = false;
                                });

                        } else {
                            $scope.onComplete();
                        }
                    };

                    $scope.isSubmitDisabled = function ( ) {
                        var form = getForm(),
                            disabled = true;

                        if(form) {
                            disabled = !form.$valid && !loading;
                        }

                        return loading;
                    };

                    var selected;
                    var instance_field = _.find(config.fields, { name: 'instance' });

                    instance_field.data.options =
                        _($scope.instances())
                            .map(function ( instance ) {
                                return {
                                    label: instance.instance.label + ' (' + instance.instance.fqdn + ')',
                                    value: instance.reference
                                };
                            })
                            .value().concat({
                                label: 'Geen',
                                value: null
                            });

                    var template_field = _.find(config.fields, { name: 'template' });

                    var templates = $scope.controlpanel().available_templates;

                    if (templates !== undefined) {
                        template_field.data.options = templates.map(function(template) {
                            return {
                                label : template,
                                value : template
                            };
                        });

                        template_field.data.options.unshift({
                            label: 'Geen',
                            value: null
                        });
                    }
                    else {
                        console.warn("Unable to load templates from controlpanel object");
                    }

                    if($scope.host()) {
                        /* Host exists, load previous values in form */
                        _.each($scope.host().instance, function ( value, key ) {
                            var field = _.find(config.fields, { name: key });

                            if(field) {
                                field.value = value;
                            }
                        });

                        /* Find out where this host belongs to */
                        selected = _.find($scope.instances(), function (instance) {
                            return _.find(instance.instance.hosts.rows, function (host) {
                                return $scope.host() && $scope.host().reference === host.reference;
                            });
                        });

                        if (selected) {
                            instance_field.value = selected.reference;
                        }
                    }
                }]
            };
        }]);
})();
