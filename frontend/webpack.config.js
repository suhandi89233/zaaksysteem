const { join, resolve } = require('path');
const {
    DefinePlugin,
    optimize: {
        DedupePlugin,
        UglifyJsPlugin
    }
} = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

process.env.NODE_ENV = 'production';

module.exports = {
    devtool: 'source-map',
    output: {
        path: join(__dirname, '..', 'root', 'js'),
        publicPath: '/js/',
        filename: '[name].bundle.js'
    },
    entry: {
        zs: './index.js'
    },
    module: {
        loaders: [
            {
                test: /.*\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    // beware imports from client/src... :-(
                    babelrc: false,
                    presets: ['babel-preset-es2015'].map(require.resolve),
                    plugins: ['babel-plugin-add-module-exports'].map(require.resolve)
                }
            },
            {
                test: /.*\.html$/,
                loader: 'html?minimize=false',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
                exclude: /node_modules/
            },
            {
                test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?name=[name].[ext]&limit=10000'
            },
            {
                include: require.resolve('jquery/jquery'),
                loader: 'exports-loader?window.jQuery'
            }
        ]
    },
    resolve: {
        root: [
            resolve('../client'),
            resolve('./')
        ],
        alias: {
            jquery: 'jquery/jquery',
            angular: join(__dirname, 'angular-index.js')
        }
    },
    plugins: [
        new UglifyJsPlugin({
            comments: false,
            compress: {
                warnings: false
            }
        }),
        new DedupePlugin(),
        // beware imports from client/src... :-(
        new DefinePlugin({
            ENV: {
                IS_DEV: false
            }
        }),
        new ExtractTextPlugin('../css/[name].css', {
            allChunks: true
        })
    ],
    stats: {
        assets: true,
        cached: false,
        cachedAssets: false,
        children: false,
        chunks: false,
        chunkModules: false,
        chunkOrigins: false,
        colors: true,
        depth: false,
        entrypoints: false,
        errors: true,
        errorDetails: true,
        hash: false,
        maxModules: 0,
        modules: false,
        moduleTrace: false,
        performance: true,
        providedExports: false,
        publicPath: false,
        reasons: false,
        source: false,
        timings: true,
        usedExports: false,
        version: true,
        warnings: true,
    }
};
