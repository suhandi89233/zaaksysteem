@ECHO OFF

REM ## PLEASE CHANGE THE VALUES BELOW TO THE VALUES FILLED INTO YOUR KOPPELPROFILE
REM For more information, please see http://wiki.zaaksysteem.nl/Koppelprofiel_Scanstraat

REM When editing, see https://en.wikibooks.org/wiki/Windows_Batch_Scripting#Syntax for syntax
REM hints etc.

ECHO Make sure you installed Microsoft Visual C++ 2010 Redistributable package: http://www.microsoft.com/en-us/download/confirmation.aspx?id=14632
ECHO You can remove this warning from the file once you've established everything works.

REM Note: when using special characters in your API key (stuff like "&", "^", etc.), prefix with
REM "^". So if your API key is "bagels&beans^", type "bagels^&beans^^" here
SET APIKEY=YOUR_API_KEY
SET HOST=https://mijn.gemeente.nl
SET ROOTPATH=C:\Scanstraat

REM DO NOT CHANGE ANYTHING BELOW THIS LINE
REM --------------------------------------

SET URLPATH=/api/scanstraat/upload_document

SET FILENAME=%1
SET FILENAME=%FILENAME:"=%

ECHO Uploading %FILENAME% to %HOST%%URLPATH% with %ROOTPATH%\bin\curl.exe in about 30 seconds
timeout /t 30 /nobreak > NUL

IF NOT [%DEBUG%]==[] (
	ECHO Calling %ROOTPATH%\bin\curl.exe -k --form "api_key=%APIKEY%" --form "filename=@%FILENAME%" %HOST%%URLPATH% >> %ROOTPATH%\log\transfer.log
)

FOR /F "tokens=* USEBACKQ" %%F IN (
	`%ROOTPATH%\bin\curl.exe -k --form "api_key=%APIKEY%" --form "filename=@%FILENAME%" %HOST%%URLPATH% ^| findstr "success_count..:.1"`
) DO (
	SET CURLSUCCESS=%%F
)
IF NOT DEFINED CURLSUCCESS ECHO Failed uploading file %1 >> %ROOTPATH%\log\transfer.log
IF NOT DEFINED CURLSUCCESS ECHO Failed uploading file %1
IF DEFINED CURLSUCCESS ECHO Successful upload of file %1, removing file >> %ROOTPATH%\log\transfer.log
IF DEFINED CURLSUCCESS ECHO Successful upload of file %1, removing file
IF DEFINED CURLSUCCESS DEL %1 >> %ROOTPATH%\log\transfer.log
