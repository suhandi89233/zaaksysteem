[% USE Scalar %]
[% USE JSON %]

[% # mintloaders in this scope will upload their files here %]
[% mintloader_upload_destination = '/pip/zaak/' _ zaak.id %]

[% case_closed = zaak.is_afgehandeld %]


<div class="pip-case"
  data-ng-controller="nl.mintlab.pip.PipCaseController"
  data-ng-init="caseId='[% zaak.id %]';isClosed=[% case_closed ? 'true' : 'false' %]">

    [% IF case_closed %]
    <div class="pip-case-result">
        <i class="icon icon-font-awesome icon-check"></i> Resultaat: [% zaak.resultaat %]
    </div>
    [% END %]

    <div class="pip-case-title pip-page-title">
        <h1>[% zaak.id %]: [% zaak.zaaktype_node_id.titel | html_entity %]</h1>
        <span class="pip-case-subject">
            [% zaak.onderwerp_extern | html_entity %]
        </span>
    </div>

    [% fasen = zaak.scalar.fasen %]

    <div class="pip-case-form pip-case-component">
        [% WHILE (fase = fasen.next) %]
            [% statusnumber = fase.status %]
            [% rule_result.active_attributes = rule_result.active_attributes_per_status.$statusnumber%]
        <div class="pip-case-form-phase" data-ng-controller="nl.mintlab.core.IsolationController" data-ng-init="collapsed=[% fase.status != (zaak.milestone + 1) ? 'true': 'false' %];collapsible=[% fase.status > (zaak.milestone + 1) ? 'false' : 'true' %]" data-ng-class="{'pip-case-form-phase-collapsed': collapsed, 'pip-case-form-phase-collapsible': collapsible }">
            [% PROCESS plugins/pip/case/phase.tt %]
            <div class="pip-case-form-phase-body ng-cloak" data-ng-show="!collapsed">
                <form name="caseForm" class="ng-cloak ezra_no_waitstart" method="post" enctype="multipart/form-data" zs-case-webform zs-case-webform-rule-manager data-zs-case-webform-params="rule_params" data-zs-case-webform-rules="" zs-case-webform-immediate="true">
                <script type="text/zs-scope-data">
                    [% rule_result = rule_engine({ 'case.number_status' => fase.status }, zaak);


                    JSON.encode(rule_result) %]
                </script>
                [% PROCESS zaak/fields.tt
                    requested_fase = fase
                    from_pip = 1
                    rule_result = rule_result
                    IF fase.status <= (zaak.milestone + 1)
                %]
                </form>
            </div>
        </div>
        [% END %]
    </div>

    [%- IF pip_subcases %]
    <div class="pip-case-subcase pip-case-component">
        <h2>Acties</h2>
      [%- FOR subcase = pip_subcases %]
        <a href="[% subcase.uri %]">[% subcase.label %]</a>
        <br/>
      [% END %]
    </div>
   [%- END %]

    <div class="pip-case-documents pip-case-component ng-cloak" data-ng-controller="nl.mintlab.pip.PipDocumentController" data-ng-show="!documents||!(isClosed&&documents.length==0)">
        <h2>Mijn bijlagen</h2>
        <ul class="pip-case-document-list">
            <li class="pip-case-document" data-ng-repeat="doc in documents | orderBy:'accepted'" data-ng-controller="nl.mintlab.pip.PipFileController">
                <div class="pip-case-document-icon"><i class="icon-font-awesome icon icon-file"></i></div>
                <div class="pip-case-document-title">
                    <button data-ng-click="downloadFile($event)"><[doc.name + doc.extension]></button>
                </div>
                <div class="pip-case-document-state" data-ng-class="{ 'pip-case-document-state-queued': !doc.accepted}">
                    <div class="pip-document-state-icon">
                    </div>
                    <div class="pip-document-state-label">
                        <span data-ng-show="!doc.accepted">In beoordeling</span>
                    </div>
                </div>

            </li>
            <li class="pip-case-document pip-case-document-upload" data-ng-repeat="upload in getQueue()" data-ng-show="!upload.completed">
                <div class="pip-case-document-icon"><i class="icon-font-awesome icon icon-file"></i></div>
                <div class="pip-case-document-title">
                    <[upload.file.name]>
                </div>
                <div class="pip-case-document-state" data-ng-class="{'pip-document-state-error': upload.error, 'pip-document-state-progress': !upload.error }">
                    <div class="pip-document-state-icon">
                    </div>
                    <div class="pip-document-state-label">
                        <span data-ng-show="upload.error">Fout</span>
                        <span data-ng-show="!upload.error"><[upload.progress*100|number:0]>%</span>
                    </div>
                </div>
            </li>

        </ul>

        [% IF !case_closed %]
        <div class="pip-case-documents-upload"  data-zs-upload data-zs-upload-url="/pip/file/create/?case_id=<[caseId]>" data-zs-upload-multiple="true">
            <button type="button" class="button button-primary">
                Kies bestand
            </button>
        </div>
        [% END %]

    </div>

    [% IF zaak.locatie_zaak %]
    <div class="pip-case-location pip-case-component">
        <h2>Locatie</h2>
        [% PROCESS zaak/elements/view_maps.tt %]
    </div>
    [% END %]

    <div class="pip-case-messages pip-case-component">
        <h2>Berichten</h2>
        [% PROCESS zaak/elements/messages.tt %]
    </div>

</div>
