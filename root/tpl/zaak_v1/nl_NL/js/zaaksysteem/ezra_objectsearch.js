/*

autocomplete search boxes

enter the first letters of a street. all the streets in the database that match the letters
will be return in an autocomplete dropdown.

*/

/* global extraItem,renderDocument */
// ZS-TODO: `renderDocument` is not implemented
//          check if and where this is used

new function($) {
  $.fn.setCursorPosition = function(pos) {
    if ($(this).get(0).setSelectionRange) {
      $(this).get(0).setSelectionRange(pos, pos);
    } else if ($(this).get(0).createTextRange) {
      var range = $(this).get(0).createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }
}(jQuery);

String.prototype.trunc = function(n,useWordBoundary){
    var toLong = this.length > n,
        s_     = toLong ? this.substr(0, n - 1) : this;

    s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;

    return toLong ? s_ + '&hellip;' : s_;
};

var object_type_labels = {
    zaak:                   'Zaken',
    contact:                'Contacten',
    documents:              'Documenten',
    kennisbank_vragen:      'Vragen',
    kennisbank_producten:   'Producten',
    kenmerk:                'Kenmerken',
    file:                   'Documenten',
    product:                'Producten',
    faq:                    'Vragen',
    saved_search:           'Uitgebreid zoeken'
};

// Initialization bindings, associate a objectsearch element in the DOM with an initializing callback
var init_stubs = {
    '.ezra_objectsearch': function(jwindow) { initializeEzraSearch(jwindow); },
    '.ezra_objectsearch_betrokkene': function(jwindow) { initializeEzraBetrokkeneSearch(jwindow); },
    '.ezra_objectsearch_zaaktype': function(jwindow) { initializeEzraZaaktypeSearch(jwindow); },
    '.ezra_objectsearch_zaak': function(jwindow) { initializeEzraCaseSearch(jwindow); },
    '.ezra_objectsearch_product': function(jwindow) { initializeEzraProductSearch(jwindow); },
    '.ezra_objectsearch_kenmerk': function(jwindow) { initializeEzraKenmerkSearch(jwindow); }
};

$(document).ready(function(){
    var jdoc = $(this);

    // Iterate our init bindings
    $.each(init_stubs, function(key, init) {
        $(key).each(function() { init($(this)); });
    });

    // bonus: focus on the searchbox when you type the "s"-key and you're not
    // in a textarea or input field
    $(document).keypress(function(event) {
        var element = event.target;
        if(
            event.which == 402 &&
            element.tagName != 'INPUT' &&
            element.tagName != 'TEXTAREA'
        ) {
            $('.ezra_objectsearch input.searchbox').focus();
            $('.ezra_objectsearch_input').addClass('spotenlighter-input-active');
            return false;
        }
    });
});



/*

ezra_search: spotlight-like functionality for zaaksysteem. a query is issued, and the
server responds with results. these results contain object field so the javascript app
can display different items.

*/
function initializeEzraSearch(obj) {
    obj.find('form').unbind('submit').submit(function() {
        return false;
    });

    obj.find('.icon-cancel-search').on('click', function(event) {
        obj.data('canceled', 1);
        obj.find('input.searchbox').val('');
        obj.find('input.searchbox').focus();
        obj.find('input.searchbox').ezra_objectsearch('close');
        $(this).hide();
        return false;
    });

    $.widget( "custom.ezra_objectsearch", $.ui.autocomplete, {
        _renderItem: function( ul, item ) {
            var item_key = item.label;
            var my_item = obj.data('item_lookup')[item_key];

            if(!my_item || my_item.id === 'error') {
                var errorItem = $('<li></li>');
                errorItem[0].innerHTML = '<div class="error">' + (my_item ? my_item.label : 'Geen resultaten') + '</div>';
                errorItem.appendTo(ul);
                return;
            }

            var print_object_type  = '';
            var extra_space = '';
            var this_object_type = my_item.object_type;

            if(this_object_type == 'natuurlijk_persoon' || this_object_type == 'bedrijf') {
                this_object_type = 'contact';
            }

            if(
                obj.data('object_type') === undefined ||
                obj.data('object_type') != this_object_type
            ) {
                print_object_type = this_object_type;
                print_object_type = object_type_labels[print_object_type] || print_object_type;
                obj.data('object_type', this_object_type);

                extraItem = $('<li class="extra_space"></li>');
                extraItem.appendTo(ul);

                extraItem = $('<li class="ezra_search_category add-tooltip spotenlighter-results-title" title="Toon alleen ' + print_object_type + '"><span class="spotenlighter-results-title-text">' + print_object_type +  '</span> <span class="spotenlighter-results-title-expl">Toon alle gevonden ' + print_object_type +  '</span></li>');
                extraItem.appendTo(ul);
                extraItem.data('object_type', this_object_type);
                extraItem.addClass('spotenlighter-results-title-' + this_object_type);
            }

            var renderedItem;
            if(obj.data('global_object_type')) {
                renderedItem = '<div class="spotenlighter-results-category"><span>' + print_object_type + "</span></div>" + '<a class="ezra_search_item">';
            } else {
                renderedItem = '<div class="ezra_search_category spotenlighter-results-category" title="Toon alleen '+ print_object_type + '"><div>' + print_object_type + "</div></div>" + '<a class="ezra_search_item">';
            }

            renderedItem += renderObject(my_item);

            renderedItem = $('<li></li>').data('item.autocomplete',
                item).append(renderedItem + '</a>');


            renderedItem.find('.ezra_search_category').data('object_type', this_object_type);
            renderedItem.appendTo(ul);

            renderedItem.find('.start-persistent-user').click(function() {
                my_item.make_persistent = 1;
                return true;
            });

            ul.find('.ezra_search_category').unbind('click').click(function() {
                var category_object_type = $(this).data('object_type');
                if(category_object_type === 'saved_search') {
                    window.location = '/search';
                } else {
                    obj.data('global_object_type', category_object_type);
                    obj.find('input.searchbox').ezra_objectsearch("search");
                }
            });
        }
    });

    (function ( ) {

        var open = false;

        obj.find('input.searchbox').ezra_objectsearch({
            source: function (request,response) {

                var query = this.element.val();

                obj.find('.icon-cancel-search').hide();

                function showError ( ) {
                    var error = { id: 'error', label: 'Er ging iets fout bij het ophalen van de resultaten. Probeer het later opnieuw. ' },
                        lookup = {};

                    lookup[error.label] = error;

                    obj.data('object_type', '');
                    obj.data('item_lookup', lookup);
                    response([ error.label ] );
                }

                if(obj.data('global_object_type') != null) {
                    query += '&object_type=' + obj.data('global_object_type') + "&rows=12";
                }

                if(!query) {
                    $.ajax({
                        url: '/api/object/search/?zql=SELECT {} FROM saved_search',
                        dataType: 'json',
                        success: function (data) {
                            var keys = [],
                                lookup = {},
                                items = _.map(data.result, function ( obj ) {
                                    return {
                                        id: obj.id,
                                        label: htmlEscape(obj.values.title),
                                        object_type: 'saved_search'
                                    };
                                });

                            if(!open) {
                                response([]);
                                return;
                            }

                            items.unshift({
                                id: 'contact',
                                label: 'Alle contacten',
                                object_type: 'saved_search'
                            });

                            items.unshift({
                                id: 'mine',
                                label: 'Mijn zaken',
                                object_type: 'saved_search'
                            });

                            items.unshift({
                                id: 'my-department',
                                label: 'Mijn afdeling',
                                object_type: 'saved_search'
                            });

                            items.unshift({
                                id: 'all',
                                label: 'Alle zaken',
                                object_type: 'saved_search'
                            });

                            obj.data('object_type', '');
                            obj.data('item_lookup', lookup);

                            _.each(items, function ( item ) {
                                keys.push(item.id);
                                lookup[item.id] = item;
                            });

                            if(!items.length) {
                                showError();
                            } else {
                                obj.find('.ezra_objectsearch_closer').show();
                                response(keys);
                                if(query.length) {
                                    obj.find('.icon-cancel-search').show();
                                }
                            }
                        }
                    });
                } else {
                    $.ajax({
                        url: '/objectsearch?query=' + query,
                        dataType: 'json',
                        success: function (data) {

                            obj.find('.error').html('');
                            var items = [];

                            obj.data('item_lookup', {});
                            obj.data('object_type', '');

                            for (var i in data.json.entries) {
                                var item = data.json.entries[i];

                                /* every item must have the following fields,
                                or we have a situation, houston.
                                    - id
                                    - label
                                    - object_type
                                */
                                if(!item.id || !item.label || !item.object_type) {
                                    showError();
                                    return;
                                }

                                var item_key = String(item.id);
                                items.push(item_key);
                                obj.data('item_lookup')[item_key] = item;
                            }

                            if(items.length === 0) {
                                obj.find('.error').html('Geen resultaten');
                                items.push('Geen resultaten');
                            }

                            obj.find('.ezra_objectsearch_closer').show();
                            response(items);
                            if(query.length) {
                                obj.find('.icon-cancel-search').show();
                            }
                        },
                        error: function (xhr) {
                            /* Ignore cancelled requests */
                            if(xhr.readyState == 0) {
                                return;
                            }

                            showError();
                        }
                    });
                }
            },
            select: function( event, ui ) {

                var item_key = ui.item.value;
                var item = obj.data('item_lookup')[item_key];

                var wrapper = $(this).closest(".ezra_autocomplete");
                wrapper.find("input[name=address]").val('');

                var location_url = getObjectUrl(item);

                top.location.href = location_url;
                return false;
            },
            focus: function(event, ui) {

                return false;
            },
            open:function( event, ui ) {

                $('.ui-autocomplete.ui-menu').addClass('ui-autocomplete-spotenlighter');
                $('.ui-menu').width(398);
                //$('.ui-autocomplete-input').addClass('ui-autocomplete-input-open');
                $('.spotenlighter-input').addClass('spotenlighter-input-active');
                initializeCategoryTitleTooltip();

                open = true;
            },
            close:function( event, ui ) {

                //if(obj.data('canceled')) {
                //    obj.data('canceled', 0);
                    //return;
                //}
                $('.ui-autocomplete.ui-menu').removeClass('ui-autocomplete-spotenlighter');
                //$('.ui-autocomplete-input').removeClass('ui-autocomplete-input-open');
                $('.spotenlighter-input').removeClass('spotenlighter-input-active');

                // reset the choice of object_type
                obj.data('global_object_type', null);
                obj.find('.icon-cancel-search').hide();

                open = false;
            },
            position: {
                my : "left top",
                at: "left bottom",
                of: ".spotenlighter-input"
            }
        });

        obj.find('input.searchbox').bind('focus', function ( ) {
            open = true;
            $(this).ezra_objectsearch("search", "_EMPTY_");
        });

        obj.find('input.searchbox').bind('blur', function ( ) {
            open = false;
        });

    })();

}

function initializeEzraProductSearch(obj) {
    $.widget("custom.ezra_objectsearch_product", $.ui.autocomplete, {
        _renderItem: function( ul, item ) {
            var item_key = item.label;
            var my_item = obj.data('item_lookup')[item_key];

            return $( "<li></li>" )
                .data("item.autocomplete", item)
                .append("<a>" + my_item.object.naam + "<br><div style='text-align:right'>")
                .appendTo(ul);
        }
    });

    obj.find('input[name=product_name]').ezra_objectsearch_product({
        source: function (request,response) {
            var query = this.element.val();

            $.ajax({
                url: '/objectsearch?object_type=kennisbank_producten&query=' + query,
                dataType: 'json',
                success: function (data) {
                    obj.find('.error').html('');
                    var items = [];

                    obj.data('item_lookup', Object());

                    for (var i in data.json.entries) {
                        var item = data.json.entries[i];
                        var item_key = String(item.label + ', ' + item.woonplaats);
                        items.push(item_key);
                        obj.data('item_lookup')[item_key] = item;
                    }

                    if(items.length == 0) {
                        obj.find('.error').html('Geen resultaten');
                    }
                    response(items);
                }
            });
        },
        select: function( event, ui ) {
            var item_key = ui.item.value;
            var item = obj.data('item_lookup')[item_key];

            $(this).val(item.object.naam);
            $(this).closest('form').find('input[name="' + $(this).attr('hidden-field') + '"]').val(item.object.id);

            return false;
        },
        focus: function(event, ui) {
            return false;
        }
    });
}

function initializeEzraKenmerkSearch(obj) {
    $.widget("custom.ezra_objectsearch_kenmerk", $.ui.autocomplete, {
        _renderItem: function( ul, item ) {
            var item_key = item.value;
            var my_item = obj.data('item_lookup')[item_key];

            return $( "<li></li>" )
                .data("item.autocomplete", item)
                .append("<a>" + my_item.label + "<br><div style='text-align:right'>")
                .appendTo(ul);
        }
    });

    obj.find('input.ezra_kenmerk_autocomplete').ezra_objectsearch_kenmerk({
        source: function (request,response) {
            var query = this.element.val();

            $.ajax({
                url: '/objectsearch/attributes?query=' + query,
                dataType: 'json',
                success: function (data) {

                    obj.find('.error').html('');
                    var items = [];

                    obj.data('item_lookup', Object());

                    for (var i in data.json.entries) {
                        var item = data.json.entries[i];
                        var item_key = String(item.id);
                        items.push(item_key);
                        obj.data('item_lookup')[item_key] = item;
                    }

                    if(items.length == 0) {
                        obj.find('.error').html('Geen resultaten');
                    }
                    response(items);
                }
            });
        },
        select: function( event, ui ) {
            var item_key = ui.item.value;
            var item = obj.data('item_lookup')[item_key];

            $(this).val(item.label);
            $(this).closest('form').find('input[name="' + $(this).attr('hidden-field') + '"]').val(item.id);

            return false;
        },
        focus: function(event, ui) {
            return false;
        }
    });
}

function initializeEzraBetrokkeneSearch(obj) {

    obj.find('.icon-cancel-search').on('click', function() {
        obj.find('input.searchbox').val('');
        obj.find('input.searchbox').ezra_objectsearch_betrokkene('close');
        obj.find('input[name=ztc_aanvrager_id]').val('');
        $(this).hide();
        return false;
    });

    $.widget( "custom.ezra_objectsearch_betrokkene", $.ui.autocomplete, {
        _renderItem: function( ul, item ) {
            var item_key = item.label;
            var my_item = obj.data('item_lookup')[item_key];

            var betrokkene = renderObject(my_item);

            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a><div class='betrokkene'>" + betrokkene + "</div></a>" )
                .appendTo( ul );
            }
    });

    obj.find('input.searchbox').ezra_objectsearch_betrokkene({
        source: function (request,response) {
            var query = this.element.val();

            var betrokkene_type = getBetrokkeneType(obj);

            var minimum_query_length = 2;
            if(betrokkene_type == 'medewerker') {
                minimum_query_length = 3;
            }

            if(query.length < minimum_query_length) {
                response([]);
                return;
            }
            $.ajax({
                url: '/objectsearch/contact/' + betrokkene_type + '?query=' + query,
                dataType: 'json',
                success: function (data) {
                    obj.find('.error').html('');
                    var items = [];

                    obj.data('item_lookup', Object());

                    for (var i in data.json.entries) {
                        var item = data.json.entries[i];
                        var item_key = String(item.label);
                        items.push(item_key);
                        obj.data('item_lookup')[item_key] = item;
                    }

                    if(data.json.count == 0) {
                        obj.find('.error').html('Geen resultaten');
                    } else {
                        obj.find('.error').html('');
                    }
                    response(items);
                    if(query.length) {
                        obj.find('.icon-cancel-search').show();
                    }
                }
            });
        },
        select: function( event, ui ) {
            var item_key = ui.item.value;
            var item = obj.data('item_lookup')[item_key];

            var form = obj.closest('form');
            var betrokkene_type = getBetrokkeneType(obj);
            if((item && item.object_type == 'medewerker') || betrokkene_type == null) {
                betrokkene_type = 'medewerker';
            }

            obj.closest('div.ezra_objectsearch_betrokkene').find("input[type='hidden']").val('betrokkene-' +
                betrokkene_type + '-' + item.object.id);

            var betrokkene = selectBetrokkene(item);
            $(this).val(betrokkene);
            obj.find('.error').html('');
            return false;
        },
        open:function( event, ui ) {
            var parents = obj.parents();
            for(var i = 0; i<parents.length; i++) {
                var parent = parents[i];
                var position = $(parent).css('position');
                if(position == 'fixed') {
                    $('.ui-autocomplete.ui-menu').addClass('ui-autocomplete-spotenlighter-betrokkene');
                    break;
                }
            }
        },
        focus: function(event, ui) {
            return false;
        },
        close:function( event, ui ) {
            $('.ui-autocomplete.ui-menu').removeClass('ui-autocomplete-spotenlighter');
            obj.find('.icon-cancel-search').hide();
        },
        change:function(event,ui) {
            if ($(this).val() == '') {
                obj.closest('div.ezra_objectsearch_betrokkene').find("input[type='hidden']").val('');
            }
        }
    });
}



function selectBetrokkene(item) {
    var betrokkene = '';
    if(item.object.object_type == 'natuurlijk_persoon') {
        betrokkene += htmlEscape(item.object.decorated_name);;
        return betrokkene;
    } else if(item.object.object_type == 'bedrijf') {
        betrokkene += htmlEscape(item.object.handelsnaam);
        return betrokkene;
    } else if(item.object.object_type == 'medewerker') {
        betrokkene = htmlEscape(item.object.naam);
        return betrokkene;
    }
   // error
    return 'Er is een server fout opgetreden.';
}



function renderMedewerker( my_item ) {
    var betrokkene = '<span class="naam">' + htmlEscape(my_item.naam);
    betrokkene += '</span>';
    return betrokkene;
}


function renderNatuurlijkPersoon (my_item) {

    var betrokkene = '<span class="naam">';

    betrokkene += htmlEscape(my_item.decorated_name);

    if(my_item.geboortedatum) {
        betrokkene += ' (' + htmlEscape(my_item.geboortedatum) + ')';
    }
    betrokkene += '</span>';

    betrokkene += '<small>';

    if (my_item.adres_id) {
        if (my_item.adres_id.straatnaam) {
            betrokkene += htmlEscape(my_item.adres_id.straatnaam) + ' ';
            betrokkene += htmlEscape(my_item.adres_id.huisnummer);
            if(my_item.adres_id.huisletter) {
                betrokkene += htmlEscape(my_item.adres_id.huisletter);
            }
            if(my_item.adres_id.huisnummer_toevoeging) {
                betrokkene += htmlEscape(my_item.adres_id.huisnummertoevoeging);
            }
            betrokkene += ', ' + htmlEscape(my_item.adres_id.postcode) + ' ';
            betrokkene += htmlEscape(my_item.adres_id.woonplaats);
        } else if (my_item.adres_id.adres_buitenland1) {
            betrokkene += htmlEscape(my_item.adres_id.adres_buitenland1);
            if (my_item.adres_id.adres_buitenland2) { betrokkene += ', ' + htmlEscape(my_item.adres_id.adres_buitenland2)}
            if (my_item.adres_id.adres_buitenland3) { betrokkene += ', ' + htmlEscape(my_item.adres_id.adres_buitenland3)}
        }
    }
    else {
        betrokkene += htmlEscape("Geen adresgegevens beschikbaar");
    }


    betrokkene += "</small>";
    betrokkene += '<div class="start-persistent-user"><button class="button button-secondary"><i class="mdi mdi-play"></i></button></div>';

    return betrokkene;
}

function renderBedrijf (my_item) {
    var betrokkene = '<span class="naam">';
    betrokkene += htmlEscape(my_item.handelsnaam) + ' (Bedrijf)</span>';

    betrokkene += '<small>';

    if (my_item.vestiging_straatnaam) {
        betrokkene += htmlEscape(my_item.vestiging_straatnaam) + ' ';
        betrokkene += htmlEscape(my_item.vestiging_huisnummer);
        if(my_item.vestiging_huisnummertoevoeging) {
            betrokkene += htmlEscape(my_item.vestiging_huisnummertoevoeging);
        }
        betrokkene += ', ' + htmlEscape(my_item.vestiging_postcode) + ' ';
        betrokkene += htmlEscape(my_item.vestiging_woonplaats);
    } else if (my_item.vestiging_adres_buitenland1) {
        betrokkene += htmlEscape(my_item.vestiging_adres_buitenland1);
        if (my_item.vestiging_adres_buitenland2) { betrokkene += ', ' + htmlEscape(my_item.vestiging_adres_buitenland2); }
        if (my_item.vestiging_adres_buitenland3) { betrokkene += ', ' + htmlEscape(my_item.vestiging_adres_buitenland3); }
    }
    betrokkene += '</small>';
    betrokkene += '<div class="start-persistent-user"><button class="button button-secondary"><i class="mdi mdi-play"></i></button></div>';

    return betrokkene;
}


function renderZaak (my_item) {
    var titel = htmlEscape(my_item.zaaktype_node_id.titel);

    var html = '<span class="naam">';
    html += my_item.id + ' - ' + titel + '</span>';
    html += '<small>';
    if (my_item.description) {
        var description = htmlEscape(my_item.description);
        html += (
                description.length >45
                    ? description.substr(0, 45) + ' ... '
                    : description
        );
    }
    html += '</small>';

    return html;
}


function renderProduct (my_item) {

    var html = '<span class="naam">';
    html += my_item.naam + '</span>';

    return html;
}


function renderVraag (my_item) {

    var html = '<span class="naam">';
    html += my_item.naam + '</span>';

    return html;
}




function renderFile (my_item) {
    var html = '<span class="naam">';
    html += my_item.name + my_item.extension;
    html += '</span>';

    return html;
}

function renderZaaktype (my_item) {
    var html = '<span class="naam">';
    html += htmlEscape(my_item.zaaktype_node_id.titel) + '</span>';
    if(my_item.zaaktype_node_id.zaaktype_omschrijving) {
        var zaaktype_omschrijving = htmlEscape(my_item.zaaktype_node_id.zaaktype_omschrijving);
        zaaktype_omschrijving = zaaktype_omschrijving.trunc(70, true);
        html += '<small>' + zaaktype_omschrijving + '</small>';
    }
    return html;
}



function getObjectUrl( item) {
    var object_type = item.object_type,
        url;

    if(object_type === 'natuurlijk_persoon') {
        url = '/betrokkene/'+item.object.id+'?gm=1&type=natuurlijk_persoon';
        if(item.make_persistent) {
            url += '&enable_betrokkene_session=1';
        }
    } else if(object_type === 'bedrijf') {
        url = '/betrokkene/' +item.object.id+'?gm=1&type=bedrijf';
        if(item.make_persistent) {
            url += '&enable_betrokkene_session=1';
        }
    } else if(object_type === 'zaak') {
        url = '/zaak/' + item.object.id;
    } else if(object_type === 'file') {
        url = '/zaak/' + item.object.case_id + '#zaak-elements-documents';
    } else if(object_type === 'kennisbank_producten') {
        url = '/kennisbank/product/' + item.object.bibliotheek_categorie_id + '/' + item.object.id + '/' + item.object.naam;
    } else if(object_type === 'kennisbank_vragen') {
        url = '/kennisbank/vraag/' + item.object.bibliotheek_categorie_id + '/' + item.object.id + '/' + item.object.naam;
    } else if(object_type === 'product' || object_type === 'faq') {
        url = '/object/' + item.object.id;
    } else if(object_type === 'saved_search') {
        if(item.id === 'contact') {
            url = '/betrokkene/search';
        } else {
            url = '/search/' + item.id;
        }

    } else if(item.is_object_type_instance) {
        url = '/object/' + item.id;
    } else if(object_type === 'contact' && item.id === 'contact') {
        url = '/betrokkene/search';
    } else {
        url = '/beheer/';
    }
    return url;
}


function initializeCategoryTitleTooltip() {
    $('.add-tooltip[title]').qtip({
           position: {
              my: 'top right',
              at: 'bottom right',
              adjust: {
                     x: -30,
                     y: 3
                  }
            },
            style: {
                  tip: {
                     corner: 'top right',
                     offset:5
                  }
            },
            show: {
                event: 'mouseenter',
                solo: true
             },
             hide: {
                  event: 'mouseleave'
             }
        });
}


function renderObject(my_item) {

    if(my_item.object_type == 'natuurlijk_persoon') {
        return renderNatuurlijkPersoon(my_item.object);
    } else if(my_item.object_type == 'bedrijf') {
        return renderBedrijf(my_item.object);
    } else if(my_item.object_type == 'zaak') {
        return renderZaak(my_item.object);
    } else if(my_item.object_type == 'kennisbank_producten') {
        return renderProduct(my_item.object);
    } else if(my_item.object_type == 'kennisbank_vragen') {
        return renderVraag(my_item.object);
    } else if(my_item.object_type == 'documents') {
        return renderDocument(my_item.object);
    } else if(my_item.object_type == 'file') {
        return renderFile(my_item.object);
    } else if(my_item.object_type == 'zaaktype') {
        return renderZaaktype(my_item.object);
    } else if(my_item.object_type == 'medewerker') {
        return renderMedewerker(my_item.object);
    }

    return my_item.label;
}

function initializeEzraCaseSearch(obj) {
    obj.find('.icon-cancel-search').on('click', function() {
        obj.find('input.searchbox').val('');
        obj.find('input.searchbox').ezra_objectsearch_zaaktype('close');
        obj.find('input[name=zaak_id]').val('');
        $(this).hide();
        return false;
    });

    $.widget( "custom.ezra_objectsearch_zaak", $.ui.autocomplete, {
        _renderItem: function( ul, item ) {
            var betrokkene = renderObject(
                obj.data('item_lookup')[item.label]
            );

            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a><div class='betrokkene'>" + betrokkene + "</div></a>" )
                .appendTo( ul );
        }
    });

    obj.find('input.searchbox').ezra_objectsearch_zaak({
        source: function (request,response) {
            var trigger_element = obj.closest('form').find('input[name="ztc_trigger"]:checked');
            var trigger = trigger_element.length ? trigger_element.val() : 'extern';

            var query = this.element.val();

            if(query.length < 1) {
                response([]);
                return;
            }

            $.ajax({
                url: '/objectsearch/?object_type=zaak&trigger=' + trigger + '&query=' + query,
                dataType: 'json',
                success: function (data) {
                    obj.find('.error').html('');
                    var items = [];

                    obj.data('item_lookup', Object());

                    for (var i in data.json.entries) {
                        var item = data.json.entries[i];
                        var item_key = String(item.label);
                        items.push(item_key);
                        obj.data('item_lookup')[item_key] = item;
                    }

                    if(data.json.count == 0) {
                        obj.find('.error').html('Geen resultaten');
                    } else {
                        obj.find('.error').html('');
                    }

                    response(items);

                    if(query.length) {
                        obj.find('.icon-cancel-search').show();
                    }
                }
            });
        },

        select: function( event, ui ) {
            var item_key = ui.item.value;
            var item = obj.data('item_lookup')[item_key];

            var form = obj.closest('form');

            form.find("input.ezra_selected_zaak_id").val(item.object.id);

            var extra_info = [];

            if(item.object.fase) { extra_info.push("Fase: " + item.object.fase); }

            if(item.object.aanvrager) {
                extra_info.push("Aanvrager: " + item.object.aanvrager.naam);
            }

            if(item.object.behandelaar) {
                extra_info.push("Behandelaar: " + item.object.behandelaar.naam);
            }

            $(this).val([
                item.object.id,
                item.object.zaaktype_node_id.titel,
                "(" + extra_info.join(", ") + ")"
            ].join(' - '));

            if(
                obj.closest('.nieuwe_zaak_starten').length &&
                item.object.hasOwnProperty('preset_client')
            ) {
                var ztc_aanvrager_id = form.find('input[name=ztc_aanvrager_id]');

                if(!ztc_aanvrager_id.val()) {
                    var preset_client = item.object.preset_client;
                    ztc_aanvrager_id.val(preset_client.id);
                    form.find('input[name=betrokkene_naam]').val(preset_client.naam);
                }
            }

            obj.find('.error').html('');
            return false;
        },

        focus: function(event, ui) {
            return false;
        },

        close:function( event, ui ) {
            obj.find('.icon-cancel-search').hide();
        }
    });
}

function initializeEzraZaaktypeSearch(obj) {
    obj.find('.icon-cancel-search').on('click', function() {
        obj.find('input.searchbox').val('');
        obj.find('input.searchbox').ezra_objectsearch_zaaktype('close');
        obj.find('input[name=zaaktype_id]').val('');
        $(this).hide();
        return false;
    });


// TODO remove double code by subclassing widgets
//    if(!$.custom.ezra_objectsearch_betrokkene) {
//        initializeEzraBetrokkeneSearch($(document));
//    }
//  $.widget( "custom.ezra_objectsearch_zaaktype", $.custom.ezra_objectsearch_betrokkene, {


    $.widget( "custom.ezra_objectsearch_zaaktype", $.ui.autocomplete, {
        _renderItem: function( ul, item ) {
            var item_key = item.label;
            var my_item = obj.data('item_lookup')[item_key];

            var betrokkene = renderObject(my_item);

            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a><div class='betrokkene'>" + betrokkene + "</div></a>" )
                .appendTo( ul );
            }
    });

    obj.find('input.searchbox').ezra_objectsearch_zaaktype({
        source: function (request,response) {
            var trigger_element = obj.closest('form').find('input[name="ztc_trigger"]:checked');
            var trigger = trigger_element.length ? trigger_element.val() : 'extern';

            var betrokkene_type = obj.closest('form').find('input[name="betrokkene_type"]:checked').val();

            var query = this.element.val();

            if(query.length < 1) {
                response([]);
                return;
            }
            $.ajax({
                url: '/objectsearch/?object_type=zaaktype&betrokkene_type=' + betrokkene_type + '&trigger=' + trigger + '&query=' + query,
                dataType: 'json',
                success: function (data) {
                    obj.find('.error').html('');
                    var items = [];

                    obj.data('item_lookup', Object());

                    for (var i in data.json.entries) {
                        var item = data.json.entries[i];
                        var item_key = String(item.label);
                        items.push(item_key);
                        obj.data('item_lookup')[item_key] = item;
                    }

                    if(data.json.count == 0) {
                        obj.find('.error').html('Geen resultaten');
                    } else {
                        obj.find('.error').html('');
                    }
                    response(items);
                    if(query.length) {
                        obj.find('.icon-cancel-search').show();
                    }

                }
            });
        },
        select: function( event, ui ) {
            var item_key = ui.item.value;
            var item = obj.data('item_lookup')[item_key];

            var form = obj.closest('form');

            form.find("input.ezra_selected_zaaktype_id").val(item.object.id);
            $(this).val(item.object.zaaktype_node_id.titel);

            if(
                obj.closest('.nieuwe_zaak_starten').length &&
                item.object.hasOwnProperty('preset_client')
            ) {
                var ztc_aanvrager_id = form.find('input[name=ztc_aanvrager_id]');

                if(!ztc_aanvrager_id.val()) {
                    var preset_client = item.object.preset_client;
                    ztc_aanvrager_id.val(preset_client.id);
                    form.find('input[name=betrokkene_naam]').val(preset_client.naam);
                }
            }

            obj.find('.error').html('');
            return false;
        },
        focus: function(event, ui) {
            return false;
        },
        close:function( event, ui ) {
            obj.find('.icon-cancel-search').hide();
        }
    });
}


function getBetrokkeneType(obj) {
    var betrokkene_type = 'medewerker';

    var rel = obj.find('input[name=betrokkene_naam]').attr('rel');
    if(rel) {
        var options = getOptions(rel);
        if(options.default_betrokkene_type) {
            betrokkene_type = options.default_betrokkene_type;
        }
    }

    var widget_id = obj.closest('tr').attr('id');

    var jq = '#' + widget_id + '_type:visible .ezra_betrokkene_type input:checked';

    var selected_betrokkene_type = obj.closest('form').find(jq).val();

    if(selected_betrokkene_type) {
        betrokkene_type = selected_betrokkene_type;
    }

    return betrokkene_type;
}

function htmlEscape(text) {
    return text ? $("<b></b>").text(text).html() : '';
}
