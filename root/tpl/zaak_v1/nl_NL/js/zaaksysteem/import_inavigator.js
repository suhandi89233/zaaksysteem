/*global document,$,console,zvalidate */
(function () {
    "use strict";

    function updateDocuments(form, override) {

        var zaaktype_id = form.find('[name="zaaktype_id"]').val();
        if (!zaaktype_id) {
            return;
        }

        var documents_zaaktype_element = form.find('input[name="documents_zaaktype_id"]');

        //aborting since documents seem updated');
        if (!override && documents_zaaktype_element && documents_zaaktype_element.val() == zaaktype_id) {
            return;
        }

        var proces_id = form.find('input[name="node.code"]').val();

        var documents = form.find('.ezra_inavigator_documents');
        documents.html('<h3>Wordt geladen...</h3>');

        if (proces_id) {
            documents.load(
                documents.attr('data-uri') ? documents.attr('data-uri') : '/beheer/inavigator/documents',
                {
                    zaaktype_id: zaaktype_id,
                    proces_id: proces_id
                },
                function (responseText, status) { }
            );
        } else {
            documents.html('<h3>Geen unieke code gevonden voor dit zaaktype, kon geen documenten ophalen</h3>');
        }
    }

    function installInavigatorStuff(element) {

        element.find('.ezra_inavigator_zaaktype .ezra_inavigator_zaaktype_title').click(function () {
            var $self = $(this);

            $self.closest('.import_item_inner').toggleClass('open');
            $self.closest('form').find('.import_dependency_adjustment').toggle();
        });


        element.find('.ezra_inavigator_import_checkbox input').click(function (event) {
            event.stopPropagation();
        });


        element.find('.ezra_inavigator_do_import').submit(function () {
            var callback, $self = $(this);

            if ($self.hasClass('validated')) {
                return false;
            }

            $self.closest('form').removeClass('invalidation');
            callback = function () {
                $self.closest('form').addClass('validated');
                $.ztWaitStop();

                $.ajax({
                    url: $self.attr('action'),
                    data: $self.serialize(),
                    success: function () {
                        $.ztWaitStop();
                        $self.find('.ezra_actie_button_handling_navigation').hide();
                        $self.find('.ezra_inavigator_import_checkbox').html('Zaaktype geimporteerd.');
                        $self.find('.import_dependency_adjustment').html('');
                        $self.find('.toggle').remove();
                    }
                });
                return false;
            };

            return zvalidate($self, {
                callback: callback
            });

        });

        function handleBibliotheekCategorie(select) {
            var category_selector = select.closest('.ezra_inavigator_field').find('.ezra_inavigator_bibliotheek_categorie_selector');

            if(select.val()) {
                category_selector.show();
            } else {
                category_selector.hide();
            }
        }
        element.find('select.ezra_inavigator_case_phase_selector').each(function () {
            handleBibliotheekCategorie($(this));
        });

        element.on('change', 'select.ezra_inavigator_case_phase_selector', function () {
            handleBibliotheekCategorie($(this));
        });

        element.find('form.ezra_inavigator_do_import').each(function () {
            var form = $(this);

            var handleValidationResponse = function () {
                form.find('.import_item_error').removeClass('import_item_error');
                form.find('.validator').hide();
                form.find('.import_dependency_adjustment').hide();
                form.find('.ezra_inavigator_import_checkbox input').prop('checked', true);
                form.find('.import_item_inner').removeClass('open');
            };

            function focusError(field) {
                var input = form.find('[name="' + field + '"]'),
                    inner = input.closest('.import_inavigator_group_inner');

                input.closest('.import_dependency_adjustment').show();
                input.focus();
                inner.show();

                inner.closest('.import_inavigator_group').toggleClass('open', true);
            }

            var error_callback = function (container, data) {
                form.find('.import_item_inner').addClass('import_item_error');
                if(data.invalid.length) {
                    focusError(data.invalid[0]);
                } else if(data.missing.length) {
                    focusError(data.missing[0]);
                }
            };
            zvalidate(form, {
                callback: handleValidationResponse,
                error_callback: error_callback
            });

            form.find('select.ezra_inavigator_multiple_zaaktype_id_selector').change(function () {
                updateDocuments(form, true);
            });

            form.find('a.ezra_inavigator_validate').click(function () {
                updateDocuments(form);
                zvalidate(form, {
                    callback: handleValidationResponse,
                    error_callback: error_callback
                });
                return false;
            });

            updateDocuments(form);

            form.on('click','h3', function () {
                var h3 = $(this);
                var group = h3.closest('.import_inavigator_group').toggleClass('open');
                group.find('.import_inavigator_group_inner').toggle();
            });
        });

        element.find('.ezra_import_inavigator_import_all').submit(function () {
            element.find('form.ezra_inavigator_do_import').each(function () {
                var form = $(this);
                if(form.find('.ezra_inavigator_import_checkbox input').prop('checked')) {
                    form.submit();
                }
            });
            return false;
        });
    }




    $(document).ready(function () {

        $('.import_inavigator').each(function () {
            installInavigatorStuff($(this));
        });

    });


}());