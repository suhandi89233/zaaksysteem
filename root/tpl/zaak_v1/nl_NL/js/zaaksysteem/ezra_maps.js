// OpenLayers 2 API docs: http://dev.openlayers.org/docs/files/OpenLayers-js.html
//
// OpenLayers.Projection.getCode uses a non-existant property on Proj4js
// projections by default. Probably because it's expecting an ancient version
// of Proj4js.
OpenLayers.Projection.prototype.getCode = function() { return this.projCode; }

/* eslint no-undef:0 */
// ZS-FIXME: this file introduces a ludicrous amount of global variables
//          that should be declared in the containing IIFE scope instead.
function load_ezra_map(options) {
    if ($('.ezra_map').length) {
        $('.ezra_map').ezra_map(_.assign({
            firstTime_addLayers: true,
            layer_type: 'pdoc',
            theme: '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/style.css',
            img_path: '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/img/',
            marker_icon: '/images/marker.png',
            geocode_url: '/plugins/maps',
            viewport: {
                smallfixed: {
                    css: {
                        width: '520px',
                        height: '350px'
                    }
                },
                smallauto: {
                    css: {
                        width: 'auto',
                        height: '350px'
                    }
                },
                medium: {
                    css: {
                        width: '1024px',
                        height: '768px'
                    }
                },
                large: {
                    css: {
                        width: '900px',
                        height: '400px'
                    }
                }
            },
            proxy: '/maps-tiles',
            update: function (element) {
                var input_element = element.find('.ezra_map-kenmerkfield');
                updateField(input_element);
            }
        }, options));
    }
}

(function ($) {
    var map;
    var isIntern = false;
    var isLoadingMapLayers = false;
    var mapCallsStore = [];
    var xpathNamespaces = {};

    var methods = {
        required: function required(options) {
            var required = [
                'layer_type',
                'theme',
                'viewport'
            ];

            if (!options && required && required.length) {
                $(this).ezra_map(
                    'log',
                    'ezra_map error: No options given'
                );

                return false;
            }

            for (var i = 0; i < required.length; i++) {
                var param = required[i];

                if (!options[param]) {
                    $(this).ezra_map(
                        'log',
                        'ezra_map error: Missing required parameter ' + param
                    );

                    return false;
                }
            }

            return true;
        },

        init: function init(options) {
            return this.each(function () {
                var $this = $(this);
                var data = $this.data('ezra_map');

                // Check required parameters
                if (!$this.ezra_map('required', options)) {
                    return false;
                }

                if ($this.hasClass('ezra_map-initialized')) {
                    return true;
                }

                // If the plugin hasn't been initialized yet
                if (!data) {
                    /*
                    Do more setup stuff here
                    */
                    var layer = $this.ezra_map('_load_base_layer', options);
                    var map = $this.ezra_map('_load_map', options, layer);

                    $(this).data('ezra_map', {
                        target: $this,
                        map: map,
                        options: options
                    });

                    // Check for option center or class center
                    if ($this.find('.ezra_map-center').length) {
                        options.center = $this.find('.ezra_map-center').val();
                    }

                    $this.ezra_map('_load_autocomplete');
                    $this.ezra_map('_load_markers');

                    if (!$this.hasClass('ezra_map-readonly')) {
                        var vectorLayer = map.getLayer('Markers');

                        vectorLayer.events.includeXY = false;
                        vectorLayer.events.fallThrough = true;

                        vectorLayer.events.register('touchstart', this, function () {
                            var touch = event.changedTouches[0];
                            $this._start_clientX = event.clientX;
                            $this._start_clientY = event.clientY;
                            $this.touchStartTime = new Date().getTime();
                            return false;
                        });

                        vectorLayer.events.register('touchend', this, function (event) {
                            var touch = event.changedTouches[0];
                            var clientX = touch.clientX;
                            var clientY = touch.clientY;

                            // ZS-FIXME: empty block
                            if (Math.abs(clientX - $this._start_clientX) > 10 || Math.abs(clientY - $this._start_clientY) > 10) {
                            } else {
                                setTimeout(function () {
                                    $this.ezra_map('_set_click_handler', event);
                                }, 0);
                            }
                            return false;
                        });
                    }

                    $this.addClass('ezra_map-initialized');
                    $this.ezra_map('_createFullscreenButton');
                    $this.ezra_map('_createMeasureStuff');
                    $this.trigger('ezra_map_initialized');
                }
            });
        },

        _createFullscreenButton: function _createFullscreenButton() {
            var fullScreenButton = $('<div class="openlayers-icon-fullscreen" title="Volledig scherm"></div>');
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;

            $('body').keydown(function (e) {
                if ($this.hasClass('map-fullscreen') && e.keyCode === 27) {
                    fullScreenButton.click();
                }
            });

            fullScreenButton.on('click touchstart', function () {
                $this.toggleClass('map-fullscreen');
                $(this).toggleClass('openlayers-icon-fullscreen-active');
                $('body').toggleClass('modal-open');
                map.updateSize();

                return false;
            });

            $this.find('.olControlLayerSwitcher').append(fullScreenButton);
        },

        _createMeasureStuff: function _createMeasureStuff() {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var measureControls;
            var sketchSymbolizers;
            var measureDistanceButton;
            var measureAreaButton;
            var renderer;
            var style;
            var hasKeyListener = false;
            var styleMap;
            var control;

            // style the sketch fancy
            sketchSymbolizers = {
                'Point': {
                    pointRadius: 4,
                    graphicName: 'square',
                    fillColor: 'white',
                    fillOpacity: 1,
                    strokeWidth: 1,
                    strokeOpacity: 1,
                    strokeColor: '#333333'
                },
                'Line': {
                    strokeWidth: 3,
                    strokeOpacity: 1,
                    strokeColor: '#666666',
                    strokeDashstyle: 'dash'
                },
                'Polygon': {
                    strokeWidth: 2,
                    strokeOpacity: 1,
                    strokeColor: '#666666',
                    fillColor: 'white',
                    fillOpacity: 0.3
                }
            };

            style = new OpenLayers.Style();
            style.addRules([
                new OpenLayers.Rule({ symbolizer: sketchSymbolizers })
            ]);
            styleMap = new OpenLayers.StyleMap({ 'default': style });
            renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
            renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

            measureControls = {
                line: new OpenLayers.Control.Measure(
                    OpenLayers.Handler.Path, {
                        persist: true,
                        handlerOptions: {
                            layerOptions: {
                                renderers: renderer,
                                styleMap: styleMap
                            }
                        }
                    }
                ),
                polygon: new OpenLayers.Control.Measure(
                    OpenLayers.Handler.Polygon, {
                        persist: true,
                        handlerOptions: {
                            layerOptions: {
                                renderers: renderer,
                                styleMap: styleMap
                            }
                        }
                    }
                )
            };
            $this.find('.olControlLayerSwitcher #OpenLayers_Control_MaximizeDiv').attr('title', 'Toon lagen');

            // ZS-FIXME: comma operator & globals!
            measureOutputWrapper = $('<div class="map-measure-output-wrapper"></div>'),
                $this.find('.olMapViewport').append(measureOutputWrapper);
            measureOutputCloseButton = $('<button class="map-measure-output-close-button"><i class="mdi mdi-close"></i></button>'),
                measureOutputWrapper.append(measureOutputCloseButton);
            measureOutput = $('<div class="map-measure-output"></div>'),
                measureOutputWrapper.append(measureOutput);
            measureOutputWrapper.hide();

            function setDefaultOutputValue(type) {
                if (type === 'line') {
                    measureOutput[0].innerHTML = '<span>Klik op de kaart om de afstand te meten</span>';
                } else {
                    measureOutput[0].innerHTML = '<span>Klik op de kaart om de oppervlakte te meten</span>';
                }
            }

            function startMeasure(type) {
                stopMeasure();
                setDefaultOutputValue(type);

                if (type === 'line') {
                    measureDistanceButton.addClass('active');
                } else if (type === 'polygon') {
                    measureAreaButton.addClass('active');
                }

                measureControls[type].activate();
                startKeyListener();
            }

            function stopMeasure() {
                stopKeyListener();
                measureOutputWrapper.hide();
                measureDistanceButton.removeClass('active');
                measureAreaButton.removeClass('active');

                for (var key in measureControls) {
                    control = measureControls[key];
                    control.deactivate();
                }
            }

            function onKeyUp(event) {
                var keyCode = event.keyCode;

                if (keyCode === 27) {
                    stopMeasure();
                }
            }

            function stopKeyListener() {
                $(document).unbind('keyup', onKeyUp);
                hasKeyListener = false;
            }

            function startKeyListener() {
                if (hasKeyListener) {
                    return;
                }

                measureOutputWrapper.show();
                hasKeyListener = true;
                $(document).keyup(onKeyUp);
            }

            measureOutputCloseButton.on('click touchstart', function () {
                stopMeasure();
                return false;
            });

            // ZS-FIXME: comma operator
            // Distance:
            measureDistanceButton = $('<div class="openlayers-icon-measuredistance" title="Meet afstand"></div>'),
                measureDistanceButton.on('click touchstart', function () {
                    if ($(this).hasClass('active')) {
                        stopMeasure();
                    } else {
                        startMeasure('line');
                    }
                    return false;
                });
            $this.find('.olControlLayerSwitcher').append(measureDistanceButton);
            // ZS-FIXME: comma operator
            // Area:
            measureAreaButton = $('<div class="openlayers-icon-measurearea" title="Meet oppervlakte"></div>'),
                measureAreaButton.on('click touchstart', function () {
                    if ($(this).hasClass('active')) {
                        stopMeasure();
                    } else {
                        startMeasure('polygon');
                    }
                    return false;
                });
            $this.find('.olControlLayerSwitcher').append(measureAreaButton);

            function handleMeasurements(event) {
                var geometry = event.geometry;
                var units = event.units;
                var order = event.order;
                var measure = event.measure;
                var out = '';

                if (order == 1) {
                    out += 'Totale afstand: ' + measure.toFixed(3) + ' ' + units;
                } else {
                    out += 'Oppervlakte: ' + measure.toFixed(3) + ' ' + units + '<sup>2</sup>';
                }

                measureOutput[0].innerHTML = out;
            }

            for (var key in measureControls) {
                control = measureControls[key];
                control.events.on({
                    'measure': handleMeasurements,
                    'measurepartial': handleMeasurements
                });
                control.setImmediate(true);
                map.addControl(control);
            }
        },

        _load_autocomplete: function _load_autocomplete() {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var options = data.options;
            var isWMSMap = $this.attr('map-wms-layer-id') && $this.attr('map-wms-feature-attribute-id');

            if (!jQuery().autocomplete) {
                // No autocomplete function available, stop
                return false;
            }

            $this
                .find('input[type="text"].ezra_map-autocomplete')
                .each(function () {
                    var foundAddresses;

                    $(this).autocomplete({
                        source: function source(tag, response) {
                            $.getJSON(options.geocode_url,
                                {
                                    'term': tag['term']
                                },
                                function (data) {
                                    if (!(data.json.success == '1')) {
                                        response();
                                        return false;
                                    }

                                    var addresses = data.json.addresses,
                                        list_of_addresses = [];

                                    foundAddresses = data.json.addresses;

                                    for (var i = 0; i < addresses.length; i++) {
                                        var address = addresses[i];

                                        if (!address.identification) {
                                            continue;
                                        }

                                        list_of_addresses.push(address.identification);

                                    }

                                    response(list_of_addresses);
                                }
                            );
                        },

                        select: function select(event, ui) {
                            var only_center = 0;

                            if ($this.hasClass('ezra_map-readonly')) {
                                only_center = 1;
                            }

                            $this.ezra_map('setAddress', {
                                value: ui.item.value,
                                only_center: only_center
                            });

                            var selectedAddress = _.first(
                                foundAddresses.filter(function (address) {
                                    return address.identification === ui.item.value;
                                })
                            );
                            var epsg = $this.ezra_map('_get_epsg_points', selectedAddress.coordinates.lat, selectedAddress.coordinates.lng);

                            var identification;
                            if ($this.hasClass('ezra_map-latlononly')) {
                                identification = selectedAddress.coordinates.lat + "," + selectedAddress.coordinates.lng;
                            } else {
                                identification = selectedAddress.identification;
                            }

                            $this.ezra_map('setMarker', {
                                longitude: epsg.x,
                                latitude: epsg.y,
                                identification: identification,
                                popup_data: selectedAddress,
                                center: 1,
                                no_update: true
                            });

                        },

                        open: function open() {
                            $(this).autocomplete('widget').css('z-index', 2000);
                        }
                    });

                    $(this).autocomplete('widget').css('z-index', 1000);
                });
        },

        _set_click_handler: function _set_click_handler(event) {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var options = data.options;

            var pixel = new OpenLayers.Pixel(event.xy.x, event.xy.y);
            var lonlat = map.getLonLatFromViewPortPx(pixel);
            var wgs = $this.ezra_map('_get_wgs_latlon', lonlat.lon, lonlat.lat);

            $(this).trigger('ezra_map_click', wgs);

            if ($this.hasClass('ezra_map-latlononly')) {
                var featureWmsLayerId = $this.attr('map-wms-layer-id');

                $this.ezra_map('setMarker', {
                    longitude: lonlat.lon,
                    latitude: lonlat.lat,
                    identification: wgs.lat + ',' + wgs.lon,
                    popup_data: {
                        coordinates: {
                            lat: wgs.lat,
                            lng: wgs.lon
                        },
                        address: wgs.lat + ',' + wgs.lon
                    },
                    no_popup: featureWmsLayerId ? true : false
                });
            } else {
                $.getJSON(options.geocode_url,
                    {
                        'lat': wgs.lat,
                        'lon': wgs.lon
                    },
                    function (data) {
                        if (data.json.success == '1') {
                            var geo = data.json.addresses[0],
                                lat = geo.coordinates.lat,
                                lon = geo.coordinates.lng;

                            var epsg = $this.ezra_map('_get_epsg_points', lat, lon);

                            $this.ezra_map('setMarker', {
                                longitude: epsg['x'],
                                latitude: epsg['y'],
                                identification: geo.identification,
                                popup_data: geo
                            });
                        }
                    }
                );
            }
        },

        _load_markers: function _load_markers() {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var options = data.options;

            if (options.center) {
                var lonlat = options.center.split(' ');

                if (lonlat) {
                    var epsg = $this.ezra_map('_get_epsg_points', lonlat[0], lonlat[1]);

                    $this.ezra_map('setCenter', epsg['x'], epsg['y']);
                }
            }

            markers = new OpenLayers.Layer.Vector(
                'Markers',
                {
                    eventListeners: {
                        'featureselected': function (evt) {
                            var feature = evt.feature;
                            var popup = new OpenLayers.Popup.FramedCloud(
                                'popup',
                                OpenLayers.LonLat.fromString(feature.geometry.toShortString()),
                                new OpenLayers.Size(240, 150),
                                feature.attributes.htmlcontent,
                                null,
                                null
                            );

                            popup.panMapIfOutOfView = true;
                            popup.autoSize = true;
                            popup.minSize = new OpenLayers.Size(240, 150);
                            feature.popup = popup;

                            if (!$this.attr('map-wms-layer-id') && !$this.hasClass('ezra_map-latlononly')) {
                                map.addPopup(popup);
                                popup.show();
                            }
                        },

                        'featureunselected': function (evt) {
                            if (!$this.attr('map-wms-layer-id') && !$this.hasClass('ezra_map-latlononly')) {
                                var feature = evt.feature;

                                map.removePopup(feature.popup);
                                feature.popup.destroy();
                                feature.popup = null;
                            }
                        }
                    }
                }
            );

            markers.id = 'Markers';

            var selector = new OpenLayers.Control.SelectFeature(
                markers,
                {
                    autoActivate: true
                }
            );

            map.addControl(selector);
            map.addLayer(markers);
            data.selector = selector;

            // Make map clickable
            if (!$this.hasClass('ezra_map-readonly')) {
                map.events.register('click', this, methods['_set_click_handler']);
            }

            // Search for input fields
            $this.find('input.ezra_map-address').each(function () {
                $this.ezra_map('setAddress', {
                    value: $(this).val(),
                    no_update: 1,
                    center: 1
                });
            });

            // add markers from a array that contains address:
            if (data.options.markerAddresses) {
                var no_popup = false;

                // if there are multiple markers hide the popup:
                if (data.options.markerAddresses.length > 1) {
                    no_popup = true;
                    $this.addClass('ezra_map-multiple');
                }

                // add the markers:
                data.options.markerAddresses.forEach(function (address) {
                    $this.ezra_map('setAddress', {
                        value: address,
                        center: 1,
                        no_update: 1,
                        no_popup: no_popup
                    });
                });
            }

            // add markers from a array that contains coordinates:
            if (data.options.markerCoordinates) {
                var no_popup = false;
                var hasActiveMarker = false;

                // if there are multiple markers hide the popup:
                if (data.options.markerCoordinates.length > 1) {
                    no_popup = true;
                    $this.addClass('ezra_map-multiple');
                }

                var newBound = new OpenLayers.Bounds();

                // add the markers:
                data.options.markerCoordinates.forEach(function (coordinateData) {
                    hasActiveMarker = coordinateData.activeMarker ? true : hasActiveMarker;

                    var epsg = $this.ezra_map(
                        '_get_epsg_points',
                        coordinateData.coordinate[0],
                        coordinateData.coordinate[1]
                    );

                    $this.ezra_map('setMarker', {
                        longitude: epsg.x,
                        latitude: epsg.y,
                        popup_data: coordinateData.popupData,
                        center: data.options.markerCoordinates.length > 1 && !coordinateData.activeMarker ? 0 : 1,
                        no_update: 1,
                        no_popup: coordinateData.activeMarker ? false : no_popup
                    });
                    newBound.extend(new OpenLayers.LonLat(epsg.x, epsg.y));
                });

                // when markers are really close to each other. Do nothing.
                if (!hasActiveMarker && newBound.getWidth() + newBound.getHeight() > 500) {
                    var currentZoom = map.getZoomForExtent(newBound);

                    // zoom a bit out:
                    newBound = newBound.scale(map.getZoomForExtent(newBound) * .5);
                    map.zoomToExtent(newBound, true);
                }
            }

            // Search for input fields with json content
            $this.find('input.ezra_map-json').each(function () {
                $this.ezra_map('setFromJSON', jQuery.parseJSON($(this).val()));
            });
        },

        clearMarkers: function () {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var markers = map.getLayer('Markers');

            data.selector.unselectAll();
            markers.removeAllFeatures();
        },

        setCenter: function setCenter(lon, lat) {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var lonlat = new OpenLayers.LonLat(lon, lat);

            map.setCenter(lonlat, 9);
        },

        setMarker: function setMarker(options) {
            if (
                !options.hasOwnProperty('longitude') &&
                !options.hasOwnProperty('latitude') &&
                !options.hasOwnProperty('popup_data')       // data fields to fill out template
            ) {
                // required fields
                return;
            }

            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var markers = map.getLayer('Markers');

            var point = new OpenLayers.Geometry.Point(options.longitude, options.latitude);
            var popup_data;

            if (typeof options.popup_data === 'string') {
                popup_data = options.popup_data;
            } else {
                popup_data = $this.ezra_map('get_template', options.popup_data);
            }

            var marker = new OpenLayers.Feature.Vector(
                point,
                {
                    htmlcontent: popup_data
                },
                {
                    externalGraphic: data.options.marker_icon,
                    graphicOpacity: 1.0,
                    graphicWith: 21,
                    graphicHeight: 34,
                    graphicYOffset: -28
                }
            );



            // Clear all markers
            if (!$this.hasClass('ezra_map-multiple')) {
                $this.ezra_map('clearMarkers');
            }

            if (options.center) {
                var lonlat = new OpenLayers.LonLat(options.longitude, options.latitude);
                map.setCenter(lonlat, 9);
            }

            markers.addFeatures(marker);
            if (!options.no_popup) {
                data.selector.select(marker);
            }


            // Set address to input type = hidden
            if (options.identification) {
                $this.find('input.ezra_map-address').val(options.identification);

                if (!options.no_update) {
                    data.options.update(this);
                }
            }

        },

        get_template: function get_template(data_mapping) {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var tpl = $this.find('.ezra_map-template').clone().show().html();

            tpl = tpl.replace(/(%%.*?%%)/g, function (match, contents) {
                var map_key = contents.replace(/^%%/, '');
                map_key = map_key.replace(/%%$/, '');

                if (data_mapping[map_key]) {
                    return data_mapping[map_key];
                }

                return '';
            });

            return tpl;
        },

        setAddress: function setAddress(options) {
            var address = options.value;
            var only_center = options.only_center;
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;

            function applyCoords(data) {
                if (only_center) {
                    $this.ezra_map('setCenter', data.x, data.y);
                } else {
                    $this.ezra_map('setMarker', {
                        longitude: data.x,
                        latitude: data.y,
                        identification: data.identification,
                        popup_data: data,
                        center: 1,
                        no_update: options.no_update
                    });
                }
            }

            if ($this.hasClass('ezra_map-latlononly') && !$this.attr('map-wms-layer-id')) {
                var coordinates;
                var espg;
                var lat;
                var lng;

                if (_.isArray(address)) {
                    address = address[0];
                }

                coordinates = address ? address.split(',') : [];

                if (coordinates.length !== 2) {
                    return;
                }

                lat = parseFloat(coordinates[0].trim());
                lng = parseFloat(coordinates[1].trim());
                espg = $this.ezra_map('_get_epsg_points', lat, lng);

                applyCoords(_.merge(espg, {
                    identification: lat + ',' + lng,
                    address: lat + ',' + lng,
                    coordinates: {
                        lat: lat,
                        lng: lng
                    }
                }));
            } else {
                var callParameters = {};
                var found;
                if (found = address.match(/^([0-9]+(?:.[0-9]+)?), *([0-9]+(?:.[0-9]+)?)$/)) {
                    callParameters = {
                        lat: found[1],
                        lon: found[2]
                    };
                } else {
                    callParameters = {
                        'term': address
                    };
                }
                $.getJSON('/plugins/maps', callParameters,
                    function (data) {
                        if (!data || !data.json || !data.json.success) {
                            return;
                        }

                        if (data.json.success == '1') {
                            var geo = data.json.addresses[0];
                            var lat = geo.coordinates.lat;
                            var lon = geo.coordinates.lng;
                            var espg = $this.ezra_map('_get_epsg_points', lat, lon);

                            applyCoords(_.merge({}, geo, espg));

                            if ($this.attr('map-wms-layer-id') && $this.attr('map-wms-feature-attribute-id')) {

                                $this.find('input.ezra_map-address').val(lat + ',' + lon);
                                $this.ezra_map('getFeature');
                            }
                        }
                    }
                );
            }
        },

        /**
         * Retrieves feature based on the position of the center
         * of the map that is currently in view.
         * Use after setting a marker that also centers the map.
         **/
        getFeature: function getFeature() {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;
            var featureWmsLayerId = $this.attr('map-wms-layer-id');
            var featureWmsAttributeId = $this.attr('map-wms-feature-attribute-id');
            var featureLayer =
                map.layers.filter(
                    function (layer) {
                        return layer.$layerId === featureWmsLayerId;
                    }
                )[0];
            var WMSControl =
                _.find(map.controls,
                    function (control) {
                        return control.displayClass === 'olControlWMSGetFeatureInfo';
                    }
                );

            if (featureLayer && WMSControl) {
                var mapEl = $(this).find('.map');
                var centerX = mapEl.width() / 2;
                var centerY = mapEl.height() / 2;

                setTimeout(function () {
                    map.zoomTo(11);
                    WMSControl.request(new OpenLayers.Pixel(centerX, centerY));
                }, 300);
            }
        },

        setFromJSON: function setFromJSON(json, only_center) {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;

            lat = json.coordinates.lat;
            lon = json.coordinates.lng;

            var epsg = $this.ezra_map('_get_epsg_points', lat, lon);

            $this.ezra_map('setMarker', {
                longitude: epsg['x'],
                latitude: epsg['y'],
                identification: null,
                popup_data: json,
                center: null,
                no_popup: 1,
                no_update: 1
            });
        },

        /**
         * This calculation was based from the sourcecode of
         * Ejo Schrama's software <schrama@geo.tudelft.nl>.
         * You can find his software on:
         * http://www.xs4all.nl/~digirini/contents/gps.html
         *
         * @param point_x
         * @param point_y
         * @return {{lat: *, lon: *}}
         * @private
         */
        _get_wgs_latlon: function _get_wgs_latlon(point_x, point_y) {
            // Fixed constants / coefficients
            var x0 = 155000;
            var y0 = 463000;
            var k = 0.9999079;
            var bigr = 6382644.571;
            var m = 0.003773954;
            var n = 1.000475857;
            var lambda0 = 0.094032038;
            var phi0 = 0.910296727;
            var l0 = 0.094032038;
            var b0 = 0.909684757;
            var e = 0.081696831;
            var a = 6377397.155;

            // Convert RD to Bessel

            // Get radius from origin.
            d_1 = point_x - x0;
            d_2 = point_y - y0;
            r = Math.sqrt(Math.pow(d_1, 2) + Math.pow(d_2, 2));  // Pythagoras

            // Get Math.sin/Math.cos of the angle
            sa = (r != 0 ? d_1 / r : 0);  // the if prevents devision by zero.
            ca = (r != 0 ? d_2 / r : 0);

            psi = Math.atan2(r, k * 2 * bigr) * 2;   // php does (y,x), excel does (x,y)
            cpsi = Math.cos(psi);
            spsi = Math.sin(psi);

            sb = (ca * Math.cos(b0) * spsi) + (Math.sin(b0) * cpsi);
            d_1 = sb;

            cb = Math.sqrt(1 - Math.pow(d_1, 2));  // = Math.cos(b)
            b = Math.acos(cb);

            sdl = sa * spsi / cb;  // = Math.sin(dl)
            dl = Math.asin(sdl);         // delta-lambda

            lambda_1 = dl / n + lambda0;
            w = Math.log(Math.tan((b / 2) + (Math.PI / 4)));
            q = (w - m) / n;

            // Create first phi and delta-q
            phiprime = (Math.atan(Math.exp(q)) * 2) - (Math.PI / 2);
            dq_1 = (e / 2) * Math.log((e * Math.sin(phiprime) + 1) / (1 - e * Math.sin(phiprime)));
            phi_1 = (Math.atan(Math.exp(q + dq_1)) * 2) - (Math.PI / 2);

            // Create new phi with delta-q
            dq_2 = (e / 2) * Math.log((e * Math.sin(phi_1) + 1) / (1 - e * Math.sin(phi_1)));
            phi_2 = (Math.atan(Math.exp(q + dq_2)) * 2) - (Math.PI / 2);

            // and again..
            dq_3 = (e / 2) * Math.log((e * Math.sin(phi_2) + 1) / (1 - e * Math.sin(phi_2)));
            phi_3 = (Math.atan(Math.exp(q + dq_3)) * 2) - (Math.PI / 2);

            // and again...
            dq_4 = (e / 2) * Math.log((e * Math.sin(phi_3) + 1) / (1 - e * Math.sin(phi_3)));
            phi_4 = (Math.atan(Math.exp(q + dq_4)) * 2) - (Math.PI / 2);

            // radians to degrees
            lambda_2 = lambda_1 / Math.PI * 180;  //
            phi_5 = phi_4 / Math.PI * 180;

            // Bessel to wgs84 (lat/lon)
            dphi = phi_5 - 52;   // delta-phi
            dlam = lambda_2 - 5;   // delta-lambda

            phicor = (-96.862 - (dphi * 11.714) - (dlam * 0.125)) * 0.00001; // correction factor?
            lamcor = ((dphi * 0.329) - 37.902 - (dlam * 14.667)) * 0.00001;

            phiwgs = phi_5 + phicor;
            lamwgs = lambda_2 + lamcor;

            return {
                lat: phiwgs,
                lon: lamwgs
            };
        },

        /**
         * @param lat
         * @param lon
         * @return {{x: number|*, y: number|*}}
         * @private
         */
        _get_epsg_points: function _get_epsg_points(lat, lon) {
            // Fixed constants / coefficients
            x0 = 155000;
            y0 = 463000;
            k = 0.9999079;
            bigr = 6382644.571;
            m = 0.003773954;
            n = 1.000475857;
            lambda0 = 0.094032038;
            phi0 = 0.910296727;
            l0 = 0.094032038;
            b0 = 0.909684757;
            e = 0.081696831;
            a = 6377397.155;

            // wgs84 to bessel
            dphi = lat - 52;
            dlam = lon - 5;

            phicor = (-96.862 - dphi * 11.714 - dlam * 0.125) * 0.00001;
            lamcor = (dphi * 0.329 - 37.902 - dlam * 14.667) * 0.00001;

            phibes = lat - phicor;
            lambes = lon - lamcor;

            // bessel to rd
            phi = phibes / 180 * Math.PI;
            lambda = lambes / 180 * Math.PI;
            qprime = Math.log(Math.tan(phi / 2 + Math.PI / 4));
            dq = e / 2 * Math.log((e * Math.sin(phi) + 1) / (1 - e * Math.sin(phi)));
            q = qprime - dq;

            w = n * q + m;
            b = Math.atan(Math.exp(w)) * 2 - Math.PI / 2;
            dl = n * (lambda - lambda0);

            d_1 = Math.sin((b - b0) / 2);
            d_2 = Math.sin(dl / 2);

            s2psihalf = d_1 * d_1 + d_2 * d_2 * Math.cos(b) * Math.cos(b0);
            cpsihalf = Math.sqrt(1 - s2psihalf);
            spsihalf = Math.sqrt(s2psihalf);
            tpsihalf = spsihalf / cpsihalf;

            spsi = spsihalf * 2 * cpsihalf;
            cpsi = 1 - s2psihalf * 2;

            sa = Math.sin(dl) * Math.cos(b) / spsi;
            ca = (Math.sin(b) - Math.sin(b0) * cpsi) / (Math.cos(b0) * spsi);

            r = k * 2 * bigr * tpsihalf;
            x = r * sa + x0;
            y = r * ca + y0;

            return {
                x: x,
                y: y
            };
        },

        _load_custom_marker_icon: function () {
            var $this = $(this),
                data = $this.data('ezra_map'),
                options = data.options;

            if (!options.marker_icon) {
                return false;
            }

            var size = new OpenLayers.Size(21, 34);
            var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
            var icon = new OpenLayers.Icon(options.marker_icon, size, offset);

            return icon.clone();
        },

        _load_layer: function (options, name) {
            return options.layers[name].clone();
        },

        _load_layers: function (options) {
            var layers = [];
            jQuery.each(options.layers, function (key, value) {
                layers.push(value.clone());
            });
            return layers;
        },

        _load_base_layer: function (options) {
            var $this = $(this);

            var base_layer_name = options.base_layer;

            return $this.ezra_map('_load_layer', options, base_layer_name);
        },

        refreshViewport: function (options) {
            var $this = $(this),
                data = $this.data('ezra_map');

            if (!data && !options) {
                return false;
            }

            if (!options) {
                options = data.options;
            }

            var mapelem = $this.find('.ezra_map-viewport');

            $.each(options.viewport, function (key, value) {
                var viewport_conf = options.viewport[key];
                if ($this.hasClass('ezra_map-' + key)) {
                    $.each(viewport_conf.css, function (csskey, cssval) {
                        mapelem.css(csskey, cssval);
                    });
                }
            });

            mapelem.attr('id', 'ezra_map-id-' + Math.floor((Math.random() * 10000) + 1));

            return mapelem;
        },

        clearMouseCache: function clearMouseCache() {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var map = data.map;

            markers = map.getLayer('Markers');
            markers.events.clearMouseCache();
        },

        _load_map: function _load_map(options, layer) {
            var $this = $(this);
            var data = $this.data('ezra_map');
            var mapelem = $this.ezra_map('refreshViewport', options);

            if (options.img_path) {
                OpenLayers.ImgPath = options.img_path;
            }

            map = new OpenLayers.Map(
                mapelem.attr('id'),
                {
                    theme: options.theme,
                    xy_precision: 3,
                    zoom: 3,
                    center: '155000,463000',
                    maxExtent: new OpenLayers.Bounds(-285401.92, 22598.08, 595401.92, 903401.92),
                    controls: [
                        new OpenLayers.Control.Navigation(),
                        new OpenLayers.Control.ArgParser(),
                        new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.LayerSwitcher({ 'ascending': false }),
                        new OpenLayers.Control.ScaleLine(),
                        new OpenLayers.Control.MousePosition(),
                        new OpenLayers.Control.KeyboardDefaults(
                            {
                                observeElement: mapelem.attr('id')
                            }
                        )
                    ],
                    eventListeners: {
                        'changelayer': function (e) {
                            if (!isIntern) { // do not save setting on pip
                                return;
                            }

                            var settings = {};

                            _.each(e.object.layers, function (layer) {

                                var visible = layer.visibility === false ? false : true,
                                    layerId = layer.name;

                                settings[layer.name] = visible;

                                if (layer.name === 'Markers') {
                                    _.each(map.popups, function (popup) {
                                        if (visible) {
                                            popup.show();
                                        } else {
                                            popup.hide();
                                        }
                                    });
                                }

                            });

                            $.ajax({
                                type: 'POST',
                                url: '/api/user/settings',
                                data: JSON.stringify({ pdokSettings: settings }),
                                contentType: 'application/json;charset=UTF-8',
                                success: function success(data) {
                                    // ZS-FIXME: empty function body
                                    // your settings are saved.
                                },
                                dataType: 'json'
                            });
                        }
                    }
                }
            );

            // OpenTopo has 14 zoom levels (with OpenLayers 2)
            map.getNumZoomLevels = function() { return 14; }

            map.addLayers($this.ezra_map('_load_layers', options));
            map.setBaseLayer(layer);
            map.zoomToMaxExtent();

            var lonlat = new OpenLayers.LonLat(155000, 463000);

            map.setCenter(lonlat, 3);

            if ($this.attr('map-wms-layer-id')) {
                // set name of clicked feature as value of feature attribute id
                var featureWmsLayerId = $this.attr('map-wms-layer-id');
                var featureWmsAttributeId = $this.attr('map-wms-feature-attribute-id');
                var featureLayer;
                var featureControl;

                featureLayer = map.layers.filter(
                    function (layer) {
                        return layer.$layerId === featureWmsLayerId;
                    }
                )[0];

                if (featureLayer) {
                    featureControl = new OpenLayers.Control.WMSGetFeatureInfo({
                        url: featureLayer.url,
                        title: '',
                        layers: [featureLayer],
                        infoFormat: 'text/xml',
                        queryVisible: true
                    });

                    featureControl.events.register('getfeatureinfo', this, function (event) {
                        var parser = new DOMParser();
                        var xml = parser.parseFromString(event.text, 'text/xml');
                        var path = featureLayer.$featureInfoXPath;
                        var featureName;
                        var field = $('[name="kenmerk_id_' + featureWmsAttributeId + '"]');
                        var fieldNodeName = field.prop('nodeName');
                        var oldValue;

                        // Install Wicked Good XPath Parser to make sure IE11 has a functioning XPath parser
                        if (!xml.evaluate) {
                            wgxpath.install();
                        }

                        try {
                            var featureResult = xml.evaluate(path, xml, function (ns) {
                                return xpathNamespaces[ns];
                            }, XPathResult.ANY_TYPE, null);

                            if (featureResult) {
                                switch (featureResult.resultType) {
                                case XPathResult.STRING_TYPE:
                                    featureName = featureResult.stringValue;
                                    break;
                                case XPathResult.NUMBER_TYPE:
                                    featureName = featureResult.numberValue;
                                    break;
                                default:
                                    featureName = featureResult.iterateNext().textContent;
                                    break;
                                }
                            }

                            switch (fieldNodeName) {
                            case 'INPUT':
                                switch (field.attr('type')) {
                                case 'radio':
                                    $(field.selector).prop('checked', false);
                                    $(field.selector + '[value="' + featureName + '"]').prop('checked', true);
                                    break;
                                case 'text':
                                case 'hidden':
                                    field.val(featureName);
                                    break;
                                default:
                                    console.log('Unsupported input field type:', field.attr('type'));
                                }
                                break;
                            case 'SELECT':
                                field.val(featureName);
                                break;
                            default:
                                console.log('Unsupported field node name:', fieldNodeName);
                            }

                            var webform_controller = angular.element(field).inheritedData('$zsCaseWebformController');

                            if (!webform_controller) {
                                console.warn('Form field "' + field + '" not found. Was the "map" field in the casetype (re)configured correctly after import?');
                            }

                            var rule_manager = webform_controller.getRuleManager();

                            rule_manager.setValue(field.attr('data-magic-string'), [featureName], [featureName]);
                            rule_manager.invalidateRules();
                        } catch (error) {
                            console.warn('xpath "' + path + '" did not return a value, resolving value as empty');
                            $('[name="kenmerk_id_' + featureWmsAttributeId + '"]').val('');
                        }
                    });

                    map.addControl(featureControl);
                    featureControl.activate();
                }
            }

            map.mapLayerIsLoading = function mapLayerIsLoading(isLoadingStart) {
                if (isLoadingStart) {
                    $this.find('.spinner-groot').css('visibility', 'visible');
                    mapLayerIsLoadingCounter++;
                } else {
                    mapLayerIsLoadingCounter--;

                    if (mapLayerIsLoadingCounter <= 0) {
                        mapLayerIsLoadingCounter = 0;
                        $this.find('.spinner-groot').css('visibility', 'hidden');
                    }
                }
            };

            return map;
        },

        destroy: function destroy() {
            return this.each(function () {
                var $this = $(this);
                var data = $this.data('ezra_map');

                // Namespacing FTW
                $(window).unbind('.ezra_map');
                data.ezra_map.remove();
                $this.removeData('ezra_map');
            });
        },
        log: function log(message) {
            if (typeof console != 'undefined' && typeof console.log == 'function') {
                console.log(message);
            }
        }
    };

    var firstTime_loadSettings = true;
    var additionalLayerOptions = {
        isBaseLayer: false,
        singleTile: true,
        buffer: 0,
        visibility: false,
        featureInfoFormat: 'application/vnd.ogc.gml',
        tileOptions: {
            eventListeners: {
                loaderror: function loaderror(evt) {
                    console.error('Error loading map layer:', evt);

                    var bodyScope = angular.element(document.body).scope();

                    if (bodyScope) {
                        bodyScope.$broadcast('systemMessage', {
                            content: mapLayerLoadErrorMessage,
                            type: 'error'
                        });
                    }
                },

                loadstart: function loadstart(evt) {
                    map.mapLayerIsLoading(true);
                },

                loadend: function loadend(evt) {
                    map.mapLayerIsLoading(false);
                }
            }
        }
    };

    var mapLayerIsLoadingCounter = 0;
    var mapLayerLoadErrorMessage = 'Kaartlaag kan niet worden geladen';
    var resolutions = [
        3440.640, 1720.320, 860.160, 430.080,
        215.040, 107.520, 53.760, 26.880,
        13.440, 6.720, 3.360, 1.680,
        0.840, 0.420, 0.210, 0.105, 0.0525
    ];

    function getLayersObject(featureWmsLayerId) {
        return {
            layers: {
                opentopo: new OpenLayers.Layer.WMTS(
                    {
                        name: 'OpenTopo achtergrondkaart',
                        url: 'https://geodata.nationaalgeoregister.nl/tiles/service/wmts',
                        displayInLayerSwitcher: false,
                        layer: 'opentopoachtergrondkaart',
                        style: 'default',
                        type: 'png8',
                        transparent: true,
                        visibility: true,
                        singleTile: false,
                        alpha: true,
                        numZoomLevels: 14,
                        zoom: 6,
                        projection: 'EPSG:28992',
                        matrixSet: 'EPSG:28992',
                        units: 'm',
                        resolutions: resolutions,
                        tileOptions: {
                            eventListeners: {
                                'loadend': function (evt) {
                                    if (firstTime_loadSettings) {
                                        firstTime_loadSettings = false;
                                        if (!isIntern) { // do not load setting on pip
                                            var mapSettings = _.defaults({}, {
                                                Markers: true,
                                                'OpenTopo achtergrondkaart': true
                                            });

                                            _.each(map.layers, function (layer) {
                                                var layerId = layer.name;

                                                layer.setVisibility(mapSettings[layerId] || layer.$layerId === featureWmsLayerId);
                                            });

                                            return;
                                        }

                                        $.ajax({
                                            type: 'GET',
                                            url: '/api/user/settings',
                                            contentType: 'application/json;charset=UTF-8',
                                            success: function success(data) {
                                                if (!('pdokSettings' in data.result[0])) {
                                                    data.result[0].pdokSettings = {};
                                                }

                                                var pdokSettings = _.defaults(data.result[0].pdokSettings, {
                                                    Markers: true,
                                                    'OpenTopo achtergrondkaart': true
                                                });

                                                _.each(map.layers, function (layer) {
                                                    var layerId = layer.name;

                                                    layer.setVisibility(pdokSettings[layerId] || layer.$layerId === featureWmsLayerId);
                                                });
                                            },
                                            dataType: 'json'
                                        });
                                    }
                                }
                            }
                        }
                    }
                )
            },
            base_layer: 'opentopo'
        };
    }

    $.fn.ezra_map = function ezra_map(method) {
        function proxyCall(method, target, args) {
            if (methods[method]) {
                return methods[method].apply(target, Array.prototype.slice.call(args, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(target, args);
            } else {
                $.error('Method ' + method + ' does not exist on jQuery.ezra_map');
            }
        }

        if (isLoadingMapLayers) {
            mapCallsStore.push({
                method: method,
                target: this,
                arguments: arguments
            });
            return;
        }

        if (method.firstTime_addLayers) {
            if ($('body').hasClass('intern')) {
                isIntern = true;
            }

            var ezra_maps_layers;
            var that = this;

            delete method.firstTime_addLayers;
            firstTime_loadSettings = true;
            isLoadingMapLayers = true;

            // ZS-FIXME: function **declarations** in blocks are invalid in ES5,
            // Mozilla did support them as statements, other implementations silently
            // moved them out of the block, and now they are legal in ES2015.
            function setLayers(data) {
                var featureWmsLayerId = $(that).attr('map-wms-layer-id');

                ezra_maps_layers = getLayersObject(featureWmsLayerId);
                method.layers = ezra_maps_layers.layers;
                method.base_layer = ezra_maps_layers.base_layer;

                // add layer data to ezra_maps_layers
                data.forEach(function (layerData) {
                    if (layerData.instance.active) {
                        var layer = method.layers[layerData.instance.layer_name] = new OpenLayers.Layer.WMS(layerData.instance.label,
                            layerData.instance.url,
                            {
                                'layers': layerData.instance.layer_name,
                                'format': 'image/png',
                                'transparent': true,
                            },
                            additionalLayerOptions
                        );

                        // so we can identify it in getLayersObject()
                        layer.$layerId = layerData.instance.layer_name;
                        // so we can use it when user clicks on map
                        layer.$featureInfoXPath = layerData.instance.feature_info_xpath;
                    }
                });

                isLoadingMapLayers = false;
                // start it again:
                that.ezra_map(method);

                while (mapCallsStore.length > 0) {
                    var obj = mapCallsStore.pop();

                    proxyCall(obj.method, obj.target, obj.arguments);
                }
            }

            function setNamespaces(data) {
                xpathNamespaces = {};
                data.forEach(function (namespaceData) {
                    xpathNamespaces[namespaceData.instance.prefix] = namespaceData.instance.namespace_uri;
                });
            }

            var DEFAULT_LAYERS = [
                {
                    instance: {
                        active: true,
                        layer_name: 'gemeenten',
                        label: 'Gemeentegrenzen',
                        url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms',
                    }
                },
                {
                    instance: {
                        active: true,
                        layer_name: 'provincies',
                        label: 'Provinciegrenzen',
                        url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms',
                    }
                },
                {
                    instance: {
                        active: true,
                        layer_name: 'pand',
                        label: 'BAG-objecten',
                        url: 'https://geodata.nationaalgeoregister.nl/bag/wms',
                    }
                }
            ];

            // load layerdata:
            $.ajax({
                type: 'GET',
                url: '/api/v1/map/ol_settings',
                contentType: 'application/json;charset=UTF-8',
                dataType: 'json',

                // on error supply default layers:
                error: function error(err) {
                    console.log('Could not receive map settings. Setting default map layers.');
                    setLayers(DEFAULT_LAYERS);
                    setNamespaces([]);
                },

                success: function success(data) {
                    if ('wms_layers' in data.result.instance) {
                        setLayers(data.result.instance.wms_layers);
                        setNamespaces(data.result.instance.xml_namespaces);
                    } else {
                        console.log('Could receive map settings, but no wms_layers found. Setting default map layers.');
                        setLayers(DEFAULT_LAYERS);
                        setNamespaces([]);
                    }
                }
            });

            return;
        }

        return proxyCall(method, this, arguments);
    };
})(jQuery);

OpenLayers.Lang['nl'] = OpenLayers.Util.applyDefaults({
    'Overlays': 'Lagen'
});
