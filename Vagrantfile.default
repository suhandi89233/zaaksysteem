# -*- mode: ruby -*-
# vi: set ft=ruby :

module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
end

# vagrant plugin install vagrant-docker-compose
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.network "private_network", ip: "10.44.0.11"
  config.vm.hostname = "vagrant.zaaksysteem.nl"

  config.vm.provider "virtualbox" do |v|
    v.memory = 6 * 1024
  end

  config.vm.provision :docker
  config.vm.provision :docker_compose, yml: "/vagrant/docker-compose.yml", run: "always"

  if OS.mac? || OS.windows? then
    config.vm.synced_folder ".", "/vagrant", type: "rsync",
            rsync__exclude: [".git/", "db/initial/00_*.sql", "*.swp"]
    config.gatling.rsync_on_startup = true
  else
    config.vm.synced_folder ".", "/vagrant", type: "nfs"
  end
end
