
choice=$1

usage() {
    local rc=$1

    [ -z "$rc" ] && rc=0

    echo "Please supply a command: reset, dev or init" >&2;
    exit $rc

}

start_frontend() {
    docker-compose restart frontend
}

case $choice in
    init)
        start_frontend
        docker-compose exec -T frontend \
            /opt/zaaksysteem/dev-bin/npm_container.sh
        ;;
    dev)
        start_frontend
        docker-compose exec -T frontend \
            /opt/zaaksysteem/dev-bin/run-npm-start.sh
        ;;
    reset)
        start_frontend
        docker-compose exec -T frontend \
            /opt/zaaksysteem/dev-bin/reset-frontend.sh
        ;;
    *) usage 1;;

esac




