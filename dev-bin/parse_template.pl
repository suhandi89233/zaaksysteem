use strict;
use warnings;

use utf8;

package My::ZTT::Context;
use Moose;
with "Zaaksysteem::ZTT::Context";
use Zaaksysteem::ZTT::Element;
use List::Util 'any';

my $RICH_TEXT_FIELD = <<'RICH_TEXT_FIELD';
<p>[[ rich_complex ]]</p>
RICH_TEXT_FIELD

my $RICH_COMPLEX = <<'RICH_COMPLEX';
<h1>test</1>
<h2>gewone tekst</h2>
<p><u>eerst</u> maar eens <s>gewone</s> tekst!</p>
<p>En tekst met een geneste magic string: test_magic_plain_text «[[ text_simple ]]»</p>
<h2>numeriek lijstje</h2>
<ol>
    <div> What </div>
    <li>Eén</li>
    <li>[[ text_list_item ]]</li>
    <li>DRIE</li>
</ol>
<p>Maar hoe zit dat dan met <strong>VETTE TEKST</strong></p><p><strong>OVER</strong> meerdere paragraven zit?</p>
<ol>
    <li><u>appel</u></li>
    <li>«…[[ rich_list ]]…»</li>
    <li>woensdag
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam et urna
        ac enim efficitur molestie at dictum sapien. Aliquam bibendum ipsum
        vitae libero accumsan maximus. Sed maximus faucibus lorem quis sagittis.
        Curabitur convallis sollicitudin elementum. Etiam vel ligula justo.
        Pellentesque habitant morbi tristique senectus et netus et malesuada
        fames ac turpis egestas. Sed sed arcu pellentesque, pharetra felis sit
        amet, laoreet neque. In id auctor ipsum.</p>
    </li>
    <li>[[ text_list_item ]]</li>
</ol>
<h3>kapotte lijstjes</h3>
[[ nested_list_ul_ol ]]
<h3>overlappende spannen</h3>
<p>en <strong>hoe zit het dan met <em>overlappende</em></strong><em> letter</em>-stijlen</p>
<h3>hyper</h3>
<p>klik <a href=\"www.hier.nl\" target=\"_blank\">hier</a> voor méér informatie</p>
<h1>Unicorn support</h1>
<p><br></p>
<p>groeten van de ☃</p>
<br>
<br>
<p>en hier gaan we de boel verzieken met über-magic:</p>
<p>[[ rich_list ]]<p>
<p>en dat was het</p>
RICH_COMPLEX

my $RICH_LIST = <<'RICH_LIST';
<ul>
    <li>
        nog meer <u>onderstereping</u>
    </li>
    <li>
        <b>vette</b> tekst
    </li>
</ul>
<p>en magic_strings: {[[ text_simple ]]}<p>
RICH_LIST

my $RICH_BOLD = <<'RICH_BOLD';
<p>dit is <b>Vette Tekst</b> op één regel</p>
RICH_BOLD

my $RICH_HEADER_MAGIC = <<'RICH_HEADER_MAGIC';
<h2>[[ text_simple ]]</h2>
RICH_HEADER_MAGIC

my $RICH_HEADINGS = <<'RICH_HEADINGS';
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>
RICH_HEADINGS

my $RICH_PARAGRAPH = <<'RICH_PARAGRAPH';
<p>Dit is een korte paragraaf die slechts een paar regeltjes bevat. Het is
alleen maar een <em>voorbeeldje</em> zodat we kunnen zien dat het voor
eenvoudige tekst werk. Gebruik het type <strong>\[\[\ rich_multi_para \]\]
</strong> voor langere teksten</p>
RICH_PARAGRAPH

my $RICH_PARAGRAPHS = <<'RICH_PARAGRAPHS';
<p>Dit is een <strong>korte paragraaf</strong> die slechts een paar regeltjes
bevat. maar weest niet ongerust, er volgt meer tekst hieronder, in de tweede
paragraaf</p>
<p>Hier is die <em>tweede paragraaf</em>, maar sorry, mij ontbreekt het aan
inspiratie om hier een leuk verhaaltje van te maken en zo. Ik zou zeggen, bedenk
zelf maar wat!</p>
RICH_PARAGRAPHS

my $RICH_MULTI_PARA = <<'RICH_MULTI_PARA';
<h1>Aliniea's Een en Twee</h1>
<p>[[ rich_lorem_ipsum_1 ]]</p>
<p>[[ rich_lorem_ipsum_2 ]]</p>
<h2>Alinea Drie tot slot</h2>
<p>[[ rich_lorem_ipsum_3 ]]</p>
RICH_MULTI_PARA

my $RICH_LOREM_IPSUM_1 = <<'LOREM_IPSUM_1';
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non magna ante.
Quisque consectetur metus id turpis viverra feugiat. Etiam mollis at elit vel
sagittis. Quisque ut risus tellus. Integer porta enim a suscipit porta.
Pellentesque fermentum odio nec tincidunt maximus. Vestibulum a metus in lorem
faucibus ornare eget sit amet erat. Maecenas ullamcorper dictum libero, at
maximus ante tempor ut.</p>
LOREM_IPSUM_1

my $RICH_LOREM_IPSUM_2 = <<'LOREM_IPSUM_2';
<p>Proin quis eleifend dui, vel maximus nunc. Integer porttitor mi eget diam
lobortis bibendum. Aenean quis dolor nec mi lacinia mattis. Etiam at libero eu
ex posuere molestie. Proin condimentum auctor porttitor. Donec sodales purus id
turpis dignissim, ultrices venenatis nisl vulputate. Duis diam nibh, cursus id
placerat eget, ultricies sed ligula. Nullam sollicitudin, nunc ut suscipit
ultrices, ex erat auctor urna, sed posuere neque odio vitae ligula. Quisque ut
vehicula metus. Pellentesque dignissim lorem quis elementum consectetur. Aliquam
finibus nulla odio, eget pellentesque lorem tempus nec. Maecenas eu orci ac
ligula tempus tempus nec eu risus. Nullam porttitor purus a ligula finibus
congue. Sed sed volutpat purus. In rutrum, purus ut hendrerit pretium, nulla
magna hendrerit ante, sed efficitur eros dui id libero.</p>
LOREM_IPSUM_2
my $RICH_LOREM_IPSUM_3 = <<'LOREM_IPSUM_3';
<p>Maecenas sed odio eget enim imperdiet feugiat. Cras leo mauris, mollis a
fermentum sit amet, lobortis non velit. Nullam neque augue, auctor at ultricies
pharetra, gravida quis libero. In eget risus sed purus ultrices ultrices eget eu
nulla. Duis accumsan ipsum est, a elementum mauris pharetra ut. Donec porttitor
enim elit. Sed id vulputate enim.</p>
LOREM_IPSUM_3

my $NESTED_LIST_UL_OL = <<'NESTED_LIST_UL_OL';
<ul>
    <li>Fiets</li>
    <li>Nest</li>
    <li>
        <ol>
            <li>Kuiken</li>
            <li>Eitjes</li>
        </ol>
    </li>
    <li>Boom</li>
</ul>
NESTED_LIST_UL_OL

my $SIMPLE_LIST_OL = <<'SIMPLE_LIST_OL';
<ol>
    <li>Une</li>
    <li>Dos</li>
    <li>Très</li>
<ol>
SIMPLE_LIST_OL

my $SIMPLE_LIST_UL = <<'SIMPLE_LIST_UL';
<ul>
    <li>Foo</li>
    <li>Bar</li>
    <li>Baz</li>
    <li>Qux</li>
</ul>
SIMPLE_LIST_UL

my $SIMPLE_LISTS = <<'SIMPLE_LISTS';
<ol>
    <li>Une</li>
    <li>Dos</li>
    <li>Très</li>
<ol>
<ul>
    <li>Foo</li>
    <li>Bar</li>
    <li>Baz</li>
    <li>Qux</li>
</ul>
SIMPLE_LISTS

sub get_string_fetchers {
    return (
        string_fetcher_for_plaintext ( text_simple        => 'magically replaced plain text' ),
        string_fetcher_for_richtext  ( text_list_item     => 'THIS IS AN ITEM'               ),
        string_fetcher_for_richtext  ( text_heading_boo   => 'BOOHOOOOO'                     ),
        string_fetcher_for_richtext  ( rich_text_field    => $RICH_TEXT_FIELD                ),
        string_fetcher_for_richtext  ( rich_complex       => $RICH_COMPLEX                   ),
        string_fetcher_for_richtext  ( rich_list          => $RICH_LIST                      ),
        string_fetcher_for_richtext  ( rich_bold          => $RICH_BOLD                      ),
        string_fetcher_for_richtext  ( rich_header_magic  => $RICH_HEADER_MAGIC              ),
        string_fetcher_for_richtext  ( rich_headings      => $RICH_HEADINGS                  ),
        string_fetcher_for_richtext  ( rich_paragraph     => $RICH_PARAGRAPH                 ),
        string_fetcher_for_richtext  ( rich_multi_para    => $RICH_MULTI_PARA                ),
        string_fetcher_for_richtext  ( rich_lorem_ipsum_1 => $RICH_LOREM_IPSUM_1             ),
        string_fetcher_for_richtext  ( rich_lorem_ipsum_2 => $RICH_LOREM_IPSUM_2             ),
        string_fetcher_for_richtext  ( rich_lorem_ipsum_3 => $RICH_LOREM_IPSUM_3             ),
        string_fetcher_for_richtext  ( nested_list_ul_ol  => $NESTED_LIST_UL_OL              ),
        string_fetcher_for_richtext  ( simple_list_ol     => $SIMPLE_LIST_OL                 ),
        string_fetcher_for_richtext  ( simple_list_ul     => $SIMPLE_LIST_UL                 ),
        string_fetcher_for_richtext  ( simple_lists       => $SIMPLE_LISTS                   ),
    )
}

sub elements_from_richtext {
    my $richtext = shift;
    
    my $builder = HTML::TreeBuilder->new;
    $builder->implicit_body_p_tag(1);
    $builder->p_strict(1);
    $builder->no_space_compacting(1);
    
    my $tree = $builder->parse($richtext);
    $builder->eof;

    my $body = $tree->find('body');
    my @found = grep {
        my $tag = $_->tag();
        any { $_ eq $tag } qw( p ul ol h1 h2 h3 h4 h5 h6 )
    } $body->content_list();
    return @found
}

sub string_fetcher_for_plaintext{
    my ( $name, $value ) = @_;

    return sub {
        my $tag = shift;
        my $tagname = $tag->name;
        return unless $tagname eq $name;

        return Zaaksysteem::ZTT::Element->new(
            value => $value,
            type  => 'plaintext',
        );
    }
}

sub string_fetcher_for_richtext{
    my ( $name, $value ) = @_;

    return sub {
        my $tag = shift;
        my $tagname = $tag->name;
        return unless $tagname eq $name;

        return Zaaksysteem::ZTT::Element->new(
            value => [ elements_from_richtext(shift) ],
            type  => 'richtext',
            original_value => $value
        );
    }
}

package main;


use File::Temp;
use OpenOffice::OODoc;
use Path::Tiny;

use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::Template::OpenOffice;

my $document_template = $ARGV[0] or die "missing template file\n";
my $document_output   = $ARGV[1] or die "missing output file\n";

my $dir = File::Temp->newdir();
odfWorkingDirectory($dir);
my $odf_template = odfDocument(
    file           => $document_template,
    local_encoding => $OpenOffice::OODoc::XPath::LOCAL_CHARSET,
    opendocument   => 1,
);

$odf_template->{opendocument} = 1;

my $template = Zaaksysteem::ZTT::Template::OpenOffice->new(
    document => $odf_template
);

my $ztt = Zaaksysteem::ZTT->new();

my $ztt_context = My::ZTT::Context->new();
$ztt->add_context( $ztt_context );

my $processed_odt = $ztt->process_template($odf_template)->document;

$processed_odt->save($document_output);

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

