package Zaaksysteem::StUF::Stuurgegevens;

use Moose;
use Moose::Util qw[apply_all_roles];

use BTTW::Tools;

use Zaaksysteem::StUF::Parser;

use constant STUF_STUURGEGEVENS_FIELDS  => [qw/
    entiteittype
    berichtsoort
    sectormodel
    versieStUF
    versieSectormodel

    zender
    ontvanger
    referentienummer
    tijdstipBericht

    kennisgeving
    vraag
    antwoord
    bevestiging
    fout

    berichtcode
    crossRefnummer
/];

has [ @{ STUF_STUURGEGEVENS_FIELDS() }]     => (
    is          => 'rw',
);

has '+zender'       => (
    required    => 1,
);

has '+ontvanger'    => (
    required    => 1,
);

has '+versieStUF'   => (
    lazy        => 1,
    default     => '0204',
);

has '+versieSectormodel'   => (
    lazy        => 1,
    default     => '0204',
);

has '+sectormodel'   => (
    lazy        => 1,
    default     => 'BG',
);

has '+berichtcode'   => (
    trigger     => sub {
        my $self            = shift;

        $self->berichtsoort(shift);
    }
);

sub TO_STUF {
    my $self                = shift;

    return {
        map {
            $_  => $self->$_
        } grep { defined($self->$_) } @{ STUF_STUURGEGEVENS_FIELDS() }
    };
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STUF_STUURGEGEVENS_FIELDS

TODO: Fix the POD

=cut

=head2 TO_STUF

TODO: Fix the POD

=cut

