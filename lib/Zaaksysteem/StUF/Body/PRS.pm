package Zaaksysteem::StUF::Body::PRS;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'PRS';

use constant    OBJECT_VALUES   => [
    {
        name        => 'bsn-nummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'a-nummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'voornamen',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'voorletters',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'voorvoegselGeslachtsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'geslachtsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'geboortedatum',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'geslachtsaanduiding',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'datumOverlijden',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'indicatieGeheim',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'burgerlijkeStaat',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aanduidingNaamgebruik',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    ### Relationships
    {
        name        => 'PRSADRVBL',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'ADR',
    },
    {
        name        => 'PRSADRCOR',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'ADR',
    },
    {
        name        => 'PRSPRSHUW',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'PRS',
    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);

has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 OBJECT_NAME

TODO: Fix the POD

=cut

=head2 OBJECT_VALUES

TODO: Fix the POD

=cut

