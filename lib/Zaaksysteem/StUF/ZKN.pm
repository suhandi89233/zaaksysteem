package Zaaksysteem::StUF::ZKN;
use Moose;
with 'MooseX::Log::Log4perl';
use namespace::autoclean;

=head1 NAME

Zaaksysteem::StUF::ZKN - A generic StUF-ZKN module

=head1 DESCRIPTION

This is the base model of any StUF-ZKN module that you build. This model
takes care of receiving and sending messages for StUF-ZKN.

=head1 SYNOPSIS

    package Foo;
    use Moose;

    extends 'Zaaksysteem::StUF::ZKN';

=cut

use BTTW::Tools qw(throw sig);
use List::Util qw(any);
use UUID::Tiny ':std';
use Zaaksysteem::Constants qw(RGBZ_GEMEENTECODES);

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object, required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 interface

A Zaaksysteem::Model::DB::Interface> object, required.

=cut


has interface => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Interface',
    required => 1,
);

=head2 message_version

Message version, consumers must define this attribute

=cut

has major_version => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 message_version

Message sub version, consumers must define this attribute

=cut

has minor_version => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 municipality_code

The municipality code. Required.

=cut

has municipality_code => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 municipality_name

The name of the municipality. Automaticly generated from the
C<municipality_code>

=cut

has municipality_name => (
    is      => 'ro',
    isa     => 'Str',
    builder => '_build_municipality_name',
    lazy    => 1,
);

=head2 soap_actions

A list of supported soap actions

=cut

has soap_actions => (
    is       => 'ro',
    isa      => 'ArrayRef',
    required => 1,
);

=head2 encoder

An encoder to encode outgoing messages.

Consumers must implement the function C<_build_encoder>.

=cut

has encoder => (
    is      => 'ro',
    lazy    => 1,
    builder => '_build_encoder',
);

=head2 decoder

A decoder to decode incoming messages

Consumers must implement the function C<_build_decoder>.

=cut

has decoder => (
    is      => 'ro',
    lazy    => 1,
    builder => '_build_decoder',
);

=head1 METHODS

=head2 label

Displays a human readable label for the model

=cut

sub label {
    my $self = shift;

    return "StUF-ZKN versie " . $self->version;
}

=head2 shortname

Displays a short identitifier for the module

=cut

sub shortname {
    my $self = shift;
    return 'stufzkn_' . $self->version;
}

=head2 version

The full version of the message

=cut

sub version {
    my $self = shift;
    return sprintf("%d.%d", $self->major_version, $self->minor_version);
}

=head2 find_case_by_reference

Find an open case by the reference in the object subscription table.
Dies if we cannot find an open case.

=cut

sig find_case_by_reference => 'Str';

sub find_case_by_reference {
    my ($self, $reference) = @_;

    my $rs = $self->schema->resultset('ObjectSubscription')->search_rs(
        {
            external_id  => $reference,
            local_table  => 'Zaak',
            interface_id => $self->interface->id,
        }
    );

    my $os = $rs->next;

    if (!$os) {
        throw('stufzkn/reference/notfound',
            "$reference cannot be matched against a case");
    }
    elsif ($rs->next) {
        throw('stufzkn/reference/duplicate',
            "$reference is linked to multiple cases");
    }

    my $case
        = $self->schema->resultset('Zaak')->find($os->get_column('local_id'));

    return $case if $case && !$case->is_afgehandeld;

    throw(
        'stufzkn/reference/no_open_case',
        "No open case can be found by object_subscription: $reference",
    );
}

=head2 create_object_subscription

Create an object subscription for a case

=cut

sig create_object_subscription => 'Zaaksysteem::Schema::Zaak';

sub create_object_subscription {
    my ($self, $case) = @_;

    return $self->schema->resultset('ObjectSubscription')->create(
        {
            external_id  => create_uuid_as_string(UUID_V4),
            local_id     => $case->id,
            local_table  => 'Zaak',
            interface_id => $self->interface->id,
        },
    );
}

=head2 supports_soap_action

Check if the given SOAPAction is supported by this module.

=cut

sub supports_soap_action {
    my ($self, $action) = @_;

    if (any { ($action // '') eq $_ } @{$self->soap_actions}) {
        return 1;
    }
    return 0;
}

sub _build_municipality_name {
    my $self = shift;

    my $city = RGBZ_GEMEENTECODES->{ int($self->municipality_code) };
    return $city if $city;

    throw('stufzkn/no_valid_municipality_code',
        "Invalid municipality code found: " . $self->municipality_code);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
