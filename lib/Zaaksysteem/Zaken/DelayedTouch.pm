package Zaaksysteem::Zaken::DelayedTouch;
use Moose;

use BTTW::Tools;
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zaken::DelayedTouch - Defines a touch object, for delayed touching
of cases from an external location, like Catalyst

=head1 SYNOPSIS

    my $touch_object = Zaaksysteem::Zaken::DelayedTouch->new;

    # Adds case.
    $touch_object->add_case(Zaaksysteem::Zaken::Component->new(...));

    # Optionally modify async state (default false)
    $touch_object->async($should_execute_asynchronously_p);

    ### Later
    my $db_schema = $c->model('DB')->schema;
    my $queue_model = $c->model('Queue');

    $touch_object->execute($db_schema, $queue_model);

=head1 DESCRIPTION

Delayes touching and indexing of cases by collecting all case numbers and touching at
the end of the execution cycle.

=head1 ATTRIBUTES

=head2 cases

Collection of L<case|Zaaksysteem::Zaken::ComponentZaak> instances to be
touched.

=head3 Delegates

=over 4

=item all_cases

Returns all cases. See L<Moose::Meta::Attribute::Native::Trait::Array>'s
C<elements> description.

=item map_cases

Maps over all cases. See L<Moose::Meta::Attribute::Native::Trait::Array>'s
C<map> description.

=item _push_case

Pushes a new case on the list. See
L<Moose::Meta::Attribute::Native::Trait::Array>'s C<push> description.

Do not call this method publically, use L</add_case> instead.

=back

=cut

has cases => (
    is      => 'rw',
    isa     => 'ArrayRef[Zaaksysteem::Zaken::ComponentZaak]',
    traits  => [qw[Array]],
    default => sub { [] },
    handles => {
        all_cases => 'elements',
        map_cases => 'map',
        _push_case => 'unshift'
    }
);

=head2 METHODS

=head2 add_case

Adds a case to the delayed object for later touching.

    $touch_object->add_case(Zaaksysteem::Zaken::ComponentZaak->new(...));

Returns a list of case numbers currently slated to be touched.

=cut

sig add_case => 'Zaaksysteem::Zaken::ComponentZaak';

sub add_case {
    my $self = shift;
    my $case = shift;

    unless (grep { $_->id eq $case->id } $self->all_cases) {
        $self->_push_case($case);
    }

    return map { $_->id } $self->all_cases;
}

=head2 execute

Takes the list of L</cases> and initiates a touch cycle for each of the items.

Depending on the state of L</async>, the cycle will be performed synchronously
or be broadcast as a queued item.

Return value: $BOOL_SUCCESS

    $touch_object->execute(
        Zaaksysteem::Schema->new(...),
        Zaaksysteem::Object::Queue::Model->new(...)
    );

=cut

sig execute => 'Zaaksysteem::Schema, Zaaksysteem::Object::Queue::Model';

sub execute {
    my ($self, $schema, $queue) = @_;

    my @cases;

    $self->map_cases(
        sub {
            my $item = $queue->create_item(
                {
                    type  => 'touch_case',
                    label => 'Case synchronisation (async delayed_touch)',
                    disable_acl => 1,
                    data        => { case_object_id => $_->get_column('uuid') }
                }
            );
            $queue->queue_item($item) if $item->status ne 'cancelled';
        }
    );

    $self->cases([]);

    if ($self->log->is_trace && scalar @cases) {
        $self->log->trace("Touched cases: " . join(', ', @cases));
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
