package Zaaksysteem::Zaken::Roles::Domain;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools;
use List::Util qw[all];
use URI;
use Zaaksysteem::Environment;
use Zaaksysteem::Types qw[
    CaseConfidentiality
    CaseStatus
    UUID
];
require Zaaksysteem::Attributes;

=head1 STATE PREDICATES

=head2 ddd_is_item_editable_by

Given a subject object, returns true if the subject is allowed to edit the
state of the zaak.

    my $check = $zaak->ddd_is_item_editable_by($c->user, sub {
        return $c->check_any_given_zaak_permission(@_);
    });

=cut

sig ddd_is_item_editable_by => 'Zaaksysteem::Backend::Subject::Component, CodeRef, Str';

sub ddd_is_item_editable_by {
    my $self = shift;
    my $subject = shift;

    # This dep-injected permission checker is required to perform
    # 'check_any_zaak_permission' validation against the case. That checking
    # logic should be embedded within the case class itself, but this style
    # will do for now.
    my $permission_checker = shift;
    my $item = shift;

    my $id = $subject->uuid;

    unless (UUID->check($id)) {
        throw('zaak/ddd/is_item_editable_by/subject_has_no_uuid', sprintf(
            'Subject UUID assertion failed for "%s" (%d)',
            $subject->as_object->display_name,
            $subject->id
        ));
    }

    return unless $self->ddd_is_editable;

    return 1 if $permission_checker->($self, 'zaak_beheer');

    if ($self->ddd_has_assignee && $self->ddd_has_coordinator) {
        return 1 if $permission_checker->($self, 'zaak_edit');
    }

    return unless $self->ddd_has_assignee;

    return unless $self->behandelaar->get_column('subject_id') eq $id;

    return if $self->ddd_has_coordinator
           && $self->coordinator->get_column('subject_id') ne $id;

    return 1;
}

=head2 ddd_assert_user_authorizations

=cut

sig ddd_assert_user_authorizations => join(', ', qw[
    Zaaksysteem::Backend::Subject::Component
    CodeRef
    Str
    Str
]);

sub ddd_assert_user_authorizations {
    my ($self, $user, $checker, $context, $action) = @_;

    my $results = $self->_eval_case_action_authorization_matrix(
        $user,
        $checker,
        $context,
        $action
    );

    $self->log->trace(sprintf(
        'Case-user authorization path: %s',
        dump_terse($results)
    ));

    return;
}

=head2 ddd_has_assignee

Predicate that returns true if the zaak currently has an assignee.

=cut

sub ddd_has_assignee {
    return defined shift->get_column('behandelaar');
}

=head2 ddd_has_coordinator

Predicate that returns true if the zaak currently has a coordinator.

=cut

sub ddd_has_coordinator {
    return defined shift->get_column('coordinator');
}

=head2 ddd_has_result

Predicate that returns true if the zaak has an associated result.

=cut

sub ddd_has_result {
    my $self = shift;

    return unless all { defined } map { $self->get_column($_) } qw[
        resultaat_id
        resultaat
    ];

    return 1;
}

=head2 ddd_is_editable

Returns true if the case is fundamentally editable (not closed, not deleted).

This predicate C<MAY NOT> be used to determine if the current subject is allowed
to edit. See L</ddd_is_editable_by>.

=cut

sub ddd_is_editable {
    my $self = shift;

    return not ($self->ddd_is_deleted || $self->ddd_is_resolved);
}

=head2 ddd_is_deleted

Returns true if the zaak is deleted.

=cut

sub ddd_is_deleted {
    return shift->status eq 'deleted';
}

=head2 ddd_is_extant

Returns true if the zaak exists (is not deleted).

=cut

sub ddd_is_extant {
    return not shift->deleted;
}

=head2 ddd_is_resolved

Returns true if the zaak is resolved.

=cut

sub ddd_is_resolved {
    return shift->status eq 'resolved';
}

=head2 ddd_is_unresolved

Returns true if the zaak is not yet resolved.

=cut

sub ddd_is_unresolved {
    return not shift->ddd_is_resolved;
}

=head2 ddd_is_confidential

Returns true if the zaak is confidential.

=cut

sub ddd_is_confidential {
    return shift->confidentiality eq 'confidential';
}

=head2 ddd_is_public

Returns true if the zaak is public (not confidential, not internal)

=cut

sub ddd_is_public {
    return shift->confidentiality eq 'public';
}

=head2 ddd_user_is_assignee

=cut

sig ddd_user_is_assignee => 'Zaaksysteem::Backend::Subject::Component';

sub ddd_user_is_assignee {
    my ($self, $subject) = @_;

    return unless $self->ddd_has_assignee;

    return $self->behandelaar->get_column('subject_id') eq $subject->uuid
}

=head2 ddd_user_is_coordinator

=cut

sig ddd_user_is_coordinator => 'Zaaksysteem::Backend::Subject::Component';

sub ddd_user_is_coordinator {
    my ($self, $subject) = @_;

    return unless $self->ddd_has_coordinator;

    return $self->coordinator->get_column('subject_id') eq $subject->uuid;
}

=head2 ddd_user_is_requestor

=cut

sig ddd_user_is_requestor => 'Zaaksysteem::Backend::Subject::Component';

sub ddd_user_is_requestor {
    return shift->aanvrager->get_column('subject_id') eq shift->uuid;
}

=head2 ddd_user_is_staff

Returns true if the given user is the assignee, coordinator, or requestor of
the case and is an employee.

=cut

sig ddd_user_is_staff => 'Zaaksysteem::Backend::Subject::Component';

sub ddd_user_is_staff {
    my ($self, $subject) = @_;

    return 0 unless $subject->subject_type eq 'employee';

    return 1 if $self->ddd_user_is_assignee($subject);
    return 1 if $self->ddd_user_is_coordinator($subject);
    return 1 if $self->ddd_user_is_requestor($subject);

    return 0;
}

=head1 COMMAND METHODS

=head2 ddd_update

Update the properties on the case object.

    my @events = $zaak->ddd_update($c->user->as_object, {
        price => 1.23,
        confidentiality => 'public',
        status => 'resolved',
        result => Zaaksysteem::Model::DB::ZaaktypeResultaten->new(...)
    });

=cut

sig ddd_update => 'Zaaksysteem::Object::Types::Subject, HashRef';

sub ddd_update {
    my $self = shift;
    my $subject = shift;
    my $properties = shift;

    my %updaters = map {
        $_ => $self->can(sprintf('ddd_update_%s', $_))
    } keys %{ $properties };

    my @invalid_properties = grep {
        not defined $updaters{ $_ }
    } keys %updaters;

    if (scalar @invalid_properties) {
        throw('zaak/ddd/update/invalid_properties', sprintf(
            'Cannot update zaak for given properties: %s',
            join(', ', @invalid_properties)
        ));
    }

    my %errors;
    my @events;

    for my $property (keys %updaters) {
        try {
            push @events, $updaters{ $property }->(
                $self,
                $subject,
                $properties->{ $property }
            );
        } catch {
            $errors{ $property } = $_;
        };
    }

    if (scalar keys %errors) {
        throw('zaak/ddd/update/execution_failures', sprintf(
            "One or more errors occurred while updating zaak:\n%s",
            join("\n", map { sprintf('%s: %s', $_, $errors{ $_ }) } keys %errors)
        ));
    }

    return $self->result_source->schema->txn_do(sub {
        $self->update;

        return map { $_->insert } @events;
    });
}

=head2 ddd_update_price

Updates the C<price> field of the case.

    my @events = $zaak->ddd_update_price(13.37);

=cut

sig ddd_update_price => 'Zaaksysteem::Object::Types::Subject, Num';

sub ddd_update_price {
    my $self = shift;
    my $subject = shift;
    my $price = shift;

    my $old = $self->payment_amount;
    return if $old eq $price;

    $self->payment_amount($price);
    $self->ddd_update_system_attribute('case.price');
    return $self->_create_event($subject, 'case/update/price', {
        new => $price,
        old => $old,
    });
}

=head2 ddd_update_system_attribute

Update a system attribute value based on the name

=cut

sig ddd_update_system_attribute => 'Str';

sub ddd_update_system_attribute {
    my ($self, $name) = @_;


    my $attr = Zaaksysteem::Attributes->get_predefined_case_attribute($name);
    $attr->init_system_value($self);
    $self->update_case_properties($attr);
    return;
}

=head2 ddd_update_system_attribute_group

Update a system attribute values based on the grouping

=cut

sub ddd_update_system_attribute_group {
    my ($self, $name) = @_;

    # system attributes for result, sa for 80 length
    my $sa = Zaaksysteem::Attributes->get_predefined_case_attribute_grouping(
        $name
    );
    $_->init_system_value($self) for @$sa;
    $self->update_case_properties(@$sa);
    return;
}

=head2 ddd_update_status

Updates the C<status> field of the case.

    my @events = $zaak->ddd_update_status('resolved');

=cut

sig ddd_update_status => 'Zaaksysteem::Object::Types::Subject, Str';

sub ddd_update_status {
    my $self = shift;
    my $subject = shift;
    my $status = shift;

    if(!CaseStatus->check($status)) {
        throw(
            'zaak/ddd/update/status/invalid',
            'Invalid status specified',
        );
    }

    if ($status eq 'deleted') {
        throw(
            'zaak/ddd/update/status/deleted_not_allowed',
            'The status "deleted" is a system internal state, and cannot be set using a user initiated command'
        );
    }

    return if $status eq $self->status;

    if (!$self->ddd_has_result && $status eq 'resolved') {
        throw(
            'zaak/ddd/update/status/invalid_state',
            'The status "resolved" cannot be set if the case has no result'
        );
    }

    return if !$self->update_status($status);

    return $self->_create_event($subject, 'case/update/status', {
        status => $status
    });


}

=head2 ddd_update_confidentiality

Updates the C<confidentiality> field of the case.

    my @events = $zaak->ddd_update_confidentiality('public');

=cut

sig ddd_update_confidentiality => 'Zaaksysteem::Object::Types::Subject, Str';

sub ddd_update_confidentiality {
    my $self = shift;
    my $subject = shift;
    my $confidentiality = shift;

    if (!CaseConfidentiality->check($confidentiality)) {
        throw(
            'zaak/ddd/confidentiality/invalid',
            'Invalid confidentiality level specified',
        );
    }

    my $current = $self->confidentiality;

    $self->update_confidentiality($confidentiality);

    return $self->_create_event($subject, 'case/update/confidentiality', {
        new => $confidentiality,
        old => $current
    });
}

=head2 ddd_update_result

Updates the result of the case (C<resultaat_id>, and C<resultaat>).

Also recalculates and (re)sets the C<vernietigingsdatum>.

    my @events = $zaak->ddd_update_result(Zaaksysteem::Model::DB::ZaaktypeResultaten->new(...))

=cut

sig ddd_update_result => 'Zaaksysteem::Object::Types::Subject, Zaaksysteem::Model::DB::ZaaktypeResultaten';

sub ddd_update_result {
    my ($self, $subject, $result) = @_;

    # Nothing to see here, move on like we did update.
    return if $self->_get('resultaat_id', '') eq $result->id;

    my $current = $self->resultaat_id;
    $self->set_result_by_type($result, { logging => 0});

    return $self->_create_event($subject, 'case/update/result', {
        result => $result->resultaat,
        result_label => $result->label,
        defined $current ? (
            old_result => $current->resultaat,
            old_result_label => $current->label
        ) : (),
    });
}

=head2 ddd_update_attributes

Updates the zaak kenmerken state to reflect the provided arguments.

This method will modify the state of C<zaak_kenmerk> B<and> C<case_property>.

The state of C<object_data> is B<not> modified and must be updated via a
L<Zaaksysteem::Zaken::ComponentZaak/touch> invocation.

=cut

sig ddd_update_attributes => 'Zaaksysteem::Object::Types::Subject, HashRef';

sub ddd_update_attributes {
    my $self = shift;
    my $subject = shift;
    my $attributes = shift;

    my $updated_kenmerken = $self->zaak_kenmerken->upsert_kenmerken(
        $self,
        $attributes
    );

    # No need to do anything when updating zilch.
    return unless $updated_kenmerken;

    my $object_attributes = $self->ddd_set_attribute_values(
        keys %$updated_kenmerken
    );

    my @events;

    for my $attribute (@{ $object_attributes }) {
        my $attribute_id = $attribute->object_row->get_column(
            'bibliotheek_kenmerken_id'
        );

        push @events, $self->_create_event(
            $subject,
            'case/attribute/update',
            {
                attribute_id => $attribute_id,
                attribute_name => $attribute->property_name,
                attribute_value => $attribute->human_value,
                casetype_attribute_id => $attribute->object_row->id,
            },
            {
                component => 'kenmerk',
                component_id => $attribute_id,
            }
        )->upsert_by_recent(qw[zaak_id component component_id created_by]);
    }

    return @events;
}

sub ddd_set_attribute_values {
    my ($self, @magic_string) = @_;

    my $object_attributes = $self->casetype_object_attributes(@magic_string);

    for my $attribute (@{$object_attributes}) {
        $self->set_casetype_attribute_value($attribute);
    }

    $self->update_case_properties(@{ $object_attributes });

    return $object_attributes;
}

=head2 ddd_transition

Attempts to transition the case to the next phase.

Takes a required coderef that can validate associated
L<Zaaksysteem::Backend::Object::Mutation::Component> instances.

    my @events = $zaak->ddd_transition($c->user, sub {
        return $c->model('Object')->validate_mutation(@_);
    });

May throw a C<zaak/ddd/transition/cannot_advance> exception of the transition
requirements are not met. The exception holds a reference to an instance of
L<Zaaksysteem::Zaken::AdvanceResult> which contains an overview of the reaons
for the failure.

=cut

sig ddd_transition => 'Zaaksysteem::Backend::Subject::Component, CodeRef';

sub ddd_transition {
    my $self = shift;
    my $subject = shift;
    my $mutation_validator = shift;

    my $advance_result = $self->can_volgende_fase($mutation_validator);

    unless ($advance_result->can_advance) {
        throw(
            'zaak/ddd/transition/cannot_advance',
            'Cannot advance case, can_volgende_fase checks are false',
            $advance_result
        );
    }

    my @queued_items;
    my $rules_result;
    my $schema = $self->result_source->schema;

    $self->result_source->schema->txn_do(sub {
        $rules_result = $self->execute_rules({
            status => $self->milestone + 1
        });

        $self->set_volgende_fase;

        if ($rules_result) {
            if (my $result = $rules_result->{ wijzig_registratiedatum }) {
                $self->process_date_of_registration_rule(%{ $result });
            }
        }

        push @queued_items, $self->_fire_phase_actions($subject);
    });

    if ($rules_result) {
        if (my $result = $rules_result->{ send_external_system_message }) {
            my $config = $self->result_source->schema->resultset('Config');

            $self->send_external_system_messages(
                base_url => URI->new($config->get('instance_base_url')),
                rules_result => $result
            );
        }
    }

    if (scalar @queued_items) {
        return $schema->txn_do(sub {
            my $rs = $schema->resultset('Queue');
            my @child_ids = map { $_->id } @queued_items;

            my $meta = $rs->create_item('run_ordered_item_set', {
                object_id => $self->get_column('uuid'),
                label => 'Phase transition queue item set',
                metadata => {
                    target => 'backend',
                    subject_id => $subject->id,
                },
                data => {
                    item_ids => \@child_ids
                }
            });

            $rs->search({ id => \@child_ids })->update({
                status => 'waiting',
                parent_id => $meta->id
            });

            return $meta;
        });
    }

    $self->touch();

    return;
}

=head2 ddd_update_system_attributes

Update all system attributes of a case

=cut

sub ddd_update_system_attributes {
    my ($self, %opts) = @_;


    my $object = $self->object_data;
    my @list = Zaaksysteem::Attributes::predefined_case_attributes();
    my @copy;
    foreach (@list) {
        next if ($_->name eq 'case.num_unaccepted_files' && $opts{skip_files});
        next if $_->name eq 'case.assignee.signature';
        $_->init_system_value($self);
        push(@copy, $_);
    }
    $self->update_case_properties(@copy);
    return;
}

=head2 ddd_update_system_attributes_for_update

Update all required system attributes when a case get's updated. In use for
zaak->update();

=cut

sub ddd_update_system_attributes_for_update {
    my ($self, @attributes) = @_;
    return unless @attributes;

    my @list = Zaaksysteem::Attributes->get_from_case(@attributes);
    return unless @list;

    $_->init_system_value($self) for @list;
    $self->update_case_properties(@list);
    return;
}

=head1 PRIVATE METHODS

=head2 _create_event

Instantiates and returns a L<Zaaksysteem::DB::Component::Logging> row, which
currently is the backend for event storage.

=cut

sub _create_event {
    my $self = shift;
    my $subject = shift;
    my $type = shift;
    my $data = shift || {};
    my $fields = shift || {};

    my $dn = $subject->display_name;

    # Inject request_id so we can better trace the origin of events.
    $data->{ _request_id } = $Zaaksysteem::Environment::REQUEST_ID;

    # Manage simplified interface so the existing method on Logging table
    # processes the event properly.
    $fields->{ data } //= $data;

    # Prefill all conceivable fields, allow callers to override
    $fields->{zaak_id}               //= $self->id;
    $fields->{component}             //= 'zaak';
    $fields->{component_id}          //= $self->id;
    $fields->{restricted}            //= $self->ddd_is_public ? 0 : 1;
    $fields->{betrokkene_id}         //= $subject->old_subject_identifier;
    $fields->{created_by}            //= $subject->old_subject_identifier;
    $fields->{created_by_name_cache} //= $dn;

    # Ensure we get bare new rows, we'll deal with insertions later
    $fields->{ _instantiate } = 1;

    return $self->logging->trigger($type, $fields);
}

=head2 _get

Get-with-default helper for zaak columns.

    my $value = $case->_get('my_column', 'default');

=cut

sub _get {
    return shift->get_column(shift) // shift;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
