package Zaaksysteem::Zaken::ComponentBag;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub update_bag {
    my  $self   = shift;
    my  $params = shift;

    if ($params) {
        return 1 unless (
            UNIVERSAL::isa($params, 'HASH') &&
            scalar(%{ $params })
        );
    } else {
        $params = { $self->get_columns };
    }

    my $bag_model = $self->result_source->schema->bag_model;
    my $bag_object = $bag_model->get($params->{bag_type} => $params->{bag_id});


    my $all_identifiers = $bag_object->get_identifiers;
    $self->update($all_identifiers);
}

sub bag_object {
    my $self = shift;

    my $bag_model = $self->result_source->schema->bag_model;

    if ($self->bag_nummeraanduiding_id) {
        return $bag_model->get(nummeraanduiding => $self->bag_nummeraanduiding_id);
    }

    return $bag_model->get(openbareruimte => $self->bag_openbareruimte_id);
}

=head2 verblijfsobject_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_verblijfobject_id.

=cut

sub verblijfsobject_id {
    my $self = shift;
    return unless $self->bag_verblijfsobject_id;
    return "verblijfsobject-" . $self->bag_verblijfsobject_id;
}

=head2 openbareruimte_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_verblijfobject_id.

=cut

sub openbareruimte_id {
    my $self = shift;
    return unless $self->bag_openbareruimte_id;
    return "openbareruimte-" . $self->bag_openbareruimte_id;
}

=head2 nummeraanduiding_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_nummeraanduiding_id.

=cut

sub nummeraanduiding_id {
    my $self = shift;
    return unless $self->bag_nummeraanduiding_id;
    return "nummeraanduiding-" . $self->bag_nummeraanduiding_id;
}

=head2 pand_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_pand_id.

=cut

sub pand_id {
    my $self = shift;
    return unless $self->bag_pand_id;
    return "pand-" . $self->bag_pand_id;
}

=head2 standplaats_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_standplaats_id.

=cut

sub standplaats_id {
    my $self = shift;
    return unless $self->bag_standplaats_id;
    return "standplaats-" . $self->bag_standplaats_id;
}

=head2 ligplaats_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_ligplaats_id.

=cut

sub ligplaats_id {
    my $self = shift;
    return unless $self->bag_ligplaats_id;
    return "ligplaats-" . $self->bag_ligplaats_id;
}

sub insert {
    my $self    = shift;

    my $rv      = $self->next::method(@_);

    if (ref($self->zaak_id)) {
        $self->zaak_id->touch;
    }

    return $rv;
};

sub update {
    my $self    = shift;

    my $rv      = $self->next::method(@_);

    if (ref($self->zaak_id)) {
        $self->zaak_id->touch;
    }

    return $rv;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 columns_bag

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 update_bag

TODO: Fix the POD

=cut

