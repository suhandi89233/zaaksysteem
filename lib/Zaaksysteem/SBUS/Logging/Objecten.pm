package Zaaksysteem::SBUS::Logging::Objecten;

use Moose;

has [qw/
    object_type
    error
    objecten
    finished
/] => (
    'is'    => 'rw'
);

has 'objecten'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self = shift;

        return [];
    }
);

has 'created'   => (
    'is'        => 'rw',
    'default'   => sub {
        DateTime->now('time_zone'   => 'Europe/Amsterdam');
    }
);

sub object {
    my $self        = shift;
    my $object      = shift;

    push(
        @{ $self->objecten },
        $object
    );

    $self->finished(DateTime->now('time_zone' => 'Europe/Amsterdam'));

    return $object;
}

sub count {
    my $self        = shift;

    return scalar @{ $self->objecten };
}


sub success {
    return 1 unless shift->error;
    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 count

TODO: Fix the POD

=cut

=head2 object

TODO: Fix the POD

=cut

=head2 success

TODO: Fix the POD

=cut

