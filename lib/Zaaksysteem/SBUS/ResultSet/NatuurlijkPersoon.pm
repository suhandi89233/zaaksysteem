package Zaaksysteem::SBUS::ResultSet::NatuurlijkPersoon;

use Moose;

extends qw/DBIx::Class::ResultSet Zaaksysteem::SBUS::ResultSet::GenericImport/;

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;
use Zaaksysteem::SBUS::Logging::Object;
use Zaaksysteem::SBUS::Logging::Objecten;

use Data::Dumper;

use constant IMPORT_KERNGEGEVENS   => [qw/
    burgerservicenummer
    a_nummer
/];

use constant IMPORT_KERNGEGEVEN_LABEL   => 'burgerservicenummer';


has '_import_kerngegevens'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return IMPORT_KERNGEGEVENS;
    }
);

has '_import_kerngegeven_label'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return IMPORT_KERNGEGEVEN_LABEL;
    }
);

has '_import_objecttype'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return SBUS_LOGOBJECT_PRS;
    }
);

has '_import_entry_profile' => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return GEGEVENSMAGAZIJN_GBA_PROFILE;
    }
);

sub _delete_real_entry {
    my ($self, $params, $options) = @_;

    my $record = $self->_get_kern_record($params, @_);

    ### record not found in the first place, not in ZS
    return unless $record;

    $record->adres_id->deleted_on(DateTime->now);

    $record->adres_id->update;

    $record->deleted_on(DateTime->now);

    $record->update;
}

sub _import_real_entry {
    my ($self, $params, $options) = @_;
    my ($record, $adres_record);

    if (uc($options->{mutatie_type}) =~ /W|V/) {
        $record = $self->_get_kern_record($params, @_);

        unless ($record) {
            warn(
                'Persoon niet gevonden in database: ' .
                $params->{burgerservicenummer}
            );
            return;
        }

        if (ref($record->adres_id)) {
            $adres_record   = $record->adres_id;
        }
    } else {
        my $GBA_KERNGEGEVENS = IMPORT_KERNGEGEVENS;

        $record = $self->create(
            { map { $_ => $params->{ $_ } } @{ $GBA_KERNGEGEVENS } }
        );
    }

    if (!$adres_record) {
        $adres_record   = $self->result_source
            ->schema
            ->resultset('Adres')
            ->create({});
    }

    my $GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES
        = GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES;


    ### Alleen adres params
    my ($adres_params, $gba_params) = ({},{});
    for my $param (keys %{ $params }) {
        if (grep { $param eq $_ } @{ $GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES }) {
            $adres_params->{$param} = $params->{$param};
            next;
        }
        $gba_params->{$param} = $params->{$param};
    }

    # warn('GBA PARAMS: ' . Dumper($gba_params));

    ### Detect changes
    $self->_detect_changes($adres_params, $options, $adres_record);

    ### Detect changes
    $self->_detect_changes($gba_params, $options, $record);


    $gba_params->{adres_id}         = $adres_record->id;
    $gba_params->{authenticated}    = 1;
    $gba_params->{authenticatedby}  = 'gba';

    if ($gba_params->{datum_overlijden}) {
        $gba_params->{deleted_on} = DateTime->now();
    }

    $adres_record   ->update($adres_params);
    $record         ->update($gba_params);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES

TODO: Fix the POD

=cut

=head2 GEGEVENSMAGAZIJN_GBA_PROFILE

TODO: Fix the POD

=cut

=head2 IMPORT_KERNGEGEVENS

TODO: Fix the POD

=cut

=head2 IMPORT_KERNGEGEVEN_LABEL

TODO: Fix the POD

=cut

=head2 SBUS_LOGOBJECT_PRS

TODO: Fix the POD

=cut

