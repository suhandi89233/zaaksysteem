package Zaaksysteem::BR::Subject::Iterator::ObjectSubscription;

use Moose::Role;

=head1 NAME

Zaaksysteem::BR::Subject::Iterator::ObjectSubscription - This is a iterator extension to cache object subscriptions.

=head1 DESCRIPTION

This Iterator extension will provide the iterator with the logic to cache C<ObjectSubscription> tables in search
results. This way, we can reduce the amount of queries substantionaly.

=head1 ATTRIBUTES

=head2 _cached_subscriptions

Private store of cached subscriptions

=cut

has _cached_subscriptions => (
    is      => 'rw',
    default => 1,
);

=head1 METHODS

=head2 _can_cache_subscriptions

Returns true when caching of subscriptions for this iterator is allowed/possible

=cut

sub _can_cache_subscriptions {
    my $self    = shift;

    if ($self->_cache_is_setup && $self->rs->result_source->name =~ /^(?:natuurlijk_persoon|bedrijf)$/) {
        return 1;
    }

    return;
}

=head1 AROUND METHODS

=head2 _cache_related_rows

Caches the related rows, C<ObjectSubscription> in this case, into the iterator

=cut


around '_cache_related_rows' => sub {
    my $method  = shift;
    my $self    = shift;

    my $ok      = $self->$method(@_);

    return $ok if (!$self->_can_cache_subscriptions || $self->_in_cache_loop->{object_subscription});

    my $np_query    = $self->rs->search()->get_column($self->rs->result_source->name . '.id')->as_query;

    ### Get all subscriptions
    my @subscriptions = $self->rs->result_source->schema->resultset('ObjectSubscription')->search({
        'local_id::integer' => { in => $np_query },
        'local_table'       => $self->rs->result_source->source_name,
    }, { prefetch => 'interface_id' })->all;

    my %cached_sub;
    for my $subscription (@subscriptions) {
        my $subject_id               = $subscription->get_column('local_id');
        $cached_sub{ $subject_id }   ||= [];

        push(@{ $cached_sub{ $subject_id } }, $subscription);
    }

    $self->_in_cache_loop->{object_subscription} = 1;

    $self->_cached_subscriptions(\%cached_sub);

    return $ok;
};

=head2 _apply_cache_on_row

Applies the cached data, from C<ObjectSubscription> in this case, into the returned row

=cut

around '_apply_cache_on_row' => sub {
    my $method  = shift;
    my $self    = shift;
    my ($row)   = @_;

    my $ok      = $self->$method(@_);

    ### Only when subject_type = person
    return $ok if !$self->_can_cache_subscriptions;

    my $sub_id       = $row->get_column('id');

    $row->cached_subscriptions(
        ($self->_cached_subscriptions->{$sub_id} ? [ @{ $self->_cached_subscriptions->{$sub_id} } ] : []),
    );

    return 1;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

