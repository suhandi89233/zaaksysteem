package Zaaksysteem::Scheduler::Job::CreateCase;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Scheduler::Job::CreateCase - Role for a ScheduledJob to create a new case

=head1 SYNOPSIS

    my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
        job             => 'CreateCase',
        next_run        => $now->clone,
        interval_period => 'weeks',
        interval_value  => 2,
        runs_left       => 2,
    );
    ensure_all_roles($sj, 'Zaaksysteem::Scheduler::Job::CreateCase');

    # Make sure at least a CaseType and a Case ("source" case) are related
    $sj->relate(
        $casetype_object,
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );
    $sj->relate(
        $case_object,
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );
    # Other objects can be linked like this:
    $sj->relate(
        $random_other_object,
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );

    my $newly_created_case = $sj->run($dummy_context);

=cut

use BTTW::Tools;

=head1 METHODS

=head2 run

Run the scheduled job: create a new case.

The ScheduledJob this role is applied to must have at least a related
"casetype" object and a related "case" object, to know what kind of case to
create, and to link it with the old one.

=cut

sub run {
    my ($self, $c) = @_;

    my $db = $c->model('DB');

    my $casetype_obj = do {
        my @related_casetypes = $self->filter_relations(sub { $_->related_object_type eq 'casetype' });
        if (@related_casetypes != 1) {
            throw(
                'scheduler/job/create_case/relations',
                sprintf('CreateCase job %s not have exactly one related "casetype"', $self->id//'<unsaved>'),
            );
        }

        $c->model('Object')->retrieve(uuid => $related_casetypes[0]->related_object_id);
    };

    my $originator_obj = do {
        my @originator_cases = $self->filter_relations(sub {
                   $_->related_object_type eq 'case'
                && $_->relationship_name_b eq 'scheduled_by'
            });
        if (@originator_cases != 1) {
            throw(
                'scheduler/job/create_case/originator_case',
                sprintf('CreateCase job %s not have exactly one "originator" case', $self->id//'<unsaved>'),
            );
        }

        $db->resultset('ObjectData')->find(
            $originator_cases[0]->related_object_id,
        );
    };

    my $requestor = $originator_obj->get_object_attribute('case.requestor.id');
    my $case      = $db->resultset('Zaak')->find(
        $originator_obj->get_object_attribute('case.number')->value
    );

    my $new_case = $db->resultset('Zaak')->create_zaak(
        {
            aanvraag_trigger => 'extern',
            aanvragers       => [
                {
                    betrokkene  => $requestor->value,
                    verificatie => 'medewerker',
                }
            ],
            registratiedatum         => DateTime->now(),
            contactkanaal            => 'behandelaar',
            zaaktype_id              => $casetype_obj->casetype_id,

            relatie                  => 'vervolgzaak_datum',
            actie_kopieren_kenmerken => 0,
            zaak_id                  => $case->id,
        }
    );

    my @other_relations = $self->filter_relations(
        sub {
               ($_->related_object_type ne 'casetype')
            && ($_->related_object_type ne 'case')
        }
    );

    for my $relation (@other_relations) {
        $db->resultset('ObjectRelationships')->create(
            {
                owner_object_uuid => $new_case->object_data->uuid,

                object1_uuid      => $new_case->object_data->uuid,
                object1_type      => 'case',
                type1             => 'embeds',

                object2_uuid      => $relation->related_object_id,
                object2_type      => $relation->related_object_type,
                type2             => 'embedded',
            }
        );
    }

    return $new_case;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
