=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Config::Panel - Type definition for
config/panel objects

=head1 DESCRIPTION

This page documents the serialization of C<config/panel> objects.

=head1 JSON

=begin javascript

	{
		"type" : "config/panel",
		"reference" : null,
		"instance" : {
			"date_created" : "2018-02-27T09:39:28Z",
			"date_modified" : "2018-02-27T09:39:28Z",
			"name" : "Systeemconfiguratie",
			"categories" : { ... },
			"definitions" : { ... },
			"items" : { ... }
		}
	}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 name E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Display name for the panel

=head2 categories E<raquo> L<C<set>|Zaaksysteem::Manual::API::V1::Types::Set>

Set of
L<C<config/category>|Zaaksysteem::Manual::API::V1::Types::Config::Category>
instances.

=head2 definitions E<raquo> L<C<set>|Zaaksysteem::Manual::API::V1::Types::Set>

Set of
L<C<config/definition>|Zaaksysteem::Manual::API::V1::Types::Config::Definition>
instances.

=head2 items E<raquo> L<C<set>|Zaaksysteem::Manual::API::V1::Types::Set>

Set of L<C<config/item>|Zaaksysteem::Manual::API::V1::Types::Config::Item>
instances.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
