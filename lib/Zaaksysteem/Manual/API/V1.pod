=head1 NAME

Zaaksysteem::Manual::API::V1 - Zaaksysteem API manual v1.0

=head1 Description

This document describes our API version 1.0, how to authenticate to it and you will learn how
to read the possible output / errors.

=head1 Components

Our API is divided in different components, which can be found in the documentation index. For instance,
to find information about retrieving and creating cases, go to the manual belonging to C<Case>.

When you are finding yourself here for the first time, be sure to read the basics of our API in L<Zaaksysteem::Manual::API> and
the below information. So you understand concepts like our versioning policy, but also about paging,
authenticating etc.

=head1 Authentication

Connecting to our JSON API requires authentication. There are two authentication methods. One is by sending
an API Key via a header in your request. The other is via Digest auhtentication.

You can set the username and api key in the "Koppelingen" configuration.

When using multiple API koppelingen in the koppelingen configuration
screen, make sure you send the ID of the koppeling configuration in the
C<API-Interface-Id> request header.

Also, don't forget that the Zaaksysteem API speaks JSON, and sending the right
C<Content-Type> to our application when you send updates is required.

You will get a JSON response from our API, which is explained in detail in the
specific component manual. For a basic outline, read below.

=head2 HTTP-Digest

When using HTTP-Digest as our authentication system, you are able to use "out of the box" utilities for authenticating
to our API.

For instance, to connect to our API using the commandline C<curl> utility. You could retrieve a list of cases by
calling our api like this:

  curl -H "API-Interface-Id: 4" -H "Content-Type: application/json" --digest -u "zaaksysteem:s3cr3tkey" https://your.zaaksysteem.host/api/v1/case

We use L<HTTP Digest|https://tools.ietf.org/html/rfc7235> as our authentication
method.

The configured API key should be used as the password when connecting to the
API, the user's "login password" is not used.

=head2 Header authentication

A simpler method is by supplying the API key via the header C<API-Key>. Please see the below curl example
for more inspiration:

  curl -H "API-Interface-Id: 4" -H "Content-Type: application/json" --header "API-Key: s3cr3tkey" https://your.zaaksysteem.host/api/v1/case

=head1 JSON

Although there are a lot of ways to expose our API, we settled for JSON. It has a very
small overhead, and is easy to parse by eyes and systems with low processing power like
client side javascript.

We implemented a default structure, to help you understand some basic problems you will
need to overcome when talking to an API. Like paging, how many rows to retrieve etc.

Via our JSON API, it is possible to retrieve and mutate diverse objects in Zaaksysteem. Every
search or get request on our API is returned in a basic format containing information of the request,
the version of our api and if the request went well (which should be the same as your HTTP-Status code).

=head1 API: Retrieving

When calling our API, you will get the return in proper JSON format. Because JSON is not a
descriptive language, it is import to know how te read the results.

This is best illustrated by an example response:

B</api/v1/case>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-5f4b6c-252c1b",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "attributes" : {
                     "checkbox_agreed" : [
                        null
                     ],
                     "test_decision" : [
                        null
                     ],
                     ...
                  },
                  "id" : "2ffae467-39c6-4131-96d6-c2c56dbb202f",
                  "number" : 337,
                  "phase" : "afhandelfase",
                  ...
               },
               "type" : "case",
               "type_object_id" : "662f792d-fede-4545-8d89-d5d64ff72f02"
            },
            {
               "instance" : {
                  "attributes" : {
                     "checkbox_agreed" : [
                        null
                     ],
                     "test_decision" : [
                        null
                     ],
                     ...
                  },
                  "id" : "340dcdd3-fb47-4ebd-842f-729e0bd1f70c",
                  "number" : 338,
                  "phase" : "afhandelfase",
                  ...
               },
               "type" : "case",
               "type_object_id" : "662f792d-fede-4545-8d89-d5d64ff72f02"
            },
         ]
      },
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

When an API call designed for returning data is succesful, you will get a result like shown
in the example. Before diving into paging and resultsets, lets start easy and let me explain
the basic (toplevel) attributes.

B<Attributes>

The following attributes will be returned.

=over 4

=item status_code

Type: NUMERIC

The HTTP Status code of this result. When you hit a normal request, this should be B<200>. In case
of an error, this would be C<500>

=item request_id

Type: TEXT

The identifier for this request. When something doesn't work as expected, and no results are found in your
Transactionlog, you should send this along with your bug description to Mintlab. They know what to do with
it.

=item result

Type: Object

Where all the information comes from. An object containing a set of data, or just one object containing
the information you requested.

=back

=head2 Reading the results

The information returned in the C<result> attribute contains two basic attributes which define the type
of object and the object itself, called the instance.

=over 4

=item type

This contains the name of the object, for instance: "case" or "casetype" or "document" etc. But, there is one
object which is special, and that is the B<set> object, see below.

=item instance

The content of the object named in the type attribute.

=item reference

This unique identifier gives you the possibility to call the object directly via an url: it is the ID of the
object you are processing.

=item preview

This attribute may contain a human-readable (where implemented) string
representing a 'preview' of the referenced object. Especially in the case no
instance is provided in the response, this preview string can be used to
display the reference visually without the need to query for the referenced
object.

=back

=head2 A SET of results

One special object in the "return" attribute, is the C<set> object. This object is a resultset which contains
information about paging, the number of rows etc. You can see it as the information from a table. The information
in the object can be found in the C<instance> attribute. Please look at the example above for reference.

=head3 Paging

in the attribute C<pager> you will find information about the number of rows, an URL to get the next set of
data, etc.

B<Items>

=over 4

=item next

When there are more results than can be shown, you will find the URL for calling the next page in this attribute.

=item prev

When there are more results than can be shown, you will find the URL for calling the previous page in this attribute.

=item page

The page you are currently on

=item pages

The total number of pages

=item rows

The number of rows currently shown

=item total_rows

The total number of rows which can be returned via this call

=back


=head1 API - Creating or updating

When mutating an entry in our API, make sure you call our API by a HTTP POST method. This way unintentionally
GET requests won't mutate anything in your system.

=head1 Exceptions

When we have to trigger an error in our API call, we do this by setting the
proper HTTP Response Code. Below is an example of different kind of errors.

  Everything OK:
  201   - CREATE/UPDATE/DELETE CALL SUCCESS

  Client Errors:
  400   - Bad Request - We do not know what you are talking about
  401   - Unauthorized - You are not logged in.
  403   - Forbidden - You do not have the proper permissions
  404   - Not Found - The API call you requested could not be found
  405   - Method Not Allowed - Use of wrong request method for this URL

  Server Errors:
  500   - Internal Server Error - There is an error. Error can be found in JSON message (or no message
  at all if things got really broken)

When explaining the error, for example when an internal server error (500)
occured, we will respond in a simple JSON response. Example below

=head2 Exception

When there is a global exception, we return the JSON below.

B<properties>

=over 4

=item type

IS: exception

Tells you this result type is an exception, no reference will be filled and an attribute called "instance"
will contain the detailed information about this exception, see below.

=item instance/message

The human readable message of the error

=item instance/type

An identifier of the error, this will point to the proper location of the error in the code

=back

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ddaf45-b995b6",
   "development" : false,
   "result" : {
      "instance" : {
         "message" : "There was a problem updating this controlpanel instance object: api/v1/controlpanel/instance/fqdn_exists: controlpanel instance already exists with given name\n",
         "type" : "api/v1/controlpanel/instance/fault"
      },
      "preview": "api/v1/controlpanel/instance/fault: ...",
      "reference" : null,
      "type" : "exception"
   },
   "status_code" : 500
}

=end javascript

=head2 ValidationException

When there is an exception regarding the input parameters, we could throw an exception of the type
C<validationexception>. This exception returns a structured list of problems.

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-90afd8-469c85",
   "development" : false,
   "result" : {
      "instance" : {
         "fqdn" : {
            "message" : "Veld fqdn is ongeldig",
            "validity" : "invalid"
         },
         "label" : {
            "message" : "Veld label is geldig",
            "validity" : "valid"
         },
         "otap" : {
            "message" : "Veld otap is geldig",
            "validity" : "valid"
         }
      },
      "reference" : null,
      "type" : "validationexception"
   },
   "status_code" : 500
}

=end javascript

=over 4

=item type

IS: validationexception

Tells you this result type is an exception, no reference will be filled and an attribute called "instance"
will contain the detailed information about this exception, see below.

=item instance/message

The human readable message of the error

=item instance/type

IS: params/profile

An identifier of the error, which is always C<params/profile> telling you there was something wrong with
your parameter profile

=item instance/invalid

B<TYPE>: ARRAY

A list of parameters which are invalid

=item instance/missing

B<TYPE>: ARRAY

A list of parameters which are missing (and are required)

=item instance/valid

B<TYPE>: ARRAY

A list of parameters which are valid

=back

Normally, the only thing you would need is the first value from C<messages>.

=head1 Attribute format

=head2 Serialization

Some values are in some way serialized, or need some explanation how to read them.
Below a list.

=head2 Date/time

DateTime objects are in the form of the ISO8601 and are ALWAYS in UTC. Please
use the proper locale for converting it to your local timezone.

example:

    2015-02-23T15:47:35Z

=head2 undef vs 0 vs ""

We use the proper JSON implementation for sorting out the differences, but in
short:

=begin javascript

{
    not_married_anymore: null,
    does_not_have_a_lastname_suffix: '',
    the_amount_of_cash_on_his_bankaccount: 0,
}

=end javascript

When you do not want to update a value...please leave it out of the request

=head2 Encoding

All calls should be in proper UTF-8 encoding

=head1 See also

L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
