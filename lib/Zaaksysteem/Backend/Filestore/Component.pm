package Zaaksysteem::Backend::Filestore::Component;

use Moose;

extends 'DBIx::Class';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::FilestoreModel
);

=head1 NAME

Zaaksysteem::Backend::Filestore::Component

=cut

use BTTW::Tools;

use autodie qw(symlink);

use Digest::MD5::File qw(file_md5_hex);
use File::Basename;
use File::MimeInfo qw[extensions];
use File::Temp;
use File::stat;
use File::Spec::Functions qw(catfile);
use List::Util qw(any first);
use OpenOffice::OODoc;
use Params::Profile;
use Moose::Util::TypeConstraints qw[enum];

use Zaaksysteem::Constants;
use Zaaksysteem::DocumentConverter;
use Zaaksysteem::Object::Types::File;
use Zaaksysteem::ZTT;

=head1 ATTRIBUTES

=head2 name

The name of the file without the extension

=cut

has name => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    default => sub {
        my $self = shift;
        my ($filename) = fileparse($self->original_name, '\.[^\.]*');
        return $filename;
    }
);

=head2 extension

The extension of the file, includes the C<.>.
Eg C<.txt>

=cut

has extension => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    builder  => 'derive_extension'
);

=head2 _converter

L<Zaaksysteem::DocumentConverter> instance to use to convert/transform files.

=cut

has '_converter' => (
    is => 'rw',
    isa => 'Zaaksysteem::DocumentConverter',
    lazy => 1,
    default => sub { return Zaaksysteem::DocumentConverter->new(); }
);

=head1 METHODS

=head2 derive_extension

Attempts to heuristically determine the proper file extension given the
mimetype and filename

    my $ext = $filestore_row->derive_extension;

=cut

sub derive_extension {
    my $self = shift;

    # Prepend dots... because all other code expects dots. sigh.
    my @mime_extensions = map { ".$_" } extensions($self->mimetype);

    my (undef, undef, $given_extension) = fileparse($self->original_name, qr/\.[^\.]*/);

    my $match_extension = first { $_ eq $given_extension } @mime_extensions;

    # Filetype and mimetype agree on one extension, use that
    return $match_extension if defined $match_extension;

    # No agreement, return first guess.
    return shift @mime_extensions if scalar @mime_extensions;

    # Last resort, return the extension as it was provided at upload.
    return $given_extension if length $given_extension;

    # Nothing left to guesstimate, let the caller figure it out.
    return;
}

=head2 $filestore->verify_integrity

Check a file's integrity by comparing the database md5 with the filesystem md5.

=head3 Returns

True on succes, 0 on failure.

=cut

sub verify_integrity {
    my $self = shift;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $file_path = $self->get_path;
    if (defined $file_path && -f "$file_path") {
        if ($self->md5 eq file_md5_hex("$file_path")) {
            return 1;
        }
    }

    Zaaksysteem::StatsD->statsd->end('filestore.verify.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.verify', 1);

    return 0;
}

=head2 generate_thumbnail

Create a PNG thumbnail from this file.

Optionally, a width and height can be supplied in named arguments.

=cut

define_profile generate_thumbnail => (
    required => {
        width => 'Int',
        height => 'Int'
    },
);

sub generate_thumbnail {
    my $self = shift;
    my $geometry = assert_profile({@_})->valid;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    # If format conversion is directly supported pass along the file path.
    # Otherwise convert to PDF first.
    
    my $source_file;
    if ($self->_converter->can_convert(source_type => $self->mimetype, destination_type =>'image/png')) {
        $source_file = $self->get_path();
    } elsif($self->_converter->can_convert(source_type => $self->mimetype, destination_type => 'application/pdf')) {
        my $path     = $self->get_path();
        my $pdf_temp = File::Temp->new();

        $self->_converter->convert_file(
            source_filename      => "$path",
            destination_filename => $pdf_temp->filename,
            destination_type     => 'application/pdf',
        );

        $source_file = $pdf_temp;
    } else {
        throw(
            "filestore/conversion_not_possible",
            sprintf(
                "Thumbnailing of file with MIME type '%s'".
                $self->mimetype,
            ),
        );
    }

    Zaaksysteem::StatsD->statsd->end('filestore.generate_thumbnail.tmp_files.time', $t0);

    my $tmp = File::Temp->new();

    $self->_converter->convert_file(
        source_filename      => "$source_file",
        destination_type     => 'image/png',
        destination_filename => $tmp->filename,
        filter_options => {
            width  => $geometry->{width},
            height => $geometry->{height},
        },
    );

    Zaaksysteem::StatsD->statsd->end('filestore.generate_thumbnail.total_time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.create_thumbnail', 1);

    return $tmp;
}

=head2 $filestore->convert

Convert to a different format. Returns a location of the newly converted file. It
is the responsibility of the caller to unlink this (temporary) file.

=cut

define_profile convert => (
    required => {
        target_format => 'Str',
        target_file   => 'Str',
    },
    field_filters => {
        target_format => ['lc'],
    }
);

sub convert {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $target_format = $opts->{target_format};
    my $target_file   = $opts->{target_file};
    my $local_copy    = $self->get_path;

    if ($target_format eq $self->mimetype) {
        return $local_copy;
    }

    $self->assert_convertable;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    $self->_converter->convert_file(
        source_filename      => "$local_copy",
        destination_filename => $target_file,
        destination_type     => $target_format,
    );

    Zaaksysteem::StatsD->statsd->end('filestore.convert.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.convert', 1);

    return $target_file;
}

=head2 may_convert_to_doc

Checks if a file may be converted to a Word document

=cut

sub may_convert_to_doc {
    my ($self, $suffix) = @_;

    $suffix = lc($suffix // $self->extension);
    if (MIMETYPES_ALLOWED->{$suffix}{copy2doc}) {
        return 1;
    }
    return 0;
}


=head2 may_preview_as_pdf

Checks if a file may be previewed as PDF

=cut

sub may_preview_as_pdf {
    my ($self, $suffix) = @_;

    $suffix = lc($suffix // $self->extension);

    # If we don't know the file type, don't thumbnail/PDF-preview
    if (exists MIMETYPES_ALLOWED->{$suffix}) {
        # If we do know it, and there's no "preview" flag defined, allow
        # If we do know it, and there is a preview flag defined, and it's "true", allow
        if ((!exists MIMETYPES_ALLOWED->{$suffix}{preview}) || MIMETYPES_ALLOWED->{$suffix}{preview}) {
            return 1;
        }
    }

    # Otherwise: don't allow
    return 0;
}

=head2 may_copy_to_pdf

Checks if a file may be copied to PDF by Zaaksysteem.

=cut


sub may_copy_to_pdf {
    my ($self, $suffix) = @_;

    $suffix = lc($suffix // $self->extension);

    # If we don't know the file type, don't copy as PDF
    if (exists MIMETYPES_ALLOWED->{$suffix}) {
        # If we do know it, and there's no "copy2pdf" flag defined, allow
        # If we do know it, and there is a "copy2pdf" flag defined, and it's "true", allow
        if ((!exists MIMETYPES_ALLOWED->{$suffix}{copy2pdf}) || MIMETYPES_ALLOWED->{$suffix}{copy2pdf}) {
            return 1;
        }
    }

    return 0;
}

=head2 assert_convertable

Asserts if a file may be converted by Zaaksysteem.

=cut

sub assert_convertable {
    my ($self, $suffix) = @_;

    if ($self->may_preview_as_pdf($suffix) || $self->may_copy_to_pdf($suffix) || $self->may_convert_to_doc($suffix)) {
        return 1;
    }

    throw('filestore/convert', sprintf('Not allowed to convert %s to alternate filetype', $self->extension));
}

=head2 $filestore->name_without_extension

Return the file's name stripped of its extension.

=cut

sub name_without_extension {
    my $self = shift;
    return $self->name;
}

=head2 save_magic_string_document

Converts and stores a given filestore entry. Case_id is NOT optional.

=cut

sub save_magic_string_document {
    my $self = shift;
    my ($case_id) = @_;

    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context(
        $self->result_source->schema->resultset('Zaak')->find($case_id)
    );

    return $self->save_template_document($ztt);
}

=head2 save_template_document

Converts and stores the filestore entry, using the specified
L<Zaaksysteem::ZTT> instance.

=cut

sub save_template_document {
    my $self = shift;
    my ($ztt) = @_;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $dir = File::Temp->newdir();
    my $tmp_fh = File::Temp->new(
        TEMPLATE => "ztt-process-XXXXXX",
        UNLINK   => 1,
        DIR      => $dir,
        # If there is no suffix, and the name of the DIR above contains a ".",
        # a call to Archive::ZIP done by "save" below will do bad things (it
        # strips everything after the last "." and adds ".zbk")
        SUFFIX => '.odt',
    );
    my $tmp_file = $tmp_fh->filename;
    my $path = $self->get_path;

    odfWorkingDirectory($dir);
    my $encoding = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    my $document = odfDocument(
        file            => "$path" || undef,
        local_encoding  => $encoding,
    );

    $document = $ztt->process_template($document)->document;

    $document->save($tmp_file);

    Zaaksysteem::StatsD->statsd->end('filestore.save_template_doc.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.save_template_doc', 1);

    return $tmp_fh;
}

=head2 content

Returns content of this file as a string

=cut

sub content {
    my $self = shift;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $file = $self->read_handle();
    my $content = join "", <$file>;
    close $file;

    Zaaksysteem::StatsD->statsd->end('filestore.content.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.content', 1);

    return $content;
}

=head2 read_handle

Return a handle to the file (on disk), ready for reading.

=cut

sub read_handle {
    my $self = shift;
    return $self->_filestore_engine->get_fh($self->uuid);
}

=head2 get_path

Returns a local path for the file.

Note: callers should stringify the return value themselves, as this method
could return a L<File::Temp> object (that gets removed auto-removed on
destruction).

=cut

sub get_path {
    my $self = shift;
    return $self->_filestore_engine->get_path($self->uuid);
}

=head2 download_url

Returns the download URL for this file, for use with Nginx C<X-Accel-Redirect>.

=cut

sub download_url {
    my $self = shift;
    return $self->_filestore_engine->download_url($self->uuid);
}

=head2 as_object

Returns a L<Zaaksysteem::Object::Types::File> instance.

=cut

sub as_object {
    my $self = shift;

    return Zaaksysteem::Object::Types::File->new(
        date_created      => $self->date_created,
        date_modified     => $self->date_created,
        name              => $self->original_name,
        size              => $self->size,
        mimetype          => $self->mimetype,
        md5               => $self->md5,
        id                => $self->uuid,
        virus_scan_status => $self->virus_scan_status,
        archivable        => $self->is_archivable,
    );
}

=head2 _filestore_engine

Return a L<Zaaksysteem::Filestore::Engine> that can be used to retrieve the stored file.

=cut

sub _filestore_engine {
    my $self = shift;
    
    my $engine_name = $self->storage_location->[0];
    return $self->filestore_model->get_engine_by_name($engine_name);
}

1;

=head2 update_status_from_viruses

Sets the C<virus_scan_status> field depending on the list of viruses:

if the list is empty, the status will be C<ok>, otherwise C<found>.

=cut

sig update_status_from_viruses => 'ArrayRef';

sub update_status_from_viruses {
    my $self = shift;
    my $viruses = shift;

    my $status = scalar @$viruses ? 'found' : 'ok';
    $self->update( { virus_scan_status => $status } );

    my $created = $self->date_created->hires_epoch();
    my $t0 = [split(/\./, $created)];

    Zaaksysteem::StatsD->statsd->end('filestore.virus_scan_status.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.virus_scan_status.count', 1);

    return;
}

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
