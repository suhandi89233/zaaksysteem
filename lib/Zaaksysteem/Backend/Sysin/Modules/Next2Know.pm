package Zaaksysteem::Backend::Sysin::Modules::Next2Know;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
);

use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use IO::All;

use Zaaksysteem::Backend::Sysin::Next2Know::Model;

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

my $MODULE_SETTINGS = {
    name                    => 'next2know',
    label                   => 'Next2Know',
    sensitive_config_fields => [qw(oauth_username oauth_password)],
    interface_config        => [
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_endpoint_oauth',
            type        => 'text',
            label       => 'OAuth endpoint',
            description => 'API endpoint voor OAuth authenticatie',
            data        => { pattern => '^https:\/\/.+' },
            required    => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_endpoint',
            type        => 'text',
            label       => 'Documents endpoint URI',
            required    => 1,
            description => 'URI naar Next2Know API, zonder slash aan het eind. Voorbeeld: https://zaaksysteem-api.next2know.nl/v1',
            data        => { pattern => '^https:\/\/.+' },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ca_cert',
            type        => 'file',
            label       => 'CA-certificaat',
            required    => 0,
            description => "CA-certificaat de CA van de Next2Know API"
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_oauth_username',
            type     => 'text',
            label    => 'Client ID',
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_oauth_password',
            type     => 'password',
            label    => 'Client secret',
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name => 'interface_result_grouping',
            type => 'select',
            label => 'Resultaat groepering',
            required => 1,
            description => 'Het documentattribuut waarop de resultaten gegroepeerd worden.',
            default => 'none',
            data => {
                options => [
                    { value => 'none', label => 'Geen' },
                    { value => 'dossier_name', label => 'Dossiernaam' }
                ]
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_error_text',
            type        => 'textarea',
            label       => 'Foutmelding',
            description => 'Standaard foutmelding die getoond wordt aan de gebruiker indien calls naar Next2know fout gaan',
        ),
    ],
    direction                     => 'outgoing',
    retry_on_error                => 0,
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    test_interface                => 1,
    test_definition               => {
        description =>
            'Hier kunt u de verbinding met Next2Know testen.',
        tests => [
            {
                id     => 1,
                label  => 'Test',
                name   => 'test_secure_connection',
                method => 'test_secure_connection',
                description =>
                    'Deze test probeert verbinding te maken met de server(s) van Next2Know.',
            },
        ],
    },
    trigger_definition =>
        { get_oauth_token => { method => 'get_oauth_token' }, },
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{$MODULE_SETTINGS});
};

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $schema    = $interface->result_source->schema;
    my $config    = $interface->get_interface_config;

    my $ca_file = $self->_get_ca_file($interface);
    my $model   = Zaaksysteem::Backend::Sysin::Next2Know::Model->new(
        oauth_endpoint => $config->{endpoint_oauth},
        username       => $config->{oauth_username},
        password       => $config->{oauth_password},
        ($ca_file ? (ca_file => $ca_file) : ()),
    );
    return $model;
}

=head2 get_oauth_token

Get the OAuth token from the model

=cut

sub get_oauth_token {
    my ($self, $params, $interface) = @_;

    return try {
        $interface->model->get_oauth_token(username => $params->{username});
    }
    catch {
        $self->log->error("Unable to get oauth token: $_");
        return undef;
    };
}

=head2 _get_ca_file

Get the CA file from the interface

=cut

sub _get_ca_file {
    my ($self, $interface) = @_;
    my $uuid = $interface->jpath('$.ca_cert[0].id');
    if ($uuid) {
        my $schema  = $interface->result_source->schema;
        my $ca_file = $schema->resultset("Filestore")->find($uuid);
        if (!$ca_file) {
            throw(
                'sysin/modules/ca_file/error',
                'CA certificate is not present.'
            );
        }
        return $ca_file->get_path;
    }
    return undef;
}

=head2 test_secure_connection

Test if the connection is secure

=cut

sub test_secure_connection {
    my ($self, $interface) = @_;

    my $ca_file = $self->_get_ca_file($interface);

    foreach (qw(endpoint_oauth endpoint)) {
        $self->test_host_port_ssl($interface->jpath('$.' . $_),
            $ca_file);
    }
}

=head2 get_public_interface_config

Returns the interface configuration and a token for the current user.

=cut

sub get_public_interface_config {
    my ($self, $interface, $serializer_options) = @_;

    my $config = $interface->get_interface_config;
    if (!$serializer_options->{is_admin}) {
        delete $config->{$_}
            for @{ $interface->module_object->{sensitive_config_fields} };
    }

    my $ca_file = $self->_get_ca_file($interface);
    if ($ca_file) {
        $config->{ca_cert} = io($ca_file)->slurp;
    }
    else {
        delete $config->{ca_cert};
    }

    $config->{username} = $serializer_options->{user}->username;
    $config->{token} = $interface->process_trigger(
        'get_oauth_token',
        { username => $config->{username} }
    );

    return $config;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
