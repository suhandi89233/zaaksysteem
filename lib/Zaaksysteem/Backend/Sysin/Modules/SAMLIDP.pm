package Zaaksysteem::Backend::Sysin::Modules::SAMLIDP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Storable qw/dclone/;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Tests::SAML
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI
    Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant
/;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

my $interface_description = qq{

De SAML Identity Provider koppeling zorgt voor de verschillende SAML
koppelvlakken, zoals DigiD, eHerkenning, AD FS, Lastpass en soortgelijke
authenticatiemiddelen.

Let u op dat u de configuratie-optie <i>"Log SAML XML"</i> niet zomaar
aanzet. Deze optie is voornamelijk ingebouwd om op het moment van
problemen de koppeling makkelijker te kunnen debuggen. Bij normaal
gebruik is deze logging overbodig en vult onnodig het
transactieoverzicht. Veelal zullen support-medewerkers van Mintlab deze
optie aanzetten indien dat noodzakelijk is.

};

use constant INTERFACE_ID => 'samlidp';

sub build_config_fields {
    my $self   = shift;
    my @fields = (
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_authentication_level',
            type  => 'select',
            label => 'Betrouwbaarheidsniveau',
            default =>
                'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
            description =>
                '<p>Selecteer het minimaal vereiste betrouwbaarheidsniveau</p><ul><li>Niveau 1: geen controle</li><li>Niveau 2 / 10: Username + Password</li><li>Niveau 2+: Username + Password + SMS code</li><li>Niveau 3 / 20: Username + Password + SMS code (op een geregistreerd nummer)</li><li>Niveau 25: Smartcard</li><li>Niveau 4 / 30: Smartcard met PKI</li></ul>',
            when     => '!interface_disable_requested_authncontext',
            data     => {
                options => [
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified',
                        label => 'eH:Niveau 1 (unspecified)'
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
                        label =>
                            'eH:Niveau 2 / DigiD:Niveau 10 (PasswordProtectedTransport)'
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorUnregistered',
                        label => 'eH:Niveau 2+ (MobileTwoFactorUnregistered)'
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract',
                        label => 'eH:Niveau 3 / DigiD:Niveau 20 (MobileTwoFactorContract)',
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:Smartcard',
                        label => 'DigiD:Niveau 25 (Smartcard)'
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI',
                        label => 'eH:Niveau 4 / DigiD:Niveau 30 (SmartcardPKI)'
                    },
                    # https://afsprakenstelsel.etoegang.nl/display/as/Level+of+assurance
                    {
                        value => 'urn:etoegang:core:assurance-class:loa1',
                        label => 'eIDAS Non existent / eToegang Level of Assurance 1'
                    },
                    {
                        value => 'urn:etoegang:core:assurance-class:loa2',
                        label => 'eIDAS Low / eToegang Level of Assurance 2'
                    },
                    {
                        value => 'urn:etoegang:core:assurance-class:loa2plus',
                        label => 'eIDAS Low / eToegang Level of Assurance 2+'
                    },
                    {
                        value => 'urn:etoegang:core:assurance-class:loa3',
                        label => 'eIDAS Substantial / eToegang Level of Assurance 3'
                    },
                    {
                        value => 'urn:etoegang:core:assurance-class:loa4',
                        label => 'eIDAS High / eToegang Level of Assurance 4'
                    },
                ],
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'interface_binding',
            type    => 'select',
            label   => 'SSO Binding',
            default => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
            when    => 'interface_saml_type != "eidas"',
            description =>
                'De <abbr title="Single-Sign-On, de generieke SAML term voor de loginprocedure">SSO</abbr> Binding bepaalt hoe de SAML Protocol Exchange plaatsvindt, en is afhankelijk van de eisen van de <abbr title="Identity Provider">IdP</abbr>',
            required => 1,
            data     => {
                options => [
                    {
                        value =>
                            'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                        label => 'HTTP POST'
                    },
                    {
                        value =>
                            'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
                        label => 'HTTP Artifact'
                    },
                    {
                        value =>
                            'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                        label => 'HTTP Redirect'
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                        label => 'SOAP'
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:bindings:POAS',
                        label => 'Reverse SOAP'
                    }
                ]
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'interface_saml_type',
            type    => 'select',
            label   => 'SAML Implementatie',
            default => 'digid',
            data    => {
                options => [
                    {
                        value => 'digid',
                        label => 'Logius'
                    },
                    {
                        value => 'eherkenning',
                        label => 'eHerkenning / KPN Lokale Overheid',
                    },
                    {
                        value => 'eidas',
                        label => 'eIDAS',
                    },
                    {
                        value => 'adfs',
                        label => 'Microsoft AD FS',
                    },
                    {
                        value => 'minimal',
                        label => 'Custom SAML (alleen voor gebruikerslogin)',
                    },
                    {
                        value => 'spoof',
                        label => 'Mintlab Spoofmode'
                    }
                ],
            },
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_signature_algorithm',
            type  => 'select',
            label => 'Signature algoritme',
            when  => 'interface_saml_type === "eidas"',
            data    => {
                options => [
                    {
                        value => 'sha1',
                        label => 'SHA-1',
                    },
                    {
                        value => 'sha256',
                        label => 'SHA-256',
                    },
                ],
            },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_spoof_warning_text_citizen',
            type  => 'display',
            label => '',
            when =>
                'interface_saml_type == "spoof" && interface_login_type_citizen',
            data => {
                template =>
                    '<em>Let op:</em> door gebruik te maken van de spoof implementatie kunnen willekeurige gebruikers inloggen als burger op de PIP van de omgeving.'
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_spoof_warning_text_company',
            type  => 'display',
            label => '',
            when =>
                'interface_saml_type == "spoof" && interface_login_type_company',
            data => {
                template =>
                    '<em>Let op:</em> door gebruik te maken van de spoof implementatie kunnen willekeurige gebruikers inloggen als bedrijf op de PIP van de omgeving.'
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_spoof_warning_text_user',
            type  => 'display',
            label => '',
            when =>
                'interface_saml_type == "spoof" && interface_login_type_user',
            data => {
                template =>
                    '<em>Let op:</em> door gebruik te maken van de spoof implementatie kunnen willekeurige gebruikers inloggen als medewerker.'
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_sp_metadata',
            label => 'SAML SP metadata-URL',
            description =>
                '<p>Dit is de metadata-URL die door de Identity Provider gebruikt wordt om de meest recente metadata op te halen.</p><p>Door deze IdP-specifieke metadata-URL te gebruiken, is het mogelijk om voor elke IdP een eigen set certificaten te gebruiken.</p>',
            type => 'display',
            data => {
                template =>
                    '<[field.value]>auth/saml/metadata?idp_id=<[activeLink.id]>'
            },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_idp_metadata',
            type  => 'text',
            label => 'SAML IdP metadata-URL',
            description =>
                'Geef de URL op waar de metadata van de <abbr title="Identity Provider">IdP</abbr> te verkrijgen is. Deze URL wordt door de IdP geleverd',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_idp_metadata_filename',
            type  => 'file',
            label => 'SAML Metadata File',
            description =>
                'Wanneer een URL niet mogelijk is, upload hier de metadata van uw <abbr title="Identity Provider">IdP</abbr>',
            when => '!interface_idp_metadata'
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_idp_entity_id',
            type     => 'text',
            label    => 'Entity ID',
            required => 0,
            description =>
                'Voer hier uw toegewezen EntityID in in het geval de <abbr title="Identity Provider">IdP</abbr> deze aangeleverd heeft.<br />Indien geen waarde opgevoerd is wordt de basis URL uit de <abbr title="Service Provider (Zaaksysteem)">SP</abbr> definitie gebruikt.',
            data => {
                placeholder =>
                    'urn:etoegang:DV:00000003123456780000:entities:1'
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_idp_service_id',
            type     => 'text',
            label    => 'Service ID',
            required => 0,
            description => q{
                Voer hier het ServiceID in dat u wilt gebruiken voor deze koppeling.
            },
            data => {
                placeholder =>
                    'urn:etoegang:DV:00000003123456780000:services:1'
            },
            when => 'interface_saml_type == "eherkenning" || interface_saml_type == "eidas"',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_idp_acs_index',
            type     => 'text',
            label    => 'AttributeConsumingService Index',
            required => 1,
            default  => 0,
            when     => 'interface_saml_type == "eherkenning" || interface_saml_type == "eidas"',
            description => q{
                Hier kunt u een AttributeConsumingService Index opgeven. Dit gegeven dient afgestemd te worden met de SAML IdP leverancier en moet gelijk zijn aan de cijfers achter de laatste dubbele punt (&quot;:&quot;) van het ServiceID.
            },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_idp_ca',
            type  => 'file',
            label => 'CA Certificaat van de IdP',
            description =>
                'Upload hier het certificaat van de <abbr title="Certificate Authority (de partij die de IdP heeft gecertificeerd)">CA</abbr> die het certificaat gebruikt door de <abbr title="Identity Provider">IdP</abbr> heeft ondertekend.',
            required => 0,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_idp_cert',
            type  => 'file',
            label => 'Certificaat en private key',
            description =>
                'Private key en certificaat die gebruikt worden in de communicatie met deze <abbr title="identity provider">IdP</abbr>. Als dit veld gevuld is, wordt de waarde uit het SAML SP-koppelprofiel voor deze IdP-koppeling genegeerd.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_login_type_citizen',
            label => 'Gebruik op burgerloginpagina',
            type  => 'checkbox',
            when =>
                'interface_saml_type == "digid" || interface_saml_type == "eidas" || interface_saml_type == "spoof"',
            options => [{ value => 'company', label => 'Burgerinlogpagina' },]
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_login_type_company',
            label => 'Gebruik op bedrijfloginpagina',
            type  => 'checkbox',
            when =>
                'interface_saml_type == "eherkenning" || interface_saml_type == "spoof"',
            options =>
                [{ value => 'company', label => 'Bedrijfinlogpagina' },]
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_login_type_user',
            label => 'Gebruik op medewerkerloginpagina',
            type  => 'checkbox',
            when =>
                'interface_saml_type == "adfs" || interface_saml_type == "minimal" || interface_saml_type == "spoof"',
            options =>
                [{ value => 'company', label => 'Medewerkerinlogpagina' },]
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_disable_requested_authncontext',
            label => 'Compatibiliteit met ADFS SSO',
            type  => 'checkbox',
            when  => 'interface_saml_type == "adfs" && interface_login_type_user',
            description => '<p>ADFS single sign-on werkt niet goed als er een minimaal betrouwbaarheidsniveau wordt opgegeven in het authenticatieverzoek. Door deze optie aan te zetten wordt dit niet opgenomen in het authenticatieverzoek, en vertrouwt Zaaksysteem volledig op de instellingen van ADFS.</p>',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_use_saml_slo',
            label => 'Ondersteun eenmalig in- en uitloggen',
            type  => 'checkbox',
            when  => '((interface_saml_type == "adfs" || interface_type == "minimal") && interface_login_type_user) || interface_login_type_citizen && interface_saml_type == "digid"',
            description => qq{
                <p>Veel Identity Providers bieden naast &quot;single
                sign-on&quot; ook een &quot;single logout&quot;-dienst, zodat de
                IdP weet dat de gebruikt uitgelogd is bij de applicatie.</p>
                <p><strong>Let op:</strong> als je deze optie aanzet bij ADFS, moet eerst
                een &quot;NameID&quot; claim rule geconfigureerd worden in de
                Relying Party-instellingen van AD FS.</p>
                <p><strong>Let op:</strong> als je deze optie aanzet bij DigID, moet je eerst
                contact opnemen met Logius</p>
            },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_use_nameid',
            type  => 'checkbox',
            label => 'NameID gebruiken als gebruikersnaam',
            when  => 'interface_saml_type == "adfs" && interface_login_type_user && !interface_use_upn',
            description =>
                '<p>De SAML/AD FS-server kan Zaaksysteem een unieke gebruikersnaam '
                . 'voor een inloggende gebruiker geven in het veld &quot;NameID&quot;.'
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_use_upn',
            type  => 'checkbox',
            label => 'Volledige UPN gebruiken als gebruikersnaam',
            when  => 'interface_saml_type == "adfs" && interface_login_type_user && !interface_use_nameid',
            description =>
                '<p>In het geval van meerdere AD domeinen kan het nodig zijn om'
                . ' de volledige (of een andere username) te hebben dan alleen '
                . ' de inlognaam. Voor meer informatie: <a'
                . ' href="https://docs.microsoft.com/nl-nl/windows-server/identity/ad-fs/operations/configuring-alternate-login-id">'
                . ' Alternatieve aanmeldings-id configureren</a></p>'
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_saml_request_nameid',
            label => 'NameID-opmaak',
            type  => 'select',
            when  => 'interface_use_saml_slo || interface_use_nameid',
            description => qq{
                <p>Het soort NameID dat opgevraagd wordt in authenticatieverzoeken
                naar de SAML IdP.</p>
            },
            data    => {
                options => [
                    {
                        value => '',
                        label => 'Geen (standaard)',
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
                        label => 'Email-adres',
                    },
                    {
                        value => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
                        label => 'Vaste identifier ("persistent")',
                    },
                ],
            },

        ),

        Zaaksysteem::ZAPI::Form::Field->new(
            name => 'interface_login_type_user_direct',
            label =>
                'Stuur medewerkers direct naar SAML loginpagina bij inloggen',
            description =>
                'Wanneer deze SAML interface wordt gebruikt voor de medewerkerlogin, dan kunt u ervoor kiezen om de loginpagina van Zaaksysteem helemaal niet meer te laten zien en altijd terug te keren naar uw active directory service',
            type => 'checkbox',
            when =>
                '(interface_saml_type == "adfs" || interface_saml_type == "minimal" || interface_saml_type == "spoof") && interface_login_type_user',
            options =>
                [{ value => 'company', label => 'Medewerkerinlogpagina' },]
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_log_saml_response',
            label => 'Log SAML XML (debugging feature)',
            description =>
                'Hiermee worden alle XML berichten welke opgestuurd/ontvangen worden van de IDP (Logius, eHerkenning, ADFS, etc) bijgeschreven in het transactielog. Deze functionaliteit alleen aanzetten als men de koppeling moet testen ism support',
            type    => 'checkbox',
            options => [
                {
                    value => 'log_saml_request',
                    label => 'Log SAML Response XML'
                },
            ]
        ),
    );
    return \@fields;
}

my $MODULE_SETTINGS = {
    name                          => INTERFACE_ID,
    label                         => 'SAML 2.0 Identity Provider',
    description                   => $interface_description // '',
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    test_interface                => 1,
    credential_module             => 'saml',
    test_definition               => {
        description => qq|
            Om te controleren of het Zaaksysteem correct is geconfigureerd
            om een SAML Identity Provider te gebruiker als authenticatie
            middel kunt u hieronder een aantal tests uitvoeren. Hiermee
            controleert u of de geconfigureerde Identity Provider bereikbaar
            is en de certificering klopt.
        |,
        tests => [
            {
                id     => 1,
                label  => 'Test configuratie',
                name   => 'instantiation_test',
                method => 'test_idp'
            }
        ]
    },
    trigger_definition => {
        register_changes => { method => 'register_changes', },
        log_saml_xml     => { method => 'log_saml_xml', },
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    $class->$orig(%{$MODULE_SETTINGS});
};

has 'subject_type' => (
    is      => 'ro',
    default => 'employee',
);

=head2 uri_values

Configuration keys to fill with the base URI of the current instance.

See L<Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI> for more information.

=cut

has uri_values => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        {
            interface_sp_metadata => '',
        }
    },
);

=head2 register_changes

Create a transaction (+ transaction record), logging a change to a SAML
IdP-based user account.

This should be called through the "trigger" feature.

=cut

sub register_changes {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_changes',
                changes         => $raw_params->{changes},
            },
            external_transaction_id => 'unknown',
            input_data              => 'json',
            #direct                  => 1,
        }
    );

    return $transaction;
}

sub _process_changes {
    my $self = shift;
    my $record = shift;
    my $mutations;

    my $transaction = $self->process_stash->{transaction};

    my $params = $transaction->get_processor_params();

    my $changes = $params->{changes};

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        table    => 'Subject',
        table_id => $changes->{_subject_id}
    );

    my $subject = $transaction
                ->result_source
                ->schema
                ->resultset('Subject')
                ->search({ id => $changes->{_subject_id} })
                ->first;

    throw(
        'sysin/modules/samlidp',
        'Cannot find subject by id: ' . $changes->{_subject_id}
    ) unless $subject;

    if ($changes->{_create}) {
        $mutation->create(1);
    }
    else {
        $mutation->update(1);
    }

    my $human_readable_changes = sprintf("User %s changed\n", $subject->username);

    for my $column (keys %{ $changes }) {
        my $columndata = $changes->{ $column };
        next unless ref($columndata) eq 'HASH';

        $mutation->add_mutation(
            {
                old_value   => $columndata->{old},
                new_value   => $columndata->{new},
                column      => $column,
            }
        );

        $human_readable_changes .= 'Attribuut "' . $column
                                . '" gewijzigd naar "'
                                . $columndata->{new} . "\"\n";
    }

    $self->process_stash->{row}->{mutations} = [ $mutation ];

    $record->input($human_readable_changes);
    $record->output('User updated successfully');
}

=head2 log_saml_xml

    $idpmodule->trigger(
        'log_saml_xml',
        {
            xml     => '<xml [...]',
            status  => 'success',
        }
    );

Create a transaction (+ transaction record), logging a xml response from the IDP

This should be called through the "trigger" feature.

=cut

sub log_saml_xml {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    return unless $interface->get_interface_config->{log_saml_response};

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_log_saml_xml',
                %{ $raw_params },
            },
            external_transaction_id => 'unknown',
            input_data              => "$raw_params->{xml}",
            direct                  => 1,
        }
    );

    return $transaction;
}

sub _process_log_saml_xml {
    my $self = shift;
    my $record = shift;
    my $mutations;

    my $transaction = $self->process_stash->{transaction};

    my $params      = $transaction->get_processor_params();

    $record->input("$params->{xml}");
    my $output = "Processed for:\n";
    $output .= sprintf("%-13s", $_) . ':' . $params->{$_} . "\n" for qw/context_id message/;
    $record->output($output);
    $record->preview_string(sprintf('%s verwerkt: %s', $params->{message}, $params->{context_id}));

    return $output;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
