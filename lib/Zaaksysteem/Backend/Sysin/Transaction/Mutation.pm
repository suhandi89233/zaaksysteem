package Zaaksysteem::Backend::Sysin::Transaction::Mutation;

use Moose;

use BTTW::Tools;

has 'mutations'     => (
    is      => 'rw',
    lazy    => 1,
    default => sub { return []; },
    isa     => 'ArrayRef[Zaaksysteem::Backend::Sysin::Transaction::Mutation::_COLUMN]',
);

has 'table'         => (
    required => 1,
    is      => 'rw',
    isa     => 'Str',
);

has 'table_id'      => (
    is      => 'rw',
    isa     => 'Num',
);

has 'create'        => (
    is      => 'rw',
    isa     => 'Bool',
);

has 'update'        => (
    is      => 'rw',
    isa     => 'Bool',
);

has 'delete'        => (
    is      => 'rw',
    isa     => 'Bool',
);

has 'unchanged'     => (
    is      => 'rw',
    isa     => 'Bool',
    default => 1,
);

sub TO_JSON {
    my $self        = shift;

    return {
        mutations       => $self->mutations,
        table           => $self->table,
        table_id        => $self->table_id,
        create          => $self->create,
        update          => $self->update,
        delete          => $self->delete,
        unchanged       => $self->unchanged,
    }
}

sub add_mutation {
    my $self        = shift;
    my $params      = shift;

    my @statuses = grep { $self->$_ } qw[update create delete];

    if(!$self->table_id) {
        throw('sysin/transaction/mutation/missing_table_id', 'Cannot create mutation, missing table_id') unless $self->table_id;
    }

    if(scalar @statuses != 1) {
        throw('sysin/transaction/mutation', 'Unable to add mutation, inconsistent mutation_type', \@statuses);
    }

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation::_COLUMN->new(
        %{ $params },
        mutation_type => pop @statuses
    );

    push @{ $self->mutations }, $mutation;

    $self->unchanged(0);

    return 1;
}

sub from_dbix {
    my $self        = shift;
    my $row         = shift;
    my $old_values  = shift;

    my %changes     = $row->get_columns;

    return unless scalar(keys %changes);

    ### Drop overhead
    my @changed_cols    = grep {
        $_ ne 'search_index' &&
        $_ ne 'object_type' &&
        $_ ne 'search_term' &&
        $_ ne 'search_index' &&
        $_ ne 'searchable_id' &&
        $_ ne 'id'
    } keys %changes;


    for my $col (@changed_cols) {
        my $value               = $changes{$col};

        my %mutation            = (
            column      => $col,
            new_value   => $value,
        );

        if ($self->create) {
            next unless defined($value);
            $mutation{old_value} = undef;
        } elsif ($old_values && exists $old_values->{$col}) {
            my $oldvalue = $old_values->{$col};

            ### No changes
            if (!defined $value && !defined $oldvalue) {
                next;
            }

            if (
                defined $value && defined $oldvalue &&
                $value eq $oldvalue
            ) {
                next;
            }

            $mutation{old_value} = $oldvalue;
        }


        $self->add_mutation(
            \%mutation
        )
    }
}

sub register {
    my $self        = shift;
    my $record      = shift;

    if(!$self->table_id) {
        throw('sysin/transaction/mutation/missing_table_id', 'Cannot create mutation, missing table_id') unless $self->table_id;
    }

    return $record  ->transaction_record_to_objects
                    ->transaction_record_to_object_create(
                        {
                            local_table             => $self->table,
                            local_id                => $self->table_id,
                            transaction_record_id   => $record->id,
                            mutations               => $self->mutations,
                            mutation_type           => (
                                $self->unchanged
                                    ? 'unchanged'
                                    : grep { $self->$_ } qw/
                                        create
                                        update
                                        delete
                                    /,
                            )
                        }
                    );
}

__PACKAGE__->meta->make_immutable;

package Zaaksysteem::Backend::Sysin::Transaction::Mutation::_COLUMN;

use Moose;

has 'mutation_type' => (
    required => 1,
    'is'    => 'rw',
);

has 'column'        => (
    required => 1,
    is      => 'rw',
    isa     => 'Str',
);

has 'old_value'     => (
    required => 0,
    is      => 'rw',
);

has 'new_value'     => (
    required => 0,
    is      => 'rw',
);

sub TO_JSON {
    my $self        = shift;

    return {
        old     => $self->old_value,
        new     => $self->new_value,
        column  => $self->column,
    }
}


__PACKAGE__->meta->make_immutable;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 add_mutation

TODO: Fix the POD

=cut

=head2 from_dbix

TODO: Fix the POD

=cut

=head2 register

TODO: Fix the POD

=cut

