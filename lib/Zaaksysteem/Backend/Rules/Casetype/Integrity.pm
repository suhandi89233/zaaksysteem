package Zaaksysteem::Backend::Rules::Casetype::Integrity;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Rules::Casetype::Integrity - Integrity object.

=head1 SYNOPSIS

    $integrity = Zaaksysteem::Backend::Rules->integrity;

    if ($integrity->success) {
        print "NO PROBLEMS"
    } else {
        for my $problem (@{ $integrity->problems }) {
            print "Rule number: " . $problem->{rule_number} . ', id:' . $problem->{id}
                . ', has problem: ' . $problem->{message};
        }
    }

=cut

use JSON::Path;
use BTTW::Tools;
use List::Util qw[first];
use Zaaksysteem::Backend::Rules::Integrity;
use Zaaksysteem::Backend::Rules::Serializer;

around 'integrity' => sub {
    my $method      = shift;
    my $class       = shift;
    my ($options, $schema)   = @_;
    my $problems    = [];
    $options        ||= {};

    throw(
        'rules/casetype/integrity/invalid_params',
        'from_session missing or not a HashRef'
    ) unless ($options->{from_session} && ref $options->{from_session} eq 'HASH');

    $problems       = $class->_verify_rules($problems, $options, $schema);

    return Zaaksysteem::Backend::Rules::Integrity->new(success => (scalar @$problems ? 0 : 1), problems => $problems);
};

sub _verify_rules {
    my $class                   = shift;
    my ($problems, $options, $schema)    = @_;

    ### Loop over statussen
    for my $status (keys %{ $options->{from_session}->{statussen} }) {
        my $status_data = $options->{from_session}->{statussen}->{$status}->{elementen};

        my $problem = {
            status_number   => $status,
        };

        my @bib_ids;
        for my $kenmerk_number (keys %{ $status_data->{kenmerken} }) {
            my $kenmerk = $status_data->{kenmerken}->{$kenmerk_number};

            # TODO XXX Not complete, we skip over objecttype references for now, either fix here, or prevent their
            # inclusion in bibliotheek_kenmerken_id fields
            next unless $kenmerk->{bibliotheek_kenmerken_id} && $kenmerk->{ bibliotheek_kenmerken_id } =~ m[^\d+$];

            push (@bib_ids, $kenmerk->{bibliotheek_kenmerken_id})
        }

        if (!$options->{from_session}->{node}->{id}) {
            print STDERR "Casetype::Integrity: Cannot find a node id, this should never happen\n";
            next;
        }

        my @kenmerken   = $schema->resultset('ZaaktypeKenmerken')->search(
            {
                'bibliotheek_kenmerken_id.id' => { -in => \@bib_ids },
                'me.zaaktype_node_id' => $options->{from_session}->{node}->{id},
            },
            {
                prefetch => 'bibliotheek_kenmerken_id'
            }
        )->all;

        for my $rule_number (keys %{ $status_data->{regels} }) {
            my $rule_raw    = $status_data->{regels}->{$rule_number};
            my $rule        = $class->_convert_row_to_hashref($schema, $rule_raw) or next;

            my $problem     = {
                status_number   => $status,
                rule_number     => $rule_number,
                id              => $rule_raw->{id},
                naam            => $rule->{naam},
            };

            $problems       = $class->_verify_kenmerken($schema,$problems, $problem, $status_data, $rule, \@kenmerken);
            $problems       = $class->_verify_groups($schema, $problems, $problem, $status_data, $rule);
        }
    }

    return $problems;
}

sub _verify_groups {
    my $class                   = shift;
    my ($schema,$problems, $current_problem, $status_data, $rule) = @_;

    for my $key (keys %{ $rule }) {
        my ($type, $index);
        unless (
            (($type, $index) = $key =~ /^(actie|anders)_(\d+)$/) &&
            ($rule->{$key} =~ 'hide_group' || $rule->{$key} =~ 'show_group')
        ) {
            next;
        }

        my $value = $rule->{$type . '_' . $index . '_kenmerk'};

        my $found = 0;
        for my $kenmerk_number (keys %{ $status_data->{kenmerken} }) {
            my $kenmerk = $status_data->{kenmerken}->{$kenmerk_number};

            $found++ if (
                $kenmerk->{is_group} &&
                $kenmerk->{label} eq $value
            );
        }

        next if $found;

        ### Clone and populate
        my %problem         = %$current_problem;

        $problem{message}   = 'Kenmerkgroep bestaat niet (meer) in ' . $type;

        push(@$problems, \%problem);
    }

    return $problems;
}


sub _verify_kenmerken {
    my $class = shift;
    my ($schema, $problems, $current_problem, $status_data, $rawrule, $kenmerken) = @_;


    for my $prefix (qw/actie voorwaarde anders/) {
        my $rules = $class->_convert_casetype_rule_to_array($prefix, $rawrule);
        my $message;

        for my $rule (@$rules) {
            my $bib_id  = $rule->{kenmerk};

            next unless $bib_id && $bib_id =~ /^\d+$/;

            my $bib = first {
                ($_->get_column('bibliotheek_kenmerken_id') // '') eq $bib_id
            } @{ $kenmerken };

            unless (defined $bib) {
                $message    = "Kenmerk in $prefix niet in bibliotheek gevonden";
            } else {
                my @options = @{ $bib->options };
                if (@options) {
                    my @contents = ref $rule->{value} eq 'ARRAY' ? @{ $rule->{value} } : $rule->{value};

                    if (grep { defined($_) && length($_) } @contents) {
                        my $missing;
                        for my $content (grep { defined($_) && length($_) } @contents) {
                            if (grep({ $_->{active} && defined $_->{value} && length $_->{value} && $_->{value} eq $content} @options)) {
                                if (!defined($missing)) {
                                    $missing = 0;
                                }
                                next;
                            } else {
                                $missing = 1;
                            }
                        }

                        if ($missing) {
                            $message = "Opties in $prefix zijn niet meer actief";
                        }
                    }
                }
            }
        }

        next unless $message;
        my %problem             = %$current_problem;
        $problem{message}       = $message;

        push(@$problems, \%problem);
    }

    return $problems;
}

sub _convert_row_to_hashref {
    my ($class, $schema, $entry) = @_;

    my $s = Zaaksysteem::Backend::Rules::Serializer->new(
        schema => $schema->schema,
    );
    my $rule = $s->decode_to_rule($entry->{settings});

    return unless $rule;

    $rule->{naam} = $entry->{naam};
    return $rule;
}

sub _convert_casetype_rule_to_array {
    my $self            = shift;
    my $prefix          = shift;
    my $rule            = shift;
    my (@list);

    for my $rawkey (grep(/^${prefix}_.*/, sort keys %$rule)) {
        my ($id, $var)  = $rawkey =~ /^${prefix}_(\d+)_?(.*)$/;
        $var = 'type' unless $var;

        my $index       = ($id - 1);
        unless ($list[$index]) { $list[$index] = {}; }

        $list[$index]->{$var}  = $rule->{ $rawkey };
    }

    return \@list;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
