package Zaaksysteem::DB::Component::Logging::Case::Update::Field;
use Moose::Role;

=head2 onderwerp

Return a human readable log output

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    my $new_values = $data->{new_values};
    my $value_text;
    if (ref $new_values eq 'ARRAY') {
        my @array = @{$new_values};
        my $val = pop @array;
        $value_text = sprintf("met de volgende waarden: %s en '%s'",
            join(', ', map { "'$_'" } @array), $val);

    }
    elsif (defined $new_values) {
        $value_text = "met de volgende waarde: '$new_values'";
    }
    else {
        $value_text = "er is geen waarde voorgesteld";
    }

    return
        sprintf("%s heeft een wijziging voorgesteld voor kenmerk '%s', %s. Opgegeven toelichting: '%s'",
        $data->{subject_name}, $data->{kenmerk}, $value_text, $data->{toelichting} // '<geen>');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
