package Zaaksysteem::DB::Component::Logging::Case::PostMessage;

use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::PostMessage
- Event message handler for case/post_message events.

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    my $onderwerp = sprintf('Er is een bericht verstuurd naar "%s" (%d)',
        $data->{interface_name},
        $data->{interface_id},
    );

    return $onderwerp;
}

=head2 event_category

defines the category, used by the frontend in the timeline, to display the right
icons etc.

=cut

sub event_category { 'case-mutation'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
