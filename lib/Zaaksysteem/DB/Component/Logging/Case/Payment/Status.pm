package Zaaksysteem::DB::Component::Logging::Case::Payment::Status;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return sprintf(
        "Betaalstatus aangepast: " .  decode_action($self->data->{ status_code }),
        $self->data->{ amount }
    );
}

sub decode_action {
    my $status = shift;

    my %lookup = (
        'pending' => 'Wacht op bevestiging (bedrag: &euro;%s)',
        'failed'  => 'Mislukt',
        'success' => 'Geslaagd (bedrag: &euro;%s)',
        'offline' => 'Later betalen (bedrag: &euro;%s)',
    );

    return $lookup{ $status } || $status;
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 decode_action

TODO: Fix the POD

=cut

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

