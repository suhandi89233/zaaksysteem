package Zaaksysteem::DB::Component::Logging::Auth::Login::TokenRequest;
use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Auth::Login::TokenRequest - Log line processor for "login token requested" log lines

=head1 SYNOPSIS

    my $log = $logging_rs->trigger(
        'auth/login/token_request',
        {
            data => {
                subject_id => 'XXX',
                subject_username => 'YYY',
                instance_id => 'AAA',
                instance_fqdn => 'BBB',
            }
        }
    );

=head1 METHODS

=head2 onderwerp

Formats the data contained in this log item in a human-readable way.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        'Gebruiker "%s" heeft authenticatie-machtiging aangevraagd bij "%s"',
        ($self->data->{subject_username} // ''),
        $self->data->{instance_fqdn},
    );
}

=head2 event_category

As this is a system-level event, always returns "system".

=cut

sub event_category { 'system'; }

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
