package Zaaksysteem::DB::Component::Logging::Folder::Remove;

use Moose::Role;

sub onderwerp {
    my $self = shift;
    return sprintf(
        "Folder '%s' verwijderd: %s",
        $self->data->{ name },
        $self->data->{ reason }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
