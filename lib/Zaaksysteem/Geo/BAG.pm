package Zaaksysteem::Geo::BAG;
use Moose;

with qw(MooseX::Log::Log4perl);

use constant    SOURCE_TABLES   => {
    nummeraanduiding    => 'BagNummeraanduiding',
    openbareruimte      => 'BagOpenbareruimte',
    woonplaats          => 'BagWoonplaats',
    verblijfsobject     => 'BagVerblijfsobject',
    pand                => 'BagPand',
    # Rest still unsupported
};

use constant COMMON_DATA => {
    begindatum     => 'start_date',
    einddatum      => 'end_date',
    documentdatum  => 'document_date',
    documentnummer => 'document_number',
    inonderzoek    => 'under_investigation',
};

use constant    ESSENTIAL_DATA  => {
    BagNummeraanduiding => {
        parent                  => {
            table       => 'BagOpenbareruimte',
            key         => 'openbareruimte'
        },
        fields                  => {
            %{ COMMON_DATA() },

            postcode                => 'postcode',
            huisnummer              => 'huisnummer',
            huisnummertoevoeging    => 'huisnummertoevoeging',
            huisletter              => 'huisletter',
            gps_lat_lon             => 'gps_lat_lon'
        }
    },
    BagOpenbareruimte => {
        parent                  => {
            table       => 'BagWoonplaats',
            key         => 'woonplaats'
        },
        fields                  => {
            %{ COMMON_DATA() },

            naam                    => 'straat',
            gps_lat_lon             => 'gps_lat_lon'
        }
    },
    BagWoonplaats => {
        fields                  => {
            %{ COMMON_DATA() },

            naam                    => 'woonplaats',
        }
    },
    BagVerblijfsobject => {
        fields => {
            %{ COMMON_DATA() },

            oppervlakte => 'surface_area',
        },
    },
    BagPand => {
        fields => {
            %{ COMMON_DATA() },

            bouwjaar => 'year_of_construction',
        },
    },
};

has 'apikey'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;
        my $schema  = $self->result_source->schema;

        return $schema
                ->default_resultset_attributes
                ->{config}
                ->{google_api_key}
    }
);

=head2 $component->geocode_term

Return value: $geocode_address

Generate a term for geocoding this location with a googlemaps or other
geocoder.

    e.g:
        Nederland, Amsterdam, Donker Curtiusstraat 7
        Nederland, Amsterdam, Donker Curtiusstraat

=cut

sub geocode_term {}

=head2 $self->get_record_by_source_identifier($source_identifier)

Return value: $DB_RECORD

 $self->get_record_by_source_identifier('nummeraanduiding-637372832372323')

=cut

sub get_record_by_source_identifier {
    my ($self, $source_identifier) = @_;

    die('Only call this method from a resultset object')
        unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');

    my ($type, $id)     = $source_identifier =~ /^(.+)\-(.+)$/;

    die "Need valid source_identifier (got: '$source_identifier')" unless $type && $id;
    die('Type: ' . $type . ' unsupported') unless SOURCE_TABLES()->{$type};

    return $self
        ->result_source
        ->schema->resultset(
            SOURCE_TABLES()->{$type}
        )->search(
            {
                identificatie   => $id
            }
        )->first;
}

=head2 $self->get_address_data_by_source_identifier($source_identifier)

Return value: \%ADDRESS_DATA

 print Dumper($self->get_address_data_by_source_identifier('nummeraanduiding-9876543218375842'));

 $VAR1 = {
           'huisletter' => 'A',
           'huisnummer' => 23,
           'huisnummertoevoeging' => '1rec',
           'postcode' => '1051JL',
           'straat' => 'Donker Curtiusstraat',
           'woonplaats' => 'Amsterdam'
         };
=cut

sub get_address_data_by_source_identifier {
    my ($self, $source_identifier) = @_;

    die('Only call this method from a resultset object')
        unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');

    my $record      = $self->get_record_by_source_identifier($source_identifier)
        or return;
    


    my $rv = {};
    while ($record) {
        my ($record_name) = ref($record) =~ /::([^:]+)$/;

        my $structure   = ESSENTIAL_DATA()->{ $record_name };

        for my $field (keys %{ $structure->{fields} }) {
            my $value           = $structure->{fields}->{ $field };

            # Skip (more specific) values that already exist in the return-data set.
            next if exists($rv->{$value}) && $rv->{$value};

            $rv->{ $value }     = $record->$field;
        }

        if (defined($structure->{parent}) && $structure->{parent}) {
            my $parent  = $structure->{parent}->{key};

            $record     = $record->$parent;
            next;
        }

        $record = undef;
    }

    return $rv;
}

=head2 $self->get_human_identifier_by_source_identifier($source_identifier [,\%options ])

Return value: \%ADDRESS_DATA

 print $self->get_human_identifier_by_source_identifier('nummeraanduiding-9876543218375842');

 # Returns: "Donker Curtiusstraat 23A-1rec"

Gives a human readable string of the given BAG source identifier, you can
influence the output by using one of the identifiers below

B<Options>

=over 4

=item prefix_with_city BOOLEAN

 print $self->get_human_identifier_by_source_identifier(
    'nummeraanduiding-9876543218375842'
    {
        prefix_with_city => 1,
    }
 );

 # Returns: "Amsterdam - Donker Curtiusstraat 23A-1rec"

Prefix the output with the city name

=back

=cut

sub get_human_identifier_by_source_identifier {
    my ($self, $source_identifier, $options) = @_;
    $options ||= {};

    die('Only call this method from a resultset object')
        unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');

    my $record      = $self->get_record_by_source_identifier($source_identifier)
        or return;

    my ($bagtype)   = $source_identifier =~ /^(\w+)-\d+/;

    if (lc($bagtype) eq 'nummeraanduiding') {
        return (
            (
                defined($options->{prefix_with_city}) &&
                $options->{prefix_with_city}
            )   ? $record->openbareruimte->woonplaats->naam . ' - '
                : ''
            ) .
            $record->openbareruimte->naam . ' ' .  $record->nummeraanduiding;
    }

    if (lc($bagtype) eq 'standplaats') {
        return '-';
    }

    if (lc($bagtype) eq 'ligplaats') {
        return '-';
    }

    if (lc($bagtype) eq 'openbareruimte') {
        return $record->naam .
            (
                $options->{prefix_with_city}
                ? ', ' . $record->woonplaats->naam
                : ''
            );
    }

    return '';
}

sub TO_JSON {
    my $self        = shift;

    if ($self->can('get_columns')) {
        return { $self->get_columns };
    }

    return [ $self->all ];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
