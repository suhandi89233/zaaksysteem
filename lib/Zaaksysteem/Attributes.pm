package Zaaksysteem::Attributes;
use warnings;
use strict;

use Zaaksysteem::Constants qw/
    BAG_TYPES
    RGBZ_LANDCODES
    ZAAKSYSTEEM_BETROKKENE_SUB
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_LEVERANCIER
    ZAAKSYSTEEM_LICENSE
    ZAAKSYSTEEM_NAAM
    ZAAKSYSTEEM_OPTIONS
    ZAAKSYSTEEM_STARTDATUM
/;

use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Types::MappedString;
use List::Util qw(any);
use BTTW::Tools qw(throw);

use Exporter 'import';
our @EXPORT     = qw/
    ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
    ZAAKSYSTEEM_MAGIC_STRINGS
/;


=head2 _make_array

Convenience routine.

When given a scalar, returns a list with one item.
When given a list, derefs and returns the list.

=cut

sub _make_array {
    my $thing = shift;

    return () unless $thing;
    return ref $thing eq 'ARRAY' ? @$thing : ($thing);
}

=head2 check_bwcompat

Purpose is to add a bwcompat => 'bla' entry to
a config hash, if given backwards compatible names.

=cut

sub _check_bwcompat {
    my ($bwcompat, $prefix) = @_;

    return () unless defined $bwcompat;
    return (bwcompat_name => [
        map { $prefix . $_ } _make_array($bwcompat)
    ]);
}

=head2 _get_betrokkene_attribute

If config has a systeemkenmerk_ref key, run it, otherwise defer to
the predefined sub in Constants.pm

=cut

sub _get_betrokkene_attribute {
    my ($betrokkene, $config, $historic) = @_;

    return $config->{systeemkenmerk_reference}->($betrokkene, $historic)
        if exists $config->{systeemkenmerk_reference};

    my $value = ZAAKSYSTEEM_BETROKKENE_SUB->(
        $betrokkene,
        $config->{internal_name},
        $historic
    );

    return unless defined $value;
    return $value unless $config->{post_processor};
    return $config->{post_processor}->($value);
}

BEGIN {
    my @person_fields = (
        {
            name          => 'subject_type',
            label         => 'Betrokkenetype',
            internal_name => 'btype',
        },
        {
            bwcompat      => 'aanhef',
            name          => 'salutation',
            label         => 'Aanhef',
            internal_name => 'aanhef',
        },
        {
            bwcompat      => 'aanhef1',
            label         => 'Aanhef 1',
            name          => 'salutation1',
            internal_name => 'aanhef1',
        },
        {
            bwcompat      => 'aanhef2',
            label         => 'Aanhef 2',
            name          => 'salutation2',
            internal_name => 'aanhef2',
        },
        {
            bwcompat      => 'afdeling',
            label         => 'Afdeling',
            name          => 'department',
            internal_name => 'afdeling',
        },
        {
            bwcompat      => 'achternaam',
            label         => 'Achternaam',
            name          => 'surname',
            internal_name => 'achternaam',
        },
        {
            bwcompat      => 'adellijke_titel',
            label         => 'Adellijke titel',
            name          => 'noble_title',
            internal_name => 'noble_title',
        },
        {
            bwcompat      => 'naamgebruik',
            label         => 'Naamgebruik',
            name          => 'naamgebruik', # pending a translation from Frontoffice department
            internal_name => 'naamgebruik'
        },
        {
            bwcompat      => 'burgerservicenummer',
            label         => 'Burgerservicenummer',
            name          => 'bsn',
            internal_name => 'burgerservicenummer',
            is_sensitive  => 1,
            post_processor => sub {
                my $value = shift;
                return unless int($value);
                return sprintf("%09d", $value);
            }
        },
        {
            bwcompat      => 'email',
            name          => 'email',
            label         => 'E-mailadres',
            internal_name => 'email',
        },
        {
             bwcompat      => 'geboortedatum',
             label         => 'Geboortedatum',
             name          => 'date_of_birth',
             internal_name => 'geboortedatum',
             is_sensitive  => 1,
             type          => 'datestamp',
             post_processor => sub {
                my $value = shift;
                # ZS-6321, ZS-16168, ZS-16195 - DoB bugfixes be here
                # Fundamentally, DateTime refuses to accept certain combinations of
                # timestamps in timezones because of zone jumps. If we keep
                # 'datestamps' truncated to their YMD-component and set the
                # timezone to 'floating', all valid dates should be accepted.
                $value->set_time_zone('floating');
                $value->truncate(to => 'day');
                return $value;
             }
        },
        {
            bwcompat      => 'geboorteplaats',
            label         => 'Geboorteplaats',
            name          => 'place_of_birth',
            internal_name => 'geboorteplaats',
        },
        {
            bwcompat      => 'geboorteland',
            label         => 'Geboorteland',
            name          => 'country_of_birth',
            internal_name => 'geboorteland',
        },
        {
            bwcompat      => 'geslacht',
            label         => 'Geslacht',
            name          => 'gender',
            internal_name => 'geslacht',
        },
        {
            bwcompat      => 'geslachtsnaam',
            label         => 'Geslachtsnaam',
            name          => 'family_name',
            internal_name => 'geslachtsnaam',
        },
        {
            bwcompat      => 'handelsnaam',
            label         => 'Handelsnaam',
            name          => 'trade_name',
            internal_name => 'handelsnaam',
        },
        {
            bwcompat      => ['huisnummer', 'nummeraanduiding'],
            name          => 'house_number',
            label         => 'Huisnummer',
            internal_name => 'huisnummer',
        },
        {
            bwcompat      => 'huwelijksdatum',
            name          => 'date_of_marriage',
            internal_name => 'datum_huwelijk',
            label         => 'Huwelijksdatum',
            type          => 'datestamp',
        },
        {
            bwcompat      => 'kvknummer',
            label         => 'KvK-nummer',
            name          => 'coc',
            internal_name => 'kvknummer',
        },
        {
            bwcompat      => 'mobiel',
            label         => 'Telefoonnummer (mobiel)',
            name          => 'mobile_number',
            internal_name => 'mobiel',
        },
        {
            bwcompat      => 'naam',
            label         => 'Naam',
            name          => 'name',
            internal_name => 'naam',
        },
        {
            bwcompat      => 'overlijdensdatum',
            label         => 'Overlijdensdatum',
            name          => 'date_of_death',
            internal_name => 'datum_overlijden',
            type          => 'datestamp',
        },
        {
            bwcompat      => 'postcode',
            label         => 'Postcode',
            name          => 'zipcode',
            internal_name => 'postcode',
            post_processor => sub {
                my $value = shift;
                # format with a space (1234 AB instead of 1234AB).
                # doing this in a lower level may impact
                # other processes
                $value =~ s/^(\d{4})(\w{2})$/$1 $2/;
                return $value;
            },
        },
        {
            bwcompat      => 'straat',
            label         => 'Straat',
            name          => 'street',
            internal_name => 'straat',
        },
        {
            bwcompat      => 'verblijf_straat',
            label         => 'Verblijfplaats straat',
            name          => 'residence_street',
            internal_name => 'verblijf_straat',
        },
        {
            bwcompat      => ['verblijf_huisnummer', 'verblijf_nummeraanduiding'],
            name          => 'residence_house_number',
            label         => 'Verblijfplaats huisnummer',
            internal_name => 'verblijf_huisnummer',
        },
        {
            bwcompat      => 'verblijf_postcode',
            name          => 'residence_zipcode',
            label         => 'Verblijfplaats postcode',
            internal_name => 'verblijf_postcode',
        },
        {
            bwcompat      => 'verblijf_woonplaats',
            name          => 'residence_place_of_residence',
            label         => 'Verblijfplaats woonplaats',
            internal_name => 'verblijf_woonplaats',
        },
        {
            bwcompat      => [ 'verblijf_land', 'vestiging_land' ],
            label         => 'Vestigingsland',
            name          => 'country_of_residence',
            internal_name => 'verblijf_land',
        },
        {
            bwcompat      => [ 'verblijf_buitenland1', 'verblijf_buitenland' ],
            label         => 'Adresregel 1 verblijf buitenland',
            name          => 'foreign_residence_address_line1',
            internal_name => 'verblijf_buitenland1',
        },
        {
            bwcompat      => 'verblijf_buitenland2',
            label         => 'Adresregel 2 verblijf buitenland',
            name          => 'foreign_residence_address_line2',
            internal_name => 'verblijf_buitenland2',
        },
        {
            bwcompat      => 'verblijf_buitenland3',
            label         => 'Adresregel 3 verblijf buitenland',
            name          => 'foreign_residence_address_line3',
            internal_name => 'verblijf_buitenland3',
        },
        {
            bwcompat      => 'correspondentie_straat',
            label         => 'Correspondentie straat',
            name          => 'correspondence_street',
            internal_name => 'correspondentie_straat',
        },
        {
            bwcompat      => ['correspondentie_huisnummer', 'correspondentie_nummeraanduiding'],
            name          => 'correspondence_house_number',
            label         => 'Correspondentie huisnummer',
            internal_name => 'correspondentie_huisnummer',
        },
        {
            bwcompat      => 'correspondentie_postcode',
            name          => 'correspondence_zipcode',
            label         => 'Correspondentie postcode',
            internal_name => 'correspondentie_postcode',
        },
        {
            bwcompat      => 'correspondentie_woonplaats',
            name          => 'correspondence_place_of_residence',
            label         => 'Correspondentie woonplaats',
            internal_name => 'correspondentie_woonplaats',
        },
        {
            bwcompat      => 'tel',
            label         => 'Telefoonnummer',
            name          => 'phone_number',
            internal_name => 'tel',
        },
        {
            bwcompat      => 'type',
            label         => 'Type',
            name          => 'type',
            internal_name => 'type',
        },
        {
            bwcompat      => 'volledigenaam',
            label         => 'Volledige naam',
            name          => 'full_name',
            internal_name => 'volledigenaam',
        },
        {
            bwcompat      => 'voornamen',
            label         => 'Voornamen',
            name          => 'first_names',
            internal_name => 'voornamen',
        },
        {
            bwcompat      => 'voorvoegsel',
            label         => 'Voorvoegsel',
            name          => 'surname_prefix',
            internal_name => 'voorvoegsel',
        },
        {
            bwcompat      => 'woonplaats',
            label         => 'Woonplaats',
            name          => 'place_of_residence',
            internal_name => 'woonplaats',
        },
        {
            bwcompat      => 'status',
            name          => 'status',
            label         => 'Status',
            systeemkenmerk_reference => sub {
                my $betrokkene = shift;
                my $historic   = shift;

                # Actief, Overleden, Verhuisd, (opgeheven?)
                my $deceased = ZAAKSYSTEEM_BETROKKENE_SUB->(
                    $betrokkene, 'datum_overlijden', $historic,
                );
                return 'Overleden' if $deceased;

                my $has_moved = ZAAKSYSTEEM_BETROKKENE_SUB->(
                    $betrokkene, 'is_verhuisd', $historic,
                );

                # overleden takes precedence.
                return 'Verhuisd' if $has_moved;
                return 'Actief';
            },
        },
        {
            bwcompat      => 'in_onderzoek',
            label         => 'In onderzoek',
            name          => 'investigation',
            post_processor => sub {
                my $value = shift;
                return $value ?  'In onderzoek' : 'Niet in onderzoek';
            }
        },
        {
            bwcompat      => 'datum_ontbinding_partnerschap',
            label         => 'Datum ontbinding partnerschap',
            name          => 'date_of_divorce',
            internal_name => 'datum_huwelijk_ontbinding',
            type          => 'datestamp',
        },
        {
            bwcompat      => 'rechtsvorm',
            label         => 'Rechtsvorm',
            name          => 'type_of_business_entity',
            internal_name => 'rechtsvorm'
        },
        {
            bwcompat      => 'vestigingsnummer',
            label         => 'Vestigingsnummer',
            name          => 'establishment_number',
            internal_name => 'vestigingsnummer',
        },
        {
            bwcompat      => 'heeft_briefadres',
            label         => 'Heeft briefadres',
            name          => 'has_correspondence_address',
            post_processor => sub {
                my $value = shift;
                return $value ? \1 : \0;
            }
        },
        {
            bwcompat      => 'indicatie_geheim',
            label         => 'Heeft indicatie "geheim"',
            name          => 'is_secret',
            post_processor => sub {
                my $value = shift;
                return $value ? \1 : \0;
            }
        },
        {
            bwcompat_name => 'initialen',
            label => 'Initialen',
            name => 'initials',
            internal_name => 'initialen'
        },
    );

    sub _get_aanvrager_attributes {
        my @attributes;

        my @aanvrager_fields = (
            @person_fields,
            {
                bwcompat      => 'anummer',
                label         => 'A Nummer',
                name          => 'a_number',
                internal_name => 'a_nummer',
                is_sensitive  => 1,
            },
            {
                bwcompat      => 'login',
                label         => 'Login',
                name          => 'login',
                internal_name => 'login',
            },
            {
                bwcompat      => 'password',
                label         => 'Wachtwoord',
                name          => 'password',
                internal_name => 'password',
            },
        );

        my $attr_type;
        for my $aanvrager_field (@aanvrager_fields) {
            $attr_type = delete $aanvrager_field->{type} || 'text';
            push @attributes, {
                %$aanvrager_field,
                name                     => "case.requestor.$aanvrager_field->{name}",
                attribute_type           => $attr_type,
                label                    => sprintf("Aanvrager %s", $aanvrager_field->{ label }),
                grouping                 => 'requestor',
                property_name            => $aanvrager_field->{ name },
                case_attribute           => 'aanvrager',
                systeemkenmerk_reference => sub {
                    my $case = shift;

                    return _get_betrokkene_attribute(
                        $case->aanvrager_object,
                        $aanvrager_field
                    );
                },
                _check_bwcompat($aanvrager_field->{bwcompat}, 'aanvrager_')
            }, {
                %$aanvrager_field,
                name                     => "case.requestor_snapshot.$aanvrager_field->{name}",
                attribute_type           => $attr_type,
                label                    => sprintf("Aanvrager %s", $aanvrager_field->{ label }),
                grouping                 => 'requestor_snapshot',
                property_name            => $aanvrager_field->{ name },
                case_attribute           => 'aanvrager',
                systeemkenmerk_reference => sub {
                    my $case = shift;

                    return _get_betrokkene_attribute(
                        $case->aanvrager_object,
                        $aanvrager_field,
                        1,
                    );
                },
            };
        }

        push @attributes, {
            name                     => 'case.requestor.id',
            label                    => 'Aanvragernummer',
            attribute_type           => 'text',
            grouping                 => 'requestor',
            case_attribute           => 'aanvrager',
            property_name            => 'old_subject_identifier',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $aanvrager = $zaak->aanvrager_object;

                # This happens when the aanvrager can't be found in LDAP
                return unless defined $aanvrager;
                return $aanvrager->betrokkene_identifier;
            },
        };

        push @attributes, {
            name                     => 'case.requestor.preset_client',
            label                    => 'Vooringevulde aanvrager',
            attribute_type           => 'text',
            internal_name            => 'preset_client',
            grouping                 => 'requestor',
            property_name            => 'preset_client',
            case_attribute           => 'preset_client',
            systeemkenmerk_reference => sub {
                my $case = shift;
                return $case->preset_client_dutch;
            },
        };

        push @attributes, {
            name                     => 'requestor',
            bwcompat_name            => 'aanvrager_id',
            label                    => 'Aanvrager ID',
            attribute_type           => 'text',
            case_attribute           => 'aanvrager',
            grouping                 => 'requestor',
            property_name            => 'uuid',
            systeemkenmerk_reference => sub {
                my $aanvrager = shift->aanvrager_object;

                return unless defined $aanvrager;
                return $aanvrager->uuid;
            },
        };

        # case.requestor.uuid is deprecated, but several api/v0-style calls
        # have hard dependencies on it's existance and value.
        # REMOVE-COND: zaakdossier is 100% v1
        push @attributes, {
            name                     => 'case.requestor.uuid',
            label                    => 'Aanvrager ID (oud)',
            attribute_type           => 'text',
            grouping                 => 'requestor',
            property_name            => 'uuid',
            systeemkenmerk_reference => sub {
                my $aanvrager = shift->aanvrager_object;

                return unless defined $aanvrager;
                return $aanvrager->uuid;
            },
        };
        # end deprecation warning

        push @attributes, {
            bwcompat_name            => 'aanvrager_functie',
            label                    => 'Aanvrager functie',
            name                     => 'case.requestor.title',
            attribute_type           => 'text',
            grouping                 => 'requestor',
            property_name            => 'function_title',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $aanvrager = $zaak->aanvrager_object;

                return unless $aanvrager;
                return unless $aanvrager->can('title');
                return $aanvrager->title;
            },
        };

        return @attributes;
    }

    sub _get_ontvanger_attributes {
        my @attributes;

        for my $ontvanger_field (@person_fields) {
            push @attributes, {

                name                     => "case.recipient.$ontvanger_field->{name}",
                attribute_type           => $ontvanger_field->{type} || 'text',
                label                    => $ontvanger_field->{ label },
                grouping                 => 'recipient',
                property_name            => $ontvanger_field->{ name },
                systeemkenmerk_reference => sub {
                    my $case = shift;

                    return _get_betrokkene_attribute(
                        $case->ontvanger_object,
                        $ontvanger_field,
                    );
                },
            }, {
                name                     => "case.recipient_snapshot.$ontvanger_field->{name}",
                attribute_type           => $ontvanger_field->{type} || 'text',
                label                    => $ontvanger_field->{ label },
                grouping                 => 'recipient_snapshot',
                property_name            => $ontvanger_field->{ name },
                systeemkenmerk_reference => sub {
                    my $case = shift;

                    return _get_betrokkene_attribute(
                        $case->ontvanger_object,
                        $ontvanger_field,
                        1
                    );
                },
            };
        }

        push @attributes, {
            name                     => 'case.recipient.display_name',
            label                    => 'Ontvanger',
            attribute_type           => 'text',
            grouping                 => 'recipient',
            property_name            => 'display_name',
            systeemkenmerk_reference => sub {
                my $case = shift;

                return _get_betrokkene_attribute($case->ontvanger_object, {
                    internal_name => 'display_name'
                });
            }
        }, {
            name                     => 'case.recipient_snapshot.display_name',
            label                    => 'Ontvanger',
            attribute_type           => 'text',
            grouping                 => 'recipient_snapshot',
            property_name            => 'display_name',
            systeemkenmerk_reference => sub {
                my $case = shift;

                return _get_betrokkene_attribute(
                    $case->ontvanger_object,
                    { internal_name => 'display_name' },
                    1
                );
            }
        };

        push @attributes, {
            name                     => 'case.recipient.id',
            label                    => 'Ontvangernummer',
            attribute_type           => 'text',
            grouping                 => 'recipient',
            property_name            => 'old_subject_identifier',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->ontvanger_object;
                return $zaak->ontvanger_object->betrokkene_identifier;
            },
        }, {
            name                     => 'case.recipient_snapshot.id',
            label                    => 'Ontvangernummer',
            attribute_type           => 'text',
            grouping                 => 'recipient_snapshot',
            property_name            => 'old_subject_identifier',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->ontvanger_object;
                return $zaak->ontvanger_object->betrokkene_identifier;
            },
        };

        return @attributes;
    }

    sub _get_behandelaar_attributes {

        my @behandelaar_attributes = (
            {
                bwcompat_name => 'voornaam',
                label => 'Voornaam',
                name => 'first_names',
                internal_name => 'voornamen'
            },
            {
                bwcompat_name => 'initialen',
                label => 'Initialen',
                name => 'initials',
                internal_name => 'voorletters'
            },
            {
                bwcompat_name => 'achternaam',
                label => 'Achternaam',
                name => 'last_name',
                internal_name => 'geslachtsnaam'
            },
        );

        my @attributes;
        for my $field (@behandelaar_attributes) {
            push @attributes, {
                bwcompat_name            => 'behandelaar_' . $field->{bwcompat_name},
                name                     => 'case.assignee.' . $field->{name},
                label                    => sprintf('Behandelaar %s', $field->{ label }),
                attribute_type           => 'text',
                grouping                 => 'assignee',
                case_attribute           => 'behandelaar',
                property_name            => $field->{ name },
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    my $method = $field->{internal_name};

                    return $zaak->behandelaar_object &&
                        $zaak->behandelaar_object->$method;
                },
            };
        }
        return @attributes;
    }

    sub _get_relationship_attributes {
        my @attributes;

        push @attributes, {
            name                     => 'case.related_uuids',
            label                    => 'Related objects',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            grouping                 => 'case',
            property_name            => 'related_uuids',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return join(',', @{ $zaak->relationships_by_object_uuid->{relations} });
            },
        };

        push @attributes, {
            name                     => 'case.child_uuids',
            label                    => 'Child objects',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            grouping                 => 'case',
            property_name            => 'child_uuids',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return join(',', @{ $zaak->relationships_by_object_uuid->{children} });
            },
        };

        push @attributes, {
            name                     => 'case.parent_uuid',
            label                    => 'Parent object',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            grouping                 => 'case',
            property_name            => 'parent_uuid',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return join(',', @{ $zaak->relationships_by_object_uuid->{parent} });
            },
        };

        return @attributes;

    }

    sub _get_zaaktype_attributes {
        my @attributes;

        my @properties = (
            {
                internal_name => 'aanleiding',
                name          => 'case.casetype.motivation',
                label         => 'Aanleiding'
            },
            {
                internal_name => 'archiefclassicatiecode', # [sic] ?
                name          => 'case.casetype.archive_classification_code',
                label         => 'Archiefclassificatiecode',
            },
            {
                internal_name => 'bag',
                name          => 'case.casetype.registration_bag',
                label         => 'Basisregistraties Adressen en Gebouwen'
            },
            {
                internal_name => 'beroep_mogelijk',
                bwcompat      => 'bezwaar_en_beroep_mogelijk',
                name          => 'case.casetype.objection_and_appeal',
                label         => 'Begroep mogelijk'
            },
            {
                internal_name => 'doel',
                name          => 'case.casetype.goal',
                label         => 'Doel'
            },
            {
                internal_name => 'e_formulier',
                name          => 'case.casetype.eform',
                label         => 'E-Formulier'
            },
            {
                internal_name => 'lex_silencio_positivo',
                name          => 'case.casetype.lex_silencio_positivo',
                label         => 'Lex Silencio Positivo'
            },
            {
                internal_name => 'lokale_grondslag',
                name          => 'case.casetype.principle_local',
                label         => 'Lokale grondslag'
            },
            {
                internal_name => 'opschorten_mogelijk',
                name          => 'case.casetype.suspension',
                label         => 'Opschorten mogelijk'
            },
            {
                internal_name => 'publicatie',
                name          => 'case.casetype.publication',
                label         => 'Publicatie'
            },
            {
                internal_name => 'publicatietekst',
                name          => 'case.casetype.text_for_publication',
                label         => 'Publicatietekst'
            },
            {
                internal_name => 'verantwoordelijke',
                name          => 'case.casetype.supervisor',
                label         => 'Verantwoordelijke'
            },
            {
                internal_name => 'verantwoordingsrelatie',
                name          => 'case.casetype.supervisor_relation',
                label         => 'Verantwoordingsrelatie'
            },
            {
                internal_name => 'verlenging_mogelijk',
                name          => 'case.casetype.extension',
                label         => 'Verlenging mogelijk'
            },
            {
                internal_name => 'verlengingstermijn',
                name          => 'case.casetype.extension_period',
                label         => 'Verlengingstermijn'
            },
            {
                internal_name => 'verdagingstermijn',
                name          => 'case.casetype.adjourn_period',
                label         => 'Verdagingstermijn'
            },
            {
                internal_name => 'vertrouwelijkheidsaanduiding',
                name          => 'case.casetype.designation_of_confidentiality',
                label         => 'Vertrouwelijkheidsaanduiding'
            },
            {
                internal_name => 'wet_dwangsom',
                name          => 'case.casetype.penalty',
                label         => 'Wet dwangsom'
            },
            {
                internal_name => 'wkpb',
                name          => 'case.casetype.wkpb',
                label         => 'WKPB'
            },
        );

        for my $property (@properties) {
            push @attributes, {
                bwcompat_name            => $property->{ bwcompat } // $property->{ internal_name },
                label                    => $property->{ label },
                name                     => $property->{ name },
                is_sensitive             => $property->{is_sensitive} // 0,
                attribute_type           => 'text',
                grouping                 => 'casetype',
                property_name            => ($property->{ name } =~ s/^case\.casetype\.//r),
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    return $zaak->zaaktype_property($property->{internal_name});
                }
            };
        }

        push @attributes, {
            label => 'Zaaktype referentie',
            name => 'case.casetype',
            attribute_type => 'text',
            grouping  => 'case',
            property_name => 'casetype_id',
            systeemkenmerk_reference => sub {
                return shift->zaaktype_id->_object->uuid;
            }
        };

        return @attributes;
    }

    sub _get_location_attributes {
        my @attributes;

        my %location_types = (
            locatie_zaak            => 'case_location',
            locatie_correspondentie => 'correspondence_location',
        );
        for my $location_type (keys %location_types) {
            my $prefix = $location_type eq 'locatie_zaak' ? 'Zaaklocatie' : 'Correspondentielocatie';

            for my $bag_type (@{ BAG_TYPES() }) {
                push @attributes, {
                    bwcompat_name            => lc(join("_", $prefix, $bag_type)),
                    name                     => "case.$location_types{$location_type}.${bag_type}",
                    attribute_type           => 'bag',
                    grouping                 => $location_types{ $location_type },
                    property_name            => $bag_type,
                    label                    => sprintf('%s %s', $prefix, ucfirst($bag_type)),
                    systeemkenmerk_reference => sub {
                        my $zaak = shift;
                        my $location = $zaak->$location_type;
                        return unless $location;

                        my $bag_object = $location->bag_object;
                        return unless $bag_object;

                        return $bag_object->to_attribute_value($bag_type);
                    },
                };
            }
        }

        return @attributes;
    }
}

sub _get_casetype_prices {
    my %casetype_price_fields = (
        tarief_balie       => 'case.casetype.price.counter',
        tarief_telefoon    => 'case.casetype.price.telephone',
        tarief_email       => 'case.casetype.price.email',
        tarief_behandelaar => 'case.casetype.price.employee',
        tarief_post        => 'case.casetype.price.post'
    );

    return map {
        my $key = $_;

        {
            bwcompat_name            => $key,
            name                     => $casetype_price_fields{$key},
            label                    => sprintf('Tarief (%s)', (split m[_], $key)[1]),
            attribute_type           => 'text',
            grouping                 => 'casetype',
            case_attributes          => 'zaaktype_node_id',
            property_name            => sprintf('price_%s', (split m/\./, $casetype_price_fields{ $key })[3]),
            systeemkenmerk_reference => sub {
                return shift->zaaktype_property('pdc_' . $key);
            }
        }
    } keys %casetype_price_fields;
}

sub _get_document_attributes {
    return ({
        bwcompat_name            => 'zaak_dossierdocumenten',
        name                     => 'case.case_documents',
        label                    => 'Zaakdossierdocumenten',
        attribute_type           => 'text',
        grouping                 => 'case',
        property_name            => 'case_documents',
        systeemkenmerk_reference => sub {
            my $zaak = shift;

            my @case_documents = $zaak->files->search({
                'me.date_deleted' => undef,
                'case_documents.case_document_id' => { -not => undef },
            }, {
                join => { case_documents => 'file_id' },
                order_by => 'me.name'
            })->active_files;

            return join ", ", map { sprintf "%s (%s)", $_->filename, $_->document_status } @case_documents;
        }
    },
    {
        bwcompat_name            => 'zaak_documenten',
        label                    => 'Zaakdocumenten',
        name                     => 'case.documents',
        grouping                 => 'case',
        property_name            => 'documents',
        attribute_type           => 'text',
        systeemkenmerk_reference => sub {
            my $zaak = shift;

            return join ", ", sort map { sprintf "%s (%s)", $_->filename, $_->document_status }
                grep { !$_->date_deleted } $zaak->active_files;
        }
    });
}

=head2 DYNAMIC_ATTRIBUTES

These are calculated on runtime because of time dependency.

=cut

use constant DYNAMIC_ATTRIBUTES => (
    {
        bwcompat_name  => ['dagen_resterend', 'dagen', 'days_left'],
        label          => 'Dagen resterend',
        name           => 'case.days_left',
        attribute_type => 'integer',
        dynamic_class  => 'DaysLeft',
    },
    {
        bwcompat_name  => ['voortgang', 'days_perc'],
        label          => 'Voortgang',
        name           => 'case.progress_days',
        attribute_type => 'integer',
        dynamic_class  => 'DaysPercentage',
    },
    {
        name           => 'case.destructable',
        label          => 'Vernietigbaar',
        attribute_type => 'integer',
        dynamic_class  => 'CaseDestructable',
    },
    {
        name           => 'case.destruction_blocked',
        label          => 'Vernietiging geblokkeerd',
        attribute_type => 'integer',
        dynamic_class  => 'CaseDestructionBlocked',
    },
    {
        name           => 'case.lead_time_real',
        attribute_type => 'text',
        dynamic_class  => 'DaysLeadTime',
        bwcompat_name  => 'zaak_doorlooptijd',
        label          => 'Zaakdoorlooptijd'
    },
);


use constant ZAAKSYSTEEM_SYSTEM_ATTRIBUTES => (
        {
            bwcompat_name            => 'zaaknummer',
            label                    => 'Zaaknummer',
            name                     => 'case.number',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'number',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->id;
            }
        },
        {
            bwcompat_name            => 'alternatief_zaaknummer',
            label                    => 'Alternatief zaaknummer',
            name                     => 'case.custom_number',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'alternative_case_number',
            case_attribute           => 'prefix',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if (defined $zaak->prefix && length($zaak->prefix)) {
                    return join('-', $zaak->prefix, $zaak->id);
                }
                return $zaak->id;
            }
        },
        {
            bwcompat_name            => 'startdatum',
            label                    => 'Startdatum',
            name                     => 'case.startdate',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'startdate',
            case_attribute           => 'registratiedatum',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum->dmy
                    if $zaak->registratiedatum;
            }
        },
        {
            bwcompat_name            => 'zaaktype',
            label                    => 'Zaaktype',
            name                     => 'case.casetype.name',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'name',
            case_attribute           => 'zaaktype_node_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->titel;
            }
        },
        {
            bwcompat_name            => 'zaak_fase',
            label                    => 'Zaak fase',
            name                     => 'case.phase',
            grouping                 => 'case',
            property_name            => 'phase',
            attribute_type           => 'text',
            case_attribute           => 'milestone',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if ($zaak->volgende_fase) {
                    return $zaak->volgende_fase->fase;
                }
                return;
            }
        },
        {
            bwcompat_name            => 'zaak_mijlpaal',
            label                    => 'Zaak mijlpaal',
            name                     => 'case.milestone',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'milestone',
            case_attribute           => 'milestone',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if ($zaak->volgende_fase) {
                    return $zaak->huidige_fase->naam;
                }
                return;
            }
        },
        {
            bwcompat_name            => 'uname',
            label                    => 'Systeeminformatie',
            name                     => 'system.uname',
            attribute_type           => 'text',
            grouping                 => 'system',
            property_name            => 'uname',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my $zaaksysteem_location    = $INC{'Zaaksysteem.pm'};

                return 'Zaaksysteem (unknown)' unless($zaaksysteem_location);

                my @file_information        = stat($zaaksysteem_location);

                my @uname   = (
                    ZAAKSYSTEEM_NAAM,
                    Zaaksysteem->config->{'ZS_VERSION'},
                    ZAAKSYSTEEM_STARTDATUM,
                    ZAAKSYSTEEM_LEVERANCIER,
                    ZAAKSYSTEEM_LICENSE,
                    'zaaksysteem.nl',
                );

                return join(', ', @uname);
            },
        },
        {
            bwcompat_name            => 'laatst_gewijzigd',
            label                    => 'Laatst gewijzigd',
            name                     => 'date_modified',
            attribute_type           => 'date',
            grouping                 => 'object',
            property_name            => 'date_modified',
        },
        {
            bwcompat_name            => 'datum_aangemaakt',
            label                    => 'Datum aangemaakt',
            name                     => 'date_created',
            attribute_type           => 'date',
            grouping                 => 'object',
            property_name            => 'date_created',
        },
        {
            bwcompat_name            => 'datum',
            label                    => 'Datum',
            name                     => 'system.current_date',
            attribute_type           => 'date',
            grouping                 => 'system',
            property_name            => 'current_date',
            systeemkenmerk_reference => sub {
                return DateTime->now();
            },
        },

        # This attribute is mainly for internal use, the end-user will not be
        # confronted with it, if all goes well. Context is that I needed to
        # use a filter that defined the actual text, instead of an attribute
        # So this unit always returns the empty string.
        {
            name                     => 'null',
            label                    => 'Lege waarde',
            attribute_type           => 'text',
            grouping                 => 'system',
            property_name            => 'null',
            systeemkenmerk_reference => sub { return undef }
        },
        {
            bwcompat_name            => 'zaaktype_versie',
            label                    => 'Zaaktypeversie',
            name                     => 'case.casetype.version',
            attribute_type           => 'integer',
            grouping                 => 'casetype',
            property_name            => 'version',
            case_attribute           => 'zaaktype_node_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->version;
            }
        },
        {
            bwcompat_name            => 'aggregatieniveau',
            label                    => 'Aggregatieniveau',
            name                     => 'case.aggregation_scope',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'aggregation_scope',
            systeemkenmerk_reference => sub { return 'Dossier'; }
        },
        {
            bwcompat_name            => 'zaaktype_versie_begindatum',
            label                    => 'Zaaktypeversie begindatum',
            name                     => 'case.casetype.version_date_of_creation',
            attribute_type           => 'date',
            grouping                 => 'casetype',
            case_attribute           => 'zaaktype_node_id',
            property_name            => 'version_date_of_creation',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->created;
            }
        },
        {
            bwcompat_name            => 'zaaktype_versie_einddatum',
            label                    => 'Zaaktypeversie einddatum',
            name                     => 'case.casetype.version_date_of_expiration',
            attribute_type           => 'date',
            grouping                 => 'casetype',
            case_attribute           => 'zaaktype_node_id',
            property_name            => 'version_date_of_expiration',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->deleted;
            }
        },
        {
            bwcompat_name            => 'registratiedatum',
            label                    => 'Registratiedatum',
            name                     => 'case.date_of_registration',
            case_attribute           => 'registratiedatum',
            attribute_type           => 'date',
            grouping                 => 'case',
            property_name            => 'date_of_registraiton',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum;
            }
        },
        {
            bwcompat_name            => 'registratiedatum_volledig',
            label                    => 'Registratiedatum (volledig)',
            name                     => 'case.date_of_registration_full',
            attribute_type           => 'date',
            format                   => '%d-%m-%Y %H:%M:%S',
            case_attribute           => 'registratiedatum',
            property_name            => 'date_of_registration_full',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum;
            }
        },
        {
            bwcompat_name            => 'afhandeldatum',
            label                    => 'Afhandeldatum',
            name                     => 'case.date_of_completion',
            attribute_type           => 'date',
            grouping                 => 'case',
            property_name            => 'date_of_completion',
            case_attribute           => 'afhandeldatum',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->afhandeldatum;
            }
        },
        {
            bwcompat_name            => 'afhandeldatum_volledig',
            label                    => 'Afhandeldatum (volledig)',
            name                     => 'case.date_of_completion_full',
            attribute_type           => 'date',
            case_attribute           => 'afhandeldatum',
            format                   => '%d-%m-%Y %H:%M:%S',
            property_name            => 'date_of_completion_full',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->afhandeldatum;
            },
        },
        {
            bwcompat_name            => 'afdeling',
            label                    => 'Afdeling',
            name                     => 'case.casetype.department',
            attribute_type           => 'text',
            grouping                 => 'case',
            case_attribute           => 'route_ou',
            property_name            => 'department',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my $ou_object = $zaak->ou_object;
                return $ou_object->name if $ou_object;
            }
        },
        {
            bwcompat_name            => 'uiterste_vernietigingsdatum',
            label                    => 'Uiterste vernietigingsdatum',
            name                     => 'case.date_destruction',
            attribute_type           => 'date',
            grouping                 => 'case',
            case_attribute           => 'vernietigingsdatum',
            property_name            => 'date_of_destruction',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->vernietigingsdatum;
            },
        },
        {
            bwcompat_name            => 'alle_relaties',
            label                    => 'Alle relaties',
            name                     => 'case.relations',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'related_case_numbers',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my @numbers;

                my $n_relations = $zaak->relationships_by_case_number;
                push(@numbers, @{ $n_relations->{$_} }) for keys %$n_relations;

                return join ", ", @numbers;
            },
        },
        {
            bwcompat_name            => 'zaak_relaties',
            label                    => 'Zaakrelaties',
            name                     => 'case.related_cases',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'linked_case_numbers',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return join ", ", @{ $zaak->relationships_by_case_number->{relations} };
            },
        },
        {
            bwcompat_name            => 'archiefnominatie',
            label                    => 'Archiefnominatie (waardering)',
            name                     => 'case.type_of_archiving',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'type_of_archiving',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->archiefnominatie;
                }
            },
        },
        {
            bwcompat_name            => 'zaak_brondatum_archiefprocedure',
            label                    => 'Brondatum archiefprocedure',
            name                     => 'case.retention_period_source_date',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'retention_period_source_date',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->ingang;
                }
            },
        },

        {
            label => 'Urgentieniveau',
            name => 'case.urgency',
            attribute_type => 'text',
            grouping => 'case',
            property_name => 'urgency',
            systeemkenmerk_reference => sub {
                return shift->urgency
            }
        },
        {
            bwcompat_name            => ['zaak_bedrag', 'bedrag_web', 'pdc_tarief'],
            label                    => 'Zaakbedrag',
            name                     => 'case.price',
            attribute_type           => 'text', # format_payment_amount will return '-' when no value present
            grouping                 => 'payment',
            property_name            => 'price',
            case_attribute           => 'payment_amount',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->format_payment_amount();
            },
        },
        {
            bwcompat_name            => 'betaalstatus',
            label                    => 'Betaalstatus',
            name                     => 'case.payment_status',
            attribute_type           => 'text',
            grouping                 => 'payment',
            property_name            => 'payment_status',
            case_attribute           => 'payment_status',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $ps = $zaak->payment_status;
                return unless $ps;

                return Zaaksysteem::Types::MappedString->new(
                    original => $ps,
                    mapped   => ZAAKSYSTEEM_CONSTANTS->{payment_statuses}->{$ps} || $ps,
                );
            },
        },
        {
            name                     => 'case.status',
            bwcompat_name            => 'status',
            label                    => 'Status',
            attribute_type           => 'text',
            property_name            => 'status',
            grouping                 => 'status',
            case_attribute           => 'status',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->status();
            },
        },
        {
            name                     => 'case.archival_state',
            bwcompat_name            => 'archiefstatus',
            label                    => 'Archiefstatus',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'archival_state',
            case_attribute           => 'archival_state',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->archival_state();
            },
        },
        {
            bwcompat_name            => 'statusnummer',
            label                    => 'Statusnummer',
            name                     => 'case.number_status',
            attribute_type           => 'integer',
            grouping                 => 'status',
            property_name            => 'milestone',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->milestone();
            },
        },
        {
            bwcompat_name            => 'streefafhandeldatum',
            label                    => 'Streefafhandeldatum',
            name                     => 'case.date_target',
            attribute_type           => 'timestamp_or_text',
            grouping                 => 'status',
            property_name            => 'date_target',
            case_attribute           => 'streefafhandeldatum',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                # XXX 2 returntypes (timestamp en string)
                if ($zaak->status eq 'stalled') {
                    return 'Opgeschort';
                }
                return $zaak->streefafhandeldatum;
            },
        },
        {
            bwcompat_name            => 'bewaartermijn',
            label                    => 'Bewaartermijn',
            name                     => 'case.period_of_preservation',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'preservation_period',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    my $bewaartermijn = $zaaktype_resultaat->bewaartermijn;
                    return ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$bewaartermijn};
                }
            },
        },
        {
            bwcompat_name            => 'bewaartermijn_geactiveerd',
            label                    => 'Bewaartermijn geactiveerd',
            name                     => 'case.period_of_preservation_active',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'preservation_period_active',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->trigger_archival ? "Ja" : "Nee";
                }
            },
        },
        {
            bwcompat_name            => 'contactkanaal',
            label                    => 'Contactkanaal',
            name                     => 'case.channel_of_contact',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'contact_channel',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->contactkanaal;
            },
        },
        {
            bwcompat_name            => 'pid',
            label                    => 'PID',
            name                     => 'case.number_parent',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'number_parent',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('pid');
            },
        },
        {
            bwcompat_name            => 'vervolg_van',
            label                    => 'Vervolg van', # oorsprongzaak?
            name                     => 'case.number_previous',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'number_previous',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('vervolg_van');
            },
        },
        {
            bwcompat_name            => 'voortgang_status',
            label                    => 'Voortgang status',
            name                     => 'case.progress_status',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'progress_status',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->status_perc;
            },
        },
        {
            bwcompat_name            => 'zaaknummer_hoofdzaak',
            label                    => 'Zaaknummer hoofdzaak',
            name                     => 'case.number_master',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'number_root',
            case_attribute           => 'pid',
            systeemkenmerk_reference => sub {
                return shift->master_number;
            },
        },
        {
            bwcompat_name            => 'relates_to', # doesn't sound bw-compat-ish
            label                    => 'Gerelateerd aan',
            name                     => 'case.number_relations',
            attribute_type           => 'text',
            case_attribute           => 'relates_to',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('relates_to');
            },
        },
        {
            bwcompat_name            => 'resultaat',
            label                    => 'Resultaat',
            name                     => 'case.result',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'name_generic',
            case_attribute           => 'result_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->resultaat;
            },
        },
        {
            bwcompat_name            => 'resultaat_id',
            label                    => 'Resultaat ID',
            name                     => 'case.result_id',
            attribute_type           => 'integer',
            grouping                 => 'result',
            property_name            => 'result_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->id;
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_omschrijving',
            label                    => 'Resultaatomschrijving',
            name                     => 'case.result_description',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'description',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->label;
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_toelichting',
            label                    => 'Resultaattoelichting',
            name                     => 'case.result_explanation',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'comment',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->comments;
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_selectielijst_nummer',
            label                    => 'Selectielijst-nummer',
            name                     => 'case.result_selection_list_number',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'selection_list_number',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{selectielijst_nummer};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestype_nummer',
            label                    => 'Procestype-nummer',
            name                     => 'case.result_process_type_number',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'result_process_type_number',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestype_nummer};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestype_naam',
            label                    => 'Procestype-naam',
            name                     => 'case.result_process_type_name',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'process_type_name',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestype_naam};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestype_omschrijving',
            label                    => 'Procestype-omschrijving',
            name                     => 'case.result_process_type_description',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'process_type_description',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestype_omschrijving};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestype_toelichting',
            label                    => 'Procestype-toelichting',
            name                     => 'case.result_process_type_explanation',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'process_type_comment',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestype_toelichting};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestype_generiek',
            label                    => 'Procestype-generiek',
            name                     => 'case.result_process_type_generic',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'process_type_generic',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestype_generiek};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestype_object',
            label                    => 'Procestype-object',
            name                     => 'case.result_process_type_object',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'process_type_object',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestype_object};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_herkomst',
            label                    => 'Resultaat-herkomst',
            name                     => 'case.result_origin',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'origin',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{herkomst};
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_procestermijn',
            label                    => 'Resultaat-procestermijn',
            name                     => 'case.result_process_term',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'process_term',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->properties->{procestermijn};
                }
            },
        },
        {
            bwcompat_name            => 'deelzaken_afgehandeld',
            label                    => 'Deelzaken afgehandeld',
            name                     => 'case.relations_complete',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'children_resolved',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return $zaak->zaak_children->search({
                    'me.status' => { -not_in => ['resolved'] },
                })->count ? 'Nee' : 'Ja';
            },
        },
        {
            bwcompat_name            => 'doorlooptijd_wettelijk',
            label                    => 'Doorlooptijd (wettelijk)',
            name                     => 'case.lead_time_legal',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'lead_time_legal',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->afhandeltermijn;
            },
        },
        {
            bwcompat_name            => 'doorlooptijd_service',
            label                    => 'Doorlooptijd (service)',
            name                     => 'case.lead_time_service',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'lead_time_service',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->servicenorm;
            },
        },

        {
            bwcompat_name            => 'trefwoorden',
            label                    => 'Trefwoorden',
            name                     => 'case.casetype.keywords',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'keywords',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_trefwoorden;
            },
        },
        {
            bwcompat_name            => 'trigger',
            label                    => 'Trigger', # onduidelijk
            name                     => 'case.casetype.initiator_source',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'initiator_source',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->trigger;
            },
        },

        {
            bwcompat_name            => 'gebruiker_naam',
            label                    => 'Gebruiker naam',
            name                     => 'user.name',
            attribute_type           => 'text',
            property_name            => 'name',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if ($zaak->result_source->resultset->current_user) {
                    return $zaak->result_source->resultset->current_user->naam;
                }
                return;
            },
        },

        {
            bwcompat_name            => 'generieke_categorie',
            label                    => 'Categorie (generiek)',
            name                     => 'case.casetype.generic_category',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'generic_category',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return $zaak->zaaktype_id->bibliotheek_categorie_id->naam
                    if $zaak->zaaktype_id->bibliotheek_categorie_id;
            },
        },
        {
            bwcompat_name            => 'handelingsinitiator',
            label                    => 'Handelingsinitiator',
            name                     => 'case.casetype.initiator_type',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'initiator_type',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->handelingsinitiator;
            },
        },
        {
            bwcompat_name            => 'zaaktype_id',
            label                    => 'Zaaktypenummer',
            name                     => 'case.casetype.id',
            attribute_type           => 'integer',
            grouping                 => 'casetype',
            property_name            => 'id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('zaaktype_id');
            },
        },
        {
            name                     => 'case.casetype.node.id',
            attribute_type           => 'integer',
            label                    => 'Zaaktypenodenummer',
            grouping                 => 'case',
            property_name            => 'node_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('zaaktype_node_id');
            },
        },
        {
            bwcompat_name            => 'identificatie',
            label                    => 'Identificatie',
            name                     => 'case.casetype.identification',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'identification',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->code;
            },
        },
        {
            bwcompat_name            => 'omschrijving_of_toelichting',
            label                    => 'Omschrijving of toelichting',
            name                     => 'case.casetype.description',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'description',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_omschrijving;
            },
        },
        {
            bwcompat_name            => 'openbaarheid',
            label                    => 'Openbaarheid',
            name                     => 'case.casetype.publicity',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'publicity',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->openbaarheid;
            },
        },
        {
            bwcompat_name            => 'published',
            label                    => 'Gepubliceerd',
            name                     => 'case.published',
            attribute_type           => 'text',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->published;
            },
        },

        {
            bwcompat_name            => 'opgeschort_tot',
            label                    => 'Opgeschort tot',
            name                     => 'case.stalled_until',
            attribute_type           => 'date',
            grouping                 => 'status',
            property_name            => 'date_stalled_until',
            case_attribute           => 'date_stalled_until',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->stalled_until;
            },
        },
        {
            bwcompat_name            => 'opgeschort_sinds',
            label                    => 'Opgeschort sinds',
            name                     => 'case.stalled_since',
            attribute_type           => 'date',
            grouping                 => 'status',
            property_name            => 'date_stalled_since',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->stalled_since;
            },
        },
        {
            bwcompat_name            => 'reden_opschorting',
            label                    => 'Reden opschorting',
            name                     => 'case.suspension_rationale',
            attribute_type           => 'text',
            grouping                 => 'case',
            grouping                 => 'status',
            property_name            => 'suspension_rationale',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return unless $zaak->status eq 'stalled';

                my $event = $zaak->logging->search_rs(
                    { event_type => 'case/suspend' },
                    {
                        order_by => { -desc => 'id' },
                        rows => 1
                    }
                )->next;

                return unless defined $event;

                return $event->data->{ reason };
            }
        },
        {
            bwcompat_name            => 'reden_vroegtijdig_afhandelen',
            label                    => 'Reden vroegtijdig afhandelen',
            name                     => 'case.premature_completion_rationale',
            attribute_type           => 'text',
            grouping                 => 'status',
            property_name            => 'premature_completion_rationale',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return unless $zaak->is_afgehandeld;

                my $event = $zaak->logging->search_rs(
                    { event_type => 'case/early_settle' },
                    {
                        order_by => { -desc => 'id' },
                        rows => 1
                    }
                )->next;

                return unless defined $event;

                return $event->data->{ reason };
            }
        },
        {
            bwcompat_name            => 'procesbeschrijving',
            label                    => 'Procesbeschrijving',
            name                     => 'case.casetype.process_description',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'process_description',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->procesbeschrijving;
            },
        },
        {
            bwcompat_name            => 'selectielijst',
            label                    => 'Selectielijst',
            name                     => 'case.selection_list',
            grouping                 => 'result',
            attribute_type           => 'text',
            property_name            => 'selection_list',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $resultaat = $zaak->zaaktype_resultaat;

                return unless $resultaat;

                return $resultaat->selectielijst;
            },
        },
        {
            bwcompat_name            => 'actieve_selectielijst',
            label                    => 'Actieve selectielijst',
            name                     => 'case.active_selection_list',
            attribute_type           => 'text',
            grouping                 => 'result',
            property_name            => 'selection_list',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $resultaat = $zaak->zaaktype_resultaat;

                return unless $resultaat;

                return $resultaat->selectielijst;
            }
        },
        {
            bwcompat_name            => 'wettelijke_grondslag',
            label                    => 'Wettelijke grondslag',
            name                     => 'case.principle_national',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'legal_basis',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->grondslag;
            },
        },
        {
            bwcompat_name            => 'sjabloon_aanmaakdatum',
            label                    => 'Aanmaakdatum sjabloon',
            name                     => 'case.date_current',
            attribute_type           => 'date',
            systeemkenmerk_reference => sub {
                return DateTime->now();
            },
        },
        {
            bwcompat_name            => 'vertrouwelijkheid',
            label                    => 'Vertrouwelijkheid',
            name                     => 'case.confidentiality',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'confidentiality',
            case_attribute           => 'confidentiality',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $setting = $zaak->confidentiality;

                return Zaaksysteem::Types::MappedString->new(
                    original => $setting,
                    mapped   => ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$setting} || $setting,
                );
            },
        },

        # "Behandelaar"
        {
            name                     => 'assignee',
            bwcompat_name            => 'behandelaar_id',
            label                    => 'Behandelaar ID',
            attribute_type           => 'text',
            grouping                 => 'assignee',
            property_name            => 'uuid',
            case_attribute           => 'behandelaar',
            systeemkenmerk_reference => sub {
                my $behandelaar = shift->behandelaar_object;

                return unless defined $behandelaar;
                return $behandelaar->uuid;
            },
        },
        {
            bwcompat_name            => 'behandelaar',
            label                    => 'Behandelaar',
            name                     => 'case.assignee',
            attribute_type           => 'text',
            grouping                 => 'assignee',
            property_name            => 'name',
            case_attribute           => 'behandelaar',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->naam;
            },
        },
        {
            bwcompat_name            => 'behandelaar_afdeling',
            label                    => 'Behandelaar afdeling',
            name                     => 'case.assignee.department',
            attribute_type           => 'text',
            grouping                 => 'assignee',
            property_name            => 'department',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;

                return $zaak->behandelaar_object->org_eenheid->name
                    if (
                        $zaak->behandelaar_object->btype eq 'medewerker' &&
                        $zaak->behandelaar_object->org_eenheid
                    );

                return '';
            },
        },
        {
            bwcompat_name            => 'behandelaar_email',
            label                    => 'Behandelaar e-mail',
            name                     => 'case.assignee.email',
            attribute_type           => 'text',
            grouping                 => 'assignee',
            property_name            => 'email',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->email;
            },
        },
        {
            bwcompat_name            => 'behandelaar_tel',
            label                    => 'Behandelaar telefoonnummer',
            name                     => 'case.assignee.phone_number',
            attribute_type           => 'text',
            grouping                 => 'assignee',
            property_name            => 'phone_number',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->telefoonnummer;
            },
        },
        {
            bwcompat_name            => 'behandelaar_functie',
            label                    => 'Behandelaar functie',
            name                     => 'case.assignee.title',
            attribute_type           => 'text',
            grouping                 => 'assignee',
            property_name            => 'function_title',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->title;
            },
        },
        {
            bwcompat_name            => 'behandelaar_nummer',
            label                    => 'Behandelaarnummer',
            name                     => 'case.assignee.id',
            attribute_type           => 'integer',
            grouping                 => 'assignee',
            property_name            => 'id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->ex_id;
            },
        },
        {
            bwcompat_name            => 'behandelaar_handtekening',
            label                    => 'Behandelaar handtekening',
            name                     => 'case.assignee.signature',
            attribute_type           => 'text',
            systeemkenmerk_reference => sub {
                my  $zaak   = shift;
                return unless $zaak->behandelaar_object;

                my $schema  = $zaak->result_source->schema;
                my $rs      = $schema->resultset('Subject');
                my $subject = $rs->find($zaak->behandelaar_object->id);

                my $current_id = $subject->settings->{signature_filestore_id};

                return '' unless $current_id;

                return sub {
                    return $schema->resultset('Filestore')->find($current_id)->get_path;
                };
            },
        },

        # "Coordinator"
        {
            name                     => 'coordinator',
            bwcompat_name            => 'coordinator_id',
            label                    => 'Coordinator ID',
            attribute_type           => 'text',
            grouping                 => 'coordinator',
            property_name            => 'uuid',
            case_attribute           => 'coordinator',
            systeemkenmerk_reference => sub {
                my $coordinator = shift->coordinator_object;

                return unless defined $coordinator;
                return $coordinator->uuid;
            },
        },
        {
            bwcompat_name            => 'coordinator',
            label                    => 'Coordinator',
            name                     => 'case.coordinator',
            attribute_type           => 'text',
            grouping                 => 'coordinator',
            property_name            => 'name',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->naam;
            },
        },
        {
            bwcompat_name            => 'coordinator_email',
            label                    => 'Coordinator e-mail',
            name                     => 'case.coordinator.email',
            attribute_type           => 'text',
            grouping                 => 'coordinator',
            property_name            => 'email',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->email;
            },
        },
        {
            bwcompat_name            => 'coordinator_tel',
            label                    => 'Coordinator telefoonummer',
            name                     => 'case.coordinator.phone_number',
            attribute_type           => 'text',
            grouping                 => 'coordinator',
            property_name            => 'phone_number',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->telefoonnummer;
            },
        },
        {
            bwcompat_name            => 'coordinator_functie',
            label                    => 'Coordinator functie',
            name                     => 'case.coordinator.title',
            attribute_type           => 'text',
            grouping                 => 'coordinator',
            property_name            => 'function_title',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->title;
            },
        },
        {
            bwcompat_name            => 'coordinator_nummer',
            label                    => 'Coordinatornummer',
            name                     => 'case.coordinator.id',
            attribute_type           => 'integer',
            grouping                 => 'coordinator',
            property_name            => 'id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->ex_id;
            },
        },

        # Internal
        {
            name                     => 'case.route_ou',
            label                    => 'Intaketoewijzing (groep)',
            attribute_type           => 'text',
            grouping                 => 'route',
            case_attribute           => 'route_ou',
            property_name            => 'group_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->route_ou;
            },
        },
        {
            name                     => 'case.route_role',
            label                    => 'Intaketoewijzing (rol)',
            attribute_type           => 'text',
            grouping                 => 'route',
            case_attribute           => 'route_role',
            property_name            => 'role_id',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->route_role;
            },
        },
        {
            name                     => 'case.num_unaccepted_updates',
            label                    => 'Ongeaccepteerde PIP updates',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'unaccepted_update_count',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->unaccepted_pip_updates;
            },
        },
        {
            name                     => 'case.num_unaccepted_files',
            label                    => 'Ongeaccepteerde bestanden',
            attribute_type           => 'integer',
            grouping                 => 'case',
            property_name            => 'unaccepted_document_count',
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                # Force integerness
                return 0 + $zaak->files->search->unaccepted;
            },
        },
        {
            name                     => 'case.subject',
            bwcompat_name            => 'zaak_onderwerp',
            label                    => 'Zaakonderwerp',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'subject',
            case_attribute           => 'onderwerp',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->onderwerp;
            },
        },
        {
            name                     => 'case.subject_external',
            bwcompat_name            => 'zaak_onderwerp_extern',
            label                    => 'Zaakonderwerp Extern',
            attribute_type           => 'text',
            grouping                 => 'case',
            property_name            => 'subject_external',
            case_attribute           => 'onderwerp_extern',
            systeemkenmerk_reference => sub {
                return shift->onderwerp_extern;
            }
        },

        _get_aanvrager_attributes(),
        _get_ontvanger_attributes(),
        _get_zaaktype_attributes(),
        _get_location_attributes(),
        _get_behandelaar_attributes,

        {
            name                     => 'case.casetype.price.web',
            bwcompat_name            => 'tarief_web',
            label                    => 'Tarief (web)',
            attribute_type           => 'text',
            grouping                 => 'casetype',
            property_name            => 'price_web',
            systeemkenmerk_reference => sub {
                return shift->zaaktype_node_id->zaaktype_definitie_id->pdc_tarief;
            },
        },

        _get_casetype_prices,
        _get_document_attributes,
        _get_relationship_attributes,
        DYNAMIC_ATTRIBUTES
);

=head2 predefined_case_attributes

Creates instances of all predefined case attributes.

=cut

sub predefined_case_attributes {
    my $self = shift;

    return map {
        Zaaksysteem::Object::Attribute->new(%$_)
    } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
}

=head2 find_predefined_case_attribute

Find a predefined case attribute by name. The name can be either the "new"
style or in the old style: C<case.price> vs C<zaak_bedrag>.

=cut

sub find_predefined_case_attribute {
    my ($self, $name) = @_;

    foreach (ZAAKSYSTEEM_SYSTEM_ATTRIBUTES) {
        my $ref = ref $_->{bwcompat_name};
        if (   $_->{name} eq $name
            or $ref eq '' && ($_->{bwcompat_name} // '') eq $name)
        {
            return Zaaksysteem::Object::Attribute->new(%$_);
        }
        elsif ($ref eq 'ARRAY' && any { $_ eq $name } @{ $_->{bwcompat_name} })
        {
            return Zaaksysteem::Object::Attribute->new(%$_);
        }
    }
    return;
}

=head2 get_predefined_case_attribute

Get a predefined case attribute by name. Dies when none is found.
See L<Zaaksysteem::Attributes/find_predefined_case_attribute> for more information.

=cut

sub get_predefined_case_attribute {
    my ($self, $name) = @_;

    my $found = $self->find_predefined_case_attribute($name);
    return $found if $found;

    throw('case/system_attributes/not_found',
        "Unable to find system attribute by name '$name'");
}

=head2 find_predefined_case_attribute_grouping

Find a predefined case attribute by grouping. This is usefull when wanting to
update a single attribute of a group where you need to accumulate all the
entries of said group, for example C<case.result>.

=cut

sub find_predefined_case_attribute_grouping {
    my ($self, $name) = @_;

    my @found = map { Zaaksysteem::Object::Attribute->new(%$_) }
        grep { ($_->{grouping} // '') eq $name } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES;
    return \@found if @found;
    return;
}


=head2 get_predefined_case_attribute_grouping

Get a predefined case attribute by grouping. Dies when none is found.
See L<Zaaksysteem::Attributes/get_predefined_case_attribute_grouping> for more
information.

=cut

sub get_predefined_case_attribute_grouping {
    my ($self, $name) = @_;

    my $found = $self->find_predefined_case_attribute_grouping($name);
    return $found if $found;

    throw('case/system_attributes/grouping/not_found',
        "Unable to find system attribute by grouping '$name'");
}

=head2 get_from_case

Get attributes that are use the case table

=cut

sub get_from_case {
    my ($self, @needle) = @_;

    my %grouping;
    my @found;
    foreach my $name (@needle) {
        my @things = grep { ($_->{case_attribute} // '') eq $name } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES;
        foreach (@things) {
            if (($_->{grouping} // 'case') eq 'case') {
                push(@found, Zaaksysteem::Object::Attribute->new(%$_));
            }
            else {
                next if $grouping{$_->{grouping}};
                $grouping{$_->{grouping}} = 1;
                my $found = $self->find_predefined_case_attribute_grouping($_->{grouping});
                push(@found, @$found) if $found;
            }
        }
    }

    return @found;
}

=head2 get_bwcompat_name

accessor function that always returns a list (or scalar that will shove
snugly into a list if offered)

my @aliases = $attribute->get_bwcompat_name;

=cut

sub _get_bwcompat_name {
    my $bwcompat_name = shift;

    return unless $bwcompat_name;
    return ref $bwcompat_name ? @$bwcompat_name : $bwcompat_name;
}

=head2 ZAAKSYSTEEM_MAGIC_STRINGS

This is the predefined list with attributes that are defined by Zaaksysteem,
like reserved keywords. These are not available for custom use, but always
available.

=cut

use constant ZAAKSYSTEEM_MAGIC_STRINGS => (
    map { _get_bwcompat_name($_->{bwcompat_name}) } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
