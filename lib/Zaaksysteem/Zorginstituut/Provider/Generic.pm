package Zaaksysteem::Zorginstituut::Provider::Generic;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Zorginstituut::Provider';
with 'Zaaksysteem::Zorginstituut::Roles::Provider';
use BTTW::Tools::UA;

use Zaaksysteem::Tools::SysinModules qw(:certificates);

=head1 NAME

Zaaksysteem::Zorginstituut::Providers::Generic - A generic provider with client certificate support

=head1 DESCRIPTION

Provider module to send messages to a generic provider
Extends L<Zaaksysteem::Zorginstituut::Provider> and consumes L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head1 SYNOPSIS

    use Zaaksysteem::Zorginstituut::Provider::Generic;

    my $provider = Zaaksysteem::Zorginstituut::Provider::Generic->new(
        di01_endpoint      => 'http:://opperschaap.net',
        # The following options are optional
        client_public_key  => '',
        client_private_key => '',
        ca_certificate     => '',
    );

    # Internal calls for the provider calls:
    my $req = $provider->build_request(
        endpoint      => $provider->di01_endpoint
        'soap-action' => 'washme',
        xml           => 'foo',
    );

    my $res = $provider->send_request($req);

=head1 METHODS

=head2 shortname

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head2 label

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head2 configuration_items

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=cut

sub shortname { "generic" }
sub label     { "Generieke aanbieder" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_generic_di01_endpoint',
        label       => 'Di01 endpoint',
        description => 'De URI voor Di01 (heen) berichttypes',
        type        => 'text',
        required    => 1,
        data        => { pattern => '^https:\/\/.+' },
    ),
    ca_certificate(name => 'generic_ca_certificate'),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_generic_use_client_certificates',
        label       => 'Gebruik client-certificaten',
        type        => 'checkbox',
        required    => 0,
        default     => 1,
    ),
    client_certificate(
        name     => 'generic_client_public_key',
        required => 1,
        label    => 'Publieke deel van het inkomende client-certificaat',
        when     => 'interface_generic_use_client_certificates === true',
    ),
    client_private_key(
        name     => 'generic_client_private_key',
        required => 1,
        label    => 'Uitgaand client certificaat',
        when     => 'interface_generic_use_client_certificates === true',
    ),
);

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

