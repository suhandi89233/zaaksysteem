package Zaaksysteem::Zorginstituut::Reader;
use Moose;
extends 'Zaaksysteem::XML::Reader';
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zorginstituut::Reader - A reader for validating WMO302 messages

=head1 DESCRIPTION

Reads and validates 302 messages for both WMO and JW XML messages.

=head1 SYNOPSIS

    use Zaaksysteem::Zorginstituut::Reader;
    my $reader = Zaaksysteem::Zorginstituut::Reader->new(
        base_namespace => 'https://foo',
        msg_namespace  => 'https://foo',
        code           => 123,
        xml            => '<xml/>',
    );

    if ($reader->valid) {
        # The message is correct
    }
    else {
        use Data::Dumper;
        die message contains error . Dumper $reader->errors();
    }

=cut

use BTTW::Tools;

=head1 ATTRIBUTES

=head2 base_namespace

The namespace of the basisschema

=cut

has base_namespace => (
    is       => 'ro',
    isa      => 'Str',
    required => 0,
);

=head2 msg_namespace

The namespace of the message

=cut

has msg_namespace => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 code

The code of the message that you are about to read/validate

=cut

has code => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 errors

Returns an HashRef with information about which section in the XML
contains an error

=cut

has errors => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { return {} },
);

=head2 valid

Tells you if the message is valid or not

=cut

has valid => (
    is      => 'rw',
    isa     => 'Bool',
    lazy    => 1,
    builder => '_is_valid',
);

=head2 version

The WMO/JW version number, defauls to 2.1

=cut

has version => (
    is      => 'ro',
    isa     => 'Str',
    default => "2.1",
);

=head2 client_base

The base of the msg:Client tree

=cut

has client_base => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    default => sub {
        my $self = shift;
        if ($self->version eq '2.1') {
            return '/msg:Bericht/msg:Clienten/msg:Client';
        }
        if ($self->version eq '2.2') {
            return '/msg:Bericht/msg:Client';
        }
    },
);

=head1 METHODS

=head2 is_section_valid

Check if the section is valid.

Some sections may not be preset in the XML, when they are not preset,
they are considered valid. This is for example possible when the WMO301
message contains no errors and a minimal WMO302 message is send with just
the headers. It contains no clients sections.

When the section is invalid this function fills the error attribute.

    $self-is_section_valid("Section name", "/xpath/query", $missing_ok);

=cut

sig is_section_valid => 'Str,Str,?Bool';

sub is_section_valid {
    my ($self, $section, $xpath, $missing_ok) = @_;

    my $node_list;
    if ($missing_ok) {
        $node_list = $self->has_section($section, $xpath);
        return 1 unless $node_list;
    }
    else {
        $node_list = $self->assert_section($section, $xpath);
    }

    my @rv = map { int($_->to_literal) } $node_list->grep(
        sub {
            _is_error($_);
        }
    );

    return 1 unless @rv;
    $self->errors->{$section} = \@rv;
    return 0;
}

=head2 assert_code_valid

Asserts if the message code is valid in the XML.

=cut

sub assert_code_valid {
    my $self = shift;

    my $node_list = $self->assert_section("Berichtcode", "/msg:Bericht/msg:Header/msg:BerichtCode");
    my $code = $node_list->get_node(1)->to_literal;

    if (int($code) eq $self->code) {
        return 1;
    }
    else {
        throw("zorginstituut/message/code/invalid", "Berichtcode is invalid $code, expecting " . $self->code);
    }
}

=head2 is_header_valid

Checks if the header contains no errors

=cut

sub is_header_valid {
    my $self = shift;

    my $section = "Header";
    my $base_xpath = "/msg:Bericht/msg:Header";

    $self->assert_section($section, $base_xpath);

    $self->assert_code_valid;

    return $self->is_section_valid($section, "$base_xpath/msg:RetourCodes/msg:RetourCode", 1);
}

=head2 is_clients_valid

Checks if the clients section contains no errors.
The client section is not mandatory, meaning, when this section is
present, something in the underlying nodes is invalid.

=cut

sub is_clients_valid {
    my $self = shift;

    my $section = "Clienten";
    my $base_xpath = "/msg:Bericht/msg:Clienten";

    if ($self->has_section('Clienten', 'msg:Bericht/msg:Clienten')) {
       return $self->is_client_valid;
    }

    if ($self->has_section('Client', '/msg:Bericht/msg:Client')) {
        return $self->is_client_valid;
    }

    return 1;
}

=head2 is_client_valid

Checks if the client section contains no errors.
The client section is not mandatory, meaning, when this section is
present, something in the underlying nodes is invalid.

=cut

sub is_client_valid {
    my $self = shift;

    my $section = "Client";
    my $base_xpath = $self->client_base;

    my $valid = 1;

    if ($self->has_section($section, $base_xpath)) {
        my $method = "is_%s_valid";
        my $m;
        foreach (qw(beschikking toegewezen_product beschikt_product contactgegevens)) {
            $m = sprintf($method, $_);
            if (!$self->$m) {
                $valid = 0;
            }
        }
    }
    return $valid;
}

=head2 is_contactgegevens_valid

Checks if the contactgegevens section contains no errors.

=cut

sub is_contactgegevens_valid {
    my $self = shift;

    my $section = "Contactgegevens";
    my $base_xpath = $self->client_base . '/msg:Contactgegevens/msg:Contact';

    return $self->is_section_valid($section, "$base_xpath/msg:RetourCodes/msg:RetourCode");
}

=head2 is_beschikking_valid

Checks if the beschikking section contains no errors.

=cut

sub is_beschikking_valid {
    my $self = shift;

    my $section = "Beschikking";
    my $base_xpath = $self->client_base . '/msg:Beschikking';

    return $self->is_section_valid($section, "$base_xpath/msg:RetourCodes/msg:RetourCode");
}

=head2 is_beschikt_product_valid

Checks if the beschikt_product section contains no errors.

=cut

sub is_beschikt_product_valid {
    my $self = shift;

    # is not present, so always valid
    return 1 if $self->version eq '2.2';

    my $section = "Beschikt product";
    my $xpath = '/msg:Bericht/msg:Clienten/msg:Client/msg:Beschikking/msg:Beschikt%sProducten/msg:BeschiktProduct';

    my $base_xpath = sprintf($xpath, $self->code == 437 ? 'e' : '');

    return $self->is_section_valid($section, "$base_xpath/msg:RetourCodes/msg:RetourCode");
}

=head2 is_toegewezen_product_valid

Checks if the toegewezen_product section contains no errors.

=cut

sub is_toegewezen_product_valid {
    my $self = shift;

    my $section = "Toegewezen product";
    my $base_xpath = $self->client_base . '/msg:Beschikking/msg:ToegewezenProducten/msg:ToegewezenProduct';
    return $self->is_section_valid($section, "$base_xpath/msg:RetourCodes/msg:RetourCode");
}

=head1 PRIVATE METHODS

=head2 _build_xpath

Builder of the xpath attribite

=cut

sub _build_xpath {
    my $self = shift;

    my $xc = XML::LibXML::XPathContext->new(
        XML::LibXML->load_xml(string => $self->xml));

    $xc->registerNs('base', $self->base_namespace);
    $xc->registerNs('msg',  $self->msg_namespace);

    return $xc;
}

=head2 _is_error

Checks if a node is an error or not

=cut

sub _is_error {
    my $node = shift;
    return int($node->to_literal // 0 ) eq 200 ? 0 : 1;
}

=head2 _is_valid

Builder for the valid attribute

=cut

sub _is_valid {
    my $self = shift;

    $self->is_header_valid;
    $self->is_clients_valid;

    keys %{$self->errors} ? 0 : 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
