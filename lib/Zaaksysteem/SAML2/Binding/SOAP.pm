package Zaaksysteem::SAML2::Binding::SOAP;

use Moose;

extends 'Net::SAML2::Binding::SOAP';
with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use BTTW::Tools::UA;
use HTTP::Request::Common;
use MooseX::Types::Moose qw/ Str ArrayRef /;
use XML::Generator;

use Zaaksysteem::SAML2::XML::Sig;


has '+idp_cert' => (isa => ArrayRef[Str]);
has cacert => (
    isa       => 'FileHandle',
    is        => 'ro',
    required  => 0,
    predicate => 'has_cacert'
);

has xmlsig => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    Zaaksysteem::SAML2::XML::Sig->new({
        x509 => 1,
        key => $self->key,
        cert => $self->cert
    });
});

has xmlgen => ( is => 'ro', lazy => 1, default => sub {
    XML::Generator->new(':std');
});

has ua => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;
    return new_user_agent(
        $self->has_cacert ? (ca_cert     => $self->cacert . "" ): (),
        client_cert => $self->cert,
        client_key  => $self->key,
    );
});

has soap_action => ( is => 'ro', default => '"http://www.oasis-open.org/committees/security"' );

sub request {
    my $self            = shift;

    my $request_body    = $self->create_soap_envelope(shift);

    my $req             = POST $self->url, Content => $request_body;

    $req->header('SOAPAction' => $self->soap_action);
    $req->header('Content-Type' => 'text/xml');
    $req->header('Content-Length' => length $request_body);
    $req->header('Connection' => 'Keep-Alive');

    if ($self->log->is_trace) {
        $self->log->trace(sprintf("Complete SAML SOAP request:\n%s\n", $req->as_string));
    }

    my $res     = $self->ua->request($req);
    my $content = $res->decoded_content;

    if ($self->log->is_trace) {
        $self->log->trace("SAML soap request:\n$content\n");
    }

    if (!$res->is_success) {
        $self->log->info("Failure with SAML2/SOAP request: " . $res->as_string);
        throw(
            'saml2/soap/response/failure',
            "Failure with SOAP request: $content"
        );
    }
    my $l = $res->header('Content-Length');
    if (defined $l && !$l) {
        throw(
            'saml2/soap/response',
            "SOAP response came back empty."
        );
    }

    return $content;
}

sub create_soap_envelope {
    my $self = shift;

    my $signed_message = $self->xmlsig->sign(shift);

    unless($self->xmlsig->verify($signed_message)) {
        #throw('saml2/soap/signature', 'Unable to verify XML Signature in outgoing SOAP message.');
    }

    return sprintf(
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n  <soapenv:Body>\n    %s\n  </soapenv:Body>\n</soapenv:Envelope>",
        $signed_message
    );
}

sub handle_response {
    my ($self, $response) = @_;

        # Zaaksysteem commented out
    # # verify the response
    # my $x       = $self->xmlsig;
    # my $ret = $x->verify($response);
    # die "bad SOAP response" unless $ret;

    # # verify the signing certificate
    # my $cert    = $x->signer_cert;
    # my $ca      = Crypt::OpenSSL::VerifyX509->new($self->cacert);
    # my $ret     = $ca->verify($cert);
    # die "bad signer cert" unless $ret;

    # my $subject = sprintf("%s (verified)", $cert->subject);

    # parse the SOAP response and return the payload
    my $parser = XML::XPath->new( xml => $response );
    $parser->set_namespace('soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
    $parser->set_namespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

    my $subject;
    my $saml = $parser->findnodes_as_string('/soap-env:Envelope/soap-env:Body/*');
    return ($subject, $saml);
}

=head2 in_debug_mode

Returns a true value if "SAML debugging" should be enabled. This results in
more verbose logging of the SAML/SOAP transaction.

If either of the environment variables C<CATALYST_DEBUG> or C<ZS_SAML_DEBUG> is
true, debug mode will be active.

=cut

sub _in_debug_mode {
    return 1 if $ENV{CATALYST_DEBUG};
    return 1 if $ENV{ZS_SAML_DEBUG};

    return;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_soap_envelope

TODO: Fix the POD

=cut

=head2 handle_response

TODO: Fix the POD

=cut

=head2 request

TODO: Fix the POD

=cut

