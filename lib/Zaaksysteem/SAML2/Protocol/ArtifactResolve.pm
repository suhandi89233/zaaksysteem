package Zaaksysteem::SAML2::Protocol::ArtifactResolve;

use Moose;

BEGIN { extends 'Net::SAML2::Protocol::ArtifactResolve'; }

use XML::Generator;

has xmlgen => ( is => 'ro', lazy => 1, default => sub {
    XML::Generator->new(':pretty');
});

sub as_xml {
    my ($self) = @_;

    my $saml  = ['saml'     => 'urn:oasis:names:tc:SAML:2.0:assertion'];
    my $samlp = ['samlp'    => 'urn:oasis:names:tc:SAML:2.0:protocol'];

    $self->xmlgen->ArtifactResolve(
        $samlp,

        {
            ID => $self->mangled_id,
            IssueInstant => $self->issue_instant,
            Destination => $self->destination,
            Version => '2.0'
        },

        $self->xmlgen->Issuer(
            $saml,
            $self->issuer,
        ),

        $self->xmlgen->Artifact(
            $samlp,
            $self->artifact,
        ),
    );
}

sub mangled_id {
    my $self = shift;

    return '_' . unpack 'H*', Crypt::OpenSSL::Random::random_pseudo_bytes(20);
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 as_xml

TODO: Fix the POD

=cut

=head2 mangled_id

TODO: Fix the POD

=cut

