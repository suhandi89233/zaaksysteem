package Zaaksysteem::Model::Bibliotheek::Sjablonen;
use Moose;

extends 'Catalyst::Model';
with 'MooseX::Log::Log4perl';

use Data::Dumper;
use Digest::MD5::File qw/-nofatals file_md5_hex/;
use Encode qw/encode/;
use File::Copy;
use File::Temp;
use File::stat;
use File::Spec::Functions qw(catfile);
use OpenOffice::OODoc;

use Zaaksysteem::Constants;
use Zaaksysteem::ZTT;
use BTTW::Tools;

use constant SJABLONEN              => 'sjablonen';
use constant SJABLONEN_DB           => 'DB::BibliotheekSjablonen';
use constant SJABLONEN_STRINGS_DB   => 'DB::BibliotheekSjablonenMagicString';
use constant FILESTORE_DB           => 'DB::Filestore';

has 'c' => (
    is  => 'rw',
);

{
    define_profile bewerken => (
        required => [ qw/
            naam
            bibliotheek_categorie_id
            commit_message
        /],
        optional => [ qw/
            filename
            id
            label
            description
            help
            template_external_name
            interface_id
        /],
        constraint_methods => {
            naam => qr/^\p{XPosixSpace}*(?:\p{XPosixAlnum}|\p{XPosixPunct})+/,
        },
        'require_some'      => {
            'filename_or_id'    => [1, qw/id filename interface_id/]
        },
        dependencies => {
            interface_id => [qw(template_external_name)],
        },
        # If the interface_id is 0 make it undef
        # so we don't have a dependency on the template_external_name
        field_filters => {
            interface_id => sub {
                my $val = pop;
                return $val if $val;
                return undef;
            },
        },
        msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
    );

    sub bewerken {
        my ($self, $params) = @_;

        my ($magic_strings, $old_sjabloon);

        my $dv = $self->c->check(
            params  => $params,
        );

        unless ($dv->success) {
            die "could not save sjabloon: " . Dumper $dv;
        }

        my $valid_options = $dv->valid;

        ### Rewrite some values
        my %options = map {
            $_ => $valid_options->{ $_ }
        } keys %{ $valid_options };

        if ($options{filename}) {
            $options{filename}  = $options{naam} . '.odt';
        }

        if ($options{filename}) {
            $magic_strings      = $self->_parse_file(%options)
                or return;

            $options{filestore_id} = $self->_store_file(%options)
                or return;
        } elsif (!$options{interface_id}) {
            $old_sjabloon       = $self->c->model(SJABLONEN_DB)->find(
                $options{id}
            ) or return;

            unless ($old_sjabloon->filestore_id || $valid_options->{template_external_name}) {
                $self->log->error('Missing filestore_id or template_external_name, cannot edit');
                return;
            }

            if ($old_sjabloon->filestore_id) {
                $options{filestore_id} = $old_sjabloon->get_column('filestore_id');
            } else {
                $options{filestore_id} = undef;
            }
        }

        if ($options{interface_id}) {
            my $interface = $self->c->model('DB::Interface')->find($options{interface_id});

            my $xential_ok =
                   ($interface->module eq 'xential')
                && $options{template_external_name} =~ /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i;
            my $stufdcr_ok =
                   ($interface->module eq 'stuf_dcr')
                && $options{template_external_name} !~ /^\s*$/;
            
            unless ($xential_ok || $stufdcr_ok) {
                die "could not save sjabloon: template name '$options{template_external_name}' incorrect";
            }
        }

        ### Remove unnecessary variables
        delete($options{id}) unless $options{id};
        delete($options{filename});
        delete($options{commit_message}); #handled by controller (but why :)

        if ($self->log->is_trace) {
            $self->log->trace("Creating template from model with options %s", dump_terse(\%options));
        }

        ### Ram er maar in
        my $kenmerk = $self->c->model(SJABLONEN_DB)->update_or_create(\%options);
        if (!$kenmerk) {
            die "Unable to create kenmerk!";
        }

        if ($kenmerk->bibliotheek_sjablonen_magic_strings->count) {
            $kenmerk->bibliotheek_sjablonen_magic_strings->delete;
        }

        if (ref($magic_strings)) {
            for my $magic_string (@{ $magic_strings }) {
                $self->c->log->debug('Creating template magic string: ' . $magic_string);

                $self->c->model(SJABLONEN_STRINGS_DB)->create({
                    'bibliotheek_sjablonen_id'  => $kenmerk->id,
                    'value'                     => $magic_string,
                });
            }
        }

        return $kenmerk;
    }
}

sub sjabloon_exists {
    my ($self, %opts)   = @_;

    return unless $opts{naam};

    return $self->c->model(SJABLONEN_DB)->search({
        'naam'  => $opts{naam}
    })->count;
}

sub _store_file {
    my ($self, %options) = @_;
    my ($filename);

    $filename = $options{filename};

    my $upload      = $self->c->req->upload('filename');

    my $options     = {
        original_name    => $filename,
        file_path        => $upload->tempname,
        ignore_extension => 1,
    };

    my $filestore   = $self->c->model(FILESTORE_DB)->filestore_create($options);

    if (!$filestore) {
        $self->c->log->error("Hm, kan filestore entry niet aanmaken: {$filename}");
        $self->c->push_flash_message('Sjabloonbestand kon niet worden opgeslagen');

        return;
    }

    return $filestore->id;
}

sub _parse_file {
    my ($self, %options) = @_;
    my (@magic_strings);

    my $filename        = $options{filename};

    if (!$self->c->req->upload('filename')) {
        $self->c->log->error("Bizar, kan file niet vinden: {$filename}");
        $self->c->push_flash_message('Sjabloonbestand upload mislukt');

        return;
    }

    ### Parse filename
    my $fh = $self->c->req->upload('filename')->fh or do {
        $self->c->log->error('Kan filehandle niet openen');
        $self->c->push_flash_message('Sjabloonbestand kon niet geopend worden, neem contact op met systeembeheer');

        return;
    };

    my $encoding    = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    my $doc         = odfDocument(
        file            => $self->c->req->upload('filename')->tempname,
        local_encoding  => $encoding
    ) or do {
        $self->c->log->error('Kan opendocument file niet openen');
        $self->c->push_flash_message('Ge&uuml;pload sjabloonbestand niet een geldig ODF document');

        return;
    };

    my $rawtext = $doc->getTextContent();
    (@magic_strings) = $rawtext =~ /\[\[([\w0-9_]+)\]\]/g;

    for (@magic_strings) {
        $self->c->log->debug('String: ' . $_);


        if (!$_ || $_ !~ /\[\[[\w0-9_]+\]\]/) { next; }

        my (@line_magic_strings) = $_ =~ /\[\[([\w0-9_]+)\]\]/g;

        push (@magic_strings, @line_magic_strings);
    }

    $self->c->log->debug(
        'Vond magic strings: '
        . "\n -" . join("\n -", @magic_strings)
    );

    return \@magic_strings if scalar(@magic_strings);
    return 1;
}

sub retrieve {
    my ($self, %opt) = @_;

    return $self->c->model(SJABLONEN_DB)->find($opt{id});
}

sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->{c} = $c;

    return $self;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ACCEPT_CONTEXT

TODO: Fix the POD

=cut

=head2 FILESTORE_DB

TODO: Fix the POD

=cut

=head2 PARAMS_PROFILE_DEFAULT_MSGS

TODO: Fix the POD

=cut

=head2 SJABLONEN

TODO: Fix the POD

=cut

=head2 SJABLONEN_DB

TODO: Fix the POD

=cut

=head2 SJABLONEN_STRINGS_DB

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 bewerken

TODO: Fix the POD

=cut

=head2 retrieve

TODO: Fix the POD

=cut

=head2 sjabloon_exists

TODO: Fix the POD

=cut
