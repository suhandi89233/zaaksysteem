package Zaaksysteem::Model::PDFGenerator;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::PDFGenerator - PerRequest model factory for the PDF generator

=head1 ATTRIBUTES

This class does not have attributes.

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<Zaaksysteem::PDFGenerator>'s constructor, based on
the current request.

=cut

__PACKAGE__->config(
    class => 'Zaaksysteem::PDFGenerator',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $c) = @_;

    my %args = (
        schema => $c->model('DB')->schema,
    );

    return \%args;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
