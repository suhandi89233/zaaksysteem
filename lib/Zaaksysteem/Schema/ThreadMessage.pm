use utf8;
package Zaaksysteem::Schema::ThreadMessage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ThreadMessage

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<thread_message>

=cut

__PACKAGE__->table("thread_message");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'thread_message_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 thread_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 type

  data_type: 'text'
  is_nullable: 0

=head2 message_slug

  data_type: 'text'
  is_nullable: 0

=head2 created_by_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 created_by_displayname

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 0
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 thread_message_note_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 thread_message_contact_moment_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 thread_message_external_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "thread_message_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "thread_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "type",
  { data_type => "text", is_nullable => 0 },
  "message_slug",
  { data_type => "text", is_nullable => 0 },
  "created_by_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "created_by_displayname",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 0, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "thread_message_note_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "thread_message_contact_moment_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "thread_message_external_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<thread_message_thread_message_contact_moment_id_key>

=over 4

=item * L</thread_message_contact_moment_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "thread_message_thread_message_contact_moment_id_key",
  ["thread_message_contact_moment_id"],
);

=head2 C<thread_message_thread_message_external_id_key>

=over 4

=item * L</thread_message_external_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "thread_message_thread_message_external_id_key",
  ["thread_message_external_id"],
);

=head2 C<thread_message_thread_message_note_id_key>

=over 4

=item * L</thread_message_note_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "thread_message_thread_message_note_id_key",
  ["thread_message_note_id"],
);

=head2 C<thread_message_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("thread_message_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 thread_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Thread>

=cut

__PACKAGE__->belongs_to(
  "thread_id",
  "Zaaksysteem::Schema::Thread",
  { id => "thread_id" },
);

=head2 thread_message_attachments

Type: has_many

Related object: L<Zaaksysteem::Schema::ThreadMessageAttachment>

=cut

__PACKAGE__->has_many(
  "thread_message_attachments",
  "Zaaksysteem::Schema::ThreadMessageAttachment",
  { "foreign.thread_message_id" => "self.id" },
  undef,
);

=head2 thread_message_contact_moment_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ThreadMessageContactMoment>

=cut

__PACKAGE__->belongs_to(
  "thread_message_contact_moment_id",
  "Zaaksysteem::Schema::ThreadMessageContactMoment",
  { id => "thread_message_contact_moment_id" },
);

=head2 thread_message_external_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ThreadMessageExternal>

=cut

__PACKAGE__->belongs_to(
  "thread_message_external_id",
  "Zaaksysteem::Schema::ThreadMessageExternal",
  { id => "thread_message_external_id" },
);

=head2 thread_message_note_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ThreadMessageNote>

=cut

__PACKAGE__->belongs_to(
  "thread_message_note_id",
  "Zaaksysteem::Schema::ThreadMessageNote",
  { id => "thread_message_note_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-10-11 10:11:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kZ3QkNLibHejSJQAzk3+Zw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
