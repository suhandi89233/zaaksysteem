package Zaaksysteem::Schema::CasetypeACL;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');
__PACKAGE__->table('zaaktype_acl');

__PACKAGE__->result_source_instance->deploy_depends_on(
    [
        qw(
            Zaaksysteem::Schema::Zaaktype
            Zaaksysteem::Schema::ObjectData
            Zaaksysteem::Schema::ObjectAclEntry
            Zaaksysteem::Schema::Subject
            Zaaksysteem::Schema::UserEntity
            )
    ]
);

__PACKAGE__->result_source_instance->is_virtual(1);

__PACKAGE__->result_source_instance->view_definition(
qq{
SELECT DISTINCT
    ct.id casetype_id,
    ct.active casetype_active,
    od.uuid casetype_uuid,
    s.username username,
    acl.capability capability,
    CASE WHEN acl.groupname = 'confidential' then true
    ELSE false END confidential
FROM
    object_data od
JOIN
    zaaktype ct
ON
    (
        od.object_class = 'casetype'
        AND
        od.object_id = ct.id
    )
JOIN
    object_acl_entry acl
ON
    (
        od.uuid = acl.object_uuid
        AND
        acl.scope = 'type'
    )
JOIN
    groups g
ON
    split_part(acl.entity_id, '|', 1)::int = g.id
JOIN
    groups g2
ON
    g.id = ANY (g2.path)
JOIN
    subject s
ON
    ( g.id = ANY (s.group_ids) or g2.id = ANY (s.group_ids))
JOIN
    roles r
ON
    (
        r.id = split_part(acl.entity_id, '|', 2)::int
        AND
        r.id = ANY (s.role_ids)
    )
JOIN
    user_entity ue
ON
    (
        ue.subject_id = s.id
        AND (
            ue.date_deleted IS NULL
            OR
            ue.date_deleted > NOW()
        )
    )
}
);

__PACKAGE__->add_columns(
    'casetype_id'     => { data_type => 'integer' },
    'casetype_active' => { data_type => 'boolean' },
    'casetype_uuid'   => { data_type => 'UUID' },
    'username'        => { data_type => 'text' },
    'capability'      => { data_type => 'text' },
    'confidential'    => { data_type => 'boolean' },
);

__PACKAGE__->set_primary_key(qw(casetype_id username confidential casetype_uuid));

1;

__END__

=head1 NAME

Zaaksysteem::Schema::CasetypeACL - A view for casetype ACL's

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
