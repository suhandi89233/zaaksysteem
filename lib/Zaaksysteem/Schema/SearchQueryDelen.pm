use utf8;
package Zaaksysteem::Schema::SearchQueryDelen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SearchQueryDelen

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<search_query_delen>

=cut

__PACKAGE__->table("search_query_delen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'search_query_delen_id_seq'

=head2 search_query_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "search_query_delen_id_seq",
  },
  "search_query_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 search_query_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::SearchQuery>

=cut

__PACKAGE__->belongs_to(
  "search_query_id",
  "Zaaksysteem::Schema::SearchQuery",
  { id => "search_query_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/Es6bccDB1B7D9r6EDho4w





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

