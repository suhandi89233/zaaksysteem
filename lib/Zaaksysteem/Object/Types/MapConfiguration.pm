package Zaaksysteem::Object::Types::MapConfiguration;
use Moose;

extends 'Zaaksysteem::Object';

use MooseX::Types::Moose qw(Str ArrayRef);
use Zaaksysteem::Object::Types::WMSLayer;
use Zaaksysteem::Object::Types::XMLNamespace;

override type => sub { 'ol_settings' };

=head1 NAME

Zaaksysteem::Object::Types::MapConfiguration - Object representing the map configuration

=head1 DESCRIPTION

A class describing the Zaaksysteem map configuration

=head1 ATTRIBUTES

=head2 wms_layers

An array of L<Zaaksysteem::Object::Types::WMSLayer> objects, describing the
configured map layers.

=cut

has wms_layers  => (
    is          => 'rw',
    isa         => ArrayRef['Zaaksysteem::Object::Types::WMSLayer'],
    default     => sub { []; },
    traits      => [qw(OR)],
    label       => "WMS layers",
);

=head2 xml_namespaces

An array of L<Zaaksysteem::Object::Types::XMLNamespace> objects, describing the
XML namespaces to define for XPath lookups.

=cut

has xml_namespaces  => (
    is          => 'rw',
    isa         => ArrayRef['Zaaksysteem::Object::Types::XMLNamespace'],
    default     => sub { []; },
    traits      => [qw(OR)],
    label       => "XML namespaces",
);

=head2 map_center

A string containing the configured map center. The format of this string is:

    "longitude,latitude"

Where longitude and latitude are both numbers (with a "." as a decimal separator).

=cut

has map_center  => (
    is          => 'rw',
    isa         => Str,
    traits      => [qw(OA)],
    label       => "Map center",
);

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
