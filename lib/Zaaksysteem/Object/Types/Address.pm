package Zaaksysteem::Object::Types::Address;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation);

use BTTW::Tools;
use Zaaksysteem::Types qw(AddressNumber AddressNumberLetter AddressNumberSuffix Zipcode);

use Zaaksysteem::Object::Types::CountryCode;

=head1 NAME

Zaaksysteem::Object::Types::Address - Addresses in zaaksysteem

=head1 DESCRIPTION

Address object, for companies or persons

=head1 ATTRIBUTES

=head2 street

The name of the street

=cut

has street => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Name of the street',
);

=head2 street_number

The exact location, "housenumber", in the street.

=cut

has street_number => (
    is       => 'rw',
    isa      => AddressNumber,
    traits   => [qw(OA)],
    label    => 'Street number',
);

=head2 street_number_letter

The "houseletter" in the street.

=cut

has street_number_letter => (
    is       => 'rw',
    isa      => AddressNumberLetter,
    traits   => [qw(OA)],
    label    => 'Letter suffix for street number',
);

=head2 street_number_suffix

The "housesuffix" in the street.

=cut

has street_number_suffix => (
    is       => 'rw',
    isa      => AddressNumberSuffix,
    traits   => [qw(OA)],
    label    => 'Suffix for street number',
);

=head2 zipcode

The "postalcode/zipcode/postcode" of this address object.

=cut

has zipcode => (
    is       => 'rw',
    isa      => Zipcode,
    traits   => [qw(OA)],
    label    => 'Zipcode',
);

=head2 city

The city for this object.

=cut

has city => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'City',
);

=head2 country

B<Type>: L<Zaaksysteem::Object::Types::CountryCode>

The related C<country> object for this address object.

=cut

has country => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::CountryCode',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Country',
);

=head2 foreign_address_line1

When the country for this object is different from The Netherlands (6030), we do not require
zipcode's, streets and numbers.

Instead we require at least one C<foreign_address_line>.

=cut

has foreign_address_line1 => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Foreign address line 1',
);

=head2 foreign_address_line2

When the country for this object is different from The Netherlands (6030), we do not require
zipcode's, streets and numbers.

Instead we require at least one C<foreign_address_line>.

=cut

has foreign_address_line2 => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Foreign address line 2',
);

=head2 foreign_address_line3

When the country for this object is different from The Netherlands (6030), we do not require
zipcode's, streets and numbers.

Instead we require at least one C<foreign_address_line>.

=cut

has foreign_address_line3 => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Foreign address line 3',
);

=head2 municipality

B<Type>: L<Zaaksysteem::Object::Types::MunicipalityCode>

The municipality of this address.

=cut

has municipality => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::MunicipalityCode',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Municipality code (gemeentecode)',
    required => 0,
);

=head1 METHODS

=head2 check_object

    $self->check_object

Will run a assert_profile by checking if all necessary attributes are set. Think of zipcodes
when the country is dutch etc.

=cut

define_profile check_object => (
    required        => {
        country         => 'Defined',
    },
    dependencies    => {
        country         => sub {
            my $dfv         = shift;
            my $value       = shift;
            my $code        = $value->dutch_code;

            if ($code eq '6030') {
                return [
                    'street',
                    'street_number',
                    'city',
                ];
            } else {
                return ['foreign_address_line1'];
            }
        }
    }
);

sub check_object {
    my $self        = shift;
    my (%opts)      = @_;

    my %values      = map(
        { my $key = $_->name; $key => $self->$key }
        grep({ $_->does('Zaaksysteem::Metarole::ObjectAttribute') || $_->does('Zaaksysteem::Metarole::ObjectRelation') } $self->meta->get_all_attributes)
    );

    assert_profile(\%values)->valid;

    return 1;
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
