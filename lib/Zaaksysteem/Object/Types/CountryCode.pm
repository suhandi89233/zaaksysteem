package Zaaksysteem::Object::Types::CountryCode;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use BTTW::Tools;
use Zaaksysteem::Object::ConstantTables qw/COUNTRY_TABLE/;
use Zaaksysteem::Types qw(NonEmptyStr);

use List::Util qw(first);

=head1 NAME

Zaaksysteem::Object::Types::CountryCode - Built-in object type implementing
a class for CountryCode objects

=head1 SYNOPSIS

    my $kingdom = Zaaksysteem::Object::Types::CountryCode->new(
        durch_code  => 6030,
        label       => 'Nederland',
        alpha_two   => 'NL',
        alpha_three => 'NLD',
        code        => 528,
    );

or from the country codes in use bij de Nederlandse Overheden:

    my $popedom = Zaaksysteem::Object::Types::CountryCode
        ->new_from_dutch_code(2045);
    print $popedom->label; # prints 'Vaticaanstad'

or to get the C<CountryCode> from text input:

    my $country = Zaaksysteem::Object::Types::CountryCode
        ->new_from_name('ZWITSERLAND')
    print $country->dutch_code; # 5003

=head1 DESCRIPTION

An object class for country codes. This module implements codes for both Dutch and ISO 3166 codes.

=head1 SEE ALSO

L<https://www.iso.org/obp/ui/#search>


=head1 ATTRIBUTES

=head2 dutch_code

The Dutch government uses their own codes, these are known as RGBZ land codes.

=cut

has dutch_code => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'Dutch country code',
    required => 1,
    unique   => 1,
);

=head2 label

The label of the rechtsvorm (legal entity type).

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the country',
    required => 1,
);

=head2 alpha_one DEPRICATE

The alpha one code of a country

=cut

has alpha_one => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'ISO 3166 Alpha 1 code',
    required => 0,
);

=head2 alpha_two

The ISO 3166 Alpha-2 code of a country, 2 letters

=cut

has alpha_two => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'ISO 3166 Alpha 2 code',
    required => 0,
);

=head2 alpha_three

The ISO 3166 Alpha-3 code of a country, 3 letters

=cut

has alpha_three => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'ISO 3166 Alpha 3 code',
    required => 0,
);

=head2 code

The ISO 3166 numeric code of a country, 3 digits

=cut

has code => (
    is       => 'rw',
    isa      => 'Int',
    traits   => [qw(OA)],
    label    => 'ISO 3166 numeric code',
    required => 0,
);

=head1 METHODS

=head2 new_from_dutch_code

    $country_code = Zaaksysteem::Object::Types::CountryCode->new_from_dutch_code(6030);

Loads object from dutch_code

=cut

sig new_from_dutch_code => 'Int';

sub new_from_dutch_code {
    my ($class, $dutch_code) = @_;

    my $country_code = first { $_->{dutch_code} eq int($dutch_code) } @{ COUNTRY_TABLE() };

    return $class->new(%$country_code) if $country_code;

    throw(
        'object/types/countrycode/unknown_dutch_code',
        sprintf('No country found by dutch code: "%s"',
            $dutch_code
        )
    );

    return $class->new(%$country_code);
}

=head2 new_from_name

    $country_code = Zaaksysteem::Object::Types::CountryCode
        ->new_from_name('Malta');

Loads object from the country name ... case-insensitive

CAVEAT, Names are the Dutch names, 'Frankrijk' wel, 'France' niet

=cut

sig new_from_name => 'Str';

sub new_from_name {
    my ($class, $name) = @_;

    my $country_code = first { lc($_->{label}) eq lc($name) } @{ COUNTRY_TABLE() };

    return $class->new(%$country_code) if $country_code;

    throw(
        'object/types/countrycode/unknown_name',
        sprintf('No country found by name: "%s"',
            $name,
        )
    );
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
