package Zaaksysteem::Object::Queue::Model::Casetype;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID];

=head2 touch_casetype

Handler for asynchronous case touches.

=cut

define_profile touch_casetype => (
    optional => {
        id => 'Int',
        uuid => UUID,
    },
    require_some => {
        id_or_uuid => [ 1, qw/id uuid/ ],
    }
);

sub touch_casetype {
    my $self = shift;
    my $item = shift;

    my $args = assert_profile($item->data)->valid;

    my $casetype;
    if (exists $args->{id}) {
        $casetype = $self->schema->resultset('Zaaktype')->find($args->{id})
            or throw(
                "queue/touch_casetype/not_found",
                "Case type with id='$args->{id}' not found"
            );
    } else {
        my $object_data = $self->schema->resultset('ObjectData')->search(
            {
                uuid => $args->{uuid},
                object_class => 'casetype',
            }
        )->single;

        if (not defined $object_data) {
            throw(
                "queue/touch_casetype/not_found",
                "Case type with uuid='$args->{uuid}' not found in object_data table"
            );
        }

        $casetype = $self->schema->resultset('Zaaktype')->find($object_data->object_id);

        if (not defined $casetype) {
            throw(
                "queue/touch_casetype/not_found",
                "Case type with uuid='$args->{uuid}' not found in zaaktype table"
            );
        }
    }


    $casetype->_sync_object($self->object_model);
    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
