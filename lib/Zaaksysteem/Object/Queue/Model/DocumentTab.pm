package Zaaksysteem::Object::Queue::Model::DocumentTab;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::DocumentTab - DocumentTab queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 document_tab_copy_to_case

=cut

sig document_tab_copy_to_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub document_tab_copy_to_case {
    my $self = shift;
    my $item = shift;

    my $case = $item->object_data->get_source_object;

    my $subject = $item->subject;
    my $json = $item->data->{json};
    my $target_case = $self->_rs_zaak->get_open_case_by_id($json->{case_id});

    $target_case->assert_write_permission($subject);

    my $model = $self->_document_model;

    ## Files in root directory
    if ($json->{files}) {
        $model->copy_to_case(
            subject     => $subject,
            source      => $case,
            destination => $target_case,
            file_ids    => $json->{files},
        );
    }

    my $directories = $json->{children};
    if ($json->{children}) {
        $self->_traverse_directories(
            $model,
            $case,
            $target_case,
            $subject,
            $json,
            undef,
        );
    }
}

sub _traverse_directories {
    my ($self, $model, $source, $target, $subject, $json, $root) = @_;

    my @directory = @{$json->{children}};
    foreach my $child (@directory) {

        my $dir = $self->_rs_directory->find($child->{id});

        my $new = $model->copy_to_case(
            subject     => $subject,
            source      => $source,
            destination => $target,
            directory   => $dir,
            $child->{files} ? ( file_ids => $child->{files}) : (),
            $root ? (parent_directory => $root) : (),
        );

        if ($child->{children}) {
            $self->_traverse_directories(
                $model,
                $source,
                $target,
                $subject,
                $child,
                $new,
            );
        }
    }
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
