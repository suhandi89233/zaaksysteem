package Zaaksysteem::Object::Queue::Model::Attribute;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Attribute - Attribute queue item handlers

=head1 DESCRIPTION

This role is a queue handler category for all 'attribute' (zaaktype_kenmerken)
queue items.

=head1 METHODS

=head2 update_index_schema

On update of an attribute, the search index must be updated. This queue item
handler uses L<Zaaksysteem::Object::Queue::Model/attribute_index_model> to do
so.

=cut

sub update_attribute_index_schema {
    my $self = shift;
    my $item = shift;

    my $kenmerk = $self->schema->resultset('BibliotheekKenmerken')->find(
        $item->data->{ bibliotheek_kenmerken_id }
    );

    $self->attribute_index_model->update_mapping($kenmerk);

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
