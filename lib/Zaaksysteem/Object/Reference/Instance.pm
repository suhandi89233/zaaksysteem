package Zaaksysteem::Object::Reference::Instance;

use Moose;
use namespace::autoclean;

# Zaaksysteem::Object::Reference role application would go here
# but due to complications if ordering of operations, it can be found near
# the bottom of this package.

=head1 NAME

Zaaksysteem::Object::Reference::Instance - Reference instance for objects

=head1 DESCRIPTION

This package also declares the C<Zaaksysteem::Object::Reference::Instance>
class, which is a minimal consumer of the role described above. It's meant to
be seen as a passable value that stands in for an actual object instance. This
allows a form of DB round-trip optimization if used correctly.

For instance, say there exists an object A, which has a relation with object
B. If I want to assign the related object B to another object (say, C),
without actually doing something with object B, it would be needless to
retrieve B from the database.

The reference instance is used mainly by the Object model itself, which will
usually return reference instances unless the relation had an embedded object
serialization. The reference instance is provided with an instantiator, which
allows the model to inflate the object at a later time, if required.

Reference instances can be assigned to attributes meant for object instances,
and the model will figure out the rest.

=cut

use overload '""' => sub { shift->TO_STRING };

use BTTW::Tools;

=head1 CONSTRUCTOR

=head2 Arguments

=over 4

=item id

Required, C<UUID>.

UUID of the object being referenced.

=item type

Required, C<string>.

Type string of the object being referenced

=item preview

Optional, C<string>.

Preview string of the referenced object.

=item instantiator

Optional, C<subref>.

If provided, this subroutine will be used to retrieve an instantiated version
of the object being referenced by the reference instance. This allows for a
form of auto-retrieval behavior implemented by the
L<model|Zaaksysteem::Object::Model>.

=back

=cut

# Silently fixes undef preview values
around BUILDARGS => sub {
    my ($orig, $class, %params) = @_;

    my $preview = delete $params{ preview };

    $params{ _stringification } = $preview if defined $preview;

    return $class->$orig(%params);
};

=head1 ATTRIBUTES

=head1 id

ID of the referenced object.

=cut

# Implementation below due to complications with role application.

=head2 type

Type of the referenced object.

=cut

has type => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

=head2 instantiator

Subref used for retrieving a full object instance from a reference.

=cut

has instantiator => (
    is => 'rw',
    isa => 'CodeRef',
    traits => [qw[Code]],
    predicate => '_instantiable',
    handles => { _instance => 'execute_method' }
);

has _stringification => (
    is => 'ro',
    isa => 'Str',
    predicate => '_has_stringification'
);

=head1 METHODS

=head2 TO_STRING

Implements a stringification for a reference object. If a preview was provided
during construction, that string will be returned. This methods falls back to
L<Zaaksysteem::Object/_as_string>.

=cut

sub TO_STRING {
    my $self = shift;

    return $self->_stringification if $self->_has_stringification;

    return $self->_as_string;
}

# Additional guard which checks for instantiability of the reference, and
# throws a contextualized exception if the assertion fails.
before _instance => sub {
    my $self = shift;

    return if $self->_instantiable;

    throw('object/ref/instantiator/missing', sprintf(
       'Cannot instantiate object from reference, no instantiator was provided for object %s',
       $self->_as_string
    ));
};

# Usually, role application directives are located at the top of the class
# definition, but in this case we need to have had the attributes applied
# before the reference role requires the instantiator predicate and delegates,
# so Moose won't except when compiling, because those methods don't exist yet.
with 'Zaaksysteem::Object::Reference';

# This attribute is also out-of-place, we require the applied reference role
# because it declares the attribute we're modifying here.
has '+id' => (
    required => 1
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
