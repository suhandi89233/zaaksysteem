package Zaaksysteem::Controller::Plugins::WOZ;

use Moose;

use LWP::UserAgent;
use LWP::Simple;

use XML::LibXML;
use Crypt::OpenSSL::RSA;
use MIME::Base64;
use POSIX qw/strftime/;
use URI::Escape;
use XML::Simple;
use XML::Parser;
use JSON;

BEGIN { extends 'Zaaksysteem::Controller' }

sub object : Local : Args() {
    my ($self, $c, $id) = @_;

    my $params     = $c->req->params;
    my $owner      = $params->{owner};
    my $object_id  = $params->{object_id};

    my $woz_object = $c->model('DB::WozObjects')->find({
        id => $id, # id would be enough, owner and object_id are for security
        owner => $owner,
        object_id => $object_id,
    })->TO_JSON;

    $c->res->header("application/json");

    if($woz_object) {
        $c->res->body(to_json($woz_object));
    } else {
        $c->res->body("error");
    }
}


sub settings : Local {
    my ($self, $c) = @_;

    my $settings = $c->forward('retrieve_settings');

    $c->res->header("application/json");
    $c->res->body(to_json($settings));
}


sub retrieve_settings : Private {
    my ($self, $c) = @_;

    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    return { map { $_->key => $_->value } @$settings };
}


sub picture : Local {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    my $interface = $c->model('DB::Interface')->search_active({module => 'woz'})->first;
    if (!$interface) {
        throw ("ZS/interface/woz/inactive", "No active WOZ interfaces found!");
    }

    my $config = $interface->get_interface_config;

    my $woz_object_url =
        'https://atlas.cyclomedia.com/PanoramaRendering/RenderbyAddress/NL/' .
        $params->{postcode} .
        '%20' .
        $params->{huisnummer} .
        '/?format=xml&apikey=' .
        $config->{key};

    my $ua = LWP::UserAgent->new;
    my $req = HTTP::Request->new(GET => $woz_object_url);

    $req->authorization_basic(
        $config->{username},
        $config->{password}
    );
    my $response = $ua->request($req);

    unless($response->code() eq '200') {
        die "couldn't get response from cyclomedia: " . $response->content;
    }

    my $parser = new XML::Parser( Style => 'Tree' );
    my $tree = $parser->parse(
        $response->content()
    );

    my $attributes = $tree->[1]->[0]
        or die "unexpected format in xml";

    my $recording_id = $attributes->{'recording-id'}
        or die "recording-id not present in xml";

    my $yaw = $attributes->{'yaw'}
        or die "yaw not present in xml";

    my $tid = $self->generate_tid({
        image_id    => $recording_id,
        config      => $config,
    });

    $c->stash->{json} = {
        tid             => $tid,
        woz_object_url  => $woz_object_url,
        recording_id    => $recording_id,
        yaw             => $yaw,
        postcode        => $params->{postcode},
        huisnummer      => $params->{huisnummer},
    };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


sub picture_dialog : Local {
    my ($self, $c) = @_;

    my $params                  = $c->req->params();

    $c->stash->{yaw}            = $params->{yaw};
    $c->stash->{tid}            = uri_escape($params->{tid});
    $c->stash->{recording_id}   = $params->{recording_id};
    $c->stash->{postcode}       = $params->{postcode};
    $c->stash->{huisnummer}     = $params->{huisnummer};

    $c->stash->{nowrapper}      = 1;
    $c->stash->{template}       = "plugins/woz/globespotter.tt";
}


sub generate_tid {
    my ($self, $arguments) = @_;

    my $image_id = $arguments->{image_id} or die "need image_id";
    my $config   = $arguments->{config}   or die "need config";

    my $account_id = $config->{account_id} or die "need account_id";
    my $issued_datetime = strftime "%Y-%m-%d %H:%M:%SZ", gmtime;

    my $tid = 'X' . $account_id . '&' . $image_id . '&' . $issued_datetime;

    my $rsa_priv = Crypt::OpenSSL::RSA->new_private_key($config->{private_key} // '');

    my $signature = encode_base64($rsa_priv->sign($tid), '');

    $tid .= '&' . $signature;

    return $tid;
}

sub photo : Local {
    my ($self, $c) = @_;

    # the idea is to retrieve the woz object image from a remote server.
    # this construct will help with https: issues.
    # for now this is a placeholder.
    # my $path = $c->config->{home} . '/root/tpl/zaak_v1/nl_NL/images/woz_test_foto_huis.jpg';

    # open FILE, $path or die "could not open file path $path: $!";
    # my $content = join '', <FILE>;
    # close FILE;


    # when retrieving from remote, append woz_objectnummer to this config'd
    # url, retrieve it, and serve the result to the user.
    # if error, serve a "no image available" photo
    my $settings = $c->forward('retrieve_settings');
    my $woz_photo_base_url = $settings->{woz_photo_base_url};

    if ($woz_photo_base_url) {
        my $woz_object_id = $c->req->params->{object_id};

        $woz_photo_base_url =~ s|\[\[woz_object_id\]\]|$woz_object_id|is;

        # LWP::Simple, retrieve external photo
        my $content = get($woz_photo_base_url);

        if ($content) {
            $c->res->content_type('image/jpeg');
            $c->res->body($content);
            return;
        }
    }

    # fall through on purpose
    $c->response->status(404);
    $c->res->body('');
}


# tricky to add a pure image loading feature to ezra_dialog, so here's your dirty workaround.
# yes, i just did.
sub photo_dialog : Local {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    my $url = '/plugins/woz/photo?' . join "&", map { $_ . '=' . $params->{$_} } keys %$params;

    $c->res->content_type('text/html');
    $c->res->body('<img src="' . $url. '"/>');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 generate_tid

TODO: Fix the POD

=cut

=head2 photo

TODO: Fix the POD

=cut

=head2 photo_dialog

TODO: Fix the POD

=cut

=head2 picture

TODO: Fix the POD

=cut

=head2 picture_dialog

TODO: Fix the POD

=cut

=head2 read_private_key

TODO: Fix the POD

=cut

=head2 retrieve_settings

TODO: Fix the POD

=cut

=head2 object

TODO: Fix the POD

=head2 settings

TODO: Fix the POD

=cut

