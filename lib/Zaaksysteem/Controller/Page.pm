package Zaaksysteem::Controller::Page;

use Moose;

use File::stat;
use URI;
use List::Util qw[any];

use BTTW::Tools;
use Log::Log4perl::MDC;
use Data::UUID;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Page - Main application-level authorization logic

=head1 DESCRIPTION

This package contains some basic actions and request authorization logic.

B<BEWARE>: Contained code is archiac in design and known to be very fragile.
Handle with care and consideration, or there's a good chance subtle auth bugs
will get introduced (most notably API and globally callable actions).

=head1 ACTIONS

=head2 index

Fallback for "/page" controller.

=cut

sub index : Path : Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Page in Page.');
}

=head2 commit_message

Part of a deprecated confirmation dialog implementation.

=head3 URL

C</page/commit_message>

=cut

sub commit_message : Chained('/') : PathPart('page/commit_message') {
    my ($self, $c) = @_;

    $c->stash->{confirmation}->{type}       = 'yesno';

    $c->stash->{confirmation}->{uri}     = $c->req->uri;
    $c->stash->{confirmation}->{commit_message} = 1;

    $c->detach('/page/confirmation');
}

=head2 alert

Part of a deprecated alert dialog implementation.

=head3 URL

C</page/alert>

=cut

sub alert : Chained('/') : PathPart('page/alert') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'alert.tt';
    $c->stash->{nowrapper} = 1;
    $c->detach;
}

=head2 confirmation

Main implementation of an XHR-based confirmation dialog.

This action should be considered to be deprecated.

=head3 URL

C</page/confirmation>

=cut

sub confirmation : Chained('/') : PathPart('page/confirmation') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'confirmation.tt';

    my $params = $c->req->params();

    if($params->{commit_message}) {
        $c->stash->{confirmation}->{commit_message} = 1;
    }

    $c->stash->{confirmation}->{message} ||= $params->{message};

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper}  = 1;
        $c->stash->{xmlrequest} = 1;
    }
}

=head2 css_minified

Static content action for minified CSS.

=head3 URL

C</tpl/zaak_v1/nl_NL/css>

=cut

sub css_minified : Path('/tpl/zaak_v1/nl_NL/css') {
    my ($self, $c, $template) = @_;

    $template =~ s/zsv_.*?-(\w+)\.css$/$1/;

    $c->detach('minified', [ $template, 'css' ]);
}

=head2 frontend_i18n_fallback

Static content action for internationalisation definitions for the Zaaksysteem
frontend application.

=cut

sub frontend_i18n_fallback : Regex('^html/(.*?)$') {
    my ($self, $c) = @_;

    $c->serve_static_file(
        $c->config->{root} . '/html/nl/' . $c->req->captures->[0]
    );
}

=head2 minified

Static content action for minified frontend resources.

=cut

sub minified : Local {
    my ($self, $c, $template, $cat) = @_;

    $c->stash->{nowrapper}                      = 1;
    $c->stash->{invoke_assets_minified_request} = 1;

    my $MINIFIED_MAPPING = {
        'css'   => {
            'common'    => 'common_header_includes_css.tt',
            'private'   => 'private_header_includes_css.tt',
        },
        'js'    => {
            'common'    => 'common_header_includes_js.tt',
            'private'   => 'private_header_includes_js.tt',
        },
    };

    ### Use zaaksysteem.js as modification time for last-modified header
    my $filename;
    if ($cat eq 'js') {
        $c->response->content_type("text/javascript");
        $filename    = $c->path_to(
            '/root/tpl/zaak_v1/nl_NL/js/zaaksysteem.js'
        );
    } else {
        $c->response->content_type('text/css');
        $filename    = $c->path_to(
            '/root/css/base.css'
        );
    }

    my $mtime       = $self->is_asset_modified($c, $filename);
    if ($MINIFIED_MAPPING->{$cat}->{$template}) {
        $c->stash->{template} = 'layouts/' .
            $MINIFIED_MAPPING->{$cat}->{$template};

        $c->response->headers->last_modified($mtime);
        $c->detach;
    };

    $c->res->body('Forbidden');
    $c->res->status(403);
}

=head2 is_asset_modified

Local action that may detach the request if the requested filepath has not
been modified since the provided If-Modified-Since header.

=cut

sub is_asset_modified : Local {
    my ($self, $c, $filename) = @_;

    my $fileinfo = stat($filename);

    if (!$fileinfo) {
        $c->res->body('Forbidden');
        $c->res->status(403);
        $c->detach;
    }

    if (
        $c->req->headers->if_modified_since &&
        $c->req->headers->if_modified_since < time() &&
        $c->req->headers->if_modified_since >= $fileinfo->mtime
    ) {
        $c->res->status(304);
        $c->detach;
    }

    return $fileinfo->mtime;
}

=head2 javascript_ezra

Static content action for frontend JS resources.

=head3 URL

L</tpl/zaak_v1/nl_NL/js/javascript_ezra.js>

=cut

sub javascript_ezra : Path('/tpl/zaak_v1/nl_NL/js/javascript_ezra.js') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'layouts/javascript_ezra.tt';
    $c->stash->{nowrapper} = 1;

    $c->res->content_type('text/javascript');
}

=head2 javascript_libraries

Static content action for frontend JS resouces.

=head3 URL

C</tpl/zaak_v1/nl_NL/js/javascript_libraries.js>

=cut

sub javascript_libraries : Path('/tpl/zaak_v1/nl_NL/js/javascript_libraries.js') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'layouts/javascript_libraries.tt';
    $c->stash->{nowrapper} = 1;

    $c->res->content_type('text/javascript');
}

=head2 about

Deprecated 'about dialog' action.

=head3 URL

C</page/about>

=cut

sub about : Local {
    my ($self, $c)  = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'widgets/about.tt';
}

=head1 PRIVATE ACTIONS

=head2 begin

This action implements core authorization logic for Zaaksysteem. It's
conditional branches and exceptions are many, and is known to be fragile.
Handle with care.

=cut

sub begin : Private {
    my ($self, $c) = @_;

    ### Auth action:
    my $authaction = lc($c->req->action);
    $authaction =~ s|^/|| unless $authaction eq '/';

    Log::Log4perl::MDC->put('action_path', $authaction);
    Log::Log4perl::MDC->put('request_uri', $c->req->uri->path_query);

    if ($ENV{CATALYST_DEBUG}) {
        $c->log->debug(
            sprintf(
                "Request URI: '%s' (Auth action: '%s')",
                ${ $c->req->uri }, $authaction
            )
        );
    }

    if (lc($c->req->method) eq 'post' && $self->request_token_check_required($authaction)) {
        $c->forward('assert_request_token');
    }

    # If true, platform key OK, and we skip all underlying verification
    # Otherwise, perform all normal checks
    # If key mismatch, assert will throw an exception
    if ($self->check_platform_key($c)) {
        $c->log->debug('Platform key access granted, setting godmode');

        return 1;
    }

    if ($self->check_api_auth_token($c)) {
        $c->log->debug('Session invitation valid, access granted');

        return 1;
    }

    ### PreAUTH: Speedbump for some special actions, javascript/css bundle
    if (
        $authaction eq 'page/minified' ||
        $authaction eq 'page/css_minified' ||
        $authaction eq '^html/(.*?)$' ||
        $authaction =~ /^tpl\/zaak_v1\/nl_nl\/css/
    ) {
        return 1;
    }

    if (
        $c->session->{zaaksysteem} &&
        $c->session->{zaaksysteem}->{mode} && $c->session->{zaaksysteem}->{mode} eq 'simple'
    ) {
        $c->stash->{layout_type} = 'simple'
    }

    $c->languages(['nl']) if $c->can('languages');

    # Hacking and slashing, merry on my way
    # Since access is fundamentally decided here, hook into the access attribute set on controllers
    my $access_cleared = 0;

    if(exists $c->action->attributes->{ Access }) {
        my ($tokens) = @{ $c->action->attributes->{ Access } };

        $access_cleared = grep { $_ eq '*' } split m[,\s*], $tokens;
    }

    if ($authaction eq 'man' && $c->model('DB::Config')->get('public_manpage')) {
        $access_cleared = 1;
    }

    if (
        $c->user_exists && $c->user->is_external_api &&
        $authaction !~ m[^api/(v1|externkoppelprofiel)/.*]
    ) {
        $c->log->warn('API user tried accessing non-api namespaces, clearing session');
        $c->logout;
        $c->delete_session;
    }

    # DigiD/eHerkenning sessions
    if ($authaction !~ m#api/users/(?:session|extend)#) {
        $c->check_saml_session ? $c->extend_saml_session_timeout() : $c->extend_session_timeout();
    }

    if ($c->user_exists && !$c->user->is_external_api) {
        $c->detach('/forbidden') unless $self->_user_authorized_for_request($c);
    }

    if (
        $c->user_exists &&
        !$c->user->is_sorted &&
        $authaction ne 'firstlogin/first_login' &&
        $authaction !~ /^auth/
    ) {
        $c->res->redirect($c->uri_for('/first_login'));
        $c->detach;
    }

    ### Make sure everyone is logged in
    if (
        !$c->user_exists &&
        $authaction !~ /^auth/ &&
        $authaction !~ /^form\/(?!employee\/)/ &&
        $authaction !~ m|^sysin/interface/trigger| &&
        $authaction !~ /^plugins\/pip/ &&
        $authaction !~ /^test.*/ &&
        $authaction !~ /^plugins\/digid.*/ &&
        $authaction !~ /^plugins\/maps.*/ &&
        $authaction !~ /^plugins\/woz\/.*/ &&
        $authaction !~ /^plugins\/bedrijfid.*/ &&
        $authaction !~ /^plugins\/ogone.*/ &&
        $authaction !~ /^zaak\/handle_offline_payment.*/ &&
        $authaction !~ /^api\/qmatic.*/ && # necessary for pip
        $authaction !~ /^api\/supersaas.*/ && # necessary for pip
        $authaction !~ /^api\/appointment\/.*/ && # necessary for pip
        $authaction !~ /^api\/stuf\/bg0204.*/ &&
        $authaction !~ /^api\/stuf\/stuf0204.*/ &&
        $authaction !~ /^api\/stuf\/stuf0301.*/ &&
        $authaction !~ /^api\/rules\/base$/ &&
        $authaction !~ /^api\/publicsearchquery\/.*/ &&
        $authaction !~ /^api\/mail.*/ &&
        $authaction !~ /^api\/app\/word\/get_xml/ &&
        $authaction !~ /^api\/users.*/ &&
        $authaction !~ /^api\/message\/create/ &&
        $authaction !~ /^api\/v1\/.*/ &&
        $authaction !~ /^api\/scanstraat\/upload_document/ &&
        $authaction !~ m|^api/externkoppelprofiel/| &&
        $authaction !~ /^kcc.*/ &&
        $authaction !~ /^logout.*/ &&
        $authaction !~ /^gegevens\/bag\/search.*/ &&
        $authaction !~ /^objectsearch\/bag.*/ &&
        $authaction ne 'api/soap/soap' &&
        $authaction ne 'api/kcc/register_call' &&
        $authaction ne 'schedule/run' &&
        $authaction ne 'zaak/create' &&
        $authaction ne 'zaak/start_nieuwe_zaak' &&
        $authaction ne 'plugins/woz/woz_object_picture' &&
        $authaction ne 'plugins/woz/woz_object_picture_popup' &&
        $authaction ne 'search/public_map_by_id' &&
        $authaction ne 'sysin/interface/soap/enter' &&
        !$access_cleared &&
        ! (
            $authaction =~ /^beheer\/import\/.*\/run/ &&
            $c->config->{saas_range} &&
            $c->req->address =~ $c->config->{saas_range}
        ) &&
        ! (
            $authaction =~ /^gegevens\/bag\/import/ &&
            $c->config->{saas_range} &&
            $c->req->address =~ $c->config->{saas_range}
        )
    ) {
        if (   exists $c->action->attributes->{ZAPI}
            || exists $c->action->attributes->{JSON}
            || $c->req->path =~ m#^(?:objectsearch/.*)#
        ) {
            $self->request_unauthorized($c);
        } else {
            $c->flash->{referer} = $c->set_referer($c->req->params->{referer} // $c->req->uri);
            $c->response->redirect($c->uri_for('/auth/login'));
            $c->detach;
        }
    }

    # Hardcoded the last two implementers of the 'prepare_page' logic
    $c->controller('Betrokkene')->prepare_page($c);
    $c->controller('API::KCC')->prepare_page($c);

    $c->res->cookies->{ _request_token } = {
        value  => Data::UUID->new->create_str,
        secure => 1,
        httponly => 1,
    };

    return 1;
}

=head2 assert_request_token

Assert and update the request token using the (optionally) provided client
cookies.

=cut

sub assert_request_token : Private {
    my ($self, $c) = @_;

    my $request_token = $c->req->cookie('_request_token');

    unless (defined $request_token) {
        $c->log->debug('Could not find token in request cookies, assuming server-server API call');

        return;
    }

    $c->session->{ _request_tokens } ||= {};

    my $previous_token = $c->session->{ _request_tokens }{ $c->req->action };

    if (defined $previous_token && $previous_token eq $request_token->value) {
        $c->log->debug('Request token duplication detected, abort request');

        throw('page/validate_request_token/token_invalid', sprintf(
            'Request token duplication detected, abort request'
        )) unless ($c->debug);
    }

    $c->session->{ _request_tokens }{ $c->req->action } = $request_token->value;

    return;
}

=head2 check_platform_key

If supplied, verify the C<ZS-Platform-Key> header against C<zs_platform_key>
from our instance configuration.

=cut

sub check_platform_key {
    my ($self, $c) = @_;

    my $key = $c->req->header('ZS-Platform-Key');

    return unless defined $key;

    return 1 if $c->authenticate({ key => $key }, 'platform');

    $self->request_unauthorized($c);
}

=head2 check_api_auth_token

If supplied, verify the C<ZS-Auth-Token> header against the store of session
invitations.

=cut

sub check_api_auth_token {
    my ($self, $c) = @_;

    return 1 if $c->user_exists && $c->session->{ _api_token_session };

    my $token = $c->req->header('ZS-Auth-Token');

    return unless defined $token;

    unless ($c->authenticate({ token => $token }, 'token')) {
        $self->request_unauthorized($c);
    }

    $c->session->{ _api_token_session } = 1;

    return 1;
}

=head2 request_unauthorized

Aborts the request with a bare-bones 'unauthorized' reply.

=cut

sub request_unauthorized {
    my ($self, $c) = @_;

    $c->error('Unauthorized');

    $c->log->warn(sprintf(
        'Unauthorized access by host "%s"',
        $c->get_client_ip
    ));

    $c->res->status(401);
    $c->res->body('Unauthorized access');

    $c->detach;
}

=head2 dialog

Part of the (deprecated) dialog infrastructure. This private action is called
internally to validate user state and request parameters.

=cut

sub dialog : Private {
    my ($self, $c, $opt) = @_;

    Params::Profile->register_profile(
        'method'    => [caller(0)]->[3],
        'profile'   => $opt->{validatie}
    );

    ### Auth
    if ($opt->{permissions}) {
        $c->assert_any_zaak_permission(@{ $opt->{permissions} });
    } elsif ($opt->{user_permissions}) {
        $c->assert_any_user_permission(@{ $opt->{user_permissions} });
    } else {
        die(
            'Dialog handling: need at least permissions or '
           .' user_permissions option'
       );
    }

    my $dv = $c->zvalidate(
        undef, {
            bypass_json => 1
        }
    );

    if ($c->req->is_xhr &&
        (
            $c->req->params->{do_validation} ||
            (!$dv || !$dv->success)
        )
    ) {
        if ($c->req->params->{do_validation}) {
            $c->zvalidate;
            $c->detach;
        }

        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = $opt->{template};
        $c->detach;
    }

    if (
        $c->req->params->{confirmed} &&
        (
            my $dv = $c->zvalidate(
                undef,
                {
                    bypass_json => 1,
                }
            )
        )
    ) {
        $c->res->redirect($opt->{complete_url})
            if (defined($opt->{complete_url}));

        return $dv;
    }

    return;
}

=head1 PRIVATE METHODS

=head2 _user_authorized_for_request

Returns true if the current user is authorized for the current action.

=cut

sub _user_authorized_for_request {
    my ($self, $c) = @_;

    return unless $c->user_exists;

    return 1 if $c->check_any_user_permission('admin');

    my $namespace = $c->controller->action_namespace;
    $self->log->trace("Accessing namespace: $namespace");

    # No roles are required for these namespaces
    return 1 if any { $_ eq $namespace } qw[
        firstlogin
        auth
        man
    ];

    my @roles = @{ $c->user->roles };

    foreach (@roles) {
        return 1 if $_->is_assignee_role();
        return 1 if allow_app_user($_, $namespace);
    }
    return 0;

}

sub allow_app_user {
    my ($role, $namespace) = @_;
    return 0 if !$role->system_role;
    return 0 if $role->name ne 'App gebruiker';
    return 1 if (
        any { $_ eq $namespace } qw[
            api/v1/app
            api/v1/app/meeting
            api/v1/session
            api/v1/case
            api/v1/case/relation
            api/v1/case/document
            api/v1/case/note
            api/v1/casetype
            zaak/document
        ]
    );
    return 0;
}

=head2 request_token_check_required

Return true if the current auth action (the first argument) requires a
"duplicate auth token" check.

=cut

sub request_token_check_required {
    my $self = shift;
    my $authaction = shift;

    if ($authaction =~ m{^file/create}) {
        $self->log->debug("Skipping duplicate token check for '$authaction'");
        return;
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
