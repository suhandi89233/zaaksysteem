package Zaaksysteem::Controller::API::v1::Search;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::V1::Search - API v1 Spotenlighter searcher

=head1 DESCRIPTION

This is the controller API class for C<api/v1/subject>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Search>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Search>

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::EZSearch;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/search> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('search') : CaptureArgs(0) {}

=head2 search

C</api/v1/search[/COMPONENT[/COMPONENT2[...]]]>

See manual

=cut

define_profile search => (
    required        => {
        query       => 'Str',
    },
    optional        => {
        filter      => 'Str',
    },
    field_filters   => {
        query       => sub {
            my $val        = shift;

            ### Remove tags or other complicated problems
            $val =~ s{[<>&":]}{_}sg;

            return $val;
        },
        filter      => sub {
            my $val        = shift;

            ### Remove tags or other complicated problems
            $val =~ s{[<>&"]}{_}sg;

            return $val;
        }
    }
);

sub search : Chained('base') : PathPart('') : Args() : RO {
    my ($self, $c, $search_type)  = @_;
    my $params                  = assert_profile($c->req->params)->valid;

    throw(
        'api/v1/search',
        'Need at least one valid search_type to search within'
    ) unless ($search_type);

    my $filters                 = $self->_prepare_filters($params->{filter});

    my $ezsearch                = Zaaksysteem::EZSearch->new(
        schema      => $c->model('DB')->schema,
        query       => $params->{query},
        search_type => $search_type,
        filters     => $filters,
    );

    $c->stash->{result}         = Zaaksysteem::API::v1::ArraySet->new(
        content     => $ezsearch->search()
    );
}

=head1 PRIVATE METHODS

=head2 _prepare_filters

=over 4

=item Arguments: \@FILTERS or $FILTER

=item Return value: \%FILTERS

=back

    $self->_prepare_filters(['trigger:casetype', 'subject_type:natuurlijk_persoon'])

Transforms the given filters to a simple hashref for parsing in L<Zaaksysteem::API::EZSearch>

=cut

sub _prepare_filters {
    my ($self, $arg)    = @_;
    return {} unless $arg;

    my $filters         = (ref $arg eq 'ARRAY' ? $arg : [ $arg ]);

    my %rv;
    for my $filter (@$filters) {
        my ($key, $value)   = split(/:/, $filter);

        $rv{$key} = $value;
    }

    return \%rv;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
