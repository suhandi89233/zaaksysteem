package Zaaksysteem::Controller::API::v1::Case;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case - APIv1 controller for case objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=cut

use BTTW::Tools;
use DateTime;
use List::Util qw[first uniq];
use Zaaksysteem::API::v1::Message::Ack;
use Zaaksysteem::API::v1::PreparedFileBag;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Constants::Users qw(:all);
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::Serial;
use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::Search::ESQuery;
use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Event::Emitter;

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('api', 'app');
    $self->add_api_context_permission('extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('case') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $model = $c->model("Object");

    $c->stash->{base_rs} = $c->get_base_rs(
        object_type => 'case',
        model       => $model,
        $c->stash->{case_base_rs}
            ? (base_rs => $c->stash->{case_base_rs})
            : (),
        $c->stash->{interface} ? (interface => $c->stash->{interface}) : (),
    );

    my $iterator = Zaaksysteem::Object::Iterator->new(
        rs       => $c->stash->{base_rs},
        inflator => sub { $model->inflate_from_row(shift) },
    );

    my $set = Zaaksysteem::API::v1::Set->new(iterator => $iterator);

    $c->stash->{cases}  = $set->build_iterator->rs;
    $c->stash->{result} = $c->iterator_to_api_v1_response($iterator);
}

sub search_base : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $model = $c->model("Object");

    my $iterator = $c->parse_search_query(
        object_type => 'case',
        model       => $c->model('Object'),
        base_rs     => $c->stash->{base_rs},
    );
    $c->stash->{result} = $c->iterator_to_api_v1_response($iterator);
}

=head2 instance_base

Reserves the C</api/v1/case/[CASE_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ case } = try {
        return $c->stash->{ cases }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    unless (defined $c->stash->{ case }) {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    unless ($c->stash->{ case }->object_class eq 'case') {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    $c->stash->{zaak} = try {
        $c->stash->{case}->get_source_object;
    }
    catch {
        $c->log->warn($_);

        throw(
            'api/v1/case/retrieval_fault',
            'Case retrieval failed, unable to continue.'
        );
    };
}

=head2 list

=head3 URL Path

C</api/v1/case>

=cut

sub list : Chained('search_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    ### Load relations
    my $object_uuids_query = $c->stash->{ result }->build_iterator->get_column('uuid')->as_query;
    my $relations_rs = $c->model('DB::ObjectRelationships')->search(
        {
            -or => [
                { object1_uuid => { -in => $object_uuids_query } },
                { object2_uuid => { -in => $object_uuids_query } },
            ]
        }
    );

    $c->stash->{ serializer_opts }{ object_relationships } = [ $relations_rs->all ];
}

=head2 get

=head3 URL Path

C</api/v1/case/[CASE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ case };

    if ($c->stash->{ result }) {
        $c->stash->{ is_cacheable } = 1;
        $c->res->headers->last_modified($c->stash->{ result }->date_modified->epoch);
        $c->res->header('Cache-Control' => 'private');

        my $zaak = $c->stash->{ zaak };

        $zaak->log_view();

        my $uuid = $c->stash->{ result }->get_column('uuid');
        my $relations_rs = $c->model('DB::ObjectRelationships')->search(
            {
                -or => [
                    { object1_uuid => $uuid },
                    { object2_uuid => $uuid },
                ]
            }
        );
        $c->stash->{ serializer_opts }{ object_relationships_rs } = $relations_rs;
    }
}

=head2 casetype

=head3 URL Path

C</api/v1/case/[CASE_UUID]/casetype>

=cut

sub casetype : Chained('instance_base') : PathPart('casetype') : Args(0) : RO {
    my ($self, $c) = @_;

    # Prevent new query, use prefetched results
    my @relations = $c->stash->{ case }->object_relation_object_ids->all;

    my $relation = first { $_->object_type eq 'casetype' } @relations;

    # Safeguard for untouched case object_data rows since the introduction
    # of casetype embedded relations.
    unless (defined $relation) {
        my $zaak = $c->stash->{ zaak };

        $relation = $zaak->_rewrite_object_casetype($c->stash->{ case });
    }

    $c->stash->{ result } = try {
        $c->model('Object')->inflate_from_relation($relation);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/case/casetype/retrieval_error',
            'Casetype retrieval failed'
        );
    };
}

=head2 create

=head3 URL Path

C</api/v1/case/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    my $model = $c->model('Zaak');
    my $create_arguments = $model->prepare_case_arguments($c->req->params);

    my $zaak = $model->create_case($create_arguments);

    $zaak->_touch;

    $c->stash->{case} = $zaak->object_data;
    $c->stash->{zaak} = $zaak;
    $c->detach('get');
}

=head2 reserve_number

=head3 URL Path

C</api/v1/case/reserve_number>

=cut

sub reserve_number : Chained('base') : PathPart('reserve_number') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $id     = $c->model('Zaak')->generate_case_id();
    my $object = Zaaksysteem::Object::Types::Serial->new(
        name         => "number",
        serial       => $id,
        object_class => 'case',
    );

    $c->stash->{result}  = $object;
}

=head2 get_by_number

=head3 URL Path

C</api/v1/case/get_by_number>

Get a case object based on the case number

=cut

sub get_by_number : Chained('base') : PathPart('get_by_number') : Args(1) : RO {
    my ($self, $c, $id) = @_;

    my $case = $c->model('Zaak')->get_by_serial_number($id);

    if ($case) {
        $c->forward('instance_base', [$case->object_data->uuid]);
        $c->detach("get");
    }
    else {
        throw("api/v1/case/id/not_found",
            "Unable to find case with id: $id");
    }
}

=head2 create_delayed

=head3 URL Path

C</api/v1/case/create_delayed>

=cut

sub create_delayed : Chained('base') : PathPart('create_delayed') : Args(0) : RW {
    my ($self, $c) = @_;

    # Unfortunatly I cannot save the output of this because it contains
    # all kinds of DB things and soforth, so need to run this twice,
    # also on case creation time
    my $model  = $c->model('Zaak');
    my $params = $c->req->params;

    $model->prepare_case_arguments($params);
    my $qitem = $model->create_delayed_case($params);

    $c->forward('/execute_post_request_actions');

    $c->stash->{result}  = $qitem;

}

=head2 prepare_file

=head3 URL

C</api/v1/case/prepare_file>

=cut

sub prepare_file : Chained('base') : PathPart('prepare_file') : Args(0) : RW {
    my ($self, $c) = @_;

    my @uploads = map { ref $_ eq 'ARRAY' ? @$_ : $_ } values %{ $c->req->uploads };

    unless (scalar @uploads) {
        throw('api/v1/case/upload', sprintf(
            'Upload(s) missing.'
        ), { http_code => 400 });
    }

    my $filestore = $c->model('DB::Filestore');

    $c->stash->{ result } = Zaaksysteem::API::v1::PreparedFileBag->new;

    for my $upload (@uploads) {
        my $file = try {
            return $filestore->filestore_create({
                original_name => $upload->filename,
                file_path     => $upload->tempname,
            });
        } catch {
            $c->log->warn($_);

            throw(
                'api/v1/case/upload_validation',
                'File creation failed, unable to continue.'
            );
        };

        $c->stash->{ result }->add($file);

        my $clean_job = Zaaksysteem::Object::Types::ScheduledJob->new(
            job => 'CleanTmp',
            interval_period => 'once',
            next_run => DateTime->now->add(minutes => 15),
            data => $file->uuid
        );

        try { $c->model('Object')->save_object(object => $clean_job) } catch {
            $c->log->warn("Non-fatal; failed to schedule temporary file cleaner job. Original error follows:", $_);
        };
    }
}

=head2 process_event

Event dispatcher endpoint for event generated on the minty side.

=head3 URL Path

C</api/v1/case/[CASE_UUID]/process_event>

=cut

sub process_event : Chained('instance_base') : PathPart('process_event') : Args(0) : RW {
    my ($self, $c) = @_;

    my $event = $c->req->params || {};

    try {
        my $model = Zaaksysteem::Event::Emitter->new_from_event(
            $event,
            schema  => $c->model("DB")->schema,
            subject => $c->model("BR::Subject"),
            queue   => $c->model("Queue"),
            case    => $c->stash->{zaak},
        );
        $c->stash->{result} = $model->emit_event();
    }
    catch {
        $c->log->info("Ignoring event: $_");
        $c->stash->{ result } = Zaaksysteem::API::v1::Message::Ack->new(
            message => 'Event ignored'
        );
    };
    return;


}

=head2 transition

=head3 URL Path

C</api/v1/case/[CASE_UUID]/transition>

=cut

define_profile transition => (
    optional => {
        result_id   => 'Num',
        result      => 'Str',
    }
);

sub transition : Chained('instance_base') : PathPart('transition') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $opts = assert_profile($c->req->params)->valid;

    my $zaak = $c->stash->{zaak};
    unless ($zaak->zaaktype_node_id->properties->{ api_can_transition }) {
        throw(
            'api/v1/case/transition',
            'Case transitioning is not enabled for this casetype.'
        );
    }

    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/transition/closed',
            "Unable to transition case, case is closed ",
            { http_code => 409 }
        );
    }

    try {
        if(exists $opts->{ result_id } || exists $opts->{ result }) {
            if($zaak->afhandel_fase->status eq ($zaak->milestone + 1)) {
                if ($opts->{result_id}) {
                    $zaak->set_result_by_id($opts->{result_id});
                }
                elsif ($opts->{result}) {
                    $zaak->set_resultaat($opts->{result});
                }
                else {
                    throw('api/v1/case/set_result',
                        'Unable to set result: missing result or result_id');
                }
            }
            else {
                throw(
                    'api/v1/case/set_result',
                    'Unable to set result: case is not in final state'
                );
            }
        }

        $zaak->advance(
            object_model     => $c->model('Object'),
            betrokkene_model => $c->model('Betrokkene'),
            current_user     => $c->user,
        );
    }
    catch {
        my $errormsg = "$_";
        $c->log->info($errormsg);

        if (blessed($_) && $_->isa('BTTW::Exception::Base')) {
            ### TODO: We really need information about the "why", owner not complete? Missing result?
            my $object = $_->object;

            if ($object) {
                $errormsg
                    = 'No transition possible for this case, missing '
                    . join(
                    ', ',
                    map({
                            $_ =~ s/_complete//;
                                $_;
                        }
                        grep ({ !$object->{transition_states}->{$_} }
                            keys %{ $object->{transition_states} }))
                    );

            }
        }

        throw('api/v1/case/transition', $errormsg);
    };

    $self->_reload_case_and_get($c, $zaak);
}

=head2 upload_files

Upload files to a case

=head3 URL Path

C</api/v1/case/[CASE_UUID]/upload_files>

=cut

sub upload_files : Chained('instance_base') : PathPart('upload_files') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    my $zaak = $self->_get_case($c);

    my %uploads = %{$c->req->uploads};
    my @fields = keys %uploads;
    unless (@fields) {
        throw('api/v1/case/upload_files', sprintf(
            'Upload(s) missing.'
        ), { http_code => 400 });
    }


    my $kenmerken = $self->_get_case_attributes($c, $zaak, \@fields);

    $kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    my %document_values;
    while (my $k = $kenmerken->next) {
       $document_values{ $k->bibliotheek_kenmerken_id->magic_string } = $k->id,
    }

    $c->stash->{ result } = Zaaksysteem::API::v1::PreparedFileBag->new;

    try {
        my $counter = 0;
        my $filestore = $c->model('DB::Filestore');
        my $file_rs   = $c->model('DB::File');

        foreach my $magic_string (keys %uploads) {
            if (!$document_values{$magic_string}) {
                throw(
                    "api/v1/case/file_upload/attribute/unknown",
                    "Unknown document attribute $magic_string",
                    { http_code => 400 }
                );
            }

            my $upload = $uploads{$magic_string};

            $c->log->debug(sprintf(
                'Would create file "%s" with path "%s" as attribute "%s"',
                $upload->filename,
                $upload->tempname,
                $magic_string
            ));

            my $filestore_obj = $filestore->filestore_create({
                original_name => $upload->filename,
                file_path     => $upload->tempname,
            });

            $c->stash->{ result }->add($filestore_obj);

            $file_rs->file_create(
                {
                    disable_message   => 1,
                    case_document_ids => [ $document_values{$magic_string} ],
                    name      => $filestore_obj->original_name,
                    db_params => {
                        filestore_id => $filestore_obj->id,
                        created_by   => $c->user->betrokkene_identifier,
                        case_id      => $zaak->id
                    }
                }
            );

            $counter++;

        }

        if ($counter) {
            $zaak->create_message_for_behandelaar(
                message => sprintf(
                    "%d document%s toegevoegd door %s",
                    $counter, $counter > 1 ? 'en' : '', $c->user->display_name,
                ),
                event_type => 'api/v1/update/documents',
            );
        }

    }
    catch {
        $c->log->fatal($_);
        throw('api/v1/case/upload_validation', sprintf(
            'File creation failed, unable to continue.'
        ));
    };

}

=head2 update

=head3 URL Path

C</api/v1/case/[CASE_UUID]/update>
=cut

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my %opts = %{$c->req->params};

    my $model = $c->model("Zaak");

    my $case = $self->_get_case(
        $c,
        ($opts{payment_info} ? 1 : 0),
    );


    if ($c->check_user_mask(REGULAR)) {
        $opts{user} = $c->user;
    }

    $model->update_case($case, %opts);

    $self->_reload_case_and_get($c, $case);
}

=head2 take

=head3 URL Path

C</api/v1/case/[CASE_UUID]/take>

=cut

sub take : Chained('instance_base') : PathPart('take') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_user;

    my $zaak = $c->stash->{ zaak };
    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/take/closed',
            "Unable to 'take' case, case is closed ",
            { http_code => 409 }
            )
    }

    $zaak->open_zaak();
    $self->_reload_case_and_get($c, $zaak);
}

=head2 reject

=head3 URL Path

C</api/v1/case/[CASE_UUID]/reject>

=cut

sub reject : Chained('instance_base') : PathPart('reject') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_user;

    my $zaak = $c->stash->{ zaak };
    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/reject/closed',
            "Unable to 'reject' case, case is closed ",
            { http_code => 409 }
        );
    }

    if ($zaak->status ne 'new') {
        throw(
            'api/v1/case/reject/not_new',
            "Unable to 'reject' case, case is not in 'intake' state",
            { http_code => 409 }
        );
    }

    $zaak->reject_zaak($c->user, $c->req->params->{comment});

    $self->_reload_case_and_get($c, $zaak);
}

sub _get_case_attributes {
    my ($self, $c, $case, $fields) = @_;

    my $kenmerken = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            # Only get those kenmerken we need.
            'bibliotheek_kenmerken_id.magic_string' => { -in => $fields },


            # 'specifieke behandelrechten' always off-limits for API users
            required_permissions => [ undef, '{}' ]
        },
        {
            prefetch => [qw[bibliotheek_kenmerken_id]]
        }
    );

    unless ($kenmerken->count) {
        # Note that this error may occur when a valid attribute name was
        # provided, but it isn't bound to the current phase.
        throw('api/v1/case/nop', sprintf(
            'Refusing to update because no supplied value resolved to an (authorized) attribute'
        ), { http_code => 409 });
    }

    return $kenmerken;
}

sub _get_case {
    my $self = shift;
    my $c = shift;
    my $allow_closed = shift;

    my $zaak = $c->stash->{zaak};

    if ($zaak->is_afgehandeld && !$allow_closed) {
        throw(
            'api/v1/case/update/closed',
            "Unable to update case, case is closed ",
            { http_code => 409 }
        );
    }

    return $zaak;
}

sub _reload_case_and_get {
    my ($self, $c, $zaak) = @_;

    $zaak->discard_changes;
    $zaak->_touch;

    $c->stash->{case} = $zaak->object_data;
    $c->stash->{zaak} = $zaak;
    $c->detach('get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
