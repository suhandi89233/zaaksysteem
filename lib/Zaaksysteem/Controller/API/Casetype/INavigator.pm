package Zaaksysteem::Controller::API::Casetype::INavigator;

use Moose;

use Hash::Merge qw/merge/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub do_import : Local : ZAPI {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $casetype_id = $c->model('Zaaktypen')->import_inavigator_casetype($params);

    # flush session in case somebody has been editing this.
    delete $c->session->{zaaktypen}->{$casetype_id};

    $c->forward('casetype_info', [$casetype_id]);
}



# provide a lookup for attributes in this casetype
sub casetype_info : Local : Args(1) : ZAPI {
    my ($self, $c, $casetype_id) = @_;

    $c->forward('existing_casetypes');

    $c->stash->{zapi} = [{
        message => 'I-Navigator zaaktype geimporteerd',
        casetype => $c->model('Zaaktypen')->retrieve_session($casetype_id),
        existing_kenmerken => [$self->existing_kenmerken($c)],
        existing_categories => [$c->model('DB::BibliotheekCategorie')->tree],
        existing_casetypes => $c->stash->{existing_casetypes},
    }];
}


# TODO settings are in session now, need to be moved to general settings
sub restore_default_settings : Local : ZAPI {
    my ($self, $c) = @_;

    my $code = $c->req->params->{code} or throw('api/inavigator/missing_code', 'Zaaktypecode is verplicht');

    $c->user->remove_inavigator_settings($code);

    $c->stash->{zapi} = [{
        settings => $c->user->settings->{inavigator} || {},
        message => 'Instellingen teruggezet naar de beginstand.'
    }];
}


sub save_settings : Local : ZAPI {
    my ($self, $c) = @_;

    my $current = $c->user->settings->{inavigator_settings} || {};
    my $merged = merge $c->req->params, $current;
    $c->user->modify_setting('inavigator_settings', $merged);

    $c->stash->{zapi} = [{
        message => 'Instellingen zijn bewaard.'
    }];
}


sub existing_kenmerken : Private {
    my ($self, $c) = @_;

    my @existing = $c->model('DB::BibliotheekKenmerken')->search({
        'me.value_type' => 'file',
        'me.deleted'    => undef,
    })->all;

    return map {{
        id => $_->id,
        naam => $_->naam,
    }} @existing;
}


sub existing_casetypes : Private {
    my ($self, $c) = @_;

    my @existing = $c->model('DB::Zaaktype')->search({
        'zaaktype_node_id.deleted' => undef,
        'me.deleted'               => undef,
    }, {
        join => 'zaaktype_node_id',
        order_by => 'zaaktype_node_id.titel'
    })->all;

    $c->stash->{existing_casetypes} = [map {
        {
            id => $_->id,
            name => $_->zaaktype_node_id->titel,
            code => $_->zaaktype_node_id->code,
        }
    } @existing];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 casetype_info

TODO: Fix the POD

=cut

=head2 do_import

TODO: Fix the POD

=cut

=head2 existing_casetypes

TODO: Fix the POD

=cut

=head2 existing_kenmerken

TODO: Fix the POD

=cut

=head2 restore_default_settings

TODO: Fix the POD

=cut

=head2 save_settings

TODO: Fix the POD

=cut

