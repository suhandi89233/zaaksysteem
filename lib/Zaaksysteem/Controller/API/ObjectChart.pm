package Zaaksysteem::Controller::API::ObjectChart;

use Moose;
use namespace::autoclean;

# Temporary API for graphs using the new object_data table instead of zaak.

use BTTW::Tools;
use Zaaksysteem::Object::Chart::Generator;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('api/object_chart') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('gebruiker');
}

define_profile chart => (
    required => [qw(chart_profile zql)],
    typed => {
        chart_profile => 'Str',
        zql => 'Str',
    },
);

sub chart : Chained('base') : PathPart("") : Args(0) {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;

    my $zql = Zaaksysteem::Search::ZQL->new($params->{zql});
    die "not a select" unless $zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select');

    my $resultset = $zql->apply_to_resultset($c->model('Object')->acl_rs);

    my $chartgenerator = Zaaksysteem::Object::Chart::Generator->new({ resultset => $resultset });
    my $chart_profile = $chartgenerator->generate({ profile => $params->{chart_profile} });

    my $lookup = $self->departments_lookup($c);

    if($params->{chart_profile} eq 'cases_per_department') {
        my $categories = [
            map {
                $lookup->{$_} || 'Afdeling '. $_
            } @{
                $chart_profile->{xAxis}->{categories}
            }
        ];
        $chart_profile->{xAxis}->{categories} = $categories;
    } elsif (
        $params->{chart_profile} eq 'cases_per_department_per_month' ||
        $params->{chart_profile} eq 'cases_within_and_outside_term_per_month_per_department'
    ) {
        my $series = $chart_profile->{series};
        foreach my $part (@$series) {
            my $department = $part->{name};
            $part->{name} = $lookup->{$department} || 'Afdeling ' . $department;
        }
    }

    $c->stash->{json} = $chart_profile;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

sub departments_lookup {
    my ($self, $c) = @_;

    my $groups = $c->user->all_available_groups;

    return { map { $_->id => $_->name } @$groups };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 chart

TODO: Fix the POD

=cut

=head2 departments_lookup

TODO: Fix the POD

=cut

