package Zaaksysteem::ZTT::Converter::Pandoc;

=head1 NAME

Zaaksysteem::ZTT::Converter::Pandoc - Pandoc implementation of Converter methods

=cut

use Moose::Role;

use Path::Tiny;
use File::Temp;
use OpenOffice::OODoc;

use BTTW::Tools;

=head1 DESCRIPTION

This module implements methods that convert from one format into another using
Pandoc.

The actual conversion is done calling C<< $self->convert >>, which is
implemented through the L<Zaaksysteem::ZTT::Converter::Pandoc::CLI> role. Future
extensions may include something like
L<Zaaksysteem::ZTT::Converter::Pandoc::API>.

=head1 METHODS

=cut

=head2 from_markup_text_into_openoffice_oodoc

See L<Zaaksysteem::ZTT::Converter#from_markup_text_into_openoffice_oodoc> for a
description.

Expects markup text as first argument followed by a list of options:

    my $oodoc = $converter->from_markup_text_into_openoffice_oodoc(
        '<b>Foo</b>',
        convert_from   => 'html',
        reference_file => 'my_reference.odt',
    );

=head3 Options

=over

=item convert_from

Any from the dozens that are out in the wild

=item reference_file

An .odt file that contains style definitions that will applied whilst rendering
the markup text.

=back

=cut

around 'from_markup_text_into_openoffice_oodoc' => sub {
    my $orig = shift;
    my $self = shift;
    my $params = assert_profile( {@_} ,
        profile => {
            required => {
                reference_file => 'Path::Tiny',
            },
            optional => {
                markup_text    => 'Str',
                markup_type    => 'Str',
            },
            defaults => {
                markup_text    => '',
                markup_type    => 'html',
            },
        },
    )->valid;

    my $markup_text    = $params->{markup_text};
    my $markup_type    = $params->{markup_type};
    my $reference_file = $params->{reference_file};

    $markup_text =~ s{<p><br></p>}{<p></p>}g; # <br> causes non-empty paragraphs
    my $markup_file = Path::Tiny->tempfile;
    $markup_file->spew_utf8( $markup_text );

    $markup_type .= '+empty_paragraphs' if $markup_type eq 'html';

    my $output_file = Path::Tiny->tempfile;

    $self->convert (
         convert_from   => $markup_type,
         convert_into   => 'odt+empty_paragraphs',
         input_file     => $markup_file->canonpath,
         output_file    => $output_file->canonpath,
         reference_file => $params->{'reference_file'}->canonpath,
    );

    my $rendered_file = path( $output_file->canonpath );
    my $rendered_path = $rendered_file->canonpath;
    my $dir = File::Temp->newdir();
    odfWorkingDirectory($dir);
    my $odf_rendered = odfDocument(
        file           => $rendered_path,
        local_encoding => $OpenOffice::OODoc::XPath::LOCAL_CHARSET,
        opendocument   => 1,
    );

    return $odf_rendered

};

=head1 METHODS TO BE IMPLEMENTED ELSEWHERE

=head2 convert

Converts one markup-file into another format.

Roles implementing this method must expect the following parameters:

=over

=item convert_from

The markup format to convert from

=item convert_into

The (markup) format to convert into

=item input_file

The file location where to read the markup file from

=item output_file

The file location where to write the output to

=item reference_file

An optional location of a reference file for ODT files, that contains styling
info

=back

=cut

sub convert {
    die "method not implemented: 'convert'";
}

=head1 ROLES

The following roles are consumed:

=over

=item L<Zaaksysteem::ZTT::Converter::Pandoc::CLI>

That role should implement C<convert> as described above.

=back

=cut


with (
    'Zaaksysteem::ZTT::Converter::Pandoc::CLI'
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
