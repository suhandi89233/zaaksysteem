package Zaaksysteem::ZTT::Selection::Plaintext;

use Moose;

BEGIN { extends 'Zaaksysteem::ZTT::Selection' };

=head1 NAME

Zaaksysteem::ZTT::Selection::Plaintext - Specific implementation of selections
for plaintext templates.

=head1 ATTRIBUTES

=head2 start

This attribute holds the character position of the start of the selection
within the template as an integer.

=cut

has start => (
    is => 'ro',
    isa => 'Int',
    required => 1
);

=head2 tag

This attribute holds a reference to a Zaaksysteem::ZTT::Tag object that
represents the tag detected in the template text.

=cut

has tag => (
    is => 'ro',
    isa => 'Zaaksysteem::ZTT::Tag'
);

=head2 subtemplate

This attribute holds a string representing the subtemplate's content.

=cut

has subtemplate => (
    is => 'ro',
    isa => 'Str'
);

=head2 end

This attribute holds the character position of the end of the selection
within the template as an integer. It is implicitly set by adding the
L</length> of the selection to the L</start> position.

=cut

has end => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;

        return $self->start + $self->length;
    }
);

=head2 length

This attribute holds the length of the selected text. It is implictly set to
the length of the L<selection|Zaaksysteem::ZTT::Template/selection>'s content.

=cut

has length => (
    is => 'ro',
    lazy => 1,
    default => sub {
        return length shift->selection;
    }
);

=head1 METHODS

=head2 as_string

This method builds a string representation of the text's selection. Mostly
useful in debugging and logging contexts.

    warn $plaintext_selection->as_string;

=cut

sub as_string {
    my $self = shift;

    sprintf('%d:%d ("%s")', $self->start, $self->end, $self->selection);
}

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
