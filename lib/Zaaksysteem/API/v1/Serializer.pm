package Zaaksysteem::API::v1::Serializer;

use feature 'state';

use Moose;

use BTTW::Tools;
use List::Util qw[first];
use Module::Pluggable::Object;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::API::v1::Serializer - API v1 Serializer

=head1 SYNOPSIS

    my $serializer = Zaaksysteem::API::v1::Serializer->new(
        encoding_format => 'JSON'
    );

    my $data = $serializer->serialize($object);

    # OR

    my $data = $serializer->encode($serializer->read($object));

=head1 DESCRIPTION

This serializer works in a 2-step fashion, reading an object into a plain Perl
datastructure, followed by an encoding run based on the configured
L</encoding_format>.

Every object reader is implemented as a plugin found in the
L<Zaaksysteem::API::v1::Serializer::Reader> namespace.

Every encoder is implemented as a plugin found in the
L<Zaaksysteem::API::v1::Serializer::Encoder> namespace.

=head1 ATTRIBUTES

=head2 has_full_access

Our API exposes a select part of the available content. For internal use, and some other
uses, it is necessary to return every part of the serialized contents.

=cut

has 'has_full_access' => (
    is  => 'rw',
    isa => 'Bool',
);

=head2 v1_serializer_options

Sometimes, you would like to inform the final serializer of something. This would be the way

=cut

has 'v1_serializer_options' => (
    is          => 'rw',
    isa         => 'HashRef',
    default     => sub { return {}; },
);

=head2 readers

Returns the list of readers found in the package's C<Reader> sub-namespace.

The list is cached for subsequent calls.

=cut

sub readers {
    my $self = shift;

    state $readers = try {
        my @pkgs = Module::Pluggable::Object->new(
            search_path => sprintf('%s::Reader', __PACKAGE__),
            instantiate => 'new'
        )->plugins;

        return \@pkgs;
    } catch {
        # Ensure we log the error explicitly so it can't get gobbled up.
        $self->log->error('Caught exception while locating readers: %s', $_);

        die $_;
    };

    return @{ $readers };
}

=head2 encoders

Returns the list of encoders found in the package's C<Encoder> sub-namespace.

The list is cached for subsequent calls.

=cut

sub encoders {
    my $self = shift;

    state $encoders = try {
        my @pkgs = Module::Pluggable::Object->new(
            search_path => sprintf('%s::Encoder', __PACKAGE__),
            instantiate => 'new'
        )->plugins;

        return \@pkgs;
    } catch {
        # Ensure we log the error explicitly so it can't get gobbled up.
        $self->log->error('Caught exception while locating encoders: %s', $_);

        die $_;
    };

    return @{ $encoders };
}

=head2 preload

Preload helper so encoders and readers can be loaded in startup context.

    Zaaksysteem::API::v1::Serializer->preload;

Call this from an APP instance to prevent loading the reader classes on every
request cycle. A performance enhancement.

=cut

sub preload {
    my $self = shift;

    $self = $self->new unless blessed $self;

    my @readers = $self->readers;
    my @encoders = $self->encoders;

    $self->log->trace(sprintf(
        'Preloaded readers: %s',
        join(', ', map { (split m[::], ref)[-1] } @readers)
    ));

    $self->log->trace(sprintf(
        'Preloaded encoders: %s',
        join(', ', map { (split m[::], ref)[-1] } @encoders)
    ));
}

=head2 encoding_format

Attribute that holds a format type used to find the correct encoder plugin.

=cut

has encoding_format => (
    is => 'rw',
    default => 'JSON'
);

=head1 METHODS

=head2 serialize

This method attempts to fully serialize an object of arbitrairy type by
chaining the L</read> and L</encode> methods.

    my $data = $serializer->serialize($object);

=cut

sub serialize {
    my $self = shift;

    return $self->encode($self->read(shift));
}

=head2 encode

This method attempts to encode a plain Perl datastructure into a serialized
format.

A specific encoder will be selected based on the configured
L</encoding_format> by iterating over all available encoder plugins and asking
if the plugin handles the format.

B<Caveat>: a plugin is selected on first-come-first-serve basis, where the
order of plugins is undefined. Basically, don't build encoders with
overlapping support for one or more formats.

=cut

sub encode {
    my $self = shift;
    my $data = shift;

    # Find the first encoder that claims to encode in $fmt
    my $encoder = first {
        $_->grok($self->encoding_format)
    } $self->encoders(serializer => $self);

    unless (defined $encoder) {
        throw('api/v1/serializer', sprintf(
            'Configured to serialize to "%s", no such encoder.',
            $self->encoding_format
        ));
    }

    return $encoder->encode($self, $data);
}

=head2 read

This method attempts to read an arbitrairy object, returning a plain
(non-blessed) Perl datastructure which can be fed to an encoder.

A specific reader will be selected by iterating over all available
reader plugins and selecting the first one to claim support for the provided
object.

B<Caveat>: a plugin is selected on first-come-first-serve basis, where the
order of plugins is undefined. Basically, don't build readers with overlapping
support for one or more objecttypes.

=cut

sub read {
    my ($self, $data, @reader_args) = @_;

    # Guard gainst undefined values, we allow their passthru
    return undef unless defined $data;

    for my $reader ($self->readers) {
        my $read = $reader->grok($data);

        # grok() returns a coderef when a suitable reader is found.
        next unless defined $read;

        # early return if a reader is found.
        return $read->($self, $data, @reader_args);
    }

    throw('api/v1/serializer/missing_reader', sprintf(
        'No reader found for data: %s',
        dump_terse($data),
    ));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
