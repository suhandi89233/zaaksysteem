package Zaaksysteem::API::v1::Object::Session::Account;

use Moose;
use namespace::autoclean;

use constant CAT_TO_ACCOUNT_MAP => {
    company             => 'naam',
    company_extended    => 'naam_lang',
    company_simple      => 'naam_kort',
    place               => 'woonplaats',
    address             => 'adres',
    zipcode             => 'postcode',
    homepage            => 'website',
    email               => 'email',
    phonenumber         => 'telefoonnummer',
};

=head1 NAME

Zaaksysteem::API::v1::Object::Session::Account - Account information of the current session

=head1 SYNOPSIS

    my $obj =  Zaaksysteem::API::v1::Object::Session::Account->new_from_catalyst($c);

=head1 DESCRIPTION

A custom object for C<api/v1> containing the session accountinformation for this zaaksysteem. It contains
information about the company.

=head1 Tests

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/API/v1/Object/Session.pm

=head1 ATTRIBUTES

=head2 company

L<Str>

The company name

=head2 company_extended

L<Str>

The complete company name

=head2 company_simple

L<Str>

The short name of the company

=head2 place

L<Str>

The place of of this company, e.g. "Amsterdam"

=head2 address

L<Str>

The address of the company

=head2 zipcode

L<Str>

The zipcode (postcal code) of the company

=head2 homepage

L<Str>

The URL of the homepage of the company

=head2 email

L<Str>

The emailaddress of the company

=head2 phonenumber

L<Str>

The phonenumber of the company

=cut

has [qw/company company_extended company_simple place address zipcode homepage email phonenumber/] => (
    is  => 'ro',
    isa => 'Str',
);

=head1 METHODS

=head2 new_from_catalyst

=over 4

=item Arguments: L<Catalyst>

=item Return value: L<Zaaksysteem::API::v1::Object::Session::Account>

=back

    my $account = Zaaksysteem::API::v1::Object::Session::Account->new_from_catalyst($c);

Constructs L<Zaaksysteem::API::v1::Object::Session::Account> by inflating values from a Catalyst C<$c> object

=cut

sub new_from_catalyst {
    my $class   = shift;
    my $c       = shift;

    my $config  = $c->get_customer_info;

    ### Map only filled values
    my %params  = map {
        $_ => $config->{ CAT_TO_ACCOUNT_MAP->{$_} }
    } grep({ length $config->{ CAT_TO_ACCOUNT_MAP->{$_} } } keys %{ CAT_TO_ACCOUNT_MAP() });

    $class->new(%params);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
