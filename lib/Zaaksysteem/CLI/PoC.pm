package Zaaksysteem::CLI::PoC;
use Moose;

extends 'Zaaksysteem::CLI';

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->assert_run;

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;
            $self->log->info("Running in a transaction in " . __PACKAGE__);
        }
    );
    return 1;

};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::CLI::PoC - A proof of concept CLI script

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
