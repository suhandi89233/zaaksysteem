BEGIN;

    -- Let's use booleans for boolean attribute_values
    ALTER TABLE bibliotheek_kenmerken ADD COLUMN type_multiple_new BOOLEAN NOT NULL DEFAULT FALSE;
    UPDATE bibliotheek_kenmerken SET type_multiple_new = true where type_multiple = 1;
    ALTER TABLE bibliotheek_kenmerken DROP COLUMN type_multiple;
    ALTER TABLE bibliotheek_kenmerken RENAME COLUMN type_multiple_new to type_multiple;

    ALTER TABLE zaaktype_kenmerken ADD COLUMN is_group_new BOOLEAN NOT NULL DEFAULT FALSE;
    UPDATE zaaktype_kenmerken SET is_group_new = true where is_group = 1;
    ALTER TABLE zaaktype_kenmerken DROP COLUMN is_group;
    ALTER TABLE zaaktype_kenmerken RENAME COLUMN is_group_new to is_group;

    UPDATE zaaktype_regel SET is_group = false where is_group is null;
    ALTER TABLE zaaktype_regel ALTER COLUMN is_group SET NOT NULL;

    ALTER TABLE zaak_kenmerk ADD COLUMN attribute_values text[];
    ALTER TABLE zaak_kenmerk ALTER COLUMN value DROP NOT NULL;

    INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, attribute_values)
        SELECT zaak_id, bibliotheek_kenmerken_id, array_agg(value)
        FROM zaak_kenmerk
        GROUP BY
            zaak_id,
            bibliotheek_kenmerken_id
        ORDER BY zaak_id, bibliotheek_kenmerken_id
    ;

    DELETE FROM zaak_kenmerk WHERE value IS NOT NULL;
    ALTER TABLE zaak_kenmerk DROP COLUMN value;

    ALTER TABLE zaak_kenmerk ALTER COLUMN attribute_values SET NOT NULL;
    ALTER TABLE zaak_kenmerk RENAME COLUMN attribute_values TO value;

    CREATE UNIQUE INDEX zaak_kenmerk_bibliotheek_kenmerken_id ON zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id);

COMMIT;

VACUUM FULL zaak_kenmerk;
