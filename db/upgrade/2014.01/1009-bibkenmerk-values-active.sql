begin;
alter table bibliotheek_kenmerken_values add column active boolean not null default 't';
alter table bibliotheek_kenmerken_values add column sort_order serial;
alter table bibliotheek_kenmerken_values ADD CONSTRAINT sort_order_unique UNIQUE (sort_order, bibliotheek_kenmerken_id);
alter table bibliotheek_kenmerken add column version integer not null default 1;

alter table zaaktype_kenmerken add column version integer;
commit;