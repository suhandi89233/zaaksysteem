
BEGIN;

    CREATE TABLE rights (
        name TEXT PRIMARY KEY NOT NULL,
        description TEXT NOT NULL
    );

    CREATE TABLE role_rights (
        rights_name TEXT REFERENCES rights(name) NOT NULL,
        role_id integer REFERENCES roles(id) NOT NULL,
        PRIMARY KEY (rights_name, role_id)
    );

COMMIT;
