BEGIN;

  CREATE OR REPLACE FUNCTION get_subject_by_legacy_id(
    IN legacy_id TEXT,
    OUT subject_uuid uuid
  ) LANGUAGE plpgsql AS $$
    DECLARE
      betrokkene_id TEXT[];
      subject_type TEXT;
      subject_id INT;

    BEGIN
        betrokkene_id = string_to_array(legacy_id, '-');

        -- on our dev we have some weirdness going on
        -- unsure about live data
        IF array_length(betrokkene_id, 1) = 3
        THEN
          subject_type = betrokkene_id[2]::text;
          subject_id = betrokkene_id[3]::int;
        ELSE
          subject_type = betrokkene_id[4]::text;
          subject_id = betrokkene_id[5]::int;
        END IF;

        /* Employee or medewerker, work around a bug we have */
        IF subject_type = 'medewerker' OR subject_type = 'employee'
        THEN
          SELECT INTO subject_uuid uuid FROM subject where id = subject_id;
        ELSIF subject_type = 'natuurlijk_persoon'
        THEN
          SELECT INTO subject_uuid uuid FROM natuurlijk_persoon
          where id = subject_id;
        ELSIF subject_type = 'bedrijf'
        THEN
          SELECT INTO subject_uuid uuid FROM bedrijf where id = subject_id;
        END IF;

        RETURN;
    END $$;

  CREATE OR REPLACE FUNCTION get_subject_by_uuid(
    IN subject_uuid uuid,
    OUT subject_type TEXT
  ) LANGUAGE plpgsql AS $$
    DECLARE
      found uuid;
    BEGIN
          SELECT INTO found uuid FROM natuurlijk_persoon where uuid = subject_uuid;

          IF found IS NOT NULL
          THEN
            subject_type := 'person';
          END IF;

          SELECT INTO found uuid FROM bedrijf where uuid = subject_uuid;
          IF found is not null THEN
            subject_type := 'company';
          END IF;

          SELECT INTO found uuid FROM subject where uuid = subject_uuid
            AND subject.subject_type = 'employee';
          IF found is not null THEN
            subject_type := 'employee';
          END IF;

        RETURN;
    END $$;

  CREATE OR REPLACE FUNCTION generate_intials_for_np(
    IN name TEXT,
    OUT initials TEXT
  ) LANGUAGE plpgsql AS $$
    DECLARE
      names TEXT[];
      i int;
    BEGIN

      IF name is NULL or LENGTH(name) <1
      THEN
        initials := '';
        RETURN;
      END IF;

      names := regexp_split_to_array(name, '\s+');

      initials := substr(names[1], 1, 1);
      IF array_upper(names, 1) > 2
      THEN
        FOR i in 2 ..array_length(names, 1)
        LOOP
          initials := concat(initials, '.', substr(names[i], 1, 1));
        END LOOP;
      END IF;

      initials := UPPER(concat(initials, '.', ''));
      RETURN;

    END $$;

  CREATE OR REPLACE FUNCTION get_subject_display_name_by_uuid(
    IN subject_uuid uuid,
    OUT display_name TEXT
  ) LANGUAGE plpgsql AS $$
    DECLARE
      found record;
      initials text;
      surname text;
    BEGIN
      SELECT INTO found * FROM natuurlijk_persoon where uuid = subject_uuid;

      IF found.uuid IS NOT NULL
      THEN

        initials := found.voorletters;

    surname := found.naamgebruik;

    if surname is null
    then
        surname := found.geslachtsnaam;
        end if;

        if initials = ''
        then
          display_name := surname;
        else
          display_name := concat(initials, ' ', surname);
        end if;

        RETURN;
      END IF;

      SELECT INTO found * FROM bedrijf where uuid = subject_uuid;
      IF found.uuid is not null THEN
        display_name = found.handelsnaam;
        RETURN;
      END IF;

      SELECT INTO found * FROM subject where uuid = subject_uuid
        AND subject.subject_type = 'employee';
      IF found.uuid is not null THEN
        display_name := found.properties::jsonb->>'displayname';
        RETURN;
      END IF;

      RETURN;
    END $$;

COMMIT;

BEGIN;

  CREATE OR REPLACE FUNCTION fix_voorletters_for_people_persons() RETURNS void
  LANGUAGE plpgsql AS $$
  DECLARE
    r record;
    initials TEXT;

  BEGIN

      FOR r IN
        SELECT id, voornamen FROM natuurlijk_persoon WHERE authenticated = false
          AND deleted_on IS NULL ORDER by id
      LOOP
          RAISE NOTICE 'Updating voorletters id %', r.id;
          IF r.voornamen IS NULL
          THEN
            UPDATE natuurlijk_persoon SET voorletters = '', voornamen = '' where id = r.id;
          ELSE
            initials := generate_intials_for_np(r.voornamen);
            RAISE NOTICE 'Updating voorletters % of id %', initials, r.id;
            UPDATE natuurlijk_persoon SET voorletters = initials where id = r.id;
          END IF;

      END LOOP;
  END $$;

  SELECT fix_voorletters_for_people_persons();
  DROP FUNCTION fix_voorletters_for_people_persons();

  CREATE OR REPLACE FUNCTION update_name_cache_for_logging() RETURNS TRIGGER
  LANGUAGE 'plpgsql' AS $$
  DECLARE
    subject_uuid uuid;
    dn text;
  BEGIN
      IF NEW.created_by IS NOT NULL AND NEW.created_by_name_cache IS NULL
      THEN

        IF NEW.created_by NOT LIKE 'betrokkene-%'
        THEN
            NEW.created_by_name_cache := NEW.created_by;
            NEW.created_by := NULL;
            RETURN NEW;
        END IF;

        SELECT INTO subject_uuid get_subject_by_legacy_id(NEW.created_by);

        IF subject_uuid IS NULL
        THEN
            NEW.created_by_name_cache := '<onbekend>';
            RETURN NEW;
        END IF;

        SELECT INTO dn get_subject_display_name_by_uuid(subject_uuid);
        IF dn IS NOT NULL
        THEN
            NEW.created_by_name_cache := dn;
          RETURN NEW;
        END IF;
      END IF;

      RETURN NEW;
  END $$;

  DROP TRIGGER IF EXISTS update_name_cache_for_logging_trigger ON logging;
  CREATE TRIGGER update_name_cache_for_logging_trigger
  BEFORE UPDATE ON logging
  FOR EACH ROW
  EXECUTE PROCEDURE update_name_cache_for_logging();

  DROP TRIGGER IF EXISTS insert_name_cache_for_logging_trigger ON logging;
  CREATE TRIGGER insert_name_cache_for_logging_trigger
  BEFORE INSERT ON logging
  FOR EACH ROW
  EXECUTE PROCEDURE update_name_cache_for_logging();

  /* We don't need logging table updates for this */
  ALTER TABLE logging DISABLE trigger logging_update_trigger;

  UPDATE logging SET created_by_name_cache = NULL WHERE created_by IS NOT NULL
    AND created_by_name_cache IS NULL;

  ALTER TABLE logging ENABLE trigger logging_update_trigger;

COMMIT;

