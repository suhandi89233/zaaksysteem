BEGIN;

    ALTER table object_relation
        DROP CONSTRAINT
        IF EXISTS object_relation_object_id_fkey;
    ALTER TABLE object_relation
        ADD CONSTRAINT object_relation_object_id_fkey
        FOREIGN KEY (object_id)
        REFERENCES object_data(uuid)
        ON DELETE CASCADE DEFERRABLE;

    ALTER TABLE zaak
        DROP CONSTRAINT
        IF EXISTS zaak_object_data_uuid_fkey;
    ALTER TABLE zaak
        ADD CONSTRAINT zaak_object_data_uuid_fkey
        FOREIGN KEY (uuid) REFERENCES object_data(uuid)
        ON DELETE SET NULL;

COMMIT;
