BEGIN;

ALTER TABLE queue DROP CONSTRAINT "queue_status_check";
ALTER TABLE queue ADD CHECK (status IN ('pending', 'running', 'finished', 'failed', 'waiting'));

COMMIT;
