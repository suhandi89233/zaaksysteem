BEGIN;

    UPDATE subject SET settings = jsonb_set(
        settings::jsonb,
        '{notifications}',
        '{"new_assigned_case":true,"new_document":true,"new_ext_pip_message":true,"case_term_exceeded":true,"new_attribute_proposal":true,"case_suspension_term_exceeded":true}'
    ) WHERE subject_type = 'employee';

COMMIT;
