BEGIN;

ALTER TABLE transaction_record ADD COLUMN preview_string VARCHAR(200);

COMMIT;