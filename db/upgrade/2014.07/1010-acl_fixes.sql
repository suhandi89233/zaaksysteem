BEGIN;

ALTER TABLE object_acl_entry DROP COLUMN verdict;
ALTER TABLE object_acl_entry ADD COLUMN scope TEXT NOT NULL DEFAULT 'instance' CHECK (scope IN ('instance', 'type'));

-- add constraint such that empty-string groupnames are not allowed to avoid confusion with NULL values
ALTER TABLE object_acl_entry ADD COLUMN groupname TEXT NULL CHECK (groupname IS NULL OR character_length(groupname) > 0);

ALTER TABLE object_data ADD COLUMN acl_groupname TEXT NULL CHECK (acl_groupname IS NULL OR character_length(acl_groupname) > 0);

COMMIT;
