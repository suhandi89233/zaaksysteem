BEGIN;

CREATE TABLE public.thread_message_attachment (
	id SERIAL PRIMARY KEY,
	filestore_id integer NOT NULL REFERENCES public.filestore(id),
	thread_message_id integer NOT NULL REFERENCES public.thread_message(id)
);
	
CREATE TABLE public.thread_message_attachment_derivative (
	id SERIAL PRIMARY KEY,
	thread_message_attachment_id integer NOT NULL REFERENCES public.thread_message_attachment(id),
	filestore_id integer NOT NULL REFERENCES public.filestore(id),
	max_width integer NOT NULL, 
	max_height integer  NOT NULL, 
	date_generated timestamp without time zone NOT NULL DEFAULT now(),
	type text NOT NULL, 
	CONSTRAINT file_derivative_type CHECK (type IN ('pdf', 'thumbnail', 'doc', 'docx'))
);

COMMIT;