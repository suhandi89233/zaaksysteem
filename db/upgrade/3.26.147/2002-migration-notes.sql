BEGIN;

  CREATE OR REPLACE FUNCTION migrate_notes() RETURNS void LANGUAGE plpgsql AS $$
  DECLARE
    r record;
    h hstore;
    thread_id int;
    thread_note_id int;
    thread_msg_id int;
  BEGIN
        FOR r IN
          SELECT
            event_type event_type,
            event_data event_data,
            zaak_id case_id,
            substr(jsonb(event_data)->>'content', 0, 180) message_slug,
            /* Check on baarn/epe to see if we have this in use
            betrokkene_id,
            */
            get_subject_by_legacy_id(created_by) created_by,
            get_subject_by_legacy_id(created_for) created_for,
            created_by_name_cache created_by_displayname,
            created date_created,
            id id
          FROM logging
          WHERE
            event_type IN ('subject/note/create', 'case/note/create')
            -- TODO: subject/contactmoment/create
          ORDER BY id asc
        LOOP

          RAISE NOTICE 'Migrating note with id %', r.id;
          h := hstore(r);


          SELECT INTO thread_id migrate_note_to_thread(h);
          RAISE NOTICE 'Created thread with id %', thread_id;

          SELECT INTO thread_note_id migrate_note_to_thread_message_type(h);
          RAISE NOTICE 'Created note with id %', thread_note_id;

          SELECT INTO thread_msg_id migrate_note_to_thread_message(thread_id, thread_note_id, h);
          RAISE NOTICE 'Created thread message with id %', thread_msg_id;

        END LOOP;
        RETURN;

  END $$;

  CREATE OR REPLACE FUNCTION migrate_note_to_thread(
    IN note hstore,
    OUT thread_id int
  ) LANGUAGE plpgsql AS $$
  DECLARE
    case_id int;
    dn TEXT;
    msg_jsonb jsonb;
    s_uuid uuid;
    mytime TIMESTAMP;
    created_by TEXT;
  BEGIN

    IF note->'case_id' IS NOT NULL
    THEN
      case_id := note->'case_id';
    END IF;

    IF note->'event_type' = 'subject/note/create'
    THEN
      s_uuid = uuid(note->'created_for');
      SELECT INTO dn get_subject_display_name_by_uuid(s_uuid);
    END IF;

    IF dn IS NULL
    THEN
      dn = '';
    END IF;

    mytime := CAST(note->'date_created' AS TIMESTAMP);

    msg_jsonb := json_build_object(
      'message_type', 'note',
      'slug', note->'message_slug',
      'created_name', note->'created_by_displayname',
      'created', to_char(mytime, 'IYYY-MM-DD"T"HH24:MI:SS+00:00')
    );

    INSERT INTO thread(
      contact_uuid,
      contact_displayname,
      case_id,
      created,
      last_modified,
      thread_type,
      last_message_cache
    )
    VALUES (
      s_uuid,
      dn,
      case_id,
      mytime,
      mytime,
      'note',
      msg_jsonb::text
    )
    RETURNING id INTO thread_id;

    RETURN;
  END $$;

  CREATE OR REPLACE FUNCTION migrate_note_to_thread_message_type(
    IN note hstore,
    OUT note_id int
  ) LANGUAGE plpgsql as $$
  DECLARE
    msg TEXT;
  BEGIN

    msg := jsonb(note->'event_data')->>'content';
    IF msg IS NULL
    THEN
      msg = '';
    END IF;

    INSERT INTO thread_message_note(content) VALUES (msg)
    RETURNING id INTO note_id;

  END $$;

  CREATE OR REPLACE FUNCTION migrate_note_to_thread_message(
    IN thread_id int,
    IN thread_msg_id int,
    IN note hstore,
    OUT created_id int
  ) LANGUAGE plpgsql AS $$
  DECLARE
    dn TEXT;
    s_uuid uuid;
    case_id int;
    slug TEXT;
  BEGIN

    IF note->'created_by' IS NOT NULL
    THEN
      s_uuid := uuid(note->'created_by');
      SELECT INTO dn get_subject_display_name_by_uuid(s_uuid);
    ELSE
      /* Admin user plus fake display name */
      SELECT INTO s_uuid uuid from subject where username ='admin' limit 1;
      dn := 'Unknown user due to datamigration';

    END IF;

    if note->'message_slug' IS NOT NULL
    THEN
      slug := note->'message_slug';
    ELSE
      slug := '';
    END IF;

    INSERT INTO thread_message (
      thread_id,
      type,
      message_slug,
      created_by_uuid,
      created_by_displayname,
      created,
      last_modified,
      thread_message_note_id
    )
    VALUES (
      thread_id,
      'note',
      slug,
      s_uuid,
      dn,
      CAST(note->'date_created' AS TIMESTAMP),
      CAST(note->'date_created' AS TIMESTAMP),
      thread_msg_id
    )
    RETURNING id INTO created_id;

    RETURN;
  END $$;

  ALTER TABLE thread DISABLE trigger thread_insert_trigger;
  ALTER TABLE thread_message DISABLE trigger thread_message_insert_trigger;

  SELECT migrate_notes();

  ALTER TABLE thread ENABLE trigger thread_insert_trigger;
  ALTER TABLE thread_message ENABLE trigger thread_message_insert_trigger;

  DROP FUNCTION migrate_notes();
  DROP FUNCTION migrate_note_to_thread(hstore);
  DROP FUNCTION migrate_note_to_thread_message_type(hstore);
  DROP FUNCTION migrate_note_to_thread_message(int, int, hstore);

COMMIT;
