BEGIN;

    /* db/upgrade/2015.01/1010-acls.sql */
    CREATE TABLE groups (
        id INTEGER PRIMARY KEY NOT NULL,
        path INTEGER[] NOT NULL,
        name TEXT,
        description TEXT,
        date_created timestamp without time zone,
        date_modified timestamp without time zone
    );

    CREATE SEQUENCE groups_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;

    ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


    CREATE TABLE roles (
        id SERIAL PRIMARY KEY NOT NULL,
        parent_group_id INTEGER REFERENCES groups(id),
        name TEXT,
        description TEXT,
        system_role BOOLEAN,
        date_created timestamp without time zone,
        date_modified timestamp without time zone
    );

    ALTER TABLE subject ADD COLUMN role_ids INTEGER[];
    ALTER TABLE subject ADD COLUMN group_ids INTEGER[];


    /* db/upgrade/2015.01/1011-password_in_user_entity.sql */
    ALTER TABLE user_entity ADD COLUMN password character varying(255);

    /* db/upgrade/2015.01/1012-zaak_onderwerp_extern.sql */
    ALTER TABLE zaak ADD COLUMN onderwerp_extern TEXT;
    ALTER TABLE zaaktype_definitie ADD COLUMN extra_informatie_extern TEXT;

    ALTER TABLE config ALTER COLUMN value TYPE TEXT;
    ALTER TABLE config ALTER COLUMN value TYPE TEXT;

    INSERT INTO config (parameter, advanced)
        SELECT
            'new_user_template', TRUE
        WHERE NOT EXISTS (
            SELECT * FROM CONFIG WHERE parameter = 'new_user_template'
        );

    /* db/upgrade/2015.01/1012-new_user_template.sql */
    INSERT INTO config (parameter, value, advanced)
        SELECT
            'first_login_intro', 'Welkom!

Voordat je gebruik kan maken van zaaksysteem, zal je afdeling goedgekeurd moeten worden door de gebruikersbeheerder van je gemeente. Dit kan je handig doen door hieronder de gewenste afdeling te selecteren alvorens op volgende te klikken.

Zodra je afdeling is goedgekeurd door de gebruikersbeheerder, ontvang je een e-mail ter bevestiging en kan je gebruikmaken van zaaksysteem.', TRUE
        WHERE NOT EXISTS (
            SELECT * FROM CONFIG WHERE parameter = 'first_login_intro'
        );

    INSERT INTO config (parameter, value, advanced)
        SELECT
            'first_login_confirmation', 'Bedankt!

Zojuist hebben wij je aangemeld bij de gebruikersbeheerder, welke jouw gegevens en je afdeling zal controleren. Dit kan enige tijd in beslag nemen, dus wij zullen je een e-mailtje versturen zodra de gebruikersbeheerder je aanmelding heeft geaccepteerd.', TRUE
        WHERE NOT EXISTS (
            SELECT * FROM CONFIG WHERE parameter = 'first_login_confirmation'
        );

    -- Dit is veilig, column type is nu varchar, casten naar text werkt altijd.
    ALTER TABLE zaak ALTER COLUMN onderwerp TYPE TEXT USING onderwerp::TEXT;
    ALTER TABLE zaaktype_definitie ALTER COLUMN extra_informatie TYPE TEXT USING extra_informatie::TEXT;

    /* db/upgrade/2015.01/1013-subject_authorization.sql */
    ALTER TABLE zaak_betrokkenen ADD COLUMN pip_authorized BOOLEAN DEFAULT FALSE NOT NULL;

    /* db/upgrade/2015.01/1014-subject_uuid.sql */
    ALTER TABLE zaak_betrokkenen ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();

    /* db/upgrade/2015.01/1015-subject_authorization_template.sql */
    INSERT INTO config (parameter, value) VALUES ('subject_pip_authorization_confirmation_template_id', NULL);

    /* db/upgrade/2015.01/1020-file-status.sql */
    CREATE TYPE documentstatus AS ENUM (
        'original',
        'copy',
        'replaced',
        'converted'
    );

    ALTER TABLE
        file
    ADD COLUMN
        document_status
        documentstatus
    DEFAULT
        'original'::documentstatus
    NOT NULL;

    /* db/upgrade/2015.01/1030-zaaktype_node_flags.sql */
    ALTER TABLE zaaktype_node DROP IF EXISTS contact_info_email_required;
    ALTER TABLE zaaktype_node DROP IF EXISTS contact_info_phone_required;
    ALTER TABLE zaaktype_node DROP IF EXISTS contact_info_mobile_phone_required;

    ALTER TABLE zaaktype_node ADD contact_info_email_required BOOLEAN DEFAULT FALSE;
    ALTER TABLE zaaktype_node ADD contact_info_phone_required BOOLEAN DEFAULT FALSE;
    ALTER TABLE zaaktype_node ADD contact_info_mobile_phone_required BOOLEAN DEFAULT FALSE;

    -- Backwards compatibility
    UPDATE zaaktype_node SET contact_info_email_required = true;

    /* db/upgrade/2015.01/1040-zaaktype_relaties.sql */
    ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_authorized BOOLEAN;
    ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_notify BOOLEAN;
    ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_id TEXT;
    ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_role TEXT;
    ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_role_set TEXT;
    ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_prefix TEXT;
    ALTER TABLE zaaktype_relatie ADD COLUMN eigenaar_id TEXT;

COMMIT;
