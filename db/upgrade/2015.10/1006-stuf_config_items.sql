BEGIN;
    
    INSERT INTO config (parameter, value, advanced) VALUES ('disable_stuf_client_certificate_checks', '0', true);

COMMIT;
