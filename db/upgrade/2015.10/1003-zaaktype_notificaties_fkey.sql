BEGIN;

ALTER TABLE zaaktype_notificatie ADD CONSTRAINT zaaktype_notificatie_bibliotheek_notificaties_id_fkey FOREIGN KEY (bibliotheek_notificaties_id) REFERENCES bibliotheek_notificaties(id);

COMMIT;
