BEGIN;

ALTER TABLE object_data DROP COLUMN IF EXISTS stringification;

ALTER TABLE object_data ADD COLUMN text_vector tsvector;

CREATE INDEX object_text_vector_idx ON object_data USING gin(text_vector);

COMMIT;
