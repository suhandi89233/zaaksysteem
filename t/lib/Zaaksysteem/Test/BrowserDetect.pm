package Zaaksysteem::Test::BrowserDetect;

use Zaaksysteem::Test;
use Zaaksysteem::BrowserDetect;

sub test_setup {
    my $test = shift;

    my $test_method = $test->test_report->current_method->name;

    # Version 1.53 is minimaly required for testing IE9
    eval "use HTTP::BrowserDetect 1.53;";

    if ($@) {
        if ($test_method =~ /^test_browser_agent_ie_higher|test_browser_agent_ie_no_version/) {
            $test->test_skip("Skipping $test_method: HTTP::BrowserDetect version not at 1.53");
        }
    };

    # Version 1.63 is minimaly required for testing IE11
    eval "use HTTP::BrowserDetect 1.63;";
    if ($@) {
        if ($test_method eq 'test_supported_browsers') {
            $test->test_skip("Skipping $test_method: HTTP::BrowserDetect version not at 1.63");
        }
    };
}

sub test_unsupported_browsers {
    my $self = shift;

    my @unsupported_browsers = (
        # IE7
        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)",

        # IE9
        "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",

        # IE10
        "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 7.0; InfoPath.3; .NET CLR 3.1.40767; Trident/6.0; en-IN)",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)",
        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/4.0; InfoPath.2; SV1; .NET CLR 2.0.50727; WOW64)",
        "Mozilla/5.0 (compatible; MSIE 10.0; Macintosh; Intel Mac OS X 10_7_3; Trident/6.0)",
        "Mozilla/4.0 (Compatible; MSIE 8.0; Windows NT 5.2; Trident/6.0)",
        "Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)",
        "Mozilla/1.22 (compatible; MSIE 10.0; Windows 3.1)",

);
    my $b = Zaaksysteem::BrowserDetect->new(useragent => '');

    foreach my $ua (@unsupported_browsers) {
        $b->useragent($ua);
        ok(!$b->check(), "'$ua' is not supported");
        throws_ok(
            sub { $b->assert },
            qr#unsupported/browser/version_too_low:#,
            "User agent string '$ua' is not supported",
        );
    }

}

sub test_supported_browsers {
    my $self = shift;

    my @supported_browsers = (
        # IE 11
        "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko",
        "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
    );

    my $b = Zaaksysteem::BrowserDetect->new(useragent => '');

    foreach my $ua (@supported_browsers) {
        $b->useragent($ua);
        ok($b->check(), "'$ua' is supported");
        lives_ok(
            sub { $b->assert },
            "User agent string '$ua' is supported",
        );
    }
}

sub test_browser_agent_ie_higher {
    my $b = Zaaksysteem::BrowserDetect->new(
        useragent =>
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
        denied => [qw(
            ie::>8
        )],
    );
    throws_ok(
        sub { $b->assert },
        qr#unsupported/browser/version_too_high:#,
        "ie::>8"
    );
}

sub test_browser_agent_ie_no_version {
    my $b = Zaaksysteem::BrowserDetect->new(
        useragent =>
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
        denied => [qw(
            ie::9
        )],
    );
    throws_ok(
        sub { $b->assert },
        qr#unsupported/browser#,
        "ie::9"
    );
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::BrowserDetect - Test ZS::BrowserDetect

=head1 DESCRIPTION

Test browser detection

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::BrowserDetect

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
