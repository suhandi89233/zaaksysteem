package Zaaksysteem::Test::Zaaktypen::DateTimeWrapper;

use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::Zaaktypen::DateTimeWrapper - Test Zaaksysteem::Zaaktypen::DateTimeWrapper

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaaktypen::DateTimeWrapper

=cut

use DateTime;
use Zaaksysteem::Test;
use Zaaksysteem::Zaaktypen::DateTimeWrapper;

sub test_datetimewrapper {
    # 1.21 Jigawatts!?
    my $dt = DateTime->new(
        year      => 1955,
        month     => 11,
        day       => 12,
        hour      => 22,
        minute    => 4,
        second    => 0,
        time_zone => 'US/Pacific',
    );

    my $wrapper = Zaaksysteem::Zaaktypen::DateTimeWrapper->new_from_datetime($dt);

    is($wrapper->{utc_rd_days}, 714000, "UTC RD Days extracted correctly");
    is($wrapper->{utc_rd_secs}, 21840,  "UTC RD Seconds extracted correctly");

    my $dt2 = DateTime->from_object(object => $wrapper);
    is($dt, $dt2, "DateTime created from wrapper represents the same moment");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
