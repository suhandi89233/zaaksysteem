package Zaaksysteem::Test::StUF::ZKN;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::StUF::ZKN - Test the plugin modules of ZS::StUF::ZKN

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::StUF::ZKN

=cut

use UUID::Tiny ':std';
use IO::All;

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Test::XML qw(:all);

use BTTW::Tools qw(dump_terse);
use Data::Random::NL qw(generate_bsn);

use Zaaksysteem::XML::Generator::StUF0310;
use Zaaksysteem::StUF::0310::Processor;

sub _get_model {
    my $model = Zaaksysteem::StUF::0310::Processor->new(
        betrokkene_model  => mock_one(),
        object_model      => mock_one(),
        record            => mock_one(),
        schema            => mock_one(),
        interface         => mock_one(),
        municipality_code => '1332',
        @_,
    );
}

sub test_get_natuurlijk_persoon {

    my $model = _get_model();

    my $bsn = generate_bsn(9);
    lives_ok(
        sub {

            my $override
                = override(
                'Zaaksysteem::StUF::0310::Processor::_assert_stuf_interface'
                    => sub { mock_one(id => 42) });
            $override->replace(
                'Zaaksysteem::StUF::0310::Processor::_find_bsn' =>
                    sub { return });
            $override->replace(
                'Zaaksysteem::BR::Subject::search' => sub { return 1 });
            $override->replace(
                'Zaaksysteem::BR::Subject::remote_import' => sub { return 1 }
            );

            $model->_get_natuurlijk_persoon($bsn);
        },
        '_get_natuurlijk_persoon works fine'
    );

}

sub test__import_from_kvkapi {

    my $model = _get_model();

    my $company = undef;
    my $expected_company = 'My Company coop';

    my $override = override(
        'Zaaksysteem::StUF::0310::Processor::_assert_kvkapi_interface' => sub {
            mock_one(id => 42);
        }
    );

    my %args;
    $override->replace(
        'Zaaksysteem::BR::Subject::search' => sub {
            my $self = shift;
            my $val = shift;
            %args = %$val;
            return ('foo');
        }
    );
    $override->replace('Zaaksysteem::BR::Subject::remote_import' =>
            sub { return $expected_company });

    $company = $model->_import_from_kvkapi(location_number => '12345678');

    cmp_deeply(
        \%args,
        {
            subject_type                  => 'company',
            'subject.coc_location_number' => '12345678'
        },
        "Search to bridge has correct arguments for location number"
    );

    is ($company, $expected_company, ".. and returned the correct company");

    $company = $model->_import_from_kvkapi(company_number => '12345678');
    cmp_deeply(
        \%args,
        {
            subject_type         => 'company',
            'subject.coc_number' => '12345678'
        },
        "Search to bridge has correct arguments for company number"
    );
    is ($company, $expected_company, "Got the right company back");

}

sub test_Fo03 {

    my $model = _get_model();

    my $xml_schema = XML::Compile::Schema->new(
        [

            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/0301/stuf0301mtom.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_ent_basis.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_simpleTypes.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/gml/bag-gml.xsd
                share/wsdl/stuf/xlink/xlinks.xsd
                share/wsdl/stuf/xmlmime/xmlmime.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_bg0310_ent.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_ent_basis.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_ent_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_stuf_mutatie.xsd
            )
        ]
    );

    my $xml = $model->generate_error(
        type          => 'Fo03',
        exception     => "Foo",
        stuurgegevens => {
            receiver  => 'ZS Testsuite',
            sender    => 'Developer',
            reference => 666,
        },
        reference_id => 42,
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/StUF0301',
        type      => 'Fo03Bericht',
        testname  => 'Craft Fo03 message',
    );
}

sub test_fo02 {
    my $model = _get_model();

    my $xml_schema = XML::Compile::Schema->new(
        [

            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/0301/stuf0301mtom.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_ent_basis.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_simpleTypes.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/gml/bag-gml.xsd
                share/wsdl/stuf/xlink/xlinks.xsd
                share/wsdl/stuf/xmlmime/xmlmime.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_bg0310_ent.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_ent_basis.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_ent_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_stuf_mutatie.xsd
            )
        ]
    );

    my $xml = $model->generate_error(
        type          => 'Fo02',
        exception     => "Foo",
        stuurgegevens => {
            receiver  => 'ZS Testsuite',
            sender    => 'Developer',
            reference => 666,
        },
        reference_id => 42,
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/StUF0301',
        type      => 'Fo02Bericht',
        testname  => 'Craft Fo02 message',
    );
}

sub _mock_file {
    state $_file_id = 0;

    $_file_id++;

    return mock_strict(
        created_by => undef,
        date_created => DateTime->now->clone->subtract(days => 100),
        extension  => '.bar',
        id         => $_file_id,
        name       => "testsuite file $_file_id",
        filename   => 'bar.bar',
        version    => 42,
        document_status => 'original',
        get_column => sub {
            my $val = shift;
            if ($val eq 'case_id') {
                return 42;
            }
            return;
        },
        filestore_id => {
            content  => 'bar',
            mimetype => "foo/bar",
        },
        metadata_id => {
            trust_level       => 'Zaakvertrouwelijk',
            origin            => 'Inkomend',
            origin_date       => DateTime->now(),
            document_category => 'Aangifte',
            creation_date     => DateTime->now(),
            description       => "Hop",
        },
    );
}

sub _validate_edcLk01 {

    my ($model, $file, $message) = @_;


    my $result = $model->_format_document_full($file);
    my $data   = _test_case_data();

    $data->{stuurgegevens}{entiteittype} = 'EDC';
    $data->{object}[0] = $result;

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $xml = $generator->write_case_document(writer => $data);

    my $xml_schema = _get_xml_schema();
    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'edcLk01',
        testname  => $message,
    );
    return $xml;
}

sub test_write_case_document_edcLk01 {

    my $model = _get_model();
    my $file = _mock_file;

    my $result = $model->format_case_document_minimal($file);
    my $want = {
        identificatie     => { _ => '0000' . $file->id },
        beschrijving    => "",
        isRelevantVoor => { gerelateerde => { identificatie => '000042' }, },
        beschrijving   => { _ => $file->metadata_id->description // '' },
        naam           => { _ => $file->name },
        titel          => { _ => $file->name },
        formaat        => { _ => $file->extension },
        versie         => { _ => $file->version },
        status         => { _ => $file->document_status },
        omschrijving   => { _ => $file->metadata_id->document_category },

        taal => ignore(),

        vertrouwelijkAanduiding => ignore(),
        creatiedatum            => ignore(),
        ontvangstdatum          => ignore(),

        auteur => "Onbekend"
    };

    cmp_deeply($result, $want, '_format_case_document_minimal');

    # Adds inhoud things needed for write_case_document();
    my $xml = _validate_edcLk01($model, $file, "Inkomend document");
    my $xp = _xpath_from_xml($xml);
    my $path = '/ZKN:edcLk01/ZKN:object';
    my @nodes = $xp->findnodes($path);
    is(@nodes, 1, "Found one document node");

    my %want_document = (
        'dct.omschrijving' => 'Aangifte',
        creatiedatum       => ignore(),     # DateTime object
        ontvangstdatum     => ignore(),     # DateTime object
        titel              => ignore(),     # titel with generate ID,
        taal               => 'Nederlands',
        versie             => 42,
        status             => 'original',
        auteur             => 'Onbekend',
        vertrouwelijkAanduiding => 'ZAAKVERTROUWELIJK',
    );

    $result = xpath_document_properties($nodes[0], \%want_document);
    like($result->{titel}, qr/^testsuite file \d+$/, "... with a document title");
    like($result->{creatiedatum}, qr/^[0-9]{8}$/, "... and correct creation date");
    like($result->{ontvangstdatum}, qr/^[0-9]{8}$/, "... and a correct receive date");

    $file->{metadata_id}{origin} = 'Uitgaand';
    $result = $model->format_case_document_minimal($file);
    $want->{verzenddatum} = delete $want->{ontvangstdatum};
    $want_document{verzenddatum} = delete $want_document{ontvangstdatum};

    cmp_deeply($result, $want, '_format_case_document_minimal outgoing');
    $xml = _validate_edcLk01($model, $file, "Uitgaand document");

    $xp = _xpath_from_xml($xml);
    $path = '/ZKN:edcLk01/ZKN:object';
    @nodes = $xp->findnodes($path);
    is(@nodes, 1, "Found one document node");

    $result = xpath_document_properties($nodes[0], \%want_document);
    like($result->{titel}, qr/^testsuite file \d+$/, "... with a document title");
    like($result->{creatiedatum}, qr/^[0-9]{8}$/, "... and correct creation date");
    like($result->{verzenddatum}, qr/^[0-9]{8}$/, "... and a correct sent date");

}

sub _xpath_from_xml {
    my $xml   = shift;
    my $xpath = get_xpath($xml);
    $xpath->registerNs('ZKN',  'http://www.egem.nl/StUF/sector/zkn/0310');
    $xpath->registerNs('StUF', 'http://www.egem.nl/StUF/StUF0301');
    $xpath->registerNs('BG',   'http://www.egem.nl/StUF/sector/bg/0310');
    $xpath->registerNs('gml',  'http://www.opengis.net/gml');
    return $xpath;
}

sub xpath_document_properties {
    my ($node, $expect) = @_;

    my %result = ();
    foreach (keys %$expect) {
        $result{$_} = $node->findvalue("//ZKN:$_") // undef;
    }

    cmp_deeply(\%result, $expect,
        "... where the XML contains all the relevant keys: "
            . join(", ", keys %$expect)
    );

    return \%result;
}

sub test_case_response {
    my $model = _get_model();

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $now       = DateTime->now();
    my $start     = $now->clone->subtract(days => 7);
    my $end       = $now->clone->add(days => 14);

    my $case = mock_case(
        'X-Mock-Strict' => 1,
        afhandeldatum       => undef,
        streefafhandeldatum => $end,
        registratiedatum    => $start,
        vernietigingsdatum  => $end->clone->add(years => 2),
        get_column          => sub {
            my $val = shift;
            if ($val eq 'pid') {
                return 0;
            }
            return 1;
        },
        zaak_pids        => { count => 0 },
        zaaktype_node_id => {
            code        => "ZTN ID code",
            titel       => "ZTN titel",
            created     => $start->clone->subtract(years => 2),
            toelichting => "Toelichting",
        },
        get_zaaktype_result => undef,
        onderwerp           => "Some test we are",
        active_files        => { search_rs => undef },
    );

    my $override
        = override('Zaaksysteem::StUF::0310::Processor::_format_case_status',
        sub { return (heeft => []) });

    $override->override('Zaaksysteem::StUF::0310::Processor::_format_case_betrokkenen',
        sub { return () });

    my $file = _mock_file();
    $override->override(
        'Zaaksysteem::StUF::0310::Processor::_format_case_documents',
        sub {
            return (
                heeftRelevant => [
                    {
                        gerelateerde =>
                            $model->format_case_document_minimal($file),
                    }
                ],
            );

        }
    );

    my $case_object = $model->format_case($case);

    my $xml;
    lives_ok(
        sub {
            $xml = $generator->case_response(
                'writer',
                {
                    stuurgegevens => {
                        zender           => { applicatie => "Zaaksysteem", },
                        ontvanger        => { applicatie => "Zaaksysteem", },
                        berichtcode      => 'La01',
                        entiteittype     => 'ZAK',
                        referentienummer => '',
                        tijdstipBericht  => $now->strftime('%Y%m%d%H%M%S%03N'),
                    },
                    antwoord   => { object                => $case_object },
                    parameters => { indicatorVervolgvraag => 'false' },
                }
            );

        },
        "case_response works as a charm"
    );

    my $xpath = _xpath_from_xml($xml);

    my $path = '/ZKN:zakLa01/ZKN:antwoord/ZKN:object/ZKN:heeftRelevant';
    my @nodes = $xpath->findnodes($path);
    is(@nodes, 1, "... and we found a document for the case!");
    my $node = $nodes[0];

    my %want = (
        'dct.omschrijving'      => 'Aangifte',
        creatiedatum            => ignore(), # DateTime object
        ontvangstdatum          => ignore(), # DateTime object
        titel                   => ignore(), # titel with generate ID,
        taal                    => 'Nederlands',
        versie                  => 42,
        status                  => 'original',
        vertrouwelijkAanduiding => 'ZAAKVERTROUWELIJK',
        auteur                  => 'Onbekend',
    );

    my $result = xpath_document_properties($node, \%want);
    like($result->{titel}, qr/^testsuite file \d+$/, "... with a document title");
    like($result->{creatiedatum}, qr/^[0-9]{8}$/, "... and correct creation date");
    like($result->{ontvangstdatum}, qr/^[0-9]{8}$/, "... and a correct receive date");

    # Outgoing file type
    $file->{metadata_id}{origin} = 'Uitgaand';
    $want{verzenddatum} = delete $want{ontvangstdatum};

    $case_object = $model->format_case($case);
    lives_ok(
        sub {
            $xml = $generator->case_response(
                'writer',
                {
                    stuurgegevens => {
                        zender           => { applicatie => "Zaaksysteem", },
                        ontvanger        => { applicatie => "Zaaksysteem", },
                        berichtcode      => 'La01',
                        entiteittype     => 'ZAK',
                        referentienummer => '',
                        tijdstipBericht  => $now->strftime('%Y%m%d%H%M%S%03N'),
                    },
                    antwoord   => { object                => $case_object },
                    parameters => { indicatorVervolgvraag => 'false' },
                }
            );

        },
        "case_response works as a charm for outgoing files"
    );

    $xpath = _xpath_from_xml($xml);
    @nodes = $xpath->findnodes($path);
    is(@nodes, 1, "... and the document is found again");
    $node = $nodes[0];

    $result = xpath_document_properties($node, \%want);
    like($result->{titel}, qr/^testsuite file \d+$/, "... with a document title");
    like($result->{creatiedatum}, qr/^[0-9]{8}$/, "... and correct creation date");
    like($result->{verzenddatum}, qr/^[0-9]{8}$/, "... date sent, present");

}

sub _test_case_data {
    my $bsn       = generate_bsn(9);
    my $case      = mock_case();

    my $now  = DateTime->now();
    return {
        stuurgegevens => {
            zender           => { applicatie => "Zaaksysteem", },
            ontvanger        => { applicatie => "Zaaksysteem", },
            berichtcode      => 'Lk01',
            entiteittype     => 'ZAK',
            referentienummer => '',
            tijdstipBericht => $now->strftime('%Y%m%d%H%M%S%03N'),
        },
        parameters => {
            mutatiesoort      => 'T',
            indicatorOvername => 'V',
        },
        object => [
            {
                identificatie => { _ => sprintf('%05d', $case->id) },
                omschrijving  => 'Foo',
                startdatum         => { _ => $now->strftime('%Y%m%d') },
                registratiedatum   => { _ => $now->strftime('%Y%m%d') },
                zaakniveau         => 1,
                deelzakenIndicatie => "N",

                isVan => {
                    entiteittype => 'ZAKZKT',
                    gerelateerde => {
                        entiteittype => 'ZKT',
                        omschrijving => 'Onbekend',
                        code         => 100,
                        ingangsdatum => { _ => '20160101' },
                    },
                },
                heeftAlsInitiator => {
                    gerelateerde =>
                        { natuurlijkPersoon => { 'inp.bsn' => $bsn, } },
                },
            },
        ],
    };
}

sub _get_xml_schema {

    return XML::Compile::Schema->new(
        [

            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/0301/stuf0301mtom.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_ent_basis.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_simpleTypes.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/gml/bag-gml.xsd
                share/wsdl/stuf/xlink/xlinks.xsd
                share/wsdl/stuf/xmlmime/xmlmime.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_bg0310_ent.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_ent_basis.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_ent_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_stuf_mutatie.xsd
            )
        ]
    );
}

sub test_write_case {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $xml = $generator->write_case(writer => _test_case_data());
    my $xml_schema = _get_xml_schema();

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'zakLk01',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}


sub test_write_case_as_company {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();

    my $data = _test_case_data();
    my $requestor = {
        vestiging => {
            handelsnaam      => "Foo",
            vestigingsNummer => 1234,
            dossierNummer    => 12345678,
        },
    };

    $data->{object}[0]{heeftAlsInitiator}{gerelateerde} = $requestor;

    my $xml = $generator->write_case(writer => $data);
    my $xml_schema = _get_xml_schema();
    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'zakLk01',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}

sub test_creeer_zaak_id {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $bsn       = generate_bsn(9);

    my $now  = DateTime->now();
    my $data = {
        stuurgegevens => {
            zender           => { applicatie => "Zaaksysteem", },
            ontvanger        => { applicatie => "Zaaksysteem", },
            berichtcode      => 'Di02',
            functie          => 'genereerZaakidentificatie',
            referentienummer => '',
            tijdstipBericht => $now->strftime('%Y%m%d%H%M%S%03N'),
        },
    };

    my $method = 'generate_case_id';

    my $xml = $generator->$method(writer => $data,);

    my $xml_schema = XML::Compile::Schema->new(
        [
            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/zkn0310/zs-dms/zkn0310_msg_stuf_zs-dms.xsd
                share/wsdl/stuf/zkn0310/zs-dms/zkn0310_msg_zs-dms.xsd
                )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'genereerZaakIdentificatie_Di02',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}

sub _get_xpath_soap_action_ok {
    my ($model, $xml, $soap_action) = @_;
    my ($xpc, @actions) = $model->get_xpath_and_soap_action($xml);

    is(
        $actions[0],
        $soap_action,
        "SOAP action is correct"
    );

    return $xpc;
}


sub test_write_case_document {

    my $model = _get_model();

    my $xml = io->catfile(
        qw(t data StUF 3.10 ZKN document-edcLk01.xml)
    )->slurp;

    my $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01');

    my %args = ();
    my $file = mock_one(
        update_metadata => sub {
            %args = %{ $_[0] };
            return 1;
        }
    );

    my $object = ($xpc->findnodes('ZKN:object'))[-1];
    $model->_update_metadata($xpc, $object, $file);

    cmp_deeply(
        \%args,
        {
            description       => '',
            document_category => 'Aangifte',
            creation_date     => ignore(),
            origin_date       => ignore(),
            origin            => "Uitgaand",
        },
        "Metadata is updated correctly with NEN compliant document categories",
    );

    {
        note "NEN non complaincy categories";
        my $xml = io->catfile(
            qw(t data StUF 3.10 ZKN document-edcLk01-nonNEN.xml)
        )->slurp;

        my $xpc = _get_xpath_soap_action_ok($model, $xml,
            '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01');

        my $object = ($xpc->findnodes('ZKN:object'))[-1];
        $model->_update_metadata($xpc, $object, $file);

        cmp_deeply(
            \%args,
            { description => '',
              creation_date => ignore(),
            },
            "Metadata is updated correctly with NEN non-compliant document categories",
        );
    }
}

sub test_write_case_document_lock {

    my $model = _get_model(
        schema    => mock_one(
            update_file => sub {
                my $args = shift;

                is($args->{is_restore}, 0, 'is_restore flag to update_file is 0');
                like($args->{original_name}, qr/Brief - Ontvangstbevestiging aanvraag\.(asc|txt)$/, 'Filename is bestandsnaam.(txt|asc)');
                is($args->{subject}, 31337, 'subject is passed in');
            },
            as_object => 31337,
        ),
    );

    my $xml = io->catfile(
        qw(t data StUF 3.10 ZKN updateZaakdocument_Di02.xml)
    )->slurp;

    my $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}updateZaakdocument_Di02');

    my $response = $model->write_case_document_lock($xpc);

    my $parsed_response = XML::LibXML->load_xml(string => $response);

    is(
        $parsed_response->documentElement->localname,
        'Bv02Bericht',
        'Return XML root element name is correct'
    );
    is(
        $parsed_response->documentElement->namespaceURI,
        'http://www.egem.nl/StUF/StUF0301',
        'Return XML root element namespace URI is correct'
    );

    $model = _get_model(
        schema    => mock_one(
            update_file => sub {
                my $args = shift;
                like($args->{original_name}, qr/Test dit contact\.(asc|txt)$/, 'Filename is test dit contact.(txt|asc)');
            },
            as_object => 31337,
        ),
    );

    $xml = io->catfile(
        qw(t data StUF 3.10 ZKN updateZaakdocument_Di02-multiples.xml)
    )->slurp;

    $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}updateZaakdocument_Di02');

    my $fh = File::Temp->new();
    my $override = override(
        'File::Temp::new' => sub {
            return $fh;
        }
    );

    $response = $model->write_case_document_lock($xpc);
    {
        open my $foo, '<', "$fh";
        local $/ = undef;
        is(<$foo>, 'bar', "File contents matches!");
    }
}

sub test_get_companies_from_xml {

    my $model = _get_model(
        schema => mock_strict(
            resultset => {
                search => sub {
                    my $ref = shift;
                    if ($ref->{vestigingsnummer}) {
                        return mock_strict(first => { id => 42 });
                    }
                    elsif ($ref->{dossiernummer}) {
                        return mock_strict(first => { id => 43 });
                    }
                    die "Not implemented";
                },
            }
        ),
    );

    my $xml = io->catfile(
        qw(t data StUF 3.10 ZKN ZakLk01-company-vestigingsnummer.xml)
    )->slurp;

    my $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}zakLk01');

    my @objects = $xpc->findnodes('ZKN:object');
    my $object = $objects[-1];

    my $res = $model->_get_requestor($xpc, $object);
    is($res, 'betrokkene-bedrijf-42', "Got company 42");

    {
        my $xml = io->catfile(
            qw(t data StUF 3.10 ZKN ZakLk01-company-kvk.xml)
        )->slurp;


        my $xpc = _get_xpath_soap_action_ok($model, $xml,
            '{http://www.egem.nl/StUF/sector/zkn/0310}zakLk01');

        my @objects = $xpc->findnodes('ZKN:object');
        my $object = $objects[-1];

        my $res = $model->_get_requestor($xpc, $object);
        is($res, 'betrokkene-bedrijf-43', "Got company 43");
    }

    {
        my $xml = io->catfile(
            qw(t data StUF 3.10 ZKN ZakLk01-company-centric.xml)
        )->slurp;


        my $xpc = _get_xpath_soap_action_ok($model, $xml,
            '{http://www.egem.nl/StUF/sector/zkn/0310}zakLk01');

        my @objects = $xpc->findnodes('ZKN:object');
        my $object = $objects[-1];

        my $res = $model->_get_requestor($xpc, $object);
        is($res, 'betrokkene-bedrijf-43', "Got kvknummer case insensitive");
    }
}


sub test_document_id_is_empty {

    my $model = _get_model();

    my $xml = io->catfile(
        qw(t data StUF 3.10 ZKN document-edcLk01-no-document_id.xml)
    )->slurp;

    my $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01');

    my @objects = $xpc->findnodes('ZKN:object');
    my $object = $objects[-1];

    throws_ok(
        sub {
            $model->_get_document_id_from_object($xpc, $object);
        },
        qr#stufzkn/document_id/missing: Document ID is missing from XML \(ZKN:identificatie\)#,
        "Failure is mandatory when document ID (ZKN:identificatie) is missing",
    );

    {

        my $xml = io->catfile(
            qw(t data StUF 3.10 ZKN document-edcLk01-document_id_zero.xml)
        )->slurp;

        my $xpc = _get_xpath_soap_action_ok($model, $xml,
            '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01');

        my @objects = $xpc->findnodes('ZKN:object');
        my $object = $objects[-1];

        throws_ok(
            sub {
                $model->_get_document_id_from_object($xpc, $object);
            },
            qr#stufzkn/document_id/missing: Document ID is missing from XML \(ZKN:identificatie\)#,
            "... and also document ID 0 is failure"
        );
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
