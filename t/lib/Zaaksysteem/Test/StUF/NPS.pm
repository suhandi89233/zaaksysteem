package Zaaksysteem::Test::StUF::NPS;
use Moose;

extends 'Zaaksysteem::Test::Moose';

use FindBin;
use IO::All;
use Zaaksysteem::StUF;
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::StUF::NPS - Test the plugin modules of ZS::StUF::NPS

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::StUF::NPS

=cut

sub test_binnen_gemeente {
    my $file = io->catfile($FindBin::Bin, qw(.. t data StUF 3.10 prs 102-Lk01.xml));
    my $object = Zaaksysteem::StUF->from_file("$file", '0310');

    ok(!$object->binnen_gemeente(666), 'Object is not in gemeente 666');
    ok($object->binnen_gemeente(484), 'Object is in gemeente 484');
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
