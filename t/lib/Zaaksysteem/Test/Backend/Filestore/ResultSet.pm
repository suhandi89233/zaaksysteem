package Zaaksysteem::Test::Backend::Filestore::ResultSet;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::Backend::Filestore::ResultSet - Test methods

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Backend::Filestore::ResultSet;

=head1 METHODS

=cut

use Zaaksysteem::Test;
use Test::Deep qw(any);
use Zaaksysteem::Test::Mocks;

use Zaaksysteem::Types qw/UUID/;

use Test::Mock::Two qw(one_called one_called_ok);
use Test::MockModule;
use Test::MockObject;
use Zaaksysteem::Backend::Filestore::ResultSet;

use BTTW::Tools;
use Path::Tiny;

my $TEMPFILE_OK    = _tempfile_ok();
my $TEMPFILE_EMPTY = _tempfile_empty();
my $TEMPFILE_VIRUS = _tempfile_virus(); # we don't care, see sub _tempfile_virus

my $TEMPFILE_SIZE  = 23;

my $MD5_HASH       = '6df23dc03f9b54cc38a0fc1483df6e21';
my $UUID_STRING    = '9fbf02dc-af5e-4e4e-9ca6-965dbb1134d7';

sub test_extract_username {
    my $separator = '-';
    my $subject = mock_one(betrokkene_identifier => sub { return 'natuurlijk-persoon-42' });

    my $mock_self = mock_one(
        result_source => sub {
            return mock_one(
                schema => sub {
                    return mock_one(
                        resultset => sub {
                            my $rs = shift;

                            if ($rs eq 'Config') {
                                return mock_one(
                                    get => sub { return $separator }
                                );
                            } elsif ($rs eq 'Subject') {
                                return mock_one(
                                    search_active => sub {
                                        return mock_one(
                                            first => sub {
                                                return $subject;
                                            }
                                        );
                                    }
                                );
                            } else {
                                die "Unknown resultset requested: '$rs'";
                            }
                        }
                    );
                }
            );
        }
    );

    my %tests = (
        'piet.friet-test.pdf' => {
            username => 'piet.friet',
            subject  => 'natuurlijk-persoon-42',
            filename => 'test.pdf',
        },
        'pietfriet-test.pdf' => {
            username => 'pietfriet',
            subject  => 'natuurlijk-persoon-42',
            filename => 'test.pdf',
        },
        'pietfriet-2-test.pdf' => {
            username => 'pietfriet',
            subject  => 'natuurlijk-persoon-42',
            filename => '2-test.pdf',
        },
        'piet.friet-2-test.pdf' => {
            username => 'piet.friet',
            subject  => 'natuurlijk-persoon-42',
            filename => '2-test.pdf',
        },
        'ad.patat.pdf' => undef,
    );

    for my $test (keys %tests) {
        my $rv = Zaaksysteem::Backend::File::ResultSet::extract_username(
            $mock_self,
            $test,
        );
        cmp_deeply($rv, $tests{$test}, "File named '$test' parsed correctly");
    }

    $subject = undef;
    my $rv = Zaaksysteem::Backend::File::ResultSet::extract_username(
        $mock_self,
        'piet.friet-test.pdf',
    );
    cmp_deeply($rv, undef, "If no subject exists, nothing 'extracted' is returned");
}

=head2 test_filestore_create

A heavilly mocked test for this method ... maybe this indicates a challenge for
refactoring?

Mocked away are the following internal methods:

=over

=item ->create()

=item ->filestore_model()

=item ->result_source()

=item ->_scan_viruses()

=back

Under normal circumstances, this should:

=over

=item C<create> a row in the database

We also do check we pass in all the right values that were passed into
C<filestore_create> or derived from the file

=item have a C<filestore_replicate>

=back

Besides normal behaviour, this will also test that we throw the exceptions we
know about:

=over

=item params/profile: ... missing: file_path

=item params/profile: ... missing: original_name

=item filestore/no_readable_file: File ...

=item filestore/create/empty: Will not add an empty file

=back

Whenever one off those exceptions occur, we should not see any calls to
C<create> that would add a row to the database.

We test for the propegated exceptions from C<scan_viruses>

=over

=item filestore/scan_viruses/virus_found: Virus 'scary-virus' ...

=item filestore/scan_viruses/scanner_not_available: ...

=back

Curently, no records should be created either

Furthermore, we check that we can pass in optional arguments

=over

=item Optional argument: id

=item Optional argument: force_mimetype

=item Optional argument: ignore_extension

This optional param has never been implemented

=back

=cut

sub test_filestore_create {

    # create a test object that has all internel $self-> methods mocked away
    #
    my $test = Test::MockObject->new();

    # ->filestore_create
    # the original codeblock
    $test->mock( 'filestore_create' =>
        \&Zaaksysteem::Backend::Filestore::ResultSet::filestore_create
    );

    # ->log(), log4perl access
    $test->mock(log => sub { Log::Log4perl->get_logger(__PACKAGE__) });

    # ->create()
    # it magically creates a new ResultSet object, we only need a few things
    $test->mock( 'create' =>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__create
    );

    # ->filestore_model()
    # gives us an object that can tell us about get_default_engine
    $test->mock( 'filestore_model'=>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__filestore_model
    );

    # ->result_source()
    # which returns an object that can do 'schema'
    # which has a resultset for 'Queue items'
    $test->mock( 'result_source' =>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__result_source
    );

    # ->_scan_viruses()
    # the actual scanner that lives-ok or throws errors when viruses are found
    $test->mock( '_scan_viruses' =>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__ok
    );

    # ->_create_and_push_queue_item
    # needed to add this method to our $test
    $test->mock( '_create_and_push_file_scan_queue_item' =>
        \&Zaaksysteem::Backend::Filestore::ResultSet::_create_and_push_file_scan_queue_item
    );

    my %mock;

    # modules can only be mocked when inside the sub test_
    $mock{mod}{'Zaaksysteem::StatsD'}
        = _mock_mod__Zaaksysteem_StatsD();
    $mock{mod}{'File::ArchivableFormats'}
        = _mock_mod__File_ArchivableFormats();
    $mock{mod}{'Path::Tiny'}
        = _mock_mod__Path_Tiny();
    $mock{mod}{'UUID::Tiny'}
        = _mock_mod__UUID_Tiny();
    $mock{sub}{'File::stat::size'}
        = _mock_sub__File_stat_size( $TEMPFILE_SIZE );

    # we should get a new filestore_row object back, but since this created
    # inside the filestore_create method and will call other methods like update
    # or delete on it, we will create it here and provide it from the
    # $self->create. So we can check what happened inside.
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        = _mock_obj__Zaaksysteem_Backend_Filestore_ResultSet__create();
    _mock_set__Zaaksysteem_Backend_Filestore_ResultSet__create(
        $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
    );

    $mock{obj}{'DBIC::ResultSet'}{Queue}
        = _mock_obj__DBIC__resultset__Queue();
    _mock_set__DBIC_ResultSet( 'Queue' => $mock{obj}{'DBIC::ResultSet'}{Queue} );

    # test normal behaviour
    #
    note "normal behaviour";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();

    lives_ok {
        my $params = _filestore_create__default_params();
        my @result = $test->filestore_create( $params );
    }
    "filestore_create runs OK";

    $test->called_ok('create', "... and tried to create a database entry");

    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");

#   _cmp_deeply__queue__scan_file(\%mock); # we do not want to rescan, it was OK

    # Throws exception: missing: file_path
    #
    note "Throws exception: missing: file_path";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();

    throws_ok {
        my $params = _filestore_create__default_params();
        delete $params->{file_path};
        my @result = $test->filestore_create( $params );
    } qr|^params/profile: (.*)missing: file_path|s,
    "Throws exception: missing: file_path";

    ok ! $test->called('create'),
        "... and did not try to create a database entry";

    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";

    ok !$mock{obj}{'DBIC::ResultSet'}{Queue}->called('queue_item'),
        "... and did not add anything to queue_items at all";

    # Throws exception: missing: original_name
    #
    note "Throws exception: missing: original_name";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();

    throws_ok {
        my $params = _filestore_create__default_params();
        delete $params->{original_name};
        my @result = $test->filestore_create( $params );
    } qr|^params/profile: (.*)missing: original_name|s,
    "Throws exception: missing: original_name";

    ok ! $test->called('create'),
        "... and did not try to create a database entry";

    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";

    ok !$mock{obj}{'DBIC::ResultSet'}{Queue}->called('queue_item'),
        "... and did not add anything to queue_items at all";

    # Throws exception: no readable file
    #
    note "Throws exception: no readable file";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();

    throws_ok {
        my $params = _filestore_create__default_params();
        $params->{file_path} = 'tmp/########';
        my @result = $test->filestore_create( $params );
    } qr|^filestore/no_readable_file: File 'tmp/########' .*|s,
    "Throws exception: no readable file";

    ok ! $test->called('create'),
        "... and did not try to create a database entry";

    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";

    ok !$mock{obj}{'DBIC::ResultSet'}{Queue}->called('queue_item'),
        "... and did not add anything to queue_items at all";

    # Throws exception: Will not add empty file
    #
    note "Throws exception: Will not add empty file";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();

    throws_ok {
        my $params = _filestore_create__default_params();
        $params->{file_path} = "$TEMPFILE_EMPTY";
        local $mock{sub}{'File::stat::size'} = _mock_sub__File_stat_size( 0 );
        my @result = $test->filestore_create( $params );
    } qr|^filestore/create/empty: Will not add an empty file|s,
    "Throws exception: Will not add empty file";

    ok ! $test->called('create'),
        "... and did not try to create a database entry";

    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";

    ok !$mock{obj}{'DBIC::ResultSet'}{Queue}->called('queue_item'),
        "... and did not add anything to queue_items at all";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( '_scan_viruses' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__trouble );

    lives_ok {
        my $params = _filestore_create__default_params();
        my @result = $test->filestore_create( $params );
    }
    "Continues: Scanner Not Available";

    ok $test->called('create'),
        "... and did try to create a database entry";

    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('update'),
        "... but did not update (will need to try again later)";

    ok $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";

    ok $mock{obj}{'DBIC::ResultSet'}{Queue}->called('queue_item'),
        '... and did not anything to the queued items';

    _cmp_deeply__queue__scan_file(\%mock);

    # Optional argument: id
    #
    note "Optional argument: id";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( '_scan_viruses' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__large );

    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{id} = "MY_OWN_ID_000";
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'id'";

    $test->called_ok('create', "... and tried to create a database entry");

    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");

    _cmp_deeply__queue__scan_file(\%mock);

    # Optional argument: force_mimetype
    #
    note "Optional argument: force_mimetype";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( '_scan_viruses' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__large );

    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{force_mimetype} = "test/forced";
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'force_mimetype'";

    $test->called_ok('create', "... and tried to create a database entry");

    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");

    _cmp_deeply__queue__scan_file(\%mock);

    # Optional argument: ignore_extension
    #
    # This optional param has never been implemented
    #
    note "Optional argument: ignore_extension";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( '_scan_viruses' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__large );

    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{ignore_extension} = 1;
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'ignore_extension => 1'";
    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{ignore_extension} = 0;
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'ignore_extension => 0'";

    # Optional argument: non_existing_param
    #
    # This should just pass
    #
    note "Optional argument: non_existing_param";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( '_scan_viruses' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__large );

    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{non_existing_param} = undef;
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'non_existing_param => undef'";
    #
    # By design, define_profile will not barf for params not specified in the
    # profile, please be no longer surprised that this is OK and thus lives_ok
    # passes.

    # test normal behaviour for larger files
    #
    note "normal behaviour for larger files";

    # clear and mock internals
    $test->clear();
    $mock{obj}{'DBIC::ResultSet'}{Queue}->clear;
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( '_scan_viruses' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__large );

    lives_ok {
        my $params = _filestore_create__default_params();
        my @result = $test->filestore_create( $params );
    }
    "filestore_create runs OK";

    $test->called_ok('create', "... and tried to create a database entry");

    if ( my($method, $args) = $mock{obj}{'DBIC::ResultSet'}{Queue}->next_call() ) {
        pass "... next call, checking";
        is $method, 'create_item', '... and requested queue item creation';

        my (undef, $type, $data) = @{ $args };

        is $type, 'scan_file', '... and the expected queue item type was used';

        cmp_deeply $data, {
            label => ignore(),
            metadata => ignore(),
            data => {
                status => 'waiting',
                ignore_existing => 1,
                parameters => {
                    file_id => $UUID_STRING
                }
            }
        }, '... and the expected queue item data was produced';
    } else {
        fail "... next call, queue item creation";
    }

#   _cmp_deeply__queue__scan_file(\%mock); # see above, we just did test

    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");

}

sub test_filestore_create_in_transaction {

    my $filestore = mock_strict(
        'X-Mock-Called' => 1,
        discard_changes => sub { die "from discarding changes" },
        delete => sub { pass("Called delete") },
    );
    my $mock = mock_strict(
        filestore_model => {
            get_default_engine => {
                write => sub { die "I'm not going to write" },
                name  => 'the little train that could',
            }
        },
        create => sub { return $filestore },
    );
    my $override = override('Path::Tiny::assert' => sub { return mock_one });
    $override->override('Zaaksysteem::Backend::Filestore::ResultSet::_archivable_format' => sub { return { mime_type => 'foo' } });

    throws_ok(
        sub {
            Zaaksysteem::Backend::Filestore::ResultSet::filestore_create(
                $mock,
                {
                    file_path     => 'foo',
                    original_name => 'bar',
                },
            );
        },
        qr/I'm not going to write/,
        "Dies at write action"
    );

    ok(
        one_called($filestore, 'delete'),
        "Called delete on the filestore object"
    );

    ok(
        !one_called($filestore, 'discard_changes'),
        "Didn't call discard_changes on the filestore object"
    );
}

################################################################################
#
# done testing
#
# and 500 lines of mocking follow

sub _filestore_create__default_params {
    return {
        file_path     => $TEMPFILE_OK->stringify,
        original_name => 'Temporary File',
    }
}

# _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__create
#
# check for passed params that will be inserted into the database
# and return a previously stored object
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__create {
    my ($self, $params) = @_;
    cmp_deeply ( $params => subhashof( _expected_params() ),
        "got the right param/values to create new row"
    );

    return _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create();
}

# returns a mocked Zaaksysteem::Backend::Filestore::ResultSet object that
# implements methods being called lateron inside our filestore_create
# - delete
# - discard changes
# - id
# - update
#
sub _mock_obj__Zaaksysteem_Backend_Filestore_ResultSet__create {
    my $mock = Test::MockObject->new();
    $mock->set_true('delete', 'update');
    $mock->set_always('id' => "MY_NEW_ID_123");
    $mock->set_always('uuid' => $UUID_STRING);
    $mock->mock('discard_changes' => sub { return $_[0] } ); # self

    return $mock
}

# returns a ZS::Backend::Filestore::Component, aka filestore_row
sub _mock_obj__Zaaksysteem_Backend_Filestore_ResultSet__create__small {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Zaaksysteem::Backend::Filestore::Component');
    $mock_obj->set_true('delete', 'update');
    $mock_obj->set_always('id' => "MY_NEW_ID_123");
    $mock_obj->set_always('uuid' => $UUID_STRING);
    $mock_obj->set_always('size' => 23 );
    $mock_obj->mock('discard_changes' => sub { return $_[0] } ); # self

    return $mock_obj;
}

# returns a ZS::Backend::Filestore::Component, aka filestore_row
sub _mock_obj__Zaaksysteem_Backend_Filestore_ResultSet__create__large {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Zaaksysteem::Backend::Filestore::Component');
    $mock_obj->set_true('delete', 'update');
    $mock_obj->set_always('id' => "MY_NEW_ID_123");
    $mock_obj->set_always('uuid' => $UUID_STRING);
    $mock_obj->set_always('size' => 99_999_999_999 );
    $mock_obj->mock('discard_changes' => sub { return $_[0] } ); # self

    return $mock_obj;
}

# sets and gets a mocked Zaaksysteem::Backend::Filestore::ResultSet object like
# the above
{
my $mock;
sub _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create { return $mock }
sub _mock_set__Zaaksysteem_Backend_Filestore_ResultSet__create { $mock = shift }
}

sub _expected_params {
    return {
        is_archivable     => any ( 9, undef ), # that is us and not original module, or a bad mime-type
        md5               => $MD5_HASH, # from mocked Digest::MD5::File
        mimetype          => any ( 'test/mocked', 'test/forced' ),
        original_name     => "Temporary File",
        size              => $TEMPFILE_SIZE,
        storage_location  => [ "Mocked FileStore Engine" ],
        uuid              => $UUID_STRING,
        virus_scan_status => any ( 'pending', 'ok', 'found' ),
        id                => "MY_OWN_ID_000", # only for passed in `id` by caller
    }
}

#
# mock method Zaaksysteem::Backend::Filestore::ResultSet->filestore_model()
#

sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__filestore_model {
    my $mock = Test::MockObject->new();
    $mock->mock('get_default_engine' => \&_mock_obj__Zaaksysteem_Filestore_Model__get_default_engine );
    return $mock;
}

sub _mock_obj__Zaaksysteem_Filestore_Model__get_default_engine {
    my $mock = Test::MockObject->new();
    $mock->mock( 'write' => \&_mock_sub__Zaaksysteem_Filestore_Engine__write );
    $mock->set_always( 'name' => "Mocked FileStore Engine" );

    return $mock;
}

# _mock_sub__Zaaksysteem_Filestore_Engine__write
#
# a mocked method for, being called on the default retirned FileStore::Engine
#
sub _mock_sub__Zaaksysteem_Filestore_Engine__write {
    my $self = shift;
    my @args = @_;
    is_uuid_string ( $args[0], "Storage Engine - write method argument 0 is a UUID conferment string" );
    is_filehandle  ( $args[1], "Storage Engine - write method argument 1 might be a FileHandle" ); # fake too
    return 1; # or whatever
}

#
# mock internal sub Zaaksysteem::Backend::Filestore::ResultSet _scan_viruses
# we've tested the bahaviour at the top of this test
#

# _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__ok
#
# mocks internal method _scan_viruses()
# - exit normally
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__ok {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_OK;

    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create()
        ],
        "scan_viruses called correctly for OK"
    );

    # NOTE: the original returns implicitly the value of the last statement
    return !! undef() # which is the result of the comparison ($stat eq "FOUND")
}

# _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__virus
#
# mocks internal method _scan_viruses()
# -throws exception 'filestore/scan_viruses/virus_found'
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__virus {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_VIRUS;
    my $virus = 'scary-virus';

    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create()
        ],
        "scan_viruses called correctly for VIRUS"
    );

    throw(
        "filestore/scan_viruses/virus_found",
        "Virus '$virus' aangetroffen in bestand, niet toegevoegd."
    );
};

# mock internal method _scan_viruses()
#
# throws exception 'filestore/scan_viruses/scanner_not_available'
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__trouble {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_OK;

    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create()
        ],
        "scan_viruses called correctly for TROUBLE"
    );

    throw(
        "filestore/scan_viruses/scanner_not_available",
        "Virusscanner niet operationeel",
        { error => undef }
    )
};

# mock internal method _scan_viruses()
#
# throws exception 'filestore/scan_viruses/scanner_not_available'
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__scan_viruses__large {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_OK;

    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create()
        ],
        "scan_viruses called correctly for LARGE"
    );

    throw(
        "filestore/scan_viruses/file_too_large",
        "Bestand is te groot voor 'on-the-fly' virus-scans",
        { error => undef }
    )
};

# _mock_new__VSS_UserAgent
#
# Creates a mocked LWP::UserAgent to be returned calling the mocked
# _virus_scan_service_useragent() method.
#
sub _mock_new__VSS_UserAgent {
    my $response = shift;
    my $mock_mod = Test::MockObject->new();
    $mock_mod->set_isa('LWP::UserAgent');
    $mock_mod->mock(
        'request' => sub { return $response }
    );
    return $mock_mod;
}

sub _mock_new__VSS_UserAgent__ok {
    my $response = _mock_obj__HTTP_Response( [ ] );
    return _mock_new__VSS_UserAgent( $response );
}

sub _mock_new__VSS_UserAgent__trouble {
    my $response = _mock_obj__HTTP_Response( undef );
    return _mock_new__VSS_UserAgent( $response );
}

sub _mock_new__VSS_UserAgent__virus {
    my $response = _mock_obj__HTTP_Response( [ 'trojan', 'worm' ] );
    return _mock_new__VSS_UserAgent( $response );
}

# create the HTTP::Response for different responses from the VSS request
#
# only argument is a ArrayRef with found viruses (may be empty) or undef to indicate errors
#
sub _mock_obj__HTTP_Response {
    my $args = shift;

    my $json_string = ""; # hand crafted
    my $status_code;

    if ( $args ) {
        my $viruses = join ', ', map { qq|"$_"| } @$args;
        $json_string =
            '{"reference":null,"type":"scanner","instance":{"viruses":[' . $viruses . ']}}';
        $status_code = "200";
    } else {
        $json_string =
            '{"reference":null,"type":"exception","instance":{"type":"params/profile","message":"Error!!!","timestamp":"2018-01-05T12:15:21Z"}}';
        $status_code = "500";
    };

    return HTTP::Response->new( $status_code, undef, undef, $json_string );
}

#
# mock Queue
#

sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__result_source{
    my $mock = Test::MockObject->new();
    $mock->mock(
        schema => \&_mock_obj__DBIC_schema
    );

    return $mock
}

sub _mock_obj__DBIC_ResultSource {
    my $mock = Test::MockObject->new();
    $mock->mock(
        schema => \&_mock_sub__DBIC_schema
    );

    return $mock
}

sub _mock_obj__DBIC_schema {
    my $mock = Test::MockObject->new();
    $mock->mock(
        resultset => sub {
            my $self = shift;
            my $resultset_name = shift;
            my $resultset = _mock_get__DBIC_ResultSet($resultset_name)
                or die "missing mocked resultset $resultset_name\n";
            return $resultset
        }
    );
    $mock->mock(
        default_resultset_attributes => sub {
            my $self = shift; # don't want invocant
            return _mock_get__default_resultset_attributes( )
        }
    );
    $mock->mock(
        txn_do => sub {
            my $self = shift;
            my $code = shift;
            $code->();
            return 1;
        },
    );

    return $mock
}

sub _mock_obj__DBIC__resultset__Queue {
    my $mock = Test::MockObject->new();
    $mock->mock(
        queue_item => sub {
            my ($mock_self, $item) = @_;

            isa_ok $item, 'Zaaksysteem::Backend::Object::Queue::Component',
                '... Queue::ResultSet->queue_item argument';
        }
    );
    $mock->mock(
        create_item => sub {
            my $self = shift;
            my @args = @_;
            cmp_deeply (
                \@args => [
                    ignore(), # type
                    {
                        label => ignore(),
                        metadata => ignore(),
                        data  => ignore(), # hashref of whatever
                    }
                ],
                "... 'Queue' got right data for creating row"
            );

            return mock_one(
                'X-Mock-ISA' => 'Zaaksysteem::Backend::Object::Queue::Component',
                delete => sub { note 'delete of queue item called' },
                type => $args[0],
                label => $args[1]->{ label },
                data => $args[1]->{ data }
            );
        }
    );

    return $mock
}



{ my %data;
sub _mock_set__DBIC_ResultSet { my $name = shift; $data{ $name } = shift }
sub _mock_get__DBIC_ResultSet { my $name = shift; return $data{ $name } }
}

{ my $data = {};
sub _mock_set__default_resultset_attributes { my $name = shift; $data->{$name} = shift }
sub _mock_get__default_resultset_attributes { return $data }
}

#
# mock module Zaaksysteem::StatsD
#

sub _mock_mod__Zaaksysteem_StatsD {
    my $mock = Test::MockModule->new("Zaaksysteem::StatsD");
    $mock->mock(
        'statsd' => sub {
            my $mock_obj = Test::MockObject->new();
            $mock_obj->set_true('start', 'end', 'increment');
            return $mock_obj;
        }
    );
    return $mock;
}

#
# mock module File::ArchivableFormats
#

sub _mock_mod__File_ArchivableFormats {
    my $mock_mod = Test::MockModule->new('File::ArchivableFormats');
    $mock_mod->mock(
        'new' => sub {
            my $mock_obj = Test::MockObject->new();
            $mock_obj->mock(
                identify_from_mimetype => sub {
                    my ($self, $mime_type) = @_;
                    return _file_archivable_test_force() if $mime_type eq 'test/forced';
                    return { mime_type => 'test/does_not_exist' }
                }
            );
            $mock_obj->set_always(
                identify_from_path => _file_archivable_test_plain(),
            );
            return $mock_obj
        }
    );
    return $mock_mod
}

sub _file_archivable_test_plain {
    return {
        DANS => {
            allowed_extensions => [
                ".asc",
                ".txt"
            ],
            archivable         => 9, # that is us and not original module
            types              => [
                "Plain text (Unicode)",
                "Plain text (Non-Unicode)",
                "Statistical data (data (.csv) + setup)",
                "Raspter GIS (ASCII GRID)",
                "Raspter GIS (ASCII GRID)"
            ]
        },
        mime_type => "test/mocked"
    }
}

sub _file_archivable_test_force {
    return {
        DANS => {
            allowed_extensions => [
                ".tst"
            ],
            archivable         => 9, # that is us and not original module
            types              => [
                "Forced Test File",
            ]
        },
        mime_type => "test/forced"
    }
}

sub _mock_mod__UUID_Tiny {
    my $mock_mod = Test::MockModule->new('UUID::Tiny');
    $mock_mod->mock(
        'create_uuid_as_string' => sub { return $UUID_STRING }
    );
    return $mock_mod;
}

# mock module Path::Tiny
#
# specifacally, we want to keep track of the MD5 hash
#
sub _mock_mod__Path_Tiny {
    my $mock_mod = Test::MockModule->new('Path::Tiny');
    $mock_mod->mock(
        'digest' => sub { return $MD5_HASH }
    );
    return $mock_mod;
}

# set the file size for File::stat::size to a fixed answer
sub _mock_sub__File_stat_size {
    my $size = shift || 0;
    my $mock = Sub::Override->new();
    $mock->replace(
        'File::stat::size' => sub { $size }
    );
    return $mock;
}

sub _expected_filestore_replicate {
    return +{
        type  => 'filestore_replicate',
        label => ignore(), # Dutch labels may get translated
        data  => {
            filestore_id => 'MY_NEW_ID_123'
        }
    }
}

#
# utility
#

sub is_uuid_string {
    my ( $value, $message ) = @_;
    ok ( UUID->check($value), $message )
}

sub is_filehandle {
    my ( $test_value, $message ) = @_;
    ok ( defined $test_value, $message ); # nope this doesn't test that it actually is a FH
}

sub _tempfile_ok {
    my $file = Path::Tiny->tempfile();
    $file->spew( 'this is a test document' ); # 4dbee61dcd3395d95628595f89d1109c
    return $file
}

sub _tempfile_empty {
    my $file = Path::Tiny->tempfile();
    return $file
}

sub _tempfile_virus {
    my $file = Path::Tiny->tempfile();
    $file->spew( '** I AM INMFECTED **' ); # or what ever, we do not call ClamAV
    return $file
}

#
# test queue items
#

sub _cmp_deeply__queue__scan_file {
    my $mock = shift;

    cmp_deeply [ $mock->{obj}{'DBIC::ResultSet'}{Queue}->next_call ], [
        'create_item',
        [ ignore(), 'scan_file', ignore() ]
    ], "... and called create_item for 'scan_file'";

    cmp_deeply [ $mock->{obj}{'DBIC::ResultSet'}{Queue}->next_call ], [
        'queue_item',
        ignore()
    ], "... and queued the 'scan_file' queue item";

}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
