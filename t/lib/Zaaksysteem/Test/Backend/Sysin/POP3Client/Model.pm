package Zaaksysteem::Test::Backend::Sysin::POP3Client::Model;
use Zaaksysteem::Test;

use Zaaksysteem::Backend::Sysin::POP3Client::Model;

sub test_pop3client_stls {
    my @calls;
    my %messages = (
        1 => 'foo',
        2 => 'bar',
    );
    my @message_body = ('message', 'body');
    my $pop3 = mock_one(
        starttls => sub {
            push @calls, { starttls => \@_ };
            return 1;
        },
        login => sub {
            push @calls, { login => \@_ };
            return 1;
        },
        list => sub {
            return \%messages;
        },
        quit => sub {
            push @calls, { quit => \@_ };
            return 1;
        },
        get => sub {
            return \@message_body;
        },
        delete => sub {
            push @calls, { delete => \@_ };
            return 1;
        },
    );

    my $model = Zaaksysteem::Backend::Sysin::POP3Client::Model->new(
        pop3     => $pop3,
        protocol => 'POP3',
        host     => 'test.host',
        port     => '123',
        user     => 'testuser',
        password => 'testpassword',
    );

    $model->login();

    is_deeply(
        \@calls,
        [ 
            { starttls => [ SSL_ca_path => '/etc/ssl/certs' ] },
            { login    => [ 'testuser', 'testpassword' ] },
        ],
        'Logging in sends the correct username and password, STLS is used before login'
    );

    my $msg = $model->messages();
    isa_ok($msg, 'CODE', 'Got a code reference to fetch messages');

    while (my $m = $msg->()) {
        isa_ok(
            $m,
            'Zaaksysteem::Backend::Sysin::POP3Client::Model::Message',
            'Returned message is of correct type',
        );

        is($m->retrieve, "messagebody", "Message retrieved correctly");
        $m->delete();
        is_deeply(
            $calls[-1],
            { delete => [ $m->msgnum ] },
            "Message deleted correctly",
        );
    }

    $model->close();
    is_deeply(
        $calls[-1],
        { quit => [] },
        "Connection closed cleanly",
    );

    @calls = ();
    $model = Zaaksysteem::Backend::Sysin::POP3Client::Model->new(
        pop3     => $pop3,
        protocol => 'POP3S',
        host     => 'test.host',
        port     => '123',
        user     => 'testuser',
        password => 'testpassword',
    );

    $model->login();
    is_deeply(
        \@calls,
        [ 
            { login    => [ 'testuser', 'testpassword' ] },
        ],
        'Logging in sends the correct username and password. STLS not used on POP3S connection.'
    );

}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
