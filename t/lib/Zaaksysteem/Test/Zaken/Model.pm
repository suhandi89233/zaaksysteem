package Zaaksysteem::Test::Zaken::Model;

use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Zaken::Model - Test Zaaksysteem::Test::Zaken::Model

=head1 DESCRIPTION

Test the new case model used by api/v1

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaken::Model

=cut

use BTTW::Tools::RandomData qw(:uuid);
use Data::Random::NL qw(:all);
use DateTime;
use URI;
use Zaaksysteem::Test;
use Zaaksysteem::Zaken::Model;
use List::Util qw(all);
use Test::Mock::Two qw(one_called_times_ok);

sub test_pod {
    pod_coverage_ok('Zaaksysteem::Zaken::Model');
}

sub _subname {
    my $sub = shift;
    return "Zaaksysteem::Zaken::Model::$sub";
}

sub _get_model {

    my $model = Zaaksysteem::Zaken::Model->new(
        base_uri => URI->new('https://localhost/'),
        schema        => mock_one(
            'X-Mock-ISA' => 'Zaaksysteem::Schema',
            txn_do => sub {
                my $txn_code = shift;
                return $txn_code->(@_);
            }
        ),
        object_model  => mock_one('X-Mock-ISA' => 'Zaaksysteem::Object::Model'),
        subject_model => mock_one('X-Mock-ISA' => 'Zaaksysteem::BR::Subject'),
        queue_model   => mock_one(
            'X-Mock-ISA' => 'Zaaksysteem::Object::Queue::Model',
            create_item => sub { return \@_ },
        ),
        rs_zaak => mock_one(),
        document_model => mock_one('X-Mock-ISA' => 'Zaaksysteem::Document::Model'),
        @_,
    );
    isa_ok($model, "Zaaksysteem::Zaken::Model");
    return $model;
}

sub test_create_zaak {

    my $create_zaak_args;
    my $model = _get_model(
        schema => mock_one(
            create_zaak => sub { $create_zaak_args = shift }
        )
    );

    my $now = DateTime->now();
    my $override = override('DateTime::now' => sub { return $now->clone });

    my %args = (
        casetype        => mock_one(id => 400),
        confidentiality => 'public',
        source          => 'webform',
        requestor       => mock_one(old_subject_identifier => 10),
        values          => [],
    );

    my %expect = (
        zaaktype_id      => $args{casetype}->id,
        aanvraag_trigger => 'extern',
        confidentiality  => $args{confidentiality},
        contactkanaal    => $args{source},
        registratiedatum => $now,
        kenmerken        => $args{values},
        aanvragers       => [
            {
                betrokkene  => $args{requestor}->old_subject_identifier,
                rol         => 'Aanvrager',
                verificatie => 'n/a',
            }
        ],
        skip_required_attributes => 1
    );

    $model->_create_case(\%args);
    cmp_deeply($create_zaak_args, \%expect,
        "Passed correct arguments to create_zaak"
    );

    throws_ok(
        sub {
            my $model = _get_model(
                schema => mock_one(
                    resultset => {
                        create_zaak => sub { die "Foo" }
                    }
                )
            );
            $model->_create_case(\%args);
        },
        qr#case/create_failed: Case creation failed, unable to continue.#,
        "Throws errors like it should",
    );
    throws_ok(
        sub {
            $args{case_id} = 666;
            my $override = override(
                "Zaaksysteem::Zaken::Model::assert_case_id" => sub { return 1 }
            );
            my $model = _get_model(
                schema => mock_one(
                    resultset => {
                        create_zaak => sub { die 'db: DBI Exception: DBD::Pg::st execute failed: ERROR:  duplicate key value violates unique constraint "zaak_pkey"' }
                    }
                )
            );
            $model->_create_case(\%args);
        },
        qr#case/create_failed/case_id/exists#,
        "Throws errors like it should for reserved case numbers",
    );

    lives_ok(
        sub {
            my $override = override(
                "Zaaksysteem::Zaken::Model::assert_case_id" => sub {
                    my ($self, $id) = @_;
                    if (!is($id, 42, "Found asserted the reserved case ID")) {
                        die "horrible death";
                    }
                }
            );
            %args = (%args, case_id => 42);
            $model->_create_case(\%args);
        },
        "Able to create case with case id"
    );
}

sub test_assert_case_id {
    my $model = _get_model(schema => mock_one(dbh_do => 42 ));

    lives_ok(
        sub {
            $model->assert_case_id(42);
        },
        "assert_case_id works like a charm"
    );

    throws_ok(
        sub {
            $model->assert_case_id(666);
        },
        qr#case/sequence/too_high#,
        "assert_case_id throws error when ID is too damn high",
    );

}

sub test_get_casetype_object {

    my $uuid = generate_uuid_v4;

    throws_ok(
        sub {
            my $object_model = mock_one(retrieve => sub { die "Foo" });
            my $model = _get_model(object_model => $object_model);

            $model->get_casetype_object($uuid);
        },
        qr#case/casetype/retrieval_fault:#,
        "Casetype with $uuid cannot be retreived"
    );

    throws_ok(
        sub {
            my $object_model = mock_one(retrieve => undef);
            my $model = _get_model(object_model => $object_model);
            $model->get_casetype_object($uuid);
        },
        qr#case/casetype_not_found#,
        "Casetype with $uuid cannot be found"
    );

    lives_ok(
        sub {
            my $object_model = mock_one(retrieve => 1);
            my $model = _get_model(object_model => $object_model);
            $model->get_casetype_object($uuid);
        },
        "Casetype with $uuid could be found"
    );
}

sub test_assert_casetype {

    my $model = _get_model;
    my $active = 0;
    my $casetype = mock_one(active => sub { return $active }, zaaktype_node_id => 1);

    my $override = override(
        _subname('_find_casetype_by_id') => sub { return $casetype });

    throws_ok(
        sub {
            $model->assert_casetype($casetype);
        },
        qr#api/v1/case/casetype/node/inactive:#,
        "Inactive casetype found"
    );

    lives_ok(
        sub {
            $active = 1;
            $model->assert_casetype($casetype);
        },
        "Casetype found and active"
    );

}

sub test_assert_values {

    my $model = _get_model;

    lives_ok(
        sub {
            $model->assert_values();
        },
        "Undef is allowed (creator doesn't fill in a thing)"
    );

    my %values = ();
    lives_ok(
        sub {
            $model->assert_values(\%values);
        },
        "Empty values are allowed"
    );

    %values = (
        foo => [qw(bar baz)],
        bar => [undef],
    );

    lives_ok(
        sub {
            $model->assert_values(\%values);
        },
        "Values wrapped in an array are values"
    );

    $values{bar} = { foo => 'bar' };
    throws_ok(
        sub {
            $model->assert_values(\%values);
        },
        qr#case/create/values/non_array:#,
        "HashRef's are not allowed"
    );

    $values{bar} = undef;
    throws_ok(
        sub {
            $model->assert_values(\%values);
        },
        qr#case/create/values/non_array:#,
        "undef values are also not allowed"
    );

    TODO: {
        local $TODO
            = "Fix me, this will break later on after getting case attributes and other DB queries";
        $values{bar} = [{}];
        throws_ok(
            sub {
                $model->assert_values(\%values);
            },
            qr#case/create/values/non_array:#,
            "nested values must be an array"
        );
    }
}

sub test_assert_requestor {

    my $model = _get_model;

    my $preset_client = 42;
    my $node          = mock_one(
        preset_client => sub { return $preset_client }
    );
    my $override = override(_subname('_get_subject_by_legacy_id') => sub { shift; return shift });

    lives_ok(
        sub {
            $model->_assert_requestor($node);
        },
        "assert_requestor found something",
    );

    throws_ok(
        sub {
            $preset_client = undef;
            $model->_assert_requestor($node);

        },
        qr#api/v1/case/requestor_unresolvable#,
        "Unable to find default requestor"
    );

}

sub test_find_assignee {

    my $model = _get_model;
    my $properties = { preset_owner_identifier => "assignee found" };

    my $node = mock_one(properties => sub { return $properties });
    my $override = override(
        _subname('_get_subject_by_legacy_id') => sub { shift; return shift });

    is($model->_find_assignee($node), "assignee found", "Assignee found");

    $properties = {};
    is($model->_find_assignee($node), undef, "No assignee found");
}

sub test_assert_routing {

    my $model = _get_model;
    my $override
        = override(_subname('_get_group') => sub { shift; return shift });
    $override->replace(
        _subname('_get_role') => sub { shift; return shift }
    );

    my ($group, $role)
        = $model->assert_routing(
        { route => { group_id => 42, role_id => 666 } });
    is($group, 42,  "Got the group");
    is($role,  666, "Got the role");

    throws_ok(
        sub {
            $model->assert_routing({ route => {} });
        },
        qr#Validation of profile failed#,
        "Routing information is required"
    );

    lives_ok(
        sub {
            my ($group, $role) = $model->assert_routing({});
            is($group, undef, "Got the group");
            is($role,  undef, "Got the role");
        },
        "No routing information is ok"
    );

}

sub test_get_case_attributes {

    my $rs = mock_one(search_rs => sub { return \@_ });
    my $model = _get_model;

    my $answer = $model->get_case_attributes($rs);
    my $expect = [
        {
            'bibliotheek_kenmerken_id'            => { '!=' => undef },
            'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    ];

    cmp_deeply($answer, $expect, "Got the case attributes");

    my ($s, $o, @ms);
    my $node = mock_one(
        search_by_magic_strings => sub {
            @ms = @_;
            return mock_one(
                search_rs => sub {
                    ($s, $o) = @_;
                }
            );
        }
    );

    $model->_get_case_attributes(node => $node, magic_strings => [qw(magic)]);
    cmp_deeply(\@ms, ['magic'], "Search on magic strings");
    cmp_deeply($s, { required_permissions => [ undef, '{}' ] }, "Correct search");
    cmp_deeply($o, { prefetch =>  [qw(bibliotheek_kenmerken_id)] }, "Correct options");

    $model->_get_case_attributes(node => $node, magic_strings => [], phase => 300);
    cmp_deeply(\@ms, [], "Search on empty magic strings");

    cmp_deeply($s, { required_permissions => [ undef, '{}' ] }, "Correct search with user undefined");

    $model->_get_case_attributes(node => $node, magic_strings => [], user => 1, phase => 300);
    cmp_deeply($s, { required_permissions => [ undef, '{}' ] }, "Correct search with user defined");
}

sub test_update_case {

    my $model = _get_model();
    my $case = mock_case(
        milestone        => 4,
        update_fields    => 1,
    );

    my %args = (
        values       => { foo            => 'bar' },
        files        => ['flop'],
        user         => undef,
        payment_info => { payment_status => 'pending' },
    );

    my %test;

    my $override = override(
        "Zaaksysteem::Zaken::Model::_get_case_attributes" => sub {
            shift;
            my %args = @_;
            isnt(delete $args{node}, undef, "Node is defined");
            cmp_deeply(
                \%args,
                { magic_strings => ['foo'], phase => 5 },
                'update_case called _get_case_attributes ok'
            );
            $test{attributes} = 1;
            return 'returns attributes';
        }
    );

    $override->override(
        "Zaaksysteem::Zaken::Model::assert_case_attributes" => sub {
            shift;
            my @args = @_;
            cmp_deeply(\@args, ['returns attributes', $args{values}],
                'update_case called assert_case_attributes');
            $test{assert_attributes} = 1;
            return [{ 42 => 'bar' }];
        }
    );

    $override->override(
        "Zaaksysteem::Zaken::Model::assert_documents" => sub {
            shift;
            my @args = @_;
            cmp_deeply(\@args, [ 'document_attributes', $args{values}, $args{files} ]);
            $test{assert_documents} = 1;

            return {
                add    => 'adding a document',
                update => 'update a document'
            };
        }
    );

    $override->override(
        "Zaaksysteem::Zaken::Model::get_document_attributes" => sub {
            shift;
            my $attributes = shift;
            is($attributes, 'returns attributes', "get the attributes");
            $test{document_attributes} = 1;
            return 'document_attributes';
        }
    );

    $override->override(
        "Zaaksysteem::Zaken::Model::add_case_files" => sub {
            shift;
            my ($case, $args) = @_;
            isa_ok($case, 'Zaaksysteem::Model::DB::Zaak');
            is($args, 'adding a document', 'add_case_files with correct arguments');
            $test{add_case_files} = 1;
        }
    );

    $override->override(
        "Zaaksysteem::Zaken::Model::update_case_files" => sub {
            shift;
            my ($case, $args) = @_;
            isa_ok($case, 'Zaaksysteem::Model::DB::Zaak');
            is($args, 'update a document', 'update_case_files with correct arguments');
            $test{update_case_file} = 1;
        }
    );

    $override->override(
        "Zaaksysteem::Zaken::Model::update_payment_status" => sub {
            shift;
            my %opts = @_;
            isa_ok($opts{zaak}, 'Zaaksysteem::Model::DB::Zaak');
            is($opts{payment_status}, 'pending', "Payment status has been set");
            $test{payment} = 1;
            return mock_one;
        },
    );

    my %required = (
        attributes          => 1,
        assert_attributes   => 1,
        assert_documents    => 1,
        document_attributes => 1,
        add_case_files      => 1,
        update_case_file    => 1,
        payment             => 1,
    );

    lives_ok(
        sub {
            $model->update_case($case, %args);
        },
        "Update case works as advertised"
    );

    cmp_deeply(\%test, \%required, "All overrides are tested");
}


sub test_create_delayed_case {
    my $subject = mock_one(id => 666);
    my $model = _get_model(user => $subject);

    my $override = override(_subname('generate_case_id') => sub { return 42 });

    my $item = $model->create_delayed_case({
        my => 'test'
    });

    is $item->[0]{ type }, 'create_case', 'delayed case queue item type';
    is $item->[0]{ subject }->id, 666, 'delayed case queue item subject';

    cmp_deeply $item->[0]{ data }, {
        case_id => 42,
        create_args => { my => 'test' }
    }, 'delayed case queue item data';
}

sub test_add_case_files {

    my @files;
    my $user = mock_one(
        betrokkene_identifier => 'betrokkene-medewerker-666');
    my $mock = mock_one(
        id            => 42,
        original_name => "Ryuk",
        resultset     => { file_create => sub { push(@files, shift) }, },
    );

    my $docs = {
        1 => { filestore => $mock, attribute => $mock },
        2 => { filestore => $mock, name      => "Foo", metadata => 'foo' },
        3 => { filestore => $mock, number   => 999 },
    };

    my $case   = mock_one(id => 42);
    my $expect = [
        {
            'case_document_ids' => [42],
            'db_params'         => {
                'accepted'     => 1,
                'created_by'   => 'betrokkene-medewerker-666',
                'filestore_id' => 42
            },
            'disable_message' => 1,
            'name'            => 'Ryuk'
        },
        {
            'db_params' => {
                'accepted'     => 1,
                'created_by'   => 'betrokkene-medewerker-666',
                'filestore_id' => 42
            },
            'disable_message' => 1,
            'name'            => 'Foo',
            'metadata'        => 'foo',
        },
        {
            db_params => {
                accepted     => 1,
                created_by   => 'betrokkene-medewerker-666',
                filestore_id => 42,
                id           => 999,
            },
            disable_message => 1,
            name            => 'Ryuk',
        },
    ];

    my $model = _get_model(user => $user, schema => $mock);
    foreach (sort keys %$docs) {
        $model->_add_case_file($case, $docs->{$_});
    }

    is(delete $files[0]->{db_params}{case_id}, 42, "Case ID for file 1 is correct");
    is(delete $files[1]->{db_params}{case_id}, 42, "Case ID for file 2 is correct");
    is(delete $files[2]->{db_params}{case_id}, 42, "Case ID for file 3 is correct");

    cmp_deeply(\@files, $expect, "Documents added succesfully");

    my $number = $model->add_case_files($case, $docs);
    is($number, 3, "Three files added");
}

sub test_update_case_files {

    my (@files, @metadata);
    my $mock = mock_one(
        update_properties => sub { push(@files,    shift); },
        update_metadata   => sub { push(@metadata, shift); },
        original_name     => "Ruyk",
        id                => 42,
        as_object         => 666,
    );

    my $docs = {
        1 => { filestore => $mock, attribute => $mock, file => $mock },
        2 => {
            file      => $mock,
            filestore => $mock,
            name      => "Foo",
            metadata  => 'foo'
        },
    };
    my $case   = mock_one(id => 666);
    my $expect = [
        {
            case_document_ids => [42],
            accepted          => 1,
            subject           => 666,
        },
        {
            accepted => 1,
            subject  => 666,
            name     => "Foo",
        },
    ];

    my $model = _get_model(user => $mock);
    $model->_update_case_file($case, $docs->{1});
    $model->_update_case_file($case, $docs->{2});

    is(delete $files[0]->{case_id}, 666, "Case ID for file 1 is correct");
    is(delete $files[1]->{case_id}, 666, "Case ID for file 2 is correct");

    cmp_deeply(\@files, $expect,
        "Documents updated with the correct arguments");
    cmp_deeply(\@metadata, ['foo'], "Document metadata is correct");

    my $number = $model->update_case_files($case, $docs);
    is($number, 2, "Two files updated");

}

sub test_relate_case_subjects {

    my $model = _get_model;
    my $relate_args;
    my $case = mock_one(
        betrokkene_relateren => sub {
            $relate_args = shift;
        }
    );
    my $params = { related => [{ some => 'object' }] };

    $model->relate_case_subjects($case, $params);
    cmp_deeply($relate_args, { some => 'object' }, "betrokkene relateren");
}

sub test_get_template_from_library {

    my $id    = 42;
    my $model = _get_model(
        schema => mock_one(
            resultset => sub {
                my $func = shift;
                if ($func eq 'Config') {
                    mock_one(get => sub { return $id });
                }
                else {
                    mock_one(find => sub { return shift });
                }
            }
        )
    );

    my $template_id = $model->_get_template_from_library('foo');
    is($template_id, $id,
        "Got the notification template from the config table");

    $id = undef;
    throws_ok(
        sub {
            $model->_get_template_from_library('foo');
        },
        qr#case/template/failed/id_not_found#,
        "Unable to get the notifcation template from the config"
    );
}

sub test_send_case_email {
    my $model = _get_model;

    throws_ok(
        sub {
            $model->send_case_email();
        },
        qr#params/profile#,
        "send_case_email needs arguments"
    );

    my $args;
    my $case = mock_one(
        #'Zaaksysteem::Zaken::ComponentZaak'
        mailer => { send_case_notification => sub { $args = shift; } }
    );
    my $email = 'foo@bar.nl';

    $model->send_case_email(
        case     => $case,
        subject  => mock_one(email_address => sub { return $email }),
        template => mock_one(template_id => 'foo'),
    );

    is($args->{recipient}, $email, "Email address = $email");
    is($args->{notification}->template_id, 'foo', "Template has been passed on");

}

sub test_send_case_assignment_mail {
    my $model = _get_model;
    my $case = mock_one(
        id => 42,
        behandelaar_object => { as_object => 'mah subject' },
    );

    my %args;
    my $or = override(_subname('get_assignment_notification_template') => sub { return 'template' });
    $or->override(_subname('send_case_email') => sub { shift; %args = @_ });

    is($model->send_case_assignment_mail($case), 1, "send email to assignee");

    my $c = delete $args{case};
    is($c->id, 42, "case passed on the send_email");
    cmp_deeply(\%args, { subject => 'mah subject', template => 'template' }, "send_case_email args passed on correctly");
}

sub test_send_pip_email {

    my $model = _get_model;

    my $case = mock_one(id => 42);

    my $subject_like_object = {
        subject => { type => 'person', reference => generate_uuid_v4() },
    };

    is($model->send_pip_email($case, $subject_like_object), undef, "No PIP authorized/mail bit set");

    my %args;
    my $or = override(_subname('_get_object_model_subject') => sub { shift; return shift });
    $or->override(_subname('get_pip_notification_template') => sub { return 'template' });
    $or->override(_subname('send_case_email') => sub { shift; %args = @_ });

    $subject_like_object->{$_} = 1 for qw(send_auth_confirmation pip_authorized);

    is($model->send_pip_email($case, $subject_like_object), 1, "PIP authorized/mail bit set");

    my $c = delete $args{case};
    is($c->id, 42, "case passed on the send_email");
    cmp_deeply(\%args, { subject => $subject_like_object->{subject}, template => 'template' }, "send_case_email args passed on correctly");
}

sub test_set_case_allocation {

    my $model = _get_model;
    my $hijack;
    my %compare_stuf;
    my $coo = 0; # Coordinator on case
    my $case = mock_one(
        case_actions => {
            hijack_allocation_action => sub { $hijack = shift; }
        },
        set_behandelaar => sub { $compare_stuf{behandelaar} = shift},
        coordinator     => sub { return $coo },
        set_coordinator => sub { $compare_stuf{coordinator} = shift},
        open_zaak       => sub { $compare_stuf{open_zaak} = shift->old_subject_identifier},
        wijzig_route    => sub { $compare_stuf{route_change} = shift},
    );

    my $override = override(_subname('send_case_assignment_mail') => sub {
            $compare_stuf{send_email} = 1;
    });

    my $params = {
        routing               => { group => 42, role => 666 },
        send_assignment_email => 1,
    };

    $model->set_case_allocation($case, $params);
    cmp_deeply($hijack, { role => 666, group => 42 }, "Hijacked the case action");

    $hijack = undef;

    $params->{assignee} = mock_one(old_subject_identifier => 666);
    $params->{routing}{group} = mock_one(group_id => 42);
    $params->{routing}{role} = mock_one(role_id => 666);

    $model->set_case_allocation($case, $params);
    is($hijack, undef, "Did not hijack the case routing");
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
            send_email   => 1,
            route_change => {
                route_ou                 => $params->{routing}{group}->group_id,
                route_role               => $params->{routing}{role}->role_id,
                change_only_route_fields => 1,
            }

        },
        "Routing works for assignee"
    );

    $params->{open} = 1;
    $model->set_case_allocation($case, $params);
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
            coordinator  => 666,
            open_zaak    => 666,
            send_email   => 1,
            route_change => {
                route_ou                 => $params->{routing}{group}->group_id,
                route_role               => $params->{routing}{role}->role_id,
                change_only_route_fields => 1,
            }

        },
        "Routing works for assignee"
    );

    $coo = 1;
    $params->{send_assignment_email} = 0;
    %compare_stuf = ();

    $model->set_case_allocation($case, $params);
    is($hijack, undef, "Did not hijack the case routing");
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
            open_zaak    => 666,
            route_change => {
                route_ou                 => $params->{routing}{group}->group_id,
                route_role               => $params->{routing}{role}->role_id,
                change_only_route_fields => 1,
            }

        },
        "Did not change the coordinator and not sending the email"
    );

    delete $params->{routing};
    delete $params->{open};
    %compare_stuf = ();
    $model->set_case_allocation($case, $params);
    is($hijack, undef, "Did not hijack the case routing");
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
        },
        "Only set the assignee",
    );


    # Set coordinator on case (via stuf-zkn)
    $case = mock_one(
        'X-Mock-Called' => 1,
        set_coordinator => sub { is(shift, 42, "Set the coordinator on 42") },
    );
    $params = {
        coordinator => mock_one(old_subject_identifier => 42),
    };

    $model->set_case_allocation($case, $params);

    # The call is wrapped in a try/catch block, making the caller __ANON__
    one_called_times_ok($case, 'set_coordinator', "Zaaksysteem::Zaken::Model::__ANON__", 1);

}

sub test_update_contact_details {

    my $model = _get_model;

    my $case = mock_one(
        aanvrager_object => mock_strict(
            'X-Mock-ISA'    => 'Zaaksysteem::Betrokkene::Object::Medewerker',
        )
    );

    lives_ok(sub {
        $model->update_contact_details($case, {})
    }, "We are not calling anyone: tested by X-Mock-Strict");

    my %compare;

    my $preset_client = 0;
    $case = mock_one(
        aanvrager_object => mock_strict(
            'X-Mock-ISA' => sub {
                my $type = shift;
                return $type eq 'Zaaksysteem::Betrokkene::Object::Medewerker'
                    ? 0
                    : 1;
            },
            email                 => sub { $compare{email} = shift },
            mobiel                => sub { $compare{gsm} = shift } ,
            telefoonnummer        => sub { $compare{phone} = shift },
            id                    => sub { 2 },
            betrokkene_identifier => sub { "foo-2" },
        ),
        preset_client => sub { return $preset_client },
    );
    $model->update_contact_details($case,
        { email_address => 'foo@bar', phone_number => 'baz', mobile_number => 'foo' });
    cmp_deeply(
        \%compare,
        { email => 'foo@bar', gsm => 'foo', phone => 'baz' },
        "Contact details updated"
    );

    {
        %compare = ();
        $model->update_contact_details($case, {});

        cmp_deeply(
            \%compare,
            { gsm => undef, phone => undef, email => undef },
            "Contact details are only updated if the key is specified"
        );
    }

    {
        %compare = ();
        $model->update_contact_details($case, { email_address => 'yo@example.com' });

        cmp_deeply(
            \%compare,
            { gsm => undef, phone => undef, email => 'yo@example.com' },
            "Contact details are only updated if the key is specified"
        );
    }

    {
        $preset_client = 1;
        %compare = ();
        $model->update_contact_details($case,
            { gms => 'foo', email_address => 'yo@example.com' });

        cmp_deeply(
            \%compare,
            { },
            "Contact details are only updated if the key is specified"
        );
    }
}

sub test_find_casetype_by_id {

    my $model = _get_model(schema => mock_one(resultset => { find => 1 }));

    lives_ok(
        sub {
            $model->_find_casetype_by_id(42);
        },
        "found the casetype"
    );
    throws_ok(
        sub {
            my $model = _get_model(
                schema => mock_one(
                    resultset => {
                        find => sub { die "Foo" }
                    }
                )
            );
            $model->_find_casetype_by_id(42);
        },
        qr#api/v1/case/casetype/node/retrieval_fault#,
        "Could not find the casetype by ID",
    );
}

sub test_prepare_case_arguments {

    my $model = _get_model;

    my %arguments = (
        casetype_id     => generate_uuid_v4,
        confidentiality => 'public',
        source          => 'webformulier',
        requestor       => {},
        values          => [],
    );

    my $override = override(_subname('assert_subjects') => sub { shift; return ( foo=> 'bar' )});
    my @methods = qw(
        get_casetype_object assert_values _get_case_attributes assert_case_attributes
        assert_documents get_document_attributes normalize_attribute_value
        assert_contact_details
    );

    foreach my $method (@methods) {
        $override->replace(_subname($method) => sub { shift; return $method });
    }

    $override->replace(
        _subname('assert_routing') => sub { return (42, 666) });
    $override->replace(
        _subname('assert_casetype') => sub { return mock_one() });
    $override->replace(
        _subname('_get_case_attributes') => sub { return mock_one() });
    $override->replace(
        _subname('assert_values') => sub { return  (foo => 'bar')});

    my $rv = $model->prepare_case_arguments(\%arguments);
    is(ref $rv, 'HASH', "Got a hash");

    # Test::Mock::One doesn't really cmp well
    delete $rv->{casetype};

    my $expect = {
        foo             => 'bar',
        confidentiality => 'public',
        documents       => 'assert_documents',
        open            => 0,
        routing         => {
            group => 42,
            role  => 666
        },
        source => 'webformulier',
        values => 'assert_case_attributes',
    };

    cmp_deeply($rv, $expect, "Got the expected outcome of prepare_case_arguments");

    $arguments{contact_details} = { email => 'foo@example.com' };
    $expect->{contact_details} = 'assert_contact_details';

    $rv = $model->prepare_case_arguments(\%arguments);
    delete $rv->{casetype};
    cmp_deeply($rv, $expect, "Got the expected outcome of prepare_case_arguments with 'contactdetails'");

    $arguments{number} = 42;
    $expect->{case_id} = 42;
    $rv = $model->prepare_case_arguments(\%arguments);
    delete $rv->{casetype};
    cmp_deeply($rv, $expect, "Got the expected outcome of prepare_case_arguments with 'number'");

}

sub test_assert_document_ids {

    my $model = _get_model;

    {
        my $uuids = $model->_assert_document_ids();
        is(@$uuids, 0, "No UUIDS's found");
    }

    throws_ok(
        sub {
            $model->_assert_document_ids("foo");
        },
        qr#case/file/uuid/syntax#,
        "No valid UUID"
    );

    {
        my $uuid  = generate_uuid_v4;
        my $uuids = $model->_assert_document_ids($uuid);
        cmp_deeply($uuids, [ $uuid ], "found the corect uuid");
    }

    {
        my $uuid   = generate_uuid_v4;
        my $uuid2  = generate_uuid_v4;

        my $uuids  = $model->_assert_document_ids([$uuid, $uuid, $uuid2]);
        cmp_deeply($uuids, [ $uuid, $uuid2 ], "found the corect uuids");
        is($uuids->[0], $uuid, "Found the same one");
        is($uuids->[1], $uuid2, "F");
    }

}

sub test_assert_file_attributes {
    my $model = _get_model;

    my $zaak = mock_one();
    my $attr = mock_one(magic_string => 'my magic string');
    my ($file1, $file2) = (generate_uuid_v4, generate_uuid_v4);
    my $values = { 'my magic string' => [ $file1, $file2 ]};
    my $documents = { };

    my $override = override(
        _subname('_get_filestore_by_uuid') => sub {
            shift;
            my $uuid = shift;
            if   ($uuid eq $file1) { return (1, 1) }
            else                   { return (1, 0) }
        }
    );

    $model->_assert_file_attribute(
        $zaak, $attr, $values, $documents,
    );

    subtest 'file add' => sub {
        is(keys %{ $documents->{add} }, 1, "One file update");
        ok(exists $documents->{add}{$file2}, "$file2 will been be added");

        is(keys %{$documents->{add}{$file2}}, 2, "found all keys");
        my $found = all { defined $documents->{add}{$file2}{$_} }
            qw(attribute filestore);
        ok($found, "found all things for a file add");
    };


    subtest 'file update' => sub {
        is(keys %{ $documents->{update} }, 1, "One file update");
        ok(exists $documents->{update}{$file1}, "$file1 will be updated");
        my $found = all { defined $documents->{update}{$file1}{$_} }
            qw(file attribute filestore);
        is(keys %{$documents->{update}{$file1}}, 3, "found all keys");
        ok($found, "found all things for a file update");
    };

    my @attr = ( $attr );
    my $called = 0;
    my $attrs = mock_one(next => sub { pop(@attr) });
    $override->replace( _subname('_assert_file_attribute') => sub { return $called++; });
    $model->_assert_file_attributes($attrs);
    is($called, 1, "Called _assert_file_attributes");


}

sub test_assert_file_metadata {
    my $model = _get_model;
    is($model->_assert_file_metadata, undef, "No metadata");
    cmp_deeply(
        $model->_assert_file_metadata(
            { origin => "Uitgaand", "origin_date" => '2017-08-30T16:02:25' }
        ),
        { origin => "Uitgaand", origin_date => '2017-08-30' },
        "Meta data found"
    );
}

sub test_assert_files {
    my $model = _get_model(
        schema => mock_one(dbh_do => 999),
    );

    my $file1 = generate_uuid_v4;
    my $file2 = generate_uuid_v4;
    my $file3 = generate_uuid_v4;
    my $file4 = generate_uuid_v4;
    my $file5 = generate_uuid_v4;

    my %documents = (
        add => {
            $file1 => {},
        },
        update => {
            $file2 => {},
        },
    );
    my @files = (
        { reference => $file1 },
        { reference => $file3, name => 'file3', metadata => { origin => 'Foo' } },
        { reference => $file2, metadata => {} },
        { reference => $file4, name => 'foo' },
        { reference => $file5, number => 42 },
    );

    my $case = mock_one();
    my $override = override(
        _subname('_get_filestore_by_uuid') => sub {
            shift;
            my $uuid = shift;
            if ($uuid eq $file2 || $uuid eq $file4) {
                return ("filestore: $uuid", "file: $uuid");
            }
            else { return ("filestore: $uuid", 0) }
        }
    );

    $model->_assert_files(\%documents, \@files, $case);

    cmp_deeply(
        \%documents,
        {
            add => {
                $file1 => {
                    name     => undef,
                    metadata => undef,
                },
                $file3 => {
                    name      => 'file3',
                    metadata  => { origin => 'Foo' },
                    filestore => "filestore: $file3",
                },
                $file5 => {
                    number    => 42,
                    filestore => "filestore: $file5",
                    name      => undef,
                    metadata  => undef,
                },
            },
            update => {
                $file2 => {
                    metadata => undef,
                    name     => undef,
                },
                $file4 => {
                    file      => "file: $file4",
                    metadata  => undef,
                    filestore => "filestore: $file4",
                    name      => 'foo',
                },
            },
        },
        "File assertions ok",
    );

}

sub test_generate_case_id {
    my $model = _get_model(
        schema => mock_one(
            resultset => sub {
                return mock_one(generate_case_id => 42) if shift eq 'Zaak';
                die "Horrible death";
            }
        )
    );

    my $case_id;
    my $override = override(_subname('_log_case_id') => sub { shift; $case_id = shift; return });

    lives_ok(
        sub {
            $model->generate_case_id;
        },
        "Calls generate_case_id on schema->resultset('Zaak')"
    );

    is($case_id, 42, "Generated case ID is logged");
}


sub test_get_betrokkene_model_subject {
    my $model = _get_model;

    my $person = generate_bsn(9);
    my $company = generate_kvk;

    lives_ok(
        sub {
            $model->_get_betrokkene_model_subject('person', $person);
        },
        "Got a person"
    );

    lives_ok(
        sub {
            $model->_get_betrokkene_model_subject('company', { kvk_number => $company });
        },
        "Got a company"
    );

    throws_ok(
        sub {
            $model = _get_model(subject_model => mock_one(search => undef));
            $model->_get_betrokkene_model_subject('company',
                { kvk_number => $company });
        },
        qr#case/get_subject#,
        "Did not find a soul"
    );

    throws_ok(
        sub {
            $model->_get_betrokkene_model_subject('company',
                { kvk_number => 1 });
        },
        qr#invalid: kvk_number#,
        "KvK number isn't elfproef",
    );

    throws_ok(
        sub {
            $model->_get_betrokkene_model_subject('person', '012345678');
        },
        qr#invalid: bsn#,
        "BSN number isn't elfproef",
    );
}

sub test_get_employee_subject {
    my $model = _get_model(
        schema => mock_strict(
            resultset       => { find => { as_object => 1 } }
        )
    );

    lives_ok(
        sub {
            $model->_get_employee_subject(42);
        },
        "Called schema->resultset->search"
    );

    throws_ok(
        sub {
            my $model = _get_model(
                schema => mock_strict(
                    resultset       => { find => undef }
                )
            );
            $model->_get_employee_subject(42);
        },
        qr#api/v1/case/subject_not_found#,
        "Unable to find employee"
    );
}

sub test_get_filestore_by_uuid {
    my $model = _get_model;


    my $case;
    my $uuid = generate_uuid_v4;

    throws_ok(
        sub {
            $model->_get_filestore_by_uuid("not a uuid");
        },
        qr#case/filestore/uuid/syntax#,
        "Not a UUID"
    );

    throws_ok(
        sub {
            $model->_get_filestore_by_uuid($uuid, $case);
        },
        qr#api/v1/case/create/multiple_file_references#,
        "Too many files found"
    );

    throws_ok(
        sub {
            $model = _get_model(schema => mock_one(resultset => { find => undef }));
            $model->_get_filestore_by_uuid($uuid, $case);
        },
        qr#case/file/reference/not_found#,
        "Too many files found"
    );

    my @files = ( mock_one() );
    $model = _get_model(
        schema => mock_one(
            resultset => {
                find => {
                    next => sub { pop @files }
                }
            }
        )
    );

    lives_ok(
        sub {
            $model->_get_filestore_by_uuid($uuid, $case);
        },
        "Got one file from the filestore"
    );

    my $args;
    $model = _get_model(schema => mock_one(
        search_rs => sub { $args = shift; return (mock_one(next => undef)) },
        next => sub { pop @files }
    ));
    my @answer = $model->_get_filestore_by_uuid($uuid, $case);
    is(@answer, 2, "Got file and filestore");
    cmp_deeply($args,  {case_id => undef}, "No case supplied in search");

    $case = mock_one(id => 42);
    @answer = $model->_get_filestore_by_uuid($uuid, $case);
    is(@answer, 2, "Got file and filestore");
    cmp_deeply($args,  {case_id => 42}, "Case supplied in search");

}

sub test_get_group {
    my $model = _get_model;
    lives_ok(
        sub {
            $model->_get_group;
        },
        "_get_group called"
    );
}

sub test_get_role {
    my $model = _get_model;
    lives_ok(
        sub {
            $model->_get_role;
        },
        "_get_role called"
    );
}

sub test_get_object_model_subject {
    my $model = _get_model;

    # I'm not really sure how to test this: patch welcome
    my $subject = mock_one;
    lives_ok(
        sub {
            $model->_get_object_model_subject($subject);
        },
        "_get_object_model_subject called"
    );
}

sub test_get_subject {
    my $model = _get_model;

    # Test pass through
    my $override = override(_subname('_get_object_model_subject') => sub { shift; return shift });
    $override->replace(_subname('_get_employee_subject') => sub { shift; return shift });
    $override->replace(_subname('_get_betrokkene_model_subject') => sub { shift; return \@_ });

    is(
        $model->_get_subject({ subject => "call subject" }),
        "call subject",
        "Correct pass through"
    );
    is(
        $model->_get_subject(
            { subject_type => "employee", subject_id => "call employee" }
        ),
        "call employee",
        "Call to _get_employee_subject"
    );
    cmp_deeply(
        $model->_get_subject(
            { subject_type => "person|company", subject_id => "the id" }
        ),
        ['person|company', 'the id'],
        "Call to _get_object_model_subject"
    );
}


sub test_get_subject_by_legacy_id {
    my $model = _get_model;

    lives_ok(
        sub {
            $model->_get_subject_by_legacy_id;
        },
        '_get_subject_by_legacy_id called'
    );
}

sub test_log_case_id {

    my $args;
    my $type;
    my $model = _get_model(schema =>
            mock_one(trigger => sub { ($type, $args) = @_;}));

    $model->_log_case_id(42);

    cmp_deeply(
        $args,
        {
            component    => 'zaak',
            component_id => 42,
            data         => {
                remote_system => 'Zaaksysteem',
                interface     => 'api/v1',
            },
        },
        "logging trigger executed"
    );

    is($type, 'case/id_requested', "type is correct");
}

sub test_assert_case_attributes {
    my $model = _get_model;

    my @next = ();
    my $rs = mock_one(next => sub { pop @next });
    my $attr_values = { foo => 'bar'};
    my $values = $model->assert_case_attributes($rs, $attr_values);
    cmp_deeply($values, [], "No values to assert");

    @next = (
        mock_one(
            magic_string => 'foo',
            get_column   => sub {
                my $i = shift;
                return 'foo' if $i eq 'bibliotheek_kenmerken_id';
            }
        ),
    );

    my $override = override(_subname('normalize_attribute_value') => sub { return "this is my value"});
    $values = $model->assert_case_attributes($rs, $attr_values);
    cmp_deeply($values, [{'foo' => 'this is my value'}], "Found values");

}

sub test_assert_contact_details {
    my $model = _get_model;
    my $rv = $model->assert_contact_details;
    cmp_deeply($rv, {}, "No contact details entered");

    $rv = $model->assert_contact_details({ mobile_number => '0612345678' });
    cmp_deeply($rv, {mobile_number => '0612345678'}, "No contact details entered");
}

sub test_assert_documents {
    my $model = _get_model;

    my $override = override(_subname('_assert_file_attributes') => sub { return {foo => 'bar'} });
    $override->replace(_subname('_assert_files') => sub { return {bar => 'baz'} });

    my ($attrs, $values, $files, $case) = (mock_one, mock_one, mock_one, mock_one);
    my $rv = $model->assert_documents($attrs, $values, $files, $case);
    cmp_deeply($rv, { bar => 'baz' }, "called assert_files");

    $rv = $model->assert_documents($attrs, $values, undef, $case);
    cmp_deeply($rv, { foo => 'bar' }, "No files, so last call is _assert_file_attributes");
}

sub test_assert_subjects {
    my $model = _get_model;

    my $node = mock_one(
        'X-Mock-ISA' => "Zaaksysteem::Schema::ZaaktypeNode",
        properties   => sub { return { preset_owner_identifier => 1} }
    );
    my $subjects = {};

    my %rv = $model->assert_subjects($node, $subjects);

    # TODO: Mind boggling code..
    # Fetch the input params from Postman
    my @types = qw(assignee related requestor coordinator);
    cmp_deeply(
        [sort keys %rv],
        [sort @types],
        "Have all requestor types for preset assignee"
    );

    {
        $node = mock_one(
            'X-Mock-ISA' => "Zaaksysteem::Schema::ZaaktypeNode",
            properties   => sub { return { } }
        );
        my $subjects = {};

        my %rv = $model->assert_subjects($node, $subjects);

        # TODO: Mind boggling code..
        # Fetch the input params from Postman
        my @types = qw(related requestor);
        cmp_deeply(
            [sort keys %rv],
            [sort @types],
            "Have all requestor types with assignee"
        );
    }
}

sub test_get_document_attributes {
    my $model = _get_model;

    my $rs = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken',
        search_rs => sub { return shift },
    );
    my $args = $model->get_document_attributes($rs);
    cmp_deeply($args, { 'bibliotheek_kenmerken_id.value_type' => 'file' }, "correct search query");
}

sub test_normalize_attribute_value {
    my $model = _get_model;

    my $attr = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::DB::Component::ZaaktypeKenmerken',
        search_rs => sub { return shift },
    );

    my $value = $model->normalize_attribute_value($attr);
    is($value, undef, "No value is a undef value");

    $value = $model->normalize_attribute_value($attr, []);
    is($value, undef, "Empty array value is a undef value");

    $value = $model->normalize_attribute_value($attr, [[ 'nope' ]]);
    cmp_deeply($value, [ 'nope' ], "Multi attribute support");

    throws_ok(
        sub {
            $model->normalize_attribute_value($attr, [ 'nope' ]);
        },
        qr#api/v1/case/incorrect_values#,
        "Multiple attribute support validation"
    );

}

sub test_create_case {
    my $model = _get_model;

    my %zaak_methods_called = ( dc => 0, t => 0);

    my $zaak = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::Zaken::ComponentZaak',
        discard_changes => sub { return $zaak_methods_called{dc}++ },
        object_attributes => sub { return [] },
        _touch          => sub { return $zaak_methods_called{t}++ }
    );

    my ($create_params, $update_contact_details, $related_case_subjects,
        $set_case_allocation, $add_case_files, $update_case_files,
        $exec_actions);

    my $override = override(
        _subname('_create_case') => sub {
            shift;
            $create_params = shift;
            my $sub_ref = shift;

            $sub_ref->();

            return $zaak;
        }
    );

    # Start mocking the world
    $override->replace(_subname('update_contact_details') =>
            sub { $update_contact_details = pop});

    $override->replace(_subname('relate_case_subjects') =>
            sub { $related_case_subjects = pop });

    $override->replace(_subname('set_case_allocation') =>
            sub { $set_case_allocation = pop });

    $override->replace(
        _subname('add_case_files') => sub { $add_case_files = pop });

    $override->replace(
        _subname('update_case_files') => sub { $update_case_files = pop });

    # TODO: Add test for this method - out of scope for v1 delayed case
    $override->replace(_subname('execute_phase_actions') => sub { shift; shift; $exec_actions = shift});

    my %args = (
        contact_details => { foon => 'number' },
        related         => "related subjects",
        route           => {
            role  => 'behandelaar',
            group => 'Mintlab'
        },
        assignee  => 'behandelaar',
        open      => 'ja',
        documents => {
            add    => { file => 'add' },
            update => { file => 'update' }
        },
    );

    my %copy = %args;

    $model->create_case(\%args);

    cmp_deeply($create_params, \%copy, "Creation params are ok");

    cmp_deeply($update_contact_details, {foon => 'number'}, "updated contact details for subject");

    cmp_deeply($related_case_subjects, \%copy, "updated contact details for subject");

    cmp_deeply($set_case_allocation, \%copy, "set case allocation");

    cmp_deeply($add_case_files, { file => 'add' }, "Added case files");

    cmp_deeply($update_case_files, { file => 'update' },
        "Updated case files");

    cmp_deeply($exec_actions, { phase => 1, skip_assignment => 1}, 'Executed phase actions');
    cmp_deeply(\%zaak_methods_called, { t => 0, dc => 1}, 'Zaak discard_changes and not touched');

    delete $args{assignee};
    $model->create_case(\%args);
    cmp_deeply($exec_actions, { phase => 1, skip_assignment => 0}, 'Executed phase actions and without skipping assignment');

    $args{contact_details} = {};
    $model->create_case(\%args);
    cmp_deeply($update_contact_details, {}, "No contact details updated when not specified in create call");

    delete $args{contact_details};
    $model->create_case(\%args);
    undef $update_contact_details;
    cmp_deeply($update_contact_details, undef, "No contact details updated when not specified in create call");
}

sub test_update_payment_status {
    my $model = _get_model;

    my %zaak_methods_called = (
        set_payment_status => [],
    );
    my $zaak = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::Zaken::ComponentZaak',
        set_payment_status => sub {
            my ($status, $amount) = @_;

            push @{ $zaak_methods_called{set_payment_status} }, {
                status => $status,
                amount => $amount,
            };

            return $amount;
        },
        payment_amount     => 1234,
    );

    $model->update_payment_status(
        zaak           => $zaak,
        payment_status => 'pending',
    );

    cmp_deeply(
        \%zaak_methods_called,
        {
            set_payment_status => [
                { status => 'pending', 'amount' => 1234 },
            ],
        },
        'update_payment_status sets the payment status correctly',
    );
}

sub test_required_attributes {

    my $model = _get_model();

    my @required = (
        mock_one(id => 42, magic_string => 'foo'),
        mock_one(id => 43, magic_string => 'bar'),
    );

    my $node = mock_one(
        zaaktype_statussen => { search => { first => { id => 42 } } },
        zaaktype_kenmerken => {
            search => {
                next => sub { pop @required }
            }
        },
    );

    my %attrs = (
        42 => 'not the answer to everything'
    );

    my $override = override(
        "Zaaksysteem::Zaken::Model::generate_rule_engine" => sub {
            return {
                validation => mock_one(
                    process_params => sub {
                        return \%attrs;
                    },
                    active_attributes => sub { return [ qw(foo bar) ] }
                ),
                rules => mock_one
            };
        }
    );

    throws_ok(
        sub {
            $model->assert_required_attributes(
                node       => $node,
                channel    => 'channel',
                requestor  => 'requestor',
                attributes => {},
            );
        },
        qr#zaak/create/required/attributes/missing.*bar is missing#,
    );

    @required = (
        mock_one(id => 42, magic_string => 'foo'),
        mock_one(id => 43, magic_string => 'bar'),
    );

    $attrs{43} = 'now it is';
    lives_ok(
        sub {
            $model->assert_required_attributes(
                node       => $node,
                channel    => 'channel',
                requestor  => 'requestor',
                attributes => {},
            );
        },
    );

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
