package Zaaksysteem::Test::Object::Types::Controlpanel;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::Controlpanel;

sub test_controlpanel_object {

    my %args = (
        owner         => 'betrokkene-bedrijf-42',
        customer_type => 'government',
        domain        => 'foo.bar.example.com',
        template      => 'mintlab',
    );

    my $controlpanel = Zaaksysteem::Object::Types::Controlpanel->new(%args);
    isa_ok($controlpanel, 'Zaaksysteem::Object::Types::Controlpanel');

    foreach (keys %args) {
        is($controlpanel->$_, $args{$_},
            "Controlpanel has correct $_: $args{$_}");
    }

}

sub test_controlpanel_new_empty {
    my $controlpanel = Zaaksysteem::Object::Types::Controlpanel->new_empty();
    isa_ok($controlpanel, 'Zaaksysteem::Object::Types::Controlpanel');
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::Controlpanel - Test a control panel controlpanel object

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::Controlpanel

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
