package Zaaksysteem::Test::Object::Value;

use Zaaksysteem::Test;

use Zaaksysteem::Object::Value;
use Zaaksysteem::Object::ValueType::String;

sub test_value_interface {
    my $value = Zaaksysteem::Object::Value->new(
        type => Zaaksysteem::Object::ValueType::String->new,
        value => 'abc'
    );

    isa_ok $value, 'Zaaksysteem::Object::Value', 'value constructor';

    is $value->type_name, 'string', 'type_name delegate returns expected name';

    ok $value->has_value, 'predicate returns true with constructor arg';

    $value->clear_value;

    ok !$value->has_value, 'predicate returns false after clearing';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
